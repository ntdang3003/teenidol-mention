//
//  LFGPUImageTBlurFilter.h
//  LFLiveKit
//
//  Created by Kuroo on 9/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#if __has_include(<GPUImage/GPUImage.h>)
#import <GPUImage/GPUImage.h>
#elif __has_include("GPUImage/GPUImage.h")
#import "GPUImage/GPUImage.h"
#else
#import "GPUImage.h"
#endif

#import <Foundation/Foundation.h>

@interface LFGPUImageTBlurFilter : GPUImageGaussianBlurFilter
{
    CGFloat firstDistanceNormalizationFactorUniform;
    CGFloat secondDistanceNormalizationFactorUniform;
}

@property(nonatomic, readwrite) CGFloat distanceNormalizationFactor;

@end
