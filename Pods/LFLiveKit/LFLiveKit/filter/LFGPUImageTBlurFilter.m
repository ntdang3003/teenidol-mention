//
//  LFGPUImageTBlurFilter.m
//  LFLiveKit
//
//  Created by Kuroo on 9/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LFGPUImageTBlurFilter.h"

NSString *const kGPUImageTBlurVertexShaderString = SHADER_STRING
(
// attribute vec4 position;
// attribute vec4 inputTextureCoordinate;
// 
// const int GAUSSIAN_SAMPLES = 9;
// 
// uniform float texelWidthOffset;
// uniform float texelHeightOffset;
// 
// varying vec2 textureCoordinate;
// varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];
// 
// void main()
// {
//     gl_Position = position;
//     textureCoordinate = inputTextureCoordinate.xy;
//     
//     // Calculate the positions for the blur
//     int multiplier = 0;
//     vec2 blurStep;
//     vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);
//     
//     for (int i = 0; i < GAUSSIAN_SAMPLES; i++)
//     {
//         multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));
//         // Blur in x (horizontal)
//         blurStep = float(multiplier) * singleStepOffset;
//         blurCoordinates[i] = inputTextureCoordinate.xy + blurStep;
//     }
// }
 
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 uniform mat4   uMat;
 
 uniform float texelWidthOffset;
 uniform float texelHeightOffset;
 const int GAUSSIAN_SAMPLES = 7;
 
 varying vec2 vCoord;
 varying vec2 blurCoordx[GAUSSIAN_SAMPLES];
 
 void main(void) {
     gl_Position = position;
     vCoord = inputTextureCoordinate.xy;
     
     int multiplier = 0;
     vec2 blurStep;
     vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);
     
     for (int i = 0; i < GAUSSIAN_SAMPLES; i++)
     {
         multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));
         // Blur in x (horizontal)
         blurStep = float(multiplier) * singleStepOffset;
         blurCoordx[i] = vCoord + blurStep;
     }
 }
 );

//NSString *const kGPUImageBilateralFilterFragmentShaderString = SHADER_STRING
//(
// uniform sampler2D inputImageTexture;
// 
// const int GAUSSIAN_SAMPLES = 9;
// 
// varying vec2 textureCoordinate;
// varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];
// 
// uniform float distanceNormalizationFactor;
// 
// void main()
// {
//     vec4 centralColor;
//     float gaussianWeightTotal;
//     vec4 sum;
//     vec4 sampleColor;
//     float distanceFromCentralColor;
//     float gaussianWeight;
//     
//     centralColor = texture2D(inputImageTexture, blurCoordinates[4]);
//     gaussianWeightTotal = 0.18;
//     sum = centralColor * 0.18;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[0]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[1]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[2]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[3]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[5]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[6]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[7]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     sampleColor = texture2D(inputImageTexture, blurCoordinates[8]);
//     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
//     gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);
//     gaussianWeightTotal += gaussianWeight;
//     sum += sampleColor * gaussianWeight;
//     
//     gl_FragColor = sum / gaussianWeightTotal;
// }
// );

NSString *const kGPUImageTBlurFilterFragmentShaderString = SHADER_STRING
(
 precision mediump float;
 uniform sampler2D inputImageTexture;
 varying mediump vec2 vCoord;
 uniform float distanceNormalizationFactor;
 
 const int GAUSSIAN_SAMPLES = 7;
 
 varying vec2   blurCoordx[GAUSSIAN_SAMPLES];
 
 vec4 skinblur(vec4 centralColor) {
     float gaussianWeightTotal = 0.0;
     vec4 sum = vec4(0.0);
     vec4 sampleColor;
     float distanceFromCentralColor;
     float gaussianWeight = 0.0;
     
     centralColor = texture2D(inputImageTexture, blurCoordx[3]);
     gaussianWeightTotal = 0.24;
     sum = centralColor * 0.24;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[0]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.06 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[1]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.13 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[2]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.19 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[4]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.19 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[5]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.13 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     sampleColor = texture2D(inputImageTexture, blurCoordx[6]);
     distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
     gaussianWeight = 0.06 * (1.0 - distanceFromCentralColor);
     gaussianWeightTotal += gaussianWeight;
     sum += sampleColor * gaussianWeight;
     
     return sum / gaussianWeightTotal;
 }
 
 void main(){
     vec4 color = texture2D(inputImageTexture, vCoord);
     color = skinblur(color);
     gl_FragColor = color;
 }
 );

@implementation LFGPUImageTBlurFilter

@synthesize distanceNormalizationFactor = _distanceNormalizationFactor;

- (id)init;
{
    
    if (!(self = [super initWithFirstStageVertexShaderFromString:kGPUImageTBlurVertexShaderString
                              firstStageFragmentShaderFromString:kGPUImageTBlurFilterFragmentShaderString
                               secondStageVertexShaderFromString:kGPUImageTBlurVertexShaderString
                             secondStageFragmentShaderFromString:kGPUImageTBlurFilterFragmentShaderString])) {
        return nil;
    }
    
    firstDistanceNormalizationFactorUniform  = [filterProgram uniformIndex:@"distanceNormalizationFactor"];
    secondDistanceNormalizationFactorUniform = [filterProgram uniformIndex:@"distanceNormalizationFactor"];
    
    self.texelSpacingMultiplier = 5.0;
    self.distanceNormalizationFactor = 4.5;
    
    
    return self;
}


#pragma mark -
#pragma mark Accessors

- (void)setDistanceNormalizationFactor:(CGFloat)newValue
{
    _distanceNormalizationFactor = newValue;
    
    [self setFloat:newValue
        forUniform:firstDistanceNormalizationFactorUniform
           program:filterProgram];
    
    [self setFloat:newValue
        forUniform:secondDistanceNormalizationFactorUniform
           program:secondFilterProgram];
}

@end
