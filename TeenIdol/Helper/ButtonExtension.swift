//
//  ButtonExtension.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 2/10/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import Foundation

@IBDesignable
class BorderButton : UIButton {
    @IBInspectable var buttonCornerRadius : CGFloat = 3 {
        didSet{
            self.updateLayerProperties()
        }
    }
    
    override func layoutSubviews() {
        self.updateLayerProperties()
        super.layoutSubviews()
    }
    
    private func updateLayerProperties() {
        self.layer.cornerRadius = self.buttonCornerRadius
    }
}


@IBDesignable
class RoundedButton: UIButton {
    
    // BackgroundLayer
    var backgroundLayer: CAShapeLayer!
    @IBInspectable var backgroundLayerColor: UIColor = UIColor.whiteColor() {
        didSet {
            updateLayerProperties()
        }
    }
    @IBInspectable var backgroundLayerCornerRadius: CGFloat = 4 {
        didSet {
            updateLayerProperties()
        }
    }
    @IBInspectable var backgroundLayerBorderWidth: CGFloat = 0 {
        didSet {
            updateLayerProperties()
        }
    }
    @IBInspectable var backgroundLayerBorderColor: UIColor = UIColor.clearColor() {
        didSet {
            updateLayerProperties()
        }
    }
    
    override func layoutSubviews() {
        
        updateLayerProperties()
        
        super.layoutSubviews()
        
        
    }
    
    func layoutBackgroundLayer() {
        if backgroundLayer == nil {
            backgroundLayer = CAShapeLayer()
            layer.insertSublayer(backgroundLayer, atIndex: 0)
        }
        backgroundLayer.backgroundColor = backgroundLayerColor.CGColor
        backgroundLayer.cornerRadius = backgroundLayerCornerRadius
        backgroundLayer.borderWidth = backgroundLayerBorderWidth
        backgroundLayer.borderColor = backgroundLayerBorderColor.CGColor
        backgroundLayer.frame = bounds
    }
    
    func updateLayerProperties() {
        layoutBackgroundLayer()
    }
    
}
