//
//  SignalRHelper.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import Alamofire
import AFNetworking
let templatePatten = "(\\{([^}]+)\\})"
class SignalRHelper: NSObject {
    static var _infoCache : NSCache = NSCache()
    
    class func getVipImageAttributeString( urlStr: String) -> NSAttributedString{
        var rs = _infoCache.objectForKey(urlStr);
        if(rs == nil){
            
            var image = FunctionHelper.dowloadImageSync(urlStr)
            if(image == nil){
                image = UIImage(named: "ic_vip_id_2")
            }
            let imageAttachment = FunctionHelper.inlineTextAttachment(image!, size: CGSize(width: 14.0,height: 12.0), font: UIFont.systemFontOfSize(9))
            rs = NSAttributedString(attachment: imageAttachment)
            _infoCache.setObject(rs!, forKey: urlStr)
        }
        return rs! as! NSAttributedString
    }
    
    class func getImageAttributeString(cacheKey: String, urlStr: String, imageSize: CGSize,font: UIFont,inout placeHolder: String?) -> NSAttributedString{
        var rs = _infoCache.objectForKey(cacheKey)
        if(rs == nil){
            var image = FunctionHelper.dowloadImageSync(urlStr)
            if(image == nil){
                image = UIImage(named:placeHolder!)
            }
            let imageAttachment = FunctionHelper.inlineTextAttachment(image!, size: imageSize, font: font)
            rs = NSAttributedString(attachment: imageAttachment)
            _infoCache.setObject(rs!, forKey: cacheKey)
        }
        return rs! as! NSAttributedString
    }
    
    
    class func getHexColor(hexString: String, alpha: Float) ->UIColor? {
        if(hexString.isEmpty){return nil}
        var hex = hexString
        
        // Check for hash and remove the hash
        if hex.hasPrefix("#") {
            hex = hex.substringFromIndex(hex.startIndex.advancedBy(1))
        }
        
        if (hex.rangeOfString("(^[0-9A-Fa-f]{6}$)|(^[0-9A-Fa-f]{3}$)", options: .RegularExpressionSearch) != nil) {
            
            // Deal with 3 character Hex strings
            if hex.characters.count == 3 {
                let redHex   = hex.substringToIndex(hex.startIndex.advancedBy(1))
                let greenHex = hex.substringWithRange(Range<String.Index>(hex.startIndex.advancedBy(1) ..< hex.startIndex.advancedBy(2)))
                let blueHex  = hex.substringFromIndex(hex.startIndex.advancedBy(2))
                
                hex = redHex + redHex + greenHex + greenHex + blueHex + blueHex
            }
            
            let redHex = hex.substringToIndex(hex.startIndex.advancedBy(2))
            let greenHex = hex.substringWithRange(Range<String.Index>(hex.startIndex.advancedBy(2) ..< hex.startIndex.advancedBy(4)))
            let blueHex = hex.substringWithRange(Range<String.Index>(hex.startIndex.advancedBy(4) ..< hex.startIndex.advancedBy(6)))
            
            var redInt:   CUnsignedInt = 0
            var greenInt: CUnsignedInt = 0
            var blueInt:  CUnsignedInt = 0
            
            NSScanner(string: redHex).scanHexInt(&redInt)
            NSScanner(string: greenHex).scanHexInt(&greenInt)
            NSScanner(string: blueHex).scanHexInt(&blueInt)
            
            return UIColor(red: CGFloat(redInt) / 255.0, green: CGFloat(greenInt) / 255.0, blue: CGFloat(blueInt) / 255.0, alpha: CGFloat(alpha))
        }
        else {
            return nil
        }
    }
    
    class func getMatchesTemplate(attString: NSAttributedString) -> NSArray{
        let regex = try! NSRegularExpression(pattern: templatePatten, options: NSRegularExpressionOptions.AnchorsMatchLines)
        let range = NSMakeRange(0, attString.length)
        
       return regex.matchesInString(attString.string, options: .WithTransparentBounds, range: range).reverse()
    }
}
