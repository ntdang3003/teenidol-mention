//
//  FunctionHelper.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/24/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

class InlineTextAttachment: NSTextAttachment{
    var fontDescender : CGFloat = 0
    
    override func attachmentBoundsForTextContainer(textContainer: NSTextContainer?, proposedLineFragment lineFrag: CGRect, glyphPosition position: CGPoint, characterIndex charIndex: Int) -> CGRect {
        var rect = super.attachmentBoundsForTextContainer(textContainer, proposedLineFragment: lineFrag, glyphPosition: position, characterIndex: charIndex)
        rect.origin.y = self.fontDescender
        return rect
    }
}

protocol NavigationCustomViewDelegate {
    func navigationCustomView_onTouchAtIndex(index: Int)
}

class NavigationCustomView : UIView{
    private var _arrButton = Array<UIButton>()
    private var _footer : UIView?
    private var _currentIndex = 0
    var delegate : NavigationCustomViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(arrTitle : Array<String>){
        self.init(frame: CGRect.zero)
        var posX  : CGFloat = 0
        let margin : CGFloat = 20.0
        //for title in arrTitle{
        for index in 0..<arrTitle.count{
            let button = UIButton(frame: CGRect(x: posX, y: 0, width: 0, height: 33))
            button.setTitle(arrTitle[index], forState: .Normal)
            button.titleLabel!.font  = UIFont(name: "System-Medium", size: 15.0)
            button.tag = index
            button.sizeToFit()
            button.addTarget(self, action: #selector(touchAtIndex), forControlEvents: .TouchUpInside)
            posX += button.frame.size.width + margin
            self.addSubview(button)
            self._arrButton.append(button)
        }
        self.frame.size = CGSize(width: posX - margin, height: 33.0)
        if(_arrButton.count > 0){
            let firstButton = _arrButton[0]
            _footer = UIView(frame: CGRect.zero)
            _footer!.frame.origin = CGPoint(x: 2, y: 31)
            _footer!.frame.size = CGSize(width: firstButton.frame.size.width - 4, height: 2)
            _footer?.backgroundColor = UIColor.whiteColor()
            self.addSubview(_footer!)
        }
    }
    
    func touchAtIndex(sender: AnyObject?){
        if let temp = sender as? UIButton where self.delegate != nil{
            if(temp.tag == _currentIndex) {return}
            UIView.animateWithDuration(0.35, animations: { 
                self._footer!.frame.size.width = temp.frame.size.width
                self._footer!.center.x = temp.center.x
            })
            _currentIndex = temp.tag
            self.delegate?.navigationCustomView_onTouchAtIndex(temp.tag)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class FunctionHelper: NSObject {
    private static let _emojiCache = NSCache()
    
    class func parseStringToEmojiAttributedString(inputString: String, color: UIColor, font: UIFont, size: CGSize) -> NSAttributedString {
        let formattedText: NSString = inputString
        
        let regex = try! NSRegularExpression(pattern: "(\\{([^}]+)\\})", options: NSRegularExpressionOptions.AnchorsMatchLines)
        let range = NSMakeRange(0, inputString.characters.count)
        let matches = regex.matchesInString(inputString, options: .WithTransparentBounds, range: range).reverse()
        let attrs = [NSForegroundColorAttributeName: color, NSFontAttributeName: font]
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedText as String, attributes: attrs)
        
        for result: NSTextCheckingResult in matches {
            let match: NSString = formattedText.substringWithRange(result.range)
            let lengthOfManaName: Int = result.rangeAtIndex(1).length - 2
            let manaName: String = match.substringWithRange(NSMakeRange(1, lengthOfManaName))
            
            //Lấy từ cache
            var replacementForTemplate = _emojiCache.objectForKey(manaName) as? NSAttributedString
            if(replacementForTemplate == nil){
                let emoticonImage: UIImage? = UIImage(named: "ic_emoji_\(manaName).png")
                if emoticonImage != nil {
                    let imageAttachment = FunctionHelper.inlineTextAttachment(emoticonImage!, size: size, font: font)
                    replacementForTemplate = NSAttributedString(attachment: imageAttachment)
                    _emojiCache.setObject(replacementForTemplate!, forKey: manaName)
                }else{
                    replacementForTemplate = NSAttributedString(string: manaName)
                }

            }
            attributedString.replaceCharactersInRange(result.range, withAttributedString: replacementForTemplate!)
        }
        return attributedString
    }
    
    class func getRubyAttributeString(text: String,  textFont: UIFont,
                                      textColor: UIColor,iconSize: CGSize, iconFontSize: CGFloat)
    -> NSAttributedString{
        let rubyImageAttachment = FunctionHelper.inlineTextAttachment(KImage.UserIcon!, size: iconSize, font: UIFont.systemFontOfSize(iconFontSize))
        let rubyAttributeString = NSAttributedString(attachment: rubyImageAttachment)
        let result = NSMutableAttributedString(attributedString: rubyAttributeString)
        let textAttributeString = NSAttributedString(string: " \(text)", attributes: [NSFontAttributeName : textFont, NSForegroundColorAttributeName: textColor])
        result.appendAttributedString(textAttributeString)
        return result
    }
    
    class func getImageAttributeString(image: UIImage, text: String,  textFont: UIFont,
                                      textColor: UIColor,iconSize: CGSize, iconFontSize: CGFloat)
        -> NSAttributedString{
            let rubyImageAttachment = FunctionHelper.inlineTextAttachment(image, size: iconSize, font: UIFont.systemFontOfSize(iconFontSize))
            let rubyAttributeString = NSAttributedString(attachment: rubyImageAttachment)
            let result = NSMutableAttributedString(attributedString: rubyAttributeString)
            let textAttributeString = NSAttributedString(string: " \(text)", attributes: [NSFontAttributeName : textFont, NSForegroundColorAttributeName: textColor])
            result.appendAttributedString(textAttributeString)
            return result
    }
    
    class func dowloadImageSync(URL: String) -> UIImage?{
        if(URL.isEmpty){return nil}
        var data = NSData()
        //let temp = URL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())
        if let url = NSURL(string: URL) {
            if let newData = NSData(contentsOfURL: url){
                data = newData
            }

        }
        return UIImage(data: data)
    }
    
    class func dateToString(date: NSDate, mode: Int) -> String{
        let format: NSDateFormatter = NSDateFormatter()
        if mode == 1 {
            format.dateFormat = "dd/MM/yy"
        }
        else if mode == 2 {
            format.dateFormat = "HH:mm"
        }
        else if mode == 3 {
            format.dateFormat = "dd/MM/yy HH:mm"
        }
        else if mode == 4 {
            format.dateFormat = "dd/MM"
        }
        else if mode == 5 {
            format.dateFormat = "dd/MM/yyyy HH:mm"
        }
        else if mode == 6 {
            format.dateFormat = "dd/MM/yyyy"
        }
        format.timeZone = NSTimeZone(name: "UTC")
        //format.timeZone = NSTimeZone(abbreviation: "GMT+07:00")
        return format.stringFromDate(date)
    }
    
    class func getImageUrlBySize(url: String, imageSize: CGSize) -> String{
        if url.isEmpty || !url.containsString("https://file.teenidol.vn/image/") {
            return url
        }
        //Xác định size
        var size = DefaultImageSize[0]
        for i in 0..<DefaultImageSize.count{
            if DefaultImageSize[i] >= Double(imageSize.width*2){
                size = DefaultImageSize[i]
                break
            }	
        }
        var rs = url;
        let index : String.Index = rs.startIndex.advancedBy(url.characters.count - 5)
        rs = rs.substringToIndex(index)
        rs = "\(rs)\("\(Int(size)).jpg")"
        return rs
    }
    
    class func GetViewHUD() -> UIView {
        let animationView: UIImageView = UIImageView(frame: CGRectMake(0, 0, 130 / UIScreen.mainScreen().scale, 40 / UIScreen.mainScreen().scale))
        animationView.animationImages = [UIImage(named: "ic_loading_1.png")!, UIImage(named: "ic_loading_2.png")!, UIImage(named: "ic_loading_3.png")!]
        animationView.animationDuration = 1.5
        animationView.animationRepeatCount = 0
        animationView.startAnimating()
        return animationView
    }
    
    class func GetViewFooter(bvalue: Bool) -> UIView? {
        if bvalue {
            return nil
        }
        let view: UIView = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 50))
        let imageview: UIImageView = (self.GetViewHUD() as! UIImageView)
        imageview.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width / 2 - imageview.bounds.size.width / 2, 10, imageview.bounds.size.width, imageview.bounds.size.height)
        view.addSubview(imageview)
        return view
    }
    
    class func resizeImage(image: UIImage, size: CGSize, position: CGPoint)-> UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0);
        image.drawInRect(CGRect(x: position.x, y: position.y, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage!;
    }
    
    class func calculateTextHeightWithWidth(width: CGFloat, text: NSAttributedString, font: UIFont) -> CGSize{
        let lblCal = UILabel(frame: CGRectMake(0,0,width,17))
        lblCal.font = font
        lblCal.attributedText = text
        lblCal.sizeToFit()
        return (lblCal.attributedText?.boundingRectWithSize(CGSizeMake(width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin , context: nil).size)!
        
    }
    
    class func inlineTextAttachment(image: UIImage, size: CGSize, font: UIFont) -> InlineTextAttachment{
        let imageAttachment = InlineTextAttachment()
        imageAttachment.fontDescender = font.descender
        imageAttachment.image = resizeImage(image, size: size, position: CGPointMake(0, 0))
        return imageAttachment
    }
    
    class func makeCircularImageWithSize(inputImage: UIImage,size: CGSize) -> UIImage {
        // make a CGRect with the image's size
        let circleRect: CGRect = CGRect(origin: CGPointZero, size: size)
        // begin the image context since we're not in a drawRect:
        UIGraphicsBeginImageContextWithOptions(circleRect.size, false, 0)
        // create a UIBezierPath circle
        let circle: UIBezierPath = UIBezierPath(roundedRect: circleRect, cornerRadius: circleRect.size.width / 2)
        // clip to the circle
        circle.addClip()
        // draw the image in the circleRect *AFTER* the context is clipped
        inputImage.drawInRect(circleRect)
        // create a border (for white background pictures)
        circle.lineWidth = 1
        UIColor.darkGrayColor().set()
        circle.stroke()
        // get an image from the image context
        let roundedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        // end the image context since we're not in a drawRect:
        UIGraphicsEndImageContext()
        return roundedImage
    }
    
    /// cornerRadius = 0 : border circel
    class func makeBorderView(uiview:UIView, borderWidth: CGFloat, color: UIColor = UIColor.grayColor(), cornerRadius: CGFloat = 0)
    {
        uiview.layer.borderWidth = borderWidth;
        uiview.layer.masksToBounds = false
        uiview.layer.borderColor = color.CGColor;
        if(cornerRadius == 0)
        {
            uiview.layer.cornerRadius = (uiview.frame.height) / 2
        }
        else
        {
            uiview.layer.cornerRadius = cornerRadius;
        }
        uiview.clipsToBounds = true;
    }
    
    class func circleBorderImageView(imageView: UIImageView, color: CGColor = UIColor.grayColor().CGColor, borderWidth: CGFloat = 1, cornerRadius: CGFloat = 0){
        imageView.layer.borderWidth = borderWidth
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = color
        imageView.clipsToBounds = true
        if(cornerRadius == 0){
                imageView.layer.cornerRadius = (imageView.frame.height) / 2
        }else{
                imageView.layer.cornerRadius = cornerRadius
        }
    }
    
    class func showBannerAlertView(title: String = "LiveIdol", content: String, callback: (() -> Void)? = nil){
        let baner = Banner(title: title, subtitle: content, image: UIImage(named: "ic_alert"), backgroundColor: ColorAppDefault, didTapBlock: callback)
        baner.shouldTintImage = false
        baner.show(duration: 3)
    }
    
    class func imgToStr_Base64(img: UIImage!) -> String
    {
        let data = UIImagePNGRepresentation(img)
        return (data?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength))!
    }
    
    class func getNavigationBarButton(image: String) -> UIButton{
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: image), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        return button
    }
    
    class  func openURL(url: String){
        let encodedURL = url.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        UIApplication.sharedApplication().openURL(NSURL(string: encodedURL)!)
    }
    
    class func getDeviceTokenFromData(data: NSData) -> String{
        var rs = String(data)
        rs = rs.stringByReplacingOccurrencesOfString(" ", withString: "")
        rs = rs.stringByReplacingOccurrencesOfString("<", withString: "")
        rs = rs.stringByReplacingOccurrencesOfString(">", withString: "")
        return rs
    }
    
    
    class func stringFromTimeInterval(interval:NSTimeInterval) -> String {
        
        let ti = NSInteger(interval)
        
        //let ms = Int((interval % 1) * 1000)
        
        let seconds = ti % 60
        let minutes = (ti / 60) % 60
        let hours = (ti / 3600)
        
        //return NSString(format: "%0.2d:%0.2d:%0.2d.%0.3d",hours,minutes,seconds,ms)
        return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
    }
    
    class func getTableFooterLoadingView(value: Bool) -> UIView?{
        if(value){
            return nil
        }
        let view =  UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        view.startAnimating()
        return view
    }
    
    class func formatNumberWithSeperator(value: Int) -> String{
        //var fv = 3534234.55
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .DecimalStyle
        formatter.maximumFractionDigits = 0;
        if  let rs = formatter.stringFromNumber(value){
            return rs
        }else{
            return "0"
        }
    }
    
    class func getYoutubeVideoIdFromURL(url : String) ->String?{
        var rs : String?
        if let url = NSURL(string: url){
            var dict = [String: AnyObject]()
            // Check for query string
            if let query = url.query {
                let characterSet = NSCharacterSet(charactersInString:";&")
                for pair in query.componentsSeparatedByCharactersInSet(characterSet)
                {
                    let components = pair.componentsSeparatedByString("=")
                    if(components.count > 1){
                        dict[components[0]] = components[1]
                    }
                }
            }
            rs = dict["list"] as? String
            if(rs == nil){
                rs = dict["v"] as? String
            }
        }
        return rs
    }
    
    class func getRandomNumber(from : CGFloat, to: CGFloat) -> CGFloat{
        return (from + CGFloat(arc4random()) % (to-from+1))
    }
    
    class func runSyncWithMainThread(callback: ()->Void){
        if(NSThread.currentThread().isMainThread){
            callback()
        }else{
            dispatch_async(dispatch_get_main_queue(), {
                callback()
            })
        }
    }
    
    class func getDeviceModel() -> String{
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    class func checkIphone32Bit() -> Bool {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8 where value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPhone5,3","iPhone5,4","iPhone5,1","iPhone5,2":
            return true
        default:
            return false
        }
    }
    
    class func osVersion() -> Float{
        return (UIDevice.currentDevice().systemVersion as NSString).floatValue
    }
}
