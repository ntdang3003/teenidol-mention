//
//  ConstantHelper.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

struct KImage {
    static let StarIcon = UIImage(named: "ic_star_point")
    static let UserIcon = UIImage(named: "ic_ruby")
    static let AvatarDefault = UIImage(named: "ic_default_avatar")
}

struct KText {
    static let StarUnit = "Sao"
    static let UserUnit = "Ruby"
}

let FontNameDefault = "System"
let UserRubyChangeNotificationName = "UserRubyChangeNotification"
let HomeNeedLoadMoreDataNotification = "HomeNeedLoadMoreDataNotification"
let ConfigDidLoadNotification = "ConfigDidLoadNotification"
let UserDidLoginNotification = "UserDidLoginNotification"
let UserDidLogoutNotification = "UserDidLogoutNotification"

var ImageDefault = "ic_logo_full.png"
let IOSPaymentVersion = 1
let IOSApplicationVersion = 1

var ColorAppDefault: UIColor = UIColor(red: 44 / 255.0, green: 186 / 255.0, blue: 174 / 255.0, alpha: 1)

//var ColorAppDefault: UIColor = UIColor(red: 36 / 255.0, green: 40 / 255.0, blue: 53 / 255.0, alpha: 1)

var ColorListViewBackground: UIColor = UIColor(red: 16 / 255.0, green: 20 / 255.0, blue: 22 / 255.0, alpha: 1)
var ColorTextTable: UIColor = UIColor(red: 119.0 / 255.0, green: 119.0 / 255.0, blue: 119.0 / 255.0, alpha: 1)
var FontDefautLarge : UIFont = UIFont.systemFontOfSize(13)
var DefaultImageSize  = [80.0,160.0,320.0,640.0,750.0,800.0,1280.0]
enum ScheduleStatus : Int {
    case Booking = 0
    case BeforeCast = 1
    case Casting = 2
    case Done = 3
    case Delete = 4
    case NotCast = 5
}

enum CardType : Int {
    case Viettel = 0
    case Mobifone = 1
    case Vinaphone = 2
    case Gate = 3
    case Vcoin = 4
    case Zing = 5
    case VNMobile = 6
}
