//
//  URLConstant.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

class URLConstant: NSObject {
    static let  sharedInstance = URLConstant()
    //private static var ServiceDomain = "https://teenidol.vn:9236/TeenIdolsService.svc/"
    //var  Server_SignalR = "https://teenidol.vn:9235"
    
    static var isBeta = false
    private static var ServiceDomain : String = "https://teenidol.vn:9240/TeenIdolsService.svc/"
    var Server_SignalR = "https://teenidol.vn:9239"
    
    
    var GetSignalR_Key: String = "12356789"
    var GetSecretKey: String = "GetSecretKey?Key=%@"
    
    class func List_SuggestStar(userId: Int64, pageIndex: Int, pageSize: Int, starType: Int,
                                key: String) -> String{
        return "\(ServiceDomain)List_SuggestStars?createUserId=\(userId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&getStarType=\(starType)&key=\(key)"
    }

    class func User_GetListUserInShow(userId: Int64,showId: Int64, pageIndex: Int, pageSize: Int, key: String) -> String{
        return "\(ServiceDomain)GetListUserInShow?actionUserId=\(userId)&showId=\(showId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    class func User_ParticipateInShow() ->String{
        return  "\(ServiceDomain)User_ParticipateInShow"
    }
    
    class func User_GetUserInfoInShow(actionUserId: Int64, userId: Int64, key: String) -> String{
        return  "\(ServiceDomain)User_GetUserInfoInShow?actionUserId=\(actionUserId)&userId=\(userId)&key=\(key)"
    }
    
    class func User_GetAnimationItemJoinShow(userId: Int64, key: String) -> String{
        return  "\(ServiceDomain)User_GetAnimationItemJoinShow?actionUserId=\(userId)&key=\(key)"
    }
    
    class func StreamBegin () -> String{
        return  "\(ServiceDomain)StreamBegin"
    }
    
    class func StreamEnd () -> String{
        return  "\(ServiceDomain)StreamEnd"
    }
    
    class func List_ListGiftCategory(userId: Int64) -> String{
        return  "\(ServiceDomain)List_ListGiftCategory?actionUserId=\(userId)"
    }
    class func List_ListGiftInShow(userId: Int64,giftCategoryId: Int,pageIndex: Int,pageSize: Int) -> String{
        return  "\(ServiceDomain)List_ListGiftInShow?actionUserId=\(userId)&giftCategoryId=\(giftCategoryId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)"
    }
    
    class func User_GetDetailsV2(actionUserId: Int64, userId: Int64, key: String) ->String{
        return  "\(ServiceDomain)User_GetDetailsV2?actionUserId=\(actionUserId)&userId=\(userId)&key=\(key)"
    }
    
    class func User_FollowUser() -> String
    {
        return "\(ServiceDomain)User_FollowUser"
    }
    
    class func User_ShareFacebook() -> String
    {
        return "\(ServiceDomain)User_ShareFacebook"
    }
    
    class func User_UploadPhoto() -> String
    {
        return "\(ServiceDomain)User_UploadPhoto"
    }
    
    class func User_UpdateTeenIdolAccout() -> String
    {
        return "\(ServiceDomain)User_UpdateTeenIdolAccount"
    }
    
    class func User_UpdateNewPassword() -> String
    {
        return "\(ServiceDomain)User_UpdateNewPassWord"
    }
    
    class func User_GetListUserFollowV2(creatorId: Int64, pageIndex: Int, pageSize: Int, contextKey: String, num_UserId: Int64, key : String) -> String {
        return "\(ServiceDomain)User_GetListUserFollowV2?creatorId=\(creatorId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&str_Search=\(contextKey)&num_UserId=\(num_UserId)&key=\(key)"
    }
    
    class func User_GetListUserFanV2(creatorId: Int64, pageIndex: Int, pageSize: Int, contextKey: String, num_UserId: Int64, key : String) -> String {
        return "\(ServiceDomain)User_GetListUserFanV2?creatorId=\(creatorId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&str_Search=\(contextKey)&num_UserId=\(num_UserId)&key=\(key)"
    }
    
    class func User_GetListUserV2(creatorId: Int64,pageIndex: Int, pageSize: Int, contextkey: String, key: String) -> String
    {
        return "\(ServiceDomain)User_GetListUserV2?creatorId=\(creatorId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&str_Search=\(contextkey)&key=\(key)"
    }
    
    class func User_LoginTeenIdol() -> String
    {
        return "\(ServiceDomain)User_LoginTeenIdol"
    }
    
    class func User_LoginTeenIdolV2() -> String
    {
        return "\(ServiceDomain)User_LoginTeenIdolV2"
    }
    
    class func User_LoginByFacebook() -> String
    {
        return "\(ServiceDomain)User_LoginByFacebookV2"
    }
    
    class func User_LoginByFacebookV2() -> String
    {
        return  "\(User_LoginByFacebookV2)"
    }
    
    class func User_RegisterByTeenIdol() -> String
    {
        return "\(ServiceDomain)User_RegisterByTeenIdol"
    }
    
    class func User_RegisterByTeenIdolV2() -> String
    {
        return "\(ServiceDomain)User_RegisterByTeenIdolV2"
    }
    
    class func List_SearchListStar(createUserId: Int64,pageIndex:Int, pageSize:Int, search: String, key:String) -> String
    {
        return "\(ServiceDomain)List_SearchListStar?createUserId=\(createUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&search=\(search)&key=\(key)"
    }
    
    class func GiveGiftV2() -> String{
        return "\(ServiceDomain)GiveGiftV2"
    }
    
    class func User_DoPayment() -> String
    {
        return "\(ServiceDomain)User_DoPaymentV2"
    }
    
    class func User_GiveFreeCoin() -> String
    {
     return "\(ServiceDomain)User_GiveFreeCoin"
    }
    
    class func List_UserSpend(actionUserId: Int64, pageIndex: Int, pageSize: Int, userId: Int64, type: Int, startTime: Int64, endTime: Int64, key: String) -> String
    {
        return "\(ServiceDomain)List_UserSpend?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&userId=\(userId)&type=\(type)&startTime=\(startTime)&endTime=\(endTime)&key=\(key)"
    }
    
    class func User_GetListGiftV2(actionUserId: Int64, pageIndex: Int, pageSize:Int, key: String) -> String
    {
        return "\(ServiceDomain)User_GetListGiftV2=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func User_ListUsedGift(userSendId: Int64, scheduleId:Int64,starId: Int64,pageIndex:Int, pageSize: Int,key:String) -> String
    {
        return "\(ServiceDomain)User_ListUsedGift?userSendId=\(userSendId)&scheduleId=\(scheduleId)&starId=\(starId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func User_GetListUserOnlineV2(userId: Int64, pageIndex: Int, pageSize: Int, searchString: String = "",key: String) -> String{
        return "\(ServiceDomain)User_GetListUserOnlineV2?creatorId=\(userId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&str_Search=\(searchString)&key=\(key)"
    }
    
    class func User_GetListUserSpent(userId: Int64, pageIndex: Int, pageSize: Int, type:Int, startTime: Int64, endTime: Int64, key: String) -> String
    {
        return "\(ServiceDomain)List_UserSpend?pageIndex=\(pageIndex)&pageSize=\(pageSize)&userId=\(userId)&type=\(type)&startTime=\(startTime)&endTime=\(endTime)&key=\(key)"
    }
    
    class func User_SendCoinChat() -> String{
        return "\(ServiceDomain)SendCoinChat"
    }
    
    class func List_GetListGiftV2(actionUserId: Int64, pageIndex: Int, pageSize: Int, key: String) -> String{
        return "\(ServiceDomain)User_GetListGiftV2?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func System_GetConfig() -> String{
        return "\(ServiceDomain)GetConfig?actionUserId=-1"
    }
    
    class func System_AppPurchaseIOS() -> String{
        return "\(ServiceDomain)appPurchaseIos"
    }
    
    class func ListItemsAnimationInStore(pageIndex: Int, pageSize: Int, categoryId: Int64, isNewVersion: Bool, key: String ) ->String{

        return "\(ServiceDomain)List_ItemsAnimationInStore?pageIndex=\(pageIndex)&pageSize=\(pageSize)&categoryId=\(categoryId)&isNewVersion=\(isNewVersion)&key=\(key)";
    }
    
    class func ListItemsVipInStore(pageIndex: Int,pageSize: Int, categoryId: Int, key: String) ->String
    {
        return "\(ServiceDomain)List_ItemsVipInStore?pageIndex=\(pageIndex)&pageSize=\(pageSize)&categoryId=\(categoryId)&key=\(key)"
    }
    
    class func UserBuyItems() ->String
    {
        return "\(ServiceDomain)User_BuyItems"
    }
    
    class func User_BuyItemsForGuild() ->String
    {
        return "\(ServiceDomain)User_BuyItemsForGuild"
    }
    
    class func List_ItemCategories(pageIndex: Int, pageSize: Int, key: String) ->String
    {
        return "\(ServiceDomain)List_ItemCategories?pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func Star_RegisterStar() -> String
    {
        return "\(ServiceDomain)Star_RegisterStar"
    }
    
    class func List_GetListUserBuySeatInShow(actionUserId: Int64, scheduleId: Int64, key : String)->String{
        return "\(ServiceDomain)List_GetListUserBuySeatInShow?actionUserId=\(actionUserId)&scheduleId=\(scheduleId)&key=\(key)"
    }
    
    class func List_GetListShowItemPrice(actionUserId: Int64,pageIndex: Int, pageSize:Int,  minPrice: Int, key : String)->String{
        return "\(ServiceDomain)ShowItem_UserGetListShowItemPrice?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&minPrice=\(minPrice)&key=\(key)"
    }
    
    class func User_BuySeat() -> String{
        return "\(ServiceDomain)User_BuySeatInShow"
    }
    
    class func User_StreamInfo(scheduleId: Int64) -> String{
        return "\(ServiceDomain)StreamInfo?scheduleId=\(scheduleId)"
    }

    class func User_InsertDeviceToken() ->String{
        return "\(ServiceDomain)InsertDeviceToken"
    }
    
    class func User_LogoutTeenIdol() ->String{
        return "\(ServiceDomain)User_LogoutTeenIdol"
    }
    
    class func User_KickUser(actionUserId: Int64, targetUserId: Int64, scheduleId : Int64, key: String) -> String{
        return "\(ServiceDomain)CheckKickAction?actionUserId=\(actionUserId)&targetUserId=\(targetUserId)&scheduleId=\(scheduleId)&key=\(key)"
    }
    
    class func User_DisablePublicChat(actionUserId: Int64, targetUserId: Int64, scheduleId: Int64, isCanChat: String, key: String) -> String{
        return "\(ServiceDomain)User_CheckDisablePublicChat?actionUserId=\(actionUserId)&targetUserId=\(targetUserId)&scheduleId=\(scheduleId)&isCanChat=\(isCanChat)&key=\(key)"
    }
    
    class func User_GetListDaySignIn(actionUserId: Int64, pageIndex: Int, pageSize: Int, key: String) -> String
    {
        return "\(ServiceDomain)User_GetListDaySignIn?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func User_DoDaySignIn() -> String
    {
        return "\(ServiceDomain)User_DoDaySignIn"
    }
    
    class func User_GetUserDaySignInReward(actionUserId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetUserDaySignInReward?actionUserId=\(actionUserId)&key=\(key)"
    }
    
    class func User_GetListAnimationItem(actionUserId: Int64, userId: Int64,pageIndex: Int,pageSize: Int ,key: String) -> String{
        return  "\(ServiceDomain)User_GetListAnimationItem?actionUserId=\(actionUserId)&userId=\(userId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func User_ChoseAnimationDefaultItem(actionUserId: Int64, animationItemId: Int, rootCategoryId: Int64, key: String) -> String{
        return "\(ServiceDomain)Store_ChoseAnimationDefaultItem?actionUserId=\(actionUserId)&animationItemId=\(animationItemId)&rootCategoryId=\(rootCategoryId)&key=\(key)"
    }
    
    
    class func User_BanFacebookLiveChat() -> String{
        return "\(ServiceDomain)BanFacebookLiveChat"
    }
    
    class func User_IsUserCanFacebookChat(userId: Int64, scheduleId: Int64) -> String{
        return "\(ServiceDomain)IsUserCanFacebookChat?actionUserId=\(userId)&scheduleId=\(scheduleId)"
    }
    
    //MARK: Event 
    
    class func User_CheckIdolQuest(actionUserId: Int64, starId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_CheckIdolQuest?actionUserId=\(actionUserId)&starId=\(starId)&key=\(key)"
    }
    
    class func User_CurrentIdolQuestInfo(actionUserId: Int64, starId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_CurrentIdolQuestInfo?actionUserId=\(actionUserId)&starId=\(starId)&key=\(key)"
    }
    
    class func User_CancelIdolQuest() -> String
    {
        return "\(ServiceDomain)User_CancelIdolQuest"
    }
    
    class func User_GetRewardInfo(actionUserId: Int64, starId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetRewardInfo?actionUserId=\(actionUserId)&starId=\(starId)&key=\(key)"
    }
    
    class func User_DoneIdolQuest() -> String
    {
        //return "\(ServiceDomain)User_DoneIdolQuest?actionUserId=\(actionUserId)&scheduleId=\(scheduleId)&key=\(key)"
        return "\(ServiceDomain)User_DoneIdolQuest"
    }
    
    class func User_IdolGetQuest() -> String
    {
        return "\(ServiceDomain)User_IdolGetQuest"
    }
    
    class func User_GetTopIdolQuestStar(actionUserId: Int64, pageIndex: Int, pageSize: Int, startDate: Int64, endDate: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetTopIdolQuestStar?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&startDate=\(startDate)&endData=\(endDate)&key=\(key)";
    }
    
    class func User_GetListTopUserUseCoinInShow(actionUserId actionUserId: Int64, scheduleId: Int64, showId: Int64, starId: Int64, pageIndex: Int, pageSize: Int, startDate: Int64, endDate: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetListTopUserUseCoinInShow?actionUserId=\(actionUserId)&scheduleId=\(scheduleId)&showId=\(showId)&starId=\(starId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&startDate=\(startDate)&endData=\(endDate)&key=\(key)";
    }
    
    class func User_GetTotalCoinUsed(actionUserId actionUserId: Int64, scheduleId: Int64, showId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetTotalCoinUsed?actionUserId=\(actionUserId)&scheduleId=\(scheduleId)&showId=\(showId)&key=\(key)"
    }
    
    class func Star_InfoIdolGift(actionUserId actionUserId: Int64, starId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)Star_InfoIdolGift?actionUserId=\(actionUserId)&starId=\(starId)&key=\(key)";
    }
    
    class func User_GetListGuild(creatorId creatorId: Int64, pageIndex: Int, pageSize: Int, key: String) -> String
    {
        return "\(ServiceDomain)User_GetListGuild?creatorId=\(creatorId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func User_GetGuildInfo(actionUserId actionUserId: Int64, guildId: Int64, key: String) -> String
    {
        return "\(ServiceDomain)User_GetGuildInfo?actionUserId=\(actionUserId)&guildId=\(guildId)&key=\(key)"
    }
    
    class func User_GetListGuild_Post() -> String
    {
        return "\(ServiceDomain)User_GetListGuild"
    }
    
    class func User_GetGuildInfo_Post() -> String
    {
        return "\(ServiceDomain)User_GetGuildInfo"
    }
    
    class func User_GetListUserInGuild_Post() -> String
    {
        return "\(ServiceDomain)User_GetListUserInGuild"
    }
    
    class func List_ItemsAnimationInStoreForGuild(pageIndex: Int, pageSize: Int, categoryId: Int64, isNewVersion: Bool, guildId: Int64, key: String ) ->String{
        
        return "\(ServiceDomain)List_ItemsAnimationInStore?pageIndex=\(pageIndex)&pageSize=\(pageSize)&categoryId=\(categoryId)&isNewVersion=\(isNewVersion)&guildId=\(guildId)&key=\(key)";
    }
    
    class func User_GetListUserJoinGuild() -> String
    {
        return "\(ServiceDomain)User_GetListUserJoinGuild"
    }
    
    class func User_SendRequestJoinGuild() -> String
    {
        return "\(ServiceDomain)User_SendRequestJoinGuild"
    }
    
    class func User_DeclineUserJoinGuild() -> String
    {
        return "\(ServiceDomain)User_DeclineUserJoinGuild"
    }
    
    class func User_AcceptUserJoinGuild() -> String
    {
        return "\(ServiceDomain)User_AcceptUserJoinGuild"
    }
    
    class func User_KickOutUser() -> String
    {
        return "\(ServiceDomain)User_KickOutUser"
    }
    
    class func User_ReportIdol() -> String
    {
        return "\(ServiceDomain)User_ReportIdol"
    }
    
    class func Stream_GetStreamInfo(streamKey: String) -> String
    {
        return "\(ServiceDomain)GetStreamInfo?streamKey=\(streamKey)"
    }
    
    class func Event_GetUserLixi(actionUserId: Int64, typeId: Int, key: String) -> String{
        return "\(ServiceDomain)User_GetUserGranary?actionUserId=\(actionUserId)&typeId=\(typeId)&key=\(key)"
    }
    
    class func Event_GetGranaryInfo(actionUserId: Int64, pageIndex: Int,pageSize: Int, key: String) -> String{
        return "\(ServiceDomain)User_GetGranaryInfo?actionUserId=\(actionUserId)&pageIndex=\(pageIndex)&pageSize=\(pageSize)&key=\(key)"
    }
    
    class func Event_GetGranaryReward() -> String{
        return "\(ServiceDomain)User_GetGranaryReward"
    }
    
    class func Event_GetIdolTotalChicken(starId: Int64) -> String {
        return "\(ServiceDomain)User_GetTopUserGranary?pageIndex=0&pageSize=1&userId=\(starId)"
    }
    
    class func Event_GetNguyenLieu(actionUserId: Int64, key: String) -> String {
        return "\(ServiceDomain)User_GetListUserGranary?actionUserId=\(actionUserId)&listtypeId=12,13,14&key=\(key)"
    }
    
    class func Star_LoginTeenIdol() -> String {
        return "\(ServiceDomain)Star_LoginTeenIdol"
    }
}
