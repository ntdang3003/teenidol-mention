//
//  SignalRConnector.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SignalR_ObjC
 
public enum SignalRAlertCode : Int {
    case PublicMessage = 0
    case JoinShow = 1
    case ExitShow = 2
    case BuySeat = 3
    case GiveGift = 4
    case ShowStatus = 5
    case StartShow = 6
    case EndShow = 7
    case StarResetVote = 8
    case StarDeleteRepertoire =  9
    case VoteRepertoire = 10
    case StarCompleteRepertoire  = 11
    case DisablePublicChat = 14
    case EnablePublicChat = 15
    case KickUser = 16
    case CodePrivateMessage = 17
    case StarGivePermission = 18
    case StarEndShow = 19
    case GiveHeart = 20
    case UpdateStarInfo = 21
    case CoinMessage = 22
    case DisableChatFacebook = 23
    
    case IdolAcceptMission = 24
    case IdolCancelMission = 25
    case IdolGetReward = 26
    case UserGetReward = 27
    case IdolDoneQuest = 28
    case LixiGift = 31
}

public protocol SignalRConnectorDelegate : NSObjectProtocol{
    func signalR_DidConnect(connectionId: String)
    func signalR_DidReceiveAlert(data: JSON, code: SignalRAlertCode)
}

class SignalRConnector: NSObject, SRConnectionDelegate {
    static let sharedInstance = SignalRConnector()
    var _hubConnection: SRHubConnection? = nil
    var _hubProxy : SRHubProxy? = nil
    var delegate : SignalRConnectorDelegate? = nil
    var connectionId = ""
    
    func connectToServer(){
        _hubConnection  = SRHubConnection(URLString: URLConstant.sharedInstance.Server_SignalR)
        _hubProxy = _hubConnection!.createHubProxy("teenidol") as? SRHubProxy
        _hubProxy?.on("onShowAlert", perform: self, selector: #selector(onShowAlert))
        _hubConnection?.delegate = self
        _hubConnection?.start()
    }
    
    func disconnectToServer(){
        self.connectionId = ""
        _hubConnection?.stop()
    }
    
    func SRConnectionDidOpen(connection: SRConnectionInterface!) {
        if connection != nil && connection.connectionId != nil{
            self.connectionId = connection.connectionId
            if(delegate != nil){
                delegate?.signalR_DidConnect(self.connectionId)
            }
        }
    }
    
    //MARK: Alerts
    func onShowAlert(scheduleId: Int64, code: AnyObject, json: AnyObject){
        let _data = JSON.parse(json as! String)
        let _code : SignalRAlertCode? = SignalRAlertCode(rawValue: code as! Int)
        if(delegate != nil && _code != nil){
            self.delegate?.signalR_DidReceiveAlert(_data, code:_code!)
        }
    }
    
    //MARK: Functions
    func sendShowMessage(userSend: SignalRUserDTO, message: String, scheduleId : Int64,targetUser: SignalRUserDTO? = nil, callback: ((DoResultDTO) -> Void) ){
        var targetUserTemp = SignalRUserDTO()
        if(targetUser != nil){
            targetUserTemp = targetUser!
        }
        _hubProxy?.invoke("SendShowMessage", withArgs: [Int(scheduleId),userSend.toJSON(),
           targetUserTemp.toJSON(), message], completionHandler: { (data, error) in
            //let dic = JSON(data)
            var doRs = DoResultDTO()
            if(error != nil){
                doRs.Message = "Có lỗi khi gửi tin nhắn"
                doRs.DataObject = data
                callback(doRs)
            }else{
                let dic = JSON(data)
                if(dic.count > 2){
                    doRs = DoResultDTO(data: dic)
                    callback(doRs)
                }
            }
        })
    }
    
    func isUserCanChat(scheduleId: Int64, userId: Int64, callback: ((Bool) -> Void)){
        _hubProxy?.invoke("IsUserCanChatShow", withArgs: [NSNumber(longLong: scheduleId),NSNumber(longLong:userId)], completionHandler: { (data, error) in
            if(error == nil && data != nil){
                let json = data as! [String:AnyObject]
                if let rs = json["Result"] as? Bool{
                    callback(rs)
                }
            }else{
                callback(true)
            }
        })
    }
}
