//
//  ViewMessageHelper.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

class ViewMessageHelper: UIView {
    private static  var view: ViewMessageHelper!
    var label : UILabel?
    var image : UIImageView?
    class func shared() -> ViewMessageHelper {
        if view != nil {
            return view
        }
        var token: dispatch_once_t = 0
        dispatch_once(&token) {
            view = ViewMessageHelper()
        }
        view.backgroundColor = ColorListViewBackground
        return view
    }
    
    func CreateView(message: String, withImage strImage: String) {
        if self.label == nil {
            self.label = UILabel(frame: CGRectMake(30, 190, 260, 50))
            self.label!.numberOfLines = 2
            self.label!.font = UIFont(name: FontNameDefault, size: 14)
            self.label!.textAlignment = .Center
            self.label!.textColor = UIColor.blackColor()
            self.addSubview(self.label!)
            self.image = UIImageView(frame: CGRectMake(85, 50, 150, 150))
            self.addSubview(self.image!)
        }
        image!.image = UIImage(named: strImage)
        self.label!.text = message
    }
    
    class func showInView(message: String, withImage strImage: String, withView view: UIView) {
        let viewroot: ViewMessageHelper = self.shared()
        //ViewMessageHelper *viewroot = [[ViewMessageHelper alloc]init];
        viewroot.frame = view.bounds
        viewroot.CreateView(message, withImage: strImage)
        view.addSubview(viewroot)
        viewroot.label!.frame = CGRectMake(30, 190, 260, 50)
        viewroot.label!.font = UIFont(name: FontNameDefault, size: 14)
        viewroot.image!.frame = CGRectMake(95, 50, 130, 120)
        viewroot.label!.textColor = ColorTextTable
//        if viewroot.btn {
//            self.shared().btn.removeFromSuperview()
//        }
    }
    
    //    class func dismiss() {
    //        //-------------------------------------------------------------------------------------------------------------------------------------------------
    //        self.shared().HidenView()
    //    }
    
//    class func showinViewSmall(message: String, withImage strImage: String, withView view: UIView, withButton TypeButton: Int) {
//        var viewroot: ViewMessageHelper = self.shared()
//        if viewroot.btn {
//            self.shared().btn.removeFromSuperview()
//        }
//        viewroot.frame = view.bounds
//        viewroot.CreateView(message, withImage: strImage)
//        var btn: UIButton = self.shared().CreateButtonMore(TypeButton)
//        viewroot.addSubview(btn)
//        viewroot.label.frame = CGRectMake(30, 50, 260, 50)
//        viewroot.label.font = UIFont(name: FontNameDefaut, size: 13)
//        viewroot.image.frame = CGRectMake(135, 10, 50, 50)
//        viewroot.btn.frame = CGRectMake(60, 100, 200, 40)
//        view.addSubview(viewroot)
//    }
//    
//    class func showinView(message: String, withImage strImage: String, withView view: UIView, withButton TypeButton: Int) {
//        var viewroot: ViewMessageHelper = self.shared()
//        if viewroot.btn {
//            self.shared().btn.removeFromSuperview()
//        }
//        viewroot.frame = view.bounds
//        viewroot.CreateView(message, withImage: strImage)
//        var btn: UIButton = viewroot.CreateButtonMore(TypeButton)
//        viewroot.addSubview(btn)
//        viewroot.label.frame = CGRectMake(30, 190, 260, 50)
//        viewroot.label.textColor = ColorTextTable
//        viewroot.label.font = UIFont(name: FontNameDefaut, size: 14)
//        viewroot.image.frame = CGRectMake(85, 50, 150, 150)
//        viewroot.btn.frame = CGRectMake(60, 250, 200, 40)
//        view.addSubview(viewroot)
//    }
//    
//    func CreateButtonMore(TypeButton: Int) -> UIButton {
//        switch TypeButton {
//        case TypeButtonAddPlace:
//            btn = UIButton(frame: CGRectMake(60, 250, 200, 40))
//            btn.setBackgroundImage(UIImage(named: "ic_button_close_moreinfo_detail.png"), forState: .Normal)
//            btn.setTitle("Tạo địa điểm mới", forState: .Normal)
//            return btn
//            
//        case TypeButtonRefresh:
//            btn = UIButton(frame: CGRectMake(60, 250, 200, 40))
//            btn.setBackgroundImage(UIImage(named: "ic_button_close_moreinfo_detail.png"), forState: .Normal)
//            btn.setTitle("Nhấn để thử lại", forState: .Normal)
//            return btn
//            
//        default:
//            break
//        }
//        
//        return nil
//    }
//    
//    class func GetButtonCurrent() -> UIButton {
//        return self.shared().btn
//    }
//    
//    class func show(message: String, withImage strImage: String) {
//        self.shared().CreateView(message, withImage: strImage)
//    }
//    
//    func CreateView(message: String, withImage strImage: String) {
//        if !self.label {
//            self.label = UILabel(frame: CGRectMake(30, 190, 260, 50))
//            self.label.numberOfLines = 2
//            self.label.font = UIFont(name: FontNameDefaut, size: 14)
//            self.label.textAlignment = .Center
//            self.label.textColor = UIColor.blackColor()
//            self.addSubview(self.label)
//            self.image = UIImageView(frame: CGRectMake(85, 50, 150, 150))
//            self.addSubview(self.image)
//        }
//        image.image = UIImage(named: strImage)
//        self.label.text = message
//    }
//    
//    func HidenView() {
//        self.removeFromSuperview()
//    }
}
