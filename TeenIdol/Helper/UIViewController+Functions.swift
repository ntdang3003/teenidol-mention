//
//  UIViewController+Functions.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

extension UIViewController{
    
    func checkLogin(callback: ((SignInState) -> Void)) -> Bool{
        if(SettingsHelper.getUserID() > 0 && SettingsHelper.getUserInfo() != nil){
            callback(SignInState.Logged)
            return true
        }else{
            let signIn = UIViewController()
            signIn.view = SignInView(frame: self.view.bounds,rootViewController: signIn,callback: callback)
            //signIn.automaticallyAdjustsScrollViewInsets = false
            let navi = UINavigationController(rootViewController: signIn)
            navi.navigationBar.hidden = true
            self.presentViewController(navi, animated: true, completion: nil)
            return false
        }
    }
    
    func backButtonPressHandler()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func setupBackButton(){
        let button = FunctionHelper.getNavigationBarButton("ic_arrow_left")
        button.addTarget(self, action: #selector(backButtonPressHandler), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem( UIBarButtonItem(customView:
            button), animated: true)
    }
}

