//
//  CameraSessionHelper.h
//  TeenIdol
//
//  Created by Dang Nguyen on 4/20/16.
//  Copyright © 2016 TFL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CameraHelperRTMPSessionState)
{
    CameraHelperRTMPStateNone,
    CameraHelperRTMPStatePreviewStarted,
    CameraHelperRTMPStateStarting,
    CameraHelperRTMPStateStarted,
    CameraHelperRTMPStateEnded,
    CameraHelperRTMPStateError
    
};

typedef NS_ENUM(NSInteger, CameraHelperCameraState)
{
    CameraHelperCameraStateFront,
    CameraHelperCameraStateBack
};

typedef NS_ENUM(NSInteger, CameraHelperAspectMode)
{
    CameraHelperAspectModeFit,
    CameraHelperAscpectModeFill
};
//
////With new filters should add an enum here
typedef NS_ENUM(NSInteger, CameraFilter) {
    CameraFilterNormal,
    CameraFilterCustom,
};

@protocol CameraSessionHelperDelegate <NSObject>
@required
- (void) connectionStatusChanged: (CameraHelperRTMPSessionState) sessionState;
@optional
- (void) didAddCameraSource:(CameraHelperRTMPSessionState*)session;
@end

@interface CameraSessionHelper : NSObject
@property (nonatomic, readonly) CameraHelperRTMPSessionState rtmpSessionState;
@property (nonatomic,strong) UIView * previewView;
//@property (nonatomic,readonly) GPUImageRawDataOutput * m_outputRaw;

@property (nonatomic, assign) CGSize            videoSize;
@property (nonatomic, assign) int               bitrate;
@property (nonatomic, assign) int               fps;           
@property (nonatomic, assign, readonly) BOOL    useInterfaceOrientation;

@property (nonatomic, assign) int           audioChannelCount;
@property (nonatomic, assign) float         audioSampleRate;
@property (nonatomic, assign) float         micGain;        // [0..1]
@property (nonatomic, assign) CameraHelperCameraState cameraState;
@property (nonatomic, assign) CameraHelperAspectMode  aspectMode;
@property (nonatomic, assign) BOOL          useAdaptiveBitrate;
@property (nonatomic, assign) CGSize            previewSize;
@property (nonatomic, assign) id<CameraSessionHelperDelegate> delegate;

// -----------------------------------------------------------------------------
- (instancetype) initWithVideoSize:(CGSize)videoSize
                         frameRate:(int)fps
                           bitrate:(int)bps
           useInterfaceOrientation:(BOOL)useInterfaceOrientation
                       cameraState:(CameraHelperCameraState) cameraState
                        aspectMode:(CameraHelperAspectMode) aspectMode
                      previewSize : (CGSize) previewSize;

- (void) startRtmpSessionWithURL:(NSString*) rtmpUrl
                    andStreamKey:(NSString*) streamKey;
- (void) endRtmpSession;
-(void) cleanUp;
-(void) rotateCamera;
-(void) enableTourch;
-(bool) enableFilter;
@end
