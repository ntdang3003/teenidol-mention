//
//  CameraSessionHelper.m
//  TeenIdol
//
//  Created by Dang Nguyen on 4/20/16.
//  Copyright © 2016 TFL. All rights reserved.
//

#import "CameraSessionHelper.h"
#import <videocore/api/iOS/VCPreviewView.h>

#include <videocore/rtmp/RTMPSession.h>
#include <videocore/transforms/RTMP/AACPacketizer.h>
#include <videocore/transforms/RTMP/H264Packetizer.h>
#include <videocore/transforms/Split.h>
#include <videocore/transforms/AspectTransform.h>
#include <videocore/transforms/PositionTransform.h>

#ifdef __APPLE__
#   include <videocore/mixers/Apple/AudioMixer.h>
#   include <videocore/transforms/Apple/MP4Multiplexer.h>
#   include <videocore/transforms/Apple/H264Encode.h>
#   include <videocore/sources/Apple/PixelBufferSource.h>
#   ifdef TARGET_OS_IPHONE
#       include <videocore/sources/iOS/CameraSource.h>
#       include <videocore/sources/iOS/MicSource.h>
#       include <videocore/mixers/iOS/GLESVideoMixer.h>
#       include <videocore/transforms/iOS/AACEncode.h>
#       include <videocore/transforms/iOS/H264Encode.h>

#   else /* OS X */

#   endif
#else
#   include <videocore/mixers/GenericAudioMixer.h>
#endif

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#include <sstream>


static const int kMinVideoBitrate = 32000;

namespace videocore { namespace simpleApi {
    
    using PixelBufferCallback = std::function<void(const uint8_t* const data,
                                                   size_t size)> ;
    
    class PixelBufferOutput : public IOutput
    {
    public:
        PixelBufferOutput(PixelBufferCallback callback)
        : m_callback(callback) {};
        
        void pushBuffer(const uint8_t* const data,
                        size_t size,
                        IMetadata& metadata)
        {
            m_callback(data, size);
        }
        
    private:
        
        PixelBufferCallback m_callback;
    };
}
}


@interface CameraSessionHelper(){
    
    
    VCPreviewView* _previewView;
    
    std::shared_ptr<videocore::simpleApi::PixelBufferOutput> m_pbOutput;
    std::shared_ptr<videocore::iOS::MicSource>               m_micSource;
    std::shared_ptr<videocore::iOS::CameraSource>            m_cameraSource;
    std::shared_ptr<videocore::Apple::PixelBufferSource>     m_pixelBufferSource;
    std::shared_ptr<videocore::AspectTransform>              m_pbAspect;
    std::shared_ptr<videocore::PositionTransform>            m_pbPosition;
    
    std::shared_ptr<videocore::Split> m_videoSplit;
    std::shared_ptr<videocore::AspectTransform>   m_aspectTransform;
    videocore::AspectTransform::AspectMode m_aspectMode;
    std::shared_ptr<videocore::PositionTransform> m_positionTransform;
    std::shared_ptr<videocore::IAudioMixer> m_audioMixer;
    std::shared_ptr<videocore::IVideoMixer> m_videoMixer;
    std::shared_ptr<videocore::ITransform>  m_h264Encoder;
    std::shared_ptr<videocore::ITransform>  m_aacEncoder;
    std::shared_ptr<videocore::ITransform>  m_h264Packetizer;
    std::shared_ptr<videocore::ITransform>  m_aacPacketizer;
    
    std::shared_ptr<videocore::Split>       m_aacSplit;
    std::shared_ptr<videocore::Split>       m_h264Split;
    std::shared_ptr<videocore::Apple::MP4Multiplexer> m_muxer;
    
    std::shared_ptr<videocore::IOutputSession> m_outputSession;    // properties
    
    // properties
    
    dispatch_queue_t _graphManagementQueue;
    
    CGSize _videoSize;
    int    _bitrate;
    
    int    _fps;
    int    _bpsCeiling;
    int    _estimatedThroughput;
    
    BOOL   _useInterfaceOrientation;
    float  _videoZoomFactor;
    int    _audioChannelCount;
    float  _audioSampleRate;
    float  _micGain;
    
    CameraHelperAspectMode _aspectMode;
    CameraHelperRTMPSessionState _rtmpSessionState;
    CameraHelperCameraState _cameraState;
    BOOL   _orientationLocked;
    BOOL   _torch;
    
    BOOL _useAdaptiveBitrate;
    BOOL _continuousAutofocus;
    BOOL _continuousExposure;
    CGPoint _focusPOI;
    CGPoint _exposurePOI;
    
    CameraFilter _filter;
}

@property (nonatomic, readwrite) CameraHelperRTMPSessionState rtmpSessionState;
- (void) setupGraph;

@end

@implementation CameraSessionHelper

- (BOOL) orientationLocked
{
    return _orientationLocked;
}
- (void) setOrientationLocked:(BOOL)orientationLocked
{
    _orientationLocked = orientationLocked;
    if(m_cameraSource) {
        m_cameraSource->setOrientationLocked(orientationLocked);
    }
}

- (float) videoZoomFactor
{
    return _videoZoomFactor;
}
- (void) setVideoZoomFactor:(float)videoZoomFactor
{
    _videoZoomFactor = videoZoomFactor;
    if(m_positionTransform) {
        // We could use AVCaptureConnection's zoom factor, but in reality it's
        // doing the exact same thing as this (in terms of the algorithm used),
        // but it is not clear how CoreVideo accomplishes it.
        // In this case this is just modifying the matrix
        // multiplication that is already happening once per frame.
        m_positionTransform->setSize(self.videoSize.width * videoZoomFactor,
                                     self.videoSize.height * videoZoomFactor);
    }
}

- (void) setRtmpSessionState:(CameraHelperRTMPSessionState)rtmpSessionState
{
    _rtmpSessionState = rtmpSessionState;
    if (NSOperationQueue.currentQueue != NSOperationQueue.mainQueue) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // trigger in main thread, avoid autolayout engine exception
            if(self.delegate) {
                [self.delegate connectionStatusChanged:rtmpSessionState];
            }
        });
    } else {
        if (self.delegate) {
            [self.delegate connectionStatusChanged:rtmpSessionState];
        }
    }
}

- (instancetype) initWithVideoSize:(CGSize)videoSize
                         frameRate:(int)fps
                           bitrate:(int)bps
           useInterfaceOrientation:(BOOL)useInterfaceOrientation
                       cameraState:(CameraHelperCameraState) cameraState
                        aspectMode:(CameraHelperAspectMode) aspectMode
                      previewSize : (CGSize) previewSize
{
    if (( self = [super init] )){
        self.bitrate = bps;
        self.videoSize = videoSize;
        self.fps = fps;
        _useInterfaceOrientation = useInterfaceOrientation;
        self.micGain = 1.f;
        self.audioChannelCount = 2;
        self.audioSampleRate = 44100.;
        self.useAdaptiveBitrate = NO;
        self.aspectMode = aspectMode;
        
        _previewView = [[VCPreviewView alloc] init];
        self.videoZoomFactor = 1.f;
        
        _cameraState = cameraState;
        _exposurePOI = _focusPOI = CGPointMake(0.5f, 0.5f);
        _continuousExposure = _continuousAutofocus = YES;
        
        _graphManagementQueue = dispatch_queue_create("com.videocore.session.graph", 0);
        
        __block CameraSessionHelper* bSelf = self;
        
        dispatch_async(_graphManagementQueue, ^{
            [bSelf setupGraph];
        });
    }
    return self;
}

- (void) setupGraph
{
    const double frameDuration = 1. / static_cast<double>(self.fps);
    
    {
        // Add audio mixer
        const double aacPacketTime = 1024. / self.audioSampleRate;
        
        m_audioMixer = std::make_shared<videocore::Apple::AudioMixer>(self.audioChannelCount,
                                                                      self.audioSampleRate,
                                                                      16,
                                                                      aacPacketTime);
        
        
        // The H.264 Encoder introduces about 2 frames of latency, so we will set the minimum audio buffer duration to 2 frames.
        m_audioMixer->setMinimumBufferDuration(frameDuration*2);
    }
#ifdef __APPLE__
#ifdef TARGET_OS_IPHONE
    
    
    {
        // Add video mixer
        m_videoMixer = std::make_shared<videocore::iOS::GLESVideoMixer>(self.videoSize.width,
                                                                        self.videoSize.height,
                                                                        frameDuration);
        
    }
    
    {
        auto videoSplit = std::make_shared<videocore::Split>();
        
        m_videoSplit = videoSplit;
        VCPreviewView* preview = (VCPreviewView*)self.previewView;
        
        m_pbOutput = std::make_shared<videocore::simpleApi::PixelBufferOutput>([=](const void* const data, size_t size){
            CVPixelBufferRef ref = (CVPixelBufferRef)data;
            [preview drawFrame:ref];
//            if(self.rtmpSessionState == CameraHelperRTMPStateNone) {
//                self.rtmpSessionState = CameraHelperRTMPStateStarted;
//            }
        });
        
        videoSplit->setOutput(m_pbOutput);
        
        m_videoMixer->setOutput(videoSplit);
        
    }
    
#else
#endif // TARGET_OS_IPHONE
#endif // __APPLE__
    
    // Create sources
    {
        // Add camera source
        m_cameraSource = std::make_shared<videocore::iOS::CameraSource>();
        m_cameraSource->setOrientationLocked(self.orientationLocked);
        auto aspectTransform = std::make_shared<videocore::AspectTransform>(self.videoSize.width,self.videoSize.height,m_aspectMode);
        
        auto positionTransform = std::make_shared<videocore::PositionTransform>(self.videoSize.width/2, self.videoSize.height/2,self.videoSize.width * self.videoZoomFactor, self.videoSize.height * self.videoZoomFactor,
                                                                                self.videoSize.width, self.videoSize.height
                                                                                );
        
        
        std::dynamic_pointer_cast<videocore::iOS::CameraSource>(m_cameraSource)->setupCamera(self.fps,(self.cameraState == CameraHelperCameraStateFront),self.useInterfaceOrientation,AVCaptureSessionPresetHigh,^{
            m_cameraSource->setContinuousAutofocus(true);
            m_cameraSource->setContinuousExposure(true);
            
            m_cameraSource->setOutput(aspectTransform);
            
            //            m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.bgra")));
            //            _filter = VCFilterNormal;
            
            m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.grayscale")));
            m_videoMixer->setTestFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.sepia")),
                                        dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.invertColors")));
            
            //com.videocore.filters.invertColors
            _filter = CameraFilterCustom;
            
            
            aspectTransform->setOutput(positionTransform);
            positionTransform->setOutput(m_videoMixer);
            m_aspectTransform = aspectTransform;
            m_positionTransform = positionTransform;
            
            // Inform delegate that camera source has been added
//            if ([_delegate respondsToSelector:@selector(didAddCameraSource:)]) {
//                [_delegate didAddCameraSource:self];
//            }
        });
    }
    {
        // Add mic source
        m_micSource = std::make_shared<videocore::iOS::MicSource>(self.audioSampleRate, self.audioChannelCount);
        m_micSource->setOutput(m_audioMixer);
        
        const auto epoch = std::chrono::steady_clock::now();
        
        m_audioMixer->setEpoch(epoch);
        m_videoMixer->setEpoch(epoch);
        
        m_audioMixer->start();
        m_videoMixer->start();
        
    }
}

- (void) addEncodersAndPacketizers
{
    int ctsOffset = 2000 / self.fps; // 2 * frame duration
    {
        // Add encoders
        
        m_aacEncoder = std::make_shared<videocore::iOS::AACEncode>(self.audioSampleRate, self.audioChannelCount, 96000);
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
            // If >= iOS 8.0 use the VideoToolbox encoder that does not write to disk.
            m_h264Encoder = std::make_shared<videocore::Apple::H264Encode>(self.videoSize.width,
                                                                           self.videoSize.height,
                                                                           self.fps,
                                                                           self.bitrate,
                                                                           false,
                                                                           ctsOffset);
        } else {
            m_h264Encoder =std::make_shared<videocore::iOS::H264Encode>(self.videoSize.width,
                                                                        self.videoSize.height,
                                                                        self.fps,
                                                                        self.bitrate);
        }
        m_audioMixer->setOutput(m_aacEncoder);
        m_videoSplit->setOutput(m_h264Encoder);
        
    }
    {
        m_aacSplit = std::make_shared<videocore::Split>();
        m_h264Split = std::make_shared<videocore::Split>();
        m_aacEncoder->setOutput(m_aacSplit);
        m_h264Encoder->setOutput(m_h264Split);
        
    }
    {
        m_h264Packetizer = std::make_shared<videocore::rtmp::H264Packetizer>(ctsOffset);
        m_aacPacketizer = std::make_shared<videocore::rtmp::AACPacketizer>(self.audioSampleRate, self.audioChannelCount, ctsOffset);
        
        m_h264Split->setOutput(m_h264Packetizer);
        m_aacSplit->setOutput(m_aacPacketizer);
        
    }
    {
        /*m_muxer = std::make_shared<videocore::Apple::MP4Multiplexer>();
         videocore::Apple::MP4SessionParameters_t parms(0.) ;
         std::string file = [[[self applicationDocumentsDirectory] stringByAppendingString:@"/output.mp4"] UTF8String];
         parms.setData(file, self.fps, self.videoSize.width, self.videoSize.height);
         m_muxer->setSessionParameters(parms);
         m_aacSplit->setOutput(m_muxer);
         m_h264Split->setOutput(m_muxer);*/
    }
    
    
    m_h264Packetizer->setOutput(m_outputSession);
    m_aacPacketizer->setOutput(m_outputSession);
    
    
}

- (void) startRtmpSessionWithURL:(NSString *)rtmpUrl
                    andStreamKey:(NSString *)streamKey
{
    
    __block CameraSessionHelper* bSelf = self;
    
    dispatch_async(_graphManagementQueue, ^{
        [bSelf startSessionInternal:rtmpUrl streamKey:streamKey];
    });
}


- (void) startSessionInternal: (NSString*) rtmpUrl
                    streamKey: (NSString*) streamKey
{
    std::stringstream uri ;
    uri << (rtmpUrl ? [rtmpUrl UTF8String] : "") << "/" << (streamKey ? [streamKey UTF8String] : "");
    
    m_outputSession.reset(
                          new videocore::RTMPSession ( uri.str(),
                                                      [=](videocore::RTMPSession& session,
                                                          ClientState_t state) {
                                                          
                                                          DLog("ClientState: %d\n", state);
                                                          
                                                          switch(state) {
                                                                  
                                                              case kClientStateConnected:
                                                                  self.rtmpSessionState = CameraHelperRTMPStateStarting;
                                                                  break;
                                                              case kClientStateSessionStarted:
                                                              {
                                                                  
                                                                  __block CameraSessionHelper* bSelf = self;
                                                                  dispatch_async(_graphManagementQueue, ^{
                                                                      [bSelf addEncodersAndPacketizers];
                                                                  });
                                                              }
                                                                  self.rtmpSessionState = CameraHelperRTMPStateStarted;
                                                                  
                                                                  break;
                                                              case kClientStateError:
                                                                  //self.rtmpSessionState = CameraHelperRTMPStateError;
                                                                  [self endRtmpSession];
                                                                  break;
                                                              case kClientStateNotConnected:
                                                                  self.rtmpSessionState = CameraHelperRTMPStateEnded;
                                                                  //[self endRtmpSession];
                                                                  break;
                                                              default:
                                                                  break;
                                                                  
                                                          }
                                                          
                                                      }) );
    CameraSessionHelper* bSelf = self;
    
    _bpsCeiling = _bitrate;
    
    if ( self.useAdaptiveBitrate ) {
        _bitrate = 500000;
    }
    
    m_outputSession->setBandwidthCallback([=](float vector, float predicted, int inst)
                                          {
                                              
                                              bSelf->_estimatedThroughput = predicted;
                                              auto video = std::dynamic_pointer_cast<videocore::IEncoder>( bSelf->m_h264Encoder );
                                              auto audio = std::dynamic_pointer_cast<videocore::IEncoder>( bSelf->m_aacEncoder );
                                              if(video && audio && bSelf.useAdaptiveBitrate) {
                                                  
                                                  //                                                  if ([bSelf.delegate respondsToSelector:@selector(detectedThroughput:)]) {
                                                  //                                                      [bSelf.delegate detectedThroughput:predicted];
                                                  //                                                  }
                                                  //                                                  if ([bSelf.delegate respondsToSelector:@selector(detectedThroughput:videoRate:)]) {
                                                  //                                                      [bSelf.delegate detectedThroughput:predicted videoRate:video->bitrate()];
                                                  //                                                  }
                                                  
                                                  
                                                  int videoBr = 0;
                                                  
                                                  if(vector != 0) {
                                                      
                                                      vector = vector < 0 ? -1 : 1 ;
                                                      
                                                      videoBr = video->bitrate();
                                                      
                                                      if (audio) {
                                                          
                                                          if ( videoBr > 500000 ) {
                                                              audio->setBitrate(128000);
                                                          } else if (videoBr <= 500000 && videoBr > 250000) {
                                                              audio->setBitrate(96000);
                                                          } else {
                                                              audio->setBitrate(80000);
                                                          }
                                                      }
                                                      
                                                      
                                                      if(videoBr > 1152000) {
                                                          video->setBitrate(std::min(int((videoBr / 384000 + vector )) * 384000, bSelf->_bpsCeiling) );
                                                      }
                                                      else if( videoBr > 512000 ) {
                                                          video->setBitrate(std::min(int((videoBr / 128000 + vector )) * 128000, bSelf->_bpsCeiling) );
                                                      }
                                                      else if( videoBr > 128000 ) {
                                                          video->setBitrate(std::min(int((videoBr / 64000 + vector )) * 64000, bSelf->_bpsCeiling) );
                                                      }
                                                      else {
                                                          video->setBitrate(std::max(std::min(int((videoBr / 32000 + vector )) * 32000, bSelf->_bpsCeiling), kMinVideoBitrate) );
                                                      }
                                                      DLog("\n(%f) AudioBR: %d VideoBR: %d (%f)\n", vector, audio->bitrate(), video->bitrate(), predicted);
                                                  } /* if(vector != 0) */
                                                  
                                              } /* if(video && audio && m_adaptiveBREnabled) */
                                              
                                              
                                          });
    
    videocore::RTMPSessionParameters_t sp ( 0. );
    
    sp.setData(self.videoSize.width,
               self.videoSize.height,
               1. / static_cast<double>(self.fps),
               self.bitrate,
               self.audioSampleRate,
               (self.audioChannelCount == 2));
    
    m_outputSession->setSessionParameters(sp);
}

- (void) endRtmpSession
{
    
    m_h264Packetizer.reset();
    m_aacPacketizer.reset();
    m_videoSplit->removeOutput(m_h264Encoder);
    m_h264Encoder.reset();
    m_aacEncoder.reset();
    
    m_outputSession.reset();
    
    _bitrate = _bpsCeiling;
    self.rtmpSessionState = CameraHelperRTMPStateEnded;
}

-(void) rotateCamera{
//    if(m_cameraSource){
//        [m_cameraSource rotateCamera];
//    }
    _cameraState = _cameraState == CameraHelperCameraStateFront ? CameraHelperCameraStateBack : CameraHelperCameraStateFront ;
    if(m_cameraSource) {
        m_cameraSource->toggleCamera();
        std::string mainFilter = "";
        std::string subFilter = "";
        if(_cameraState == CameraHelperCameraStateFront){
            if(_filter == CameraFilterNormal){
                mainFilter = "com.videocore.filters.bgra";
                subFilter = "";
            }else{
                mainFilter = "com.videocore.filters.grayscale";
                subFilter = "com.videocore.filters.invertColors";
            }
        }
        else{
            if(_filter == CameraFilterNormal){
                mainFilter = "com.videocore.filters.bgra2yuva";
                subFilter = "";
            }else{
                mainFilter = "com.videocore.filters.fisheye";
                subFilter = "com.videocore.filters.glow";
            }
        }
        
        if(mainFilter != ""){
             m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(mainFilter)));
        }
        
        if(subFilter != ""){
            m_videoMixer->setTestFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.sepia")),
                                        dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(subFilter)));
        }else{
            m_videoMixer->setTestFilter(std::shared_ptr<videocore::iOS::CameraSource>(nullptr), nil,nil);
        }
    }
}

-(void) enableTourch{
    if(m_cameraSource) {
        _torch = m_cameraSource->setTorch(!_torch);
    }
}

-(bool) enableFilter{

//    m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.grayscale")));
    
    NSString *filterName = @"com.videocore.filters.bgra";
    if(_filter == CameraFilterNormal){
        _filter = CameraFilterCustom;
        filterName =  @"com.videocore.filters.grayscale";
        std::string subFilter = "com.videocore.filters.invertColors";
        
        if(_cameraState == CameraHelperCameraStateBack){
            filterName = @"com.videocore.filters.fisheye";
            subFilter = "com.videocore.filters.glow";
        }
        
        std::string convertString([filterName UTF8String]);
        m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(convertString)));
        m_videoMixer->setTestFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter("com.videocore.filters.sepia")),
                                    dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(subFilter)));
        

    }else{
        _filter = CameraFilterNormal;
        filterName = _cameraState == CameraHelperCameraStateBack  ? @"com.videocore.filters.bgra2yuva" : @"com.videocore.filters.bgra";
        std::string convertString([filterName UTF8String]);
        m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(convertString)));
        m_videoMixer->setTestFilter(std::shared_ptr<videocore::iOS::CameraSource>(nullptr), nil,nil);
        
    }
//    std::string convertString([filterName UTF8String]);
//    m_videoMixer->setSourceFilter(m_cameraSource, dynamic_cast<videocore::IVideoFilter*>(m_videoMixer->filterFactory().filter(convertString)));
    
    return _filter == CameraFilterCustom;
}

-(void) cleanUp{
    //[self endRtmpSession];
    //[self endRtmpSession];
    m_audioMixer.reset();
    m_videoMixer.reset();
    m_videoSplit.reset();
    m_aspectTransform.reset();
    m_positionTransform.reset();
    m_micSource.reset();
    m_cameraSource.reset();
    m_pbOutput.reset();
    [_previewView removeFromSuperview];
    [_previewView release];
    _previewView = nil;
    //[_graphManagementQueue release];
    dispatch_release(_graphManagementQueue);
}

- (void) dealloc
{
    //[self cleanUp];
    [super dealloc];
}
@end
