//
//  SettingHelper.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

protocol SettingsHelperDelegate {
    func getNextShow() -> (model: UserSMModel, image: UIImage?)?
    func getPreviousShow() -> (model: UserSMModel, image: UIImage?)?
}

class SettingsHelper: NSObject {
    static let sharedInstance = SettingsHelper()
    private static var listGiftAll : Array<GiftModel> = []
    private static var _keySecret : String? = nil
    private static var _currentUser: UserModel? = nil
    
    private static var _userId : Int64 = -1
    private static var _listHomeData = FListResultDTO<UserSMModel>()
    private static var _tableHome : UITableView!
    private static var _currentShowIndex = 0
    private static var _reachability : Reachability?
    
    static var notificationData : NotificationDTO?
    var delegate : SettingsHelperDelegate!
    
    
    class func getInternetChecker()-> Reachability?{
        if(_reachability == nil){
            do{
                _reachability = try Reachability.reachabilityForInternetConnection()
             
            } catch{
                return _reachability
            }
        }
        return _reachability
    }
    
    class func isInternet() -> Bool
    {
        if((getInternetChecker()?.isReachable())!)
        {
            return true
        }
        return false
    }
    
    func getNextShow() -> (model: UserSMModel, image: UIImage?)? {
        if self.delegate != nil {
            return self.delegate.getNextShow()
        }
        return nil
    }
    
    func getPreviousShow() -> (model: UserSMModel, image: UIImage?)? {
        if self.delegate != nil {
            return self.delegate.getPreviousShow()
        }
        return nil
    }
    
    class func setCurrentShowIndex(index: Int){
        _currentShowIndex = index
    }
    
    class func getCurrentUserRuby() -> Int{
        if(_currentUser != nil){
            return (_currentUser?.userInfo?.Coin)!
        }
        return 0
    }
    
    class func addCurrentUserRuby(value: Int){
        if(_currentUser != nil){
            _currentUser?.userInfo?.Coin = (_currentUser?.userInfo?.Coin)!  + value
            if(_currentUser?.userInfo?.Coin < 0){
                _currentUser?.userInfo?.Coin = 0
            }
            NSNotificationCenter.defaultCenter().postNotificationName(UserRubyChangeNotificationName, object:nil)
        }
    }
    
    class func getCurrentUserHeart() -> Int{
        if(_currentUser != nil){
            return (_currentUser?.userInfo?.TotalFreeCoin)!
        }
        return 0
    }
    class func addCurrentUserHeart(value: Int){
        if(_currentUser != nil){
            _currentUser?.userInfo?.TotalFreeCoin = (_currentUser?.userInfo?.TotalFreeCoin)!  + value
            if(_currentUser?.userInfo?.TotalFreeCoin < 0){
                _currentUser?.userInfo?.TotalFreeCoin = 0
            }
            NSNotificationCenter.defaultCenter().postNotificationName(UserRubyChangeNotificationName, object:nil)
        }
    }
    
    class func setUserInfo(model: UserModel?){
        if(model?.userInfo != nil && model?.userInfo?.Id > 0){
            self._currentUser  = model
            self.addCurrentUserRuby(0)
        }
    }
    
    class func getUserInfo() -> UserModel?{
        return self._currentUser
    }
    
    class func getUserID() -> Int64 {
       _userId = Int64(NSUserDefaults.standardUserDefaults().integerForKey("currentUserId"))
        if(_userId == 0) {_userId = -1}
        return _userId
        //return NSUserDefaults.standardUserDefaults().integerForKey("CurrentUserId")
    }
    
    class func setUserID(id: Int64) {
        NSUserDefaults.standardUserDefaults().setInteger(Int(id), forKey: "currentUserId")
        NSUserDefaults.standardUserDefaults().synchronize()
        self._userId = id
    }

    class func getListGiftAll(callBack: ((Array<GiftModel>) -> Void)){
        if(listGiftAll.count == 0){
            GiftDAO.List_GetAllGift({ (result) in
                listGiftAll.appendContentsOf(result.ListItem)
                callBack(listGiftAll)
            })
        }else{
            callBack(listGiftAll)
        }
    }
    
    class func setUserTokenKey(key: String){
        NSUserDefaults.standardUserDefaults().setValue(key, forKey: "UserTokenKey")
        NSUserDefaults.standardUserDefaults().synchronize()
        self._keySecret = key
    }
    
    class func getUserTokenKey() -> String {
        self._keySecret = NSUserDefaults.standardUserDefaults().stringForKey("UserTokenKey")
        if(self._keySecret == nil){ self._keySecret = ""}
        return self._keySecret!
    }
    
    class func isLaunched() -> Bool{
        let isLaunched = NSUserDefaults.standardUserDefaults().boolForKey("IsLaunched")
        if(!isLaunched){
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "IsLaunched")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        return isLaunched
    }
    
    class func isInstructionShowed(key: String) -> Bool{
        let isInstructionShowed = NSUserDefaults.standardUserDefaults().boolForKey(key)
        if(!isInstructionShowed){
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: key)
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        return isInstructionShowed
    }
    
    class func signOut(){
        if(_userId > 0){
            setUserID(0)
            setUserTokenKey("")
            _currentUser = nil
            NSNotificationCenter.defaultCenter().postNotificationName(UserRubyChangeNotificationName, object:nil)
        }
    }
    
    class func setDeviceToken(key: String){
        NSUserDefaults.standardUserDefaults().setValue(key, forKey: "DeviceTokenIndentifier")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func getDeviceToken() -> String{
        if let token = NSUserDefaults.standardUserDefaults().stringForKey("DeviceTokenIndentifier"){
            return token
        }
        return ""
    }
    
    class func getNotification(data: [String:String]) -> NotificationDTO?{
        if let objectId = data["objectId"],
            let objectTypeId = data["objectTypeId"],
            let streamServer = data["streamServer"],
            let streamKey = data["streamKey"]{
            let data = NotificationDTO()
            data.objectId = Int64(objectId)!
            data.objectTypeId =  Int(objectTypeId)!
            data.streamServer = streamServer
            data.streamKey = streamKey
            return data
        }
        return nil
    }
    
    class func getNotification(data: JSON) -> NotificationDTO?{
        let objectId = data["objectId"].int64Value
        let objectTypeId = data["objectTypeId"].intValue
        let streamServer = data["streamServer"].stringValue
        let streamKey = data["streamKey"].stringValue
        guard objectId > 0 && objectTypeId > 0 && streamServer.isEmpty == false &&
            streamKey.isEmpty == false else {
            return nil
        }
        let data = NotificationDTO()
        data.objectId = objectId
        data.objectTypeId =  objectTypeId
        data.streamServer = streamServer
        data.streamKey = streamKey
        return data
    }
}
