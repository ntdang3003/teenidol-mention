//
//  AppDelegate.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/15/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import Fabric
import Crashlytics

extension NSURL {
    var queryItems: [String: String]? {
        var params = [String: String]()
        return NSURLComponents(URL: self, resolvingAgainstBaseURL: false)?
            .queryItems?
            .reduce([:], combine: { (_, item) -> [String: String] in
                params[item.name] = item.value
                return params
            })
    }
}


let kApplicationVersionNumber = "1.0"
let kApplicationVersionNumberKey = "TeenIdol-874089163"
let kConversionAppUpdateID = "874089163"
let kConversionAppUpdateLabel = "LiveIdol"
let kConversionAppUpdateValue = "0.99"

func versionNumberArrayFromVersionNumberString(string: String) -> Array<Any>?{
    let stringComponents = string.componentsSeparatedByString(".")
    var numberComponents = [Any]()
    for component: String in stringComponents {
        let scanner = NSScanner(string: component)
        let integerValue = UnsafeMutablePointer<Int32>.alloc(0)
        //let integerValue : NSInteger = 0
        if !scanner.scanInt(integerValue) || !scanner.atEnd {
            break
        }
        let value = integerValue.memory
        if value == 0 {
            break
        }
        numberComponents.append(value)
    }
    if numberComponents.count == stringComponents.count {
        return numberComponents
    }
    return nil
}

func versionIsNewer(previousVersionNumberArray: Array<Any>, currentVersionNumberArray: Array<Any>) -> Bool{
    let previousCount = previousVersionNumberArray.count
    let currentCount = currentVersionNumberArray.count
    let minCount = min(previousCount, currentCount)
    for i in 0..<minCount {
        let previousValue = previousVersionNumberArray[i] as! Int32
        let currentValue = currentVersionNumberArray[i] as! Int32
        
        if previousValue < currentValue {
            return true
        }
        if previousValue > currentValue {
            return false
        }
    }
    if currentCount > previousCount {
        return true
    }
    return false
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootViewController : UIViewController? {
        return window?.rootViewController
    }
    
    func reportConversionIfUpdated() {
        var previousVersionNumber = ""
        var previousVersionNumberArray : Array<Any>!
        var currentVersionNumberArray : Array<Any>!
        
        if let _ = NSFileManager.defaultManager().ubiquityIdentityToken {
            let storage = NSUbiquitousKeyValueStore.defaultStore()
            previousVersionNumber = storage.stringForKey(kApplicationVersionNumberKey) ?? ""
            previousVersionNumberArray = versionNumberArrayFromVersionNumberString(previousVersionNumber) ?? Array<Any>()
            currentVersionNumberArray = versionNumberArrayFromVersionNumberString(kApplicationVersionNumber) ?? Array<Any>()
            if versionIsNewer(previousVersionNumberArray, currentVersionNumberArray: currentVersionNumberArray) {
                // Store the updated version number.
                storage.setObject(kApplicationVersionNumber, forKey: kApplicationVersionNumberKey)
                // Report the upgrade conversion.
                ACTConversionReporter.reportWithConversionID(kConversionAppUpdateID, label: kConversionAppUpdateLabel, value: kConversionAppUpdateValue, isRepeatable: true)
            }

        }else{
            let storage = NSUserDefaults.standardUserDefaults()
            previousVersionNumber = storage.stringForKey(kApplicationVersionNumberKey) ?? ""
            previousVersionNumberArray = versionNumberArrayFromVersionNumberString(previousVersionNumber) ?? Array<Any>()
            currentVersionNumberArray = versionNumberArrayFromVersionNumberString(kApplicationVersionNumber) ?? Array<Any>()
            if versionIsNewer(previousVersionNumberArray, currentVersionNumberArray: currentVersionNumberArray) {
                // Store the updated version number.
               storage.setObject(kApplicationVersionNumber, forKey: kApplicationVersionNumberKey)
                // Report the upgrade conversion.
                 ACTConversionReporter.reportWithConversionID(kConversionAppUpdateID, label: kConversionAppUpdateLabel, value: kConversionAppUpdateValue, isRepeatable: true)
            }
        }
    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        FBSDKAppLinkUtility.fetchDeferredAppLink { (url, error) in
            if(url != nil && error == nil){
               application.openURL(url)
            }
        }
        Fabric.with([Crashlytics.self])
        let navi = UINavigationBar.appearance()
        navi.barTintColor = ColorAppDefault
        navi.translucent = false
        navi.tintColor = UIColor.whiteColor()

        ACTAutomatedUsageTracker.enableAutomatedUsageReportingWithConversionID("874089163")
        ACTConversionReporter.reportWithConversionID("874089163", label: "TeenIdol", value: "0.99", isRepeatable: false)
        
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]
        let pushNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
        application.applicationIconBadgeNumber = 0
        
        let gai = GAI.sharedInstance()
        gai.defaultTracker = gai.trackerWithTrackingId("UA-70744448-2")
        gai.trackUncaughtExceptions = true
        gai.logger.logLevel = GAILogLevel.None
       
        self.window = UIWindow(frame:UIScreen.mainScreen().bounds)
      
        
        let userId = SettingsHelper.getUserID()
        if(userId > 0){ //Logged in
            UserDAO.User_GetDetailsV2(UserId: userId, callback: { (result) in
                if(result.ErrorCode == 0 && result.DataObject != nil){
                    SettingsHelper.setUserInfo(result.DataObject as? UserModel)
                }
            })
            showHomeViewController()
        }else{
            showSignInViewController()
        }
       
        SwiftyStoreKit.completeTransactions() { completedTransactions in
            for completedTransaction in completedTransactions {
                if completedTransaction.transactionState == .Purchased || completedTransaction.transactionState == .Restored {
                }
            }
        }
        if let notification = (launchOptions as? [String: AnyObject])?[UIApplicationLaunchOptionsRemoteNotificationKey] as? [String: AnyObject] {
            if let noticationDTO = SettingsHelper.getNotification(JSON(notification)){
                SettingsHelper.notificationData = noticationDTO
                NSNotificationCenter.defaultCenter().postNotificationName("OpenStreamFromNotification", object: self)
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(applicationLogin), name: UserDidLoginNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(applicationLogout), name: UserDidLogoutNotification, object: nil)
        
        self.reportConversionIfUpdated()
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
          FBSDKAppEvents.activateApp()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let token = FunctionHelper.getDeviceTokenFromData(deviceToken)
        SettingsHelper.setDeviceToken(token)
        UserDAO.User_InsertDeviceToken(token)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        let applicationState = UIApplication.sharedApplication().applicationState
        if(applicationState == .Active){
            
        }
        else if (applicationState == .Inactive){
            if let noticationDTO = SettingsHelper.getNotification(JSON(userInfo)){
                 SettingsHelper.notificationData = noticationDTO
                if var topController = UIApplication.sharedApplication().keyWindow?.rootViewController {
                    while let presentedViewController = topController.presentedViewController {
                        topController = presentedViewController
                    }
                    
                    if topController is UITabBarController{
                        NSNotificationCenter.defaultCenter().postNotificationName("OpenStreamFromNotification", object: self)
                    }
                    if let navigate = topController as? UINavigationController{
                        navigate.dismissViewControllerAnimated(true, completion: {
                            NSNotificationCenter.defaultCenter().postNotificationName("OpenStreamFromNotification", object: self)
                        })
                    }
                }
            }
        }
    }

    func applicationWillTerminate(application: UIApplication) {
    }
   
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        //Facebook Redirect Handler
        var _queryItems : [String:String]? = nil
        
        let _BFURL = BFURL(inboundURL: url, sourceApplication: sourceApplication)
        if _BFURL.appLinkData != nil{
            _queryItems = _BFURL.targetURL.queryItems
        }
        
        if((url.absoluteString!.rangeOfString("liveidoliosscheme")) != nil){
            _queryItems = url.queryItems
        }
        
        if(_queryItems != nil){
            if let noticationDTO = SettingsHelper.getNotification(JSON(_queryItems!)){
                SettingsHelper.notificationData = noticationDTO
                NSNotificationCenter.defaultCenter().postNotificationName("OpenStreamFromNotification", object: self)
            }
            return true
        }
        
        else{
            let handle = FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
            return handle
        }
    }
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        if (userActivity.activityType == NSUserActivityTypeBrowsingWeb){
            ACTConversionReporter.reportUniversalLinkWithUserActivity(userActivity)
            return true
        }
        return false
    }
}

extension AppDelegate { //View Controllers Handler
    func showSignInViewController(){
        let bounds = UIScreen.mainScreen().bounds
        let signIn = UIViewController()
        signIn.view = SignInView(frame: bounds, rootViewController: signIn,callback: nil)
        let navi = UINavigationController(rootViewController: signIn)
        navi.navigationBar.hidden = true
        UIView.animateWithDuration(0.5) {
            self.window?.rootViewController = navi
            self.window?.makeKeyAndVisible()
        }
    }
    
    func showHomeViewController(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewControllerWithIdentifier("HomeNavigation")
        UIView.animateWithDuration(0.5) { 
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
    }
}

extension AppDelegate { //User Session Handler
    func applicationLogout(){
        showSignInViewController()
    }
    
    func applicationLogin(){
        showHomeViewController()
    }
}
