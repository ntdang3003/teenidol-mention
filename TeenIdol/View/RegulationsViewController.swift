//
//  RegulationsViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/3/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class RegulationsViewController: UIViewController {

    @IBOutlet weak var btTabPrivacy: UIButton!;
    @IBOutlet weak var btTabAgreement: UIButton!;
    @IBOutlet weak var btOK: UIButton!;
    @IBOutlet weak var btAgree: UICheckbox!;
    
    @IBOutlet weak var txtContent: UITextView!;
    @IBOutlet weak var conViewLineToLeft : NSLayoutConstraint!
    
    var strContentPrivacy = "";
    var strContentAgreement = "";
    var currentIndexSelected = 0
    override func viewDidLoad()
    {
        super.viewDidLoad()

        setupUI()
        setupNavigationBar()
        //setupLeftMenuButton()
        
        loadData()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupUI()
    {
        btOK.enabled = false
    }
    
    func setupNavigationBar(){
        
        let title = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
        title.text = "Điều khoản LiveIdol"
        title.textColor = .whiteColor()
        
        self.navigationItem.titleView = title;
        
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true);
    }
    
    func loadData()
    {
        JTProgressHUD.show()
        do
        {
            let arrURL = ["https://teenidol.vn/File/Agreement.txt","https://teenidol.vn/File/Privacy.txt"]
            for i in 0..<arrURL.count{
                if let URL = NSURL(string: arrURL[i]){
                    var data = NSData(contentsOfURL: URL)
                    if(data == nil) { data = NSData()}
                    if(i == 0){
                        strContentAgreement = String(data: data!, encoding: NSUTF8StringEncoding)!
                    }else{
                        strContentPrivacy = String(data: data!, encoding: NSUTF8StringEncoding)!
                    }
                }
            }
        }
        JTProgressHUD.hide()
        txtContent.text = strContentPrivacy
    }
    
    @IBAction func touchHeader(sender: UIButton){
        let index = sender.tag
        if(self.currentIndexSelected == index) {return}
        self.currentIndexSelected = index
        txtContent.text = index == 0 ? strContentPrivacy : strContentAgreement
        
        self.resetButtonColor()
        self.conViewLineToLeft.constant = sender.frame.origin.x
        UIView.animateWithDuration(0.35) {
            self.view.layoutIfNeeded()
        }
        sender.setTitleColor(ColorAppDefault, forState: .Normal)
    }
    
    func resetButtonColor(){
        self.btTabPrivacy.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        self.btTabAgreement.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
    }
    
    @IBAction func btAgree(sender: AnyObject)
    {
        if(btAgree.isCheck)
        {
            btOK.enabled = true
        }
        else
        {
            btOK.enabled = false
        }
    }
    
    @IBAction func btOk(sender: AnyObject)
    {
        let vc = SignUpStreamerViewController(nibName:  "SignUpStreamerViewController", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btCancel(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
