//
//  HomeViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/4/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import StoreKit

class BannerCollectionViewCell : UICollectionViewCell {
    var imgBanner : UIImageView? = nil
    var urlLink: String? = nil;
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        if(self.imgBanner == nil){
            self.imgBanner = UIImageView(frame:  CGRect(x: 0,y:0,width: frame.size.width, height: 80))
            self.imgBanner?.contentMode = .ScaleToFill
            self.addSubview(self.imgBanner!)
        }
        self.contentView.backgroundColor =  UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
    }
    
    func setData(imgBanner: String, urlLink: String)
    {
        if let imageURL = NSURL(string: imgBanner.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!){
             self.imgBanner?.sd_setImageWithURL(imageURL, placeholderImage: UIImage(named: "bg_banner"))
        }
       
        self.urlLink = urlLink;
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class HomeViewController:  UITableViewController,
MediaStreamerProtocol, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate {
    
    var _data  = FListResultDTO<UserSMModel>()
    var _indicator: UIActivityIndicatorView?  = nil
    var _colBanner : UICollectionView?
    
    private var _internetChecker : Reachability?
    private var _connectTimer: NSTimer?;
    internal var selectedIndex = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        if(!SettingsHelper.isInternet())
        {
            _connectTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(reloadData), userInfo: nil, repeats: true)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(loadDataFromSettingHelperNotification), name: HomeNeedLoadMoreDataNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(checkNotificationData), name: "OpenStreamFromNotification", object: nil)
        
        _internetChecker = SettingsHelper.getInternetChecker()
        SettingsHelper.sharedInstance.delegate = self
        
        self.tableView.registerNib(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeCellCasting")
        self.tableView.registerNib(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeCellBeforeCast")
        self.tableView.estimatedRowHeight = 376
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.separatorStyle = .None
        self.tableView.showsVerticalScrollIndicator = false
        self.setupTabBar()
        self.setupNavigationBar()
        self.setupLeftMenuButton()
        self.setupRightMenuButton()
        self.setupPullToRefresh()
        
        self.loadData()
    }
    
    deinit{
        self.tableView.dg_removePullToRefresh()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
  
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.hidesBarsOnSwipe = true
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
         navigationController!.hidesBarsOnSwipe = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        do {
            try _internetChecker?.startNotifier()
        }catch{
            FunctionHelper.showBannerAlertView(content: "Start Notifier failed")
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        _internetChecker?.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: nil)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func checkNotificationData(){
       
        if  let data = SettingsHelper.notificationData{
            self.presentMediaStreamer(data.objectId, streamServer: data.streamServer, streamKey: data.streamKey, image: nil, status: ScheduleStatus.Casting)
            SettingsHelper.notificationData = nil
        }
    }
    
    func reloadData()
    {
        _connectTimer?.invalidate()
        
        if( SettingsHelper.isInternet() )
        {
            self._data = FListResultDTO<UserSMModel>()
            loadData()
        }
    }
    
    func  alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0) {
            if let url = NSURL(string: "itms-apps://itunes.apple.com/app/id1063020270"){
                if UIApplication.sharedApplication().canOpenURL(url) == true  {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
        }
    }
    
    func reachabilityChanged(){
        if(_internetChecker?.isReachable() ==  false){
            FunctionHelper.showBannerAlertView(content: "Không có kết nối Internet")
        }else{
            self.checkNotificationData()
        }
    }
    
    func loadDataFromSettingHelperNotification(){
        self.loadData()
    }
    
    override func loadView() {
        super.loadView()
//        _tbMain?.frame = self.view.bounds
//        _tbMain?.frame.origin = CGPoint(x: 0, y: 90)
//        _tbMain?.view.separatorStyle = .None
//        _tbMain?.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
    }
    
    //MARK: Setup
    func setupBanner(){
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        layout.itemSize = CGSize(width: self.view.frame.size.width, height: 90)
        self._colBanner = UICollectionView(frame: CGRect(x: 0,y: 0,width: self.view.frame.size.width,height: 90), collectionViewLayout: layout)
        self._colBanner?.dataSource = self
        self._colBanner?.delegate = self
        self._colBanner?.registerClass(BannerCollectionViewCell.self, forCellWithReuseIdentifier: "BannerCellIndentifier")
        self._colBanner?.backgroundColor = UIColor.whiteColor()
        self.tableView.tableHeaderView = self._colBanner!
    }
    
    func setupPullToRefresh(){
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor =  (self.tableView.backgroundColor!)
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self?._data = FListResultDTO<UserSMModel>()
            self?.loadData()
            }, loadingView: loadingView)
       self.tableView.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
        
        //Setup Loading More
        _indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        self.tableView.tableFooterView = _indicator
        var frame = self.tableView.tableFooterView?.frame
        frame?.size.height = 60;
        self.tableView.tableFooterView?.frame = frame!;
        _indicator?.startAnimating()
    }
    
    func setupNavigationBar(){
        let img: UIImage = UIImage(named: "ic_logo_home.png")!
        let imgView: UIImageView = UIImageView(frame: CGRectMake(0, 0, 150, 32))
        imgView.image = img
        imgView.contentMode = .ScaleAspectFit
        imgView.backgroundColor = UIColor.clearColor()
        self.navigationItem.titleView = imgView
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
        //navigationBar?.tintColor = UIColor.whiteColor()
        navigationBar?.barTintColor = ColorAppDefault
        navigationBar?.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    func setupTabBar(){
        let tabbar = self.tabBarController?.tabBar
        tabbar?.backgroundImage = UIImage(named:"bg_tabbar")
        tabbar?.shadowImage = UIImage()
    }
    
    func setupLeftMenuButton(){
        let btn = FunctionHelper.getNavigationBarButton("ic_search")
        btn.addTarget(self, action: #selector(touchedLeft), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem(UIBarButtonItem(customView: btn), animated: true)
        
    }
    
    func setupRightMenuButton()
    {
        let btn = FunctionHelper.getNavigationBarButton("ic_tabbar_profile_white")
        btn.addTarget(self, action: #selector(touchedRight), forControlEvents: .TouchUpInside)
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(customView: btn), animated: true)
    }
    
    func touchedLeft(sender: AnyObject){
        
        let con = SearchView(nibName: "SearchView", bundle: nil)
        con.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(con, animated: true);
        //self.presentViewController(con, animated: true,completion: nil)
    }
    
    func touchedRight(sender: AnyObject)
    {
        self.checkLogin { (loginState) in
            if(loginState == .Logged || loginState == .Successed){
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = storyboard.instantiateViewControllerWithIdentifier("ProfileViewController") as? ProfileViewController{
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    
    func loadData(){
        if(self._data.isLoadingData) {return}
        self._data.isLoadingData = true
        let _weakSelf = self;
        
        UserDAO.User_GetListUserOnlineV2(_weakSelf._data.iPageIndex, pageSize: 16) { (result) in
            _weakSelf._data.CountAllResult = result.CountAllResult
            _weakSelf._data.ListItem.appendContentsOf(result.ListItem)
            
            if(_weakSelf._data.iPageIndex == 0)
            {
                _weakSelf.tableView.reloadData()
            }else{
                var arrIndex = Array<NSIndexPath>()
                let currentRow = _weakSelf._data.ListItem.count - result.ListItem.count
                if(result.ListItem.count > 0){
                    for index in 0...result.ListItem.count - 1{
                        arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                    }
                    _weakSelf.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
                }
            }
            
            
            _weakSelf._data.iPageIndex = _weakSelf._data.iPageIndex + 1
            _weakSelf._data.isLoadingData = false
            _weakSelf.tableView.dg_stopLoading()
            
            if(_weakSelf._data.ListItem.count != _weakSelf._data.CountAllResult){
                _weakSelf.tableView.tableFooterView = _weakSelf._indicator!
                _weakSelf._indicator?.startAnimating()
                _weakSelf._indicator!.hidden = false
            }else{
                _weakSelf._indicator!.stopAnimating()
                _weakSelf._indicator!.hidden = true
            }
            //TODO: Lam lại
            //SettingsHelper.setListHomeData(&_weakSelf._data,table: &_weakSelf.tableView)
        }
    }
    
    //MARK: Collection Banner Delegate + DataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("BannerCellIndentifier", forIndexPath: indexPath) as! BannerCollectionViewCell
        return cell
    }
  
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! BannerCollectionViewCell
        FunctionHelper.openURL(cell.urlLink ?? "https://teenidol.vn")
    }
    
    
    //MARK: ASTableNode DataSource & Delegate
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _data.ListItem.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if self._data.CountAllResult != self._data.ListItem.count && self._data.ListItem.count - 1 == indexPath.row {
            self.loadData()
        }
        let model = self._data.ListItem[indexPath.row]
        let reuseId = model.schedule?.Status == .Casting ? "HomeCellCasting" : "HomeCellBeforeCast"
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseId, forIndexPath: indexPath) as! HomeTableViewCell
        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let newCell = cell as! HomeTableViewCell
        newCell.setData(self._data.ListItem[indexPath.row])
    }
    
//    func tableView(tableView: ASTableView, nodeBlockForRowAtIndexPath indexPath: NSIndexPath) -> ASCellNodeBlock{
//        let model = self._data.ListItem[indexPath.row]
//        let temp = {() in return StarInfoCellNode(model: model)}
//        //let temp = {() in return ASCellNode()}
//        return temp;
//    }
    
//    func tableView(tableView: ASTableView, willBeginBatchFetchWithContext context: ASBatchContext) {
//        //if(self._data.CountAllResult == self._data.ListItem.count){return}
//        context.beginBatchFetching()
//        self.loadData(context)
//    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? HomeTableViewCell {
            let model = self._data.ListItem[indexPath.row]
            self.selectedIndex = indexPath.row
            self.presentMediaStreamer(Int64((model.ShowInfo?.Id)!), streamServer: (model.ShowInfo?.ServerIP)!, streamKey: (model.ShowInfo?.StreamKey)!, image: cell.imageMain.image,
                                      status: (model.schedule?.Status)!)
        }
    }
    
    //MARK: Media Streamer Delegate
    func mediaStreamer_onClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func presentMediaStreamer(showId: Int64, streamServer: String?, streamKey : String?, image : UIImage?,
                              status: ScheduleStatus){
        let _weakSelf = self
        dispatch_async(dispatch_get_main_queue(), {
            if let _weakInstace = MediaStreamerViewController.shareInstance(){
                if(image != nil){
                    _weakInstace._imgBackground = image
                }
                _weakInstace.isStreamer = false
                _weakInstace.setShowInfo(showId, urlStreamer: streamServer, streamKey: streamKey)
                _weakInstace.delegate = _weakSelf
                
                let navigate = UINavigationController(rootViewController: _weakInstace)
                navigate.navigationBar.tintColor = UIColor.whiteColor()
                navigate.navigationBar.translucent = false
                navigate.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
                navigate.toolbarHidden = true
                _weakSelf.presentViewController(navigate, animated: true, completion: nil)

            }
        })
    }
    
}

extension HomeViewController : SettingsHelperDelegate { //Next, Previous show function
    func getNextShow() -> (model: UserSMModel, image: UIImage?)? {
        var index = selectedIndex + 1
        if index == _data.ListItem.count - 1 { //Last Row
            index = 0
            self.loadData()
        }
        selectedIndex = index
        return (model: self._data.ListItem[selectedIndex], image: nil)
        
//        if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? HomeTableViewCell{
//            if index == self._data.ListItem.count - 1 { //Fletch To Last Cell
//                self.loadData(nil)
//            }
//            selectedIndex = index
//            return (model: self._data.ListItem[index], image: cell.imageMain.image)
//        }
//        index = 0
//        if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? HomeTableViewCell{
//            selectedIndex = index
//            return (model: self._data.ListItem[index], image: cell.imageMain.image)
//        }
//        return nil
    }
    
    func getPreviousShow() -> (model: UserSMModel, image: UIImage?)? {
        var index = selectedIndex - 1
        if index == -1 { //Last Row
            index = _data.ListItem.count == 0 ? 0 : _data.ListItem.count - 1
            self.loadData()
        }
        selectedIndex = index
        return (model: self._data.ListItem[selectedIndex], image: nil)
//        var index = selectedIndex - 1
//        if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? HomeTableViewCell{
//            selectedIndex = index
//            return (model: self._data.ListItem[index], image: cell.imageMain.image)
//        }
//        index = self._data.ListItem.count - 1
//        if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? HomeTableViewCell{
//            selectedIndex = index
//            return (model: self._data.ListItem[index], image: cell.imageMain.image)
//        }
//        return nil
    }
}
