//
//  popupAccept.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/12/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol popupEventProtocol
{
    func accept_close()
    func accept_mission(level: Int)
    
    func detail_close()
    func detail_cancel()
    func detail_rewardInfo()
    
    func reward_get()
    func reward_close()
}

class popupAccept: UIViewController {

    @IBOutlet weak var lbCountDay: UILabel?;
    
    @IBOutlet weak var btLevel1: UIButton?;
    @IBOutlet weak var btLevel2: UIButton?;
    @IBOutlet weak var btLevel3: UIButton?;
    @IBOutlet weak var btLevel4: UIButton?;
    @IBOutlet weak var btLevel5: UIButton?;
    
    @IBOutlet weak var btAccept: UIButton?;
    
    var btLevels: [UIButton] = [];
    var delegate: popupEventProtocol? = nil;
    var starId: Int64 = 0;
    var data: IdolQuestStateDTO?;
    var level: Int = 0;
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        btLevel1?.addTarget(self, action: #selector(btLevelTouch), forControlEvents: .TouchUpInside);
        btLevel2?.addTarget(self, action: #selector(btLevelTouch), forControlEvents: .TouchUpInside);
        btLevel3?.addTarget(self, action: #selector(btLevelTouch), forControlEvents: .TouchUpInside);
        btLevel4?.addTarget(self, action: #selector(btLevelTouch), forControlEvents: .TouchUpInside);
        btLevel5?.addTarget(self, action: #selector(btLevelTouch), forControlEvents: .TouchUpInside);
        btLevel1?.tag = 1; btLevel2?.tag = 2; btLevel3?.tag = 3; btLevel4?.tag = 4; btLevel5?.tag = 5;
        
        btLevels = [btLevel1!, btLevel2!, btLevel3!, btLevel4!, btLevel5!];
        
        // level default
        /*
        btLevels[0].setImage(UIImage(named: "ic_event_mission_unit_p"), forState: .Normal)
        level = 1
         */
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //loadData()
        updateUI(data!);
    }
    
    func updateUI(data: IdolQuestStateDTO)
    {
        //self.lbCountDay?.text = "Số lần nhận hôm nay: \(data.countQuestInDay)/\(data.maxQuestInDay)"
        //self.btAccept?.enabled = data.isCanGetNewQuest!;
    }
    
    func btLevelTouch(sender: AnyObject)
    {
        for i in 0..<sender.tag
        {
            btLevels[i].setImage(UIImage(named: "ic_event_mission_unit_p"), forState: .Normal);
        }
        
        for i in sender.tag..<5
        {
            btLevels[i].setImage(UIImage(named: "ic_event_mission_unit"), forState: .Normal);
        }
        
        level = sender.tag
    }
    
    @IBAction func btAccept(sender: AnyObject)
    {
        if(delegate != nil)
        {
            if(level == 0)
            {
                FunctionHelper.showBannerAlertView(content: "Hãy chọn Cấp độ, và thử lại !")
            }
            else
            {
                delegate?.accept_mission(level)
            }
        }
    }
    
    @IBAction func btCancel(sender: AnyObject)
    {
        if(delegate != nil)
        {
            delegate?.accept_close()
        }
    }
    
}
