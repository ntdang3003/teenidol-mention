//
//  FollowViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/10/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class FollowViewController: ButtonBarPagerTabStripViewController, MediaStreamerProtocol, UIAlertViewDelegate
    //UIViewController,UITableViewDataSource,UITableViewDelegate, MediaStreamerProtocol, UIAlertViewDelegate, NavigationCustomViewDelegate
{
    
    @IBOutlet weak var tbFollow: UITableView!;
    
    @IBOutlet weak var btFollowed: UIButton!;
    @IBOutlet weak var btFanMe: UIButton!;
    @IBOutlet weak var viewMain:UIView!;
    @IBOutlet weak var conViewLineToLeft: NSLayoutConstraint!;
    
    var delegateFollow: FollowTableViewProtocol? = nil;
    var parentVC: UIViewController? = nil
    var _currentUserId:Int64 = -1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupNavigationBar()
        setupLeftMenuButton()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
         //self.navigationController?.navigationBarHidden = false
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
        
        /*
         let customView  = NavigationCustomView(arrTitle: ["Theo dõi", "Fan"])
         customView.delegate = self
         self.navigationItem.titleView = customView
         */
        
        buttonBarView.removeFromSuperview()
        buttonBarView.frame = CGRectMake(0, 0, 200, 30)
        
        let selBar = buttonBarView.selectedBar.frame;
        buttonBarView.selectedBar.frame = CGRectMake(selBar.origin.x, 28, selBar.width, selBar.height);
        
        self.navigationItem.titleView = buttonBarView
        
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(leftMenuButton), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let btn: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func leftMenuButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupUI()
    {
        settings.style.buttonBarBackgroundColor = .clearColor()
        settings.style.buttonBarItemBackgroundColor = .clearColor()
        settings.style.selectedBarBackgroundColor = .whiteColor()
        settings.style.buttonBarItemFont = UIFont.boldSystemFontOfSize(16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .blackColor()
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 10
        settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
            newCell?.label.textColor = .whiteColor()
        }
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        let child_1 = FollowTableView(style: .Plain, itemInfo: "Theo dõi", type: User_GetListType.UserFollow)
//        child_1.parentVC = self
//        let child_2 = FollowTableView(style: .Plain, itemInfo: "Fan", type: User_GetListType.UserFan)
//        child_2.parentVC = self
        
        let child_1 = FollowTableView(nibName:nil, bundle:nil, itemInfo: "Theo dõi", type: User_GetListType.UserFollow)
        child_1.parentVC = self
        let child_2 = FollowTableView(nibName:nil, bundle:nil, itemInfo: "Fan", type: User_GetListType.UserFan)
        child_2.parentVC = self
        
        return [child_1, child_2]
    }
    
    func mediaStreamer_onClose() {
        self.dismissViewControllerAnimated(true, completion: nil);
    }
}

