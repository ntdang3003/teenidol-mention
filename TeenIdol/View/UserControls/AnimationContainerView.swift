//
//  AnimationContainerView.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

enum AnimationType : Int {
    case Small = 1
    case Large = 2
}

extension UIEdgeInsets {
    var inverse: UIEdgeInsets {
        return UIEdgeInsets(top: -top, left: -(left), bottom: -bottom, right: -right)
    }
    func apply(rect: CGRect) -> CGRect {
        //let insets = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        return UIEdgeInsetsInsetRect(rect, self)
    }
}

class BubbleChatLabel : UILabel{
    var textInsets: UIEdgeInsets = UIEdgeInsets.init(top: 3, left: 10, bottom:3, right: 3)
    
    override func textRectForBounds(bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var rect = textInsets.apply(bounds)
        rect = super.textRectForBounds(rect, limitedToNumberOfLines: numberOfLines)
        return textInsets.inverse.apply(rect)
    }
    
    override func drawTextInRect(rect: CGRect) {
        super.drawTextInRect(textInsets.apply(rect))
    }
}

class BubbleMessageChatView : UIView{
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblContent: BubbleChatLabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

class AnimationContainerView: UIView {
    var animationMaxSize  = CGSize(width: 20,height: 10)
//    {
//        didSet{
//            self._inventoryAnimationStartPoint = CGPoint(x: 10, y: self.animationMaxSize.height / 1.5)
//        }
//    }
    var _queueBubbleChat = Array<SignalRUserSendMessagePopupDTO>()
    var _countMessageShowed = 0
    var _lineFree : CGFloat = 1.0
    var _maxMessageShow = 2
    let _bubbleChatPadding : CGFloat = 8.0
    
    //var _queueGiftAnimation : [(image: YYImage, size: CGSize, duration: Double)] = []
    //var _countAnimationShowed = 0
    //var _maxAnimationShow = 6
    
    //private var _inventoryAnimationStartPoint = CGPoint(x: 0, y: 0)
    //private var _queueInventoryAnimation : [(image: YYImage, size: CGSize, duration: Double)] = []
    //private var _countInventoryShowed = 0
    
    private var _queueLargeAnimation : [(image: YYImage, duration: Double)] = []
    private var _queueSmallAnimation : [(image: YYImage, duration: Double)] = []
    private var _countSmallAnimationShowing = 0
    private var _countLargeAnimationShowing = 0
    private var _maxSmallAnimationShow = 7
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("AnimationContainerView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        
    }
    
//    func getRandomNumber(from : CGFloat, to: CGFloat) -> CGFloat{
//        return (from + CGFloat(arc4random()) % (to-from+1))
//    }
//    
//    private func runSyncWithMainThread(callback: ()->Void){
//        if(NSThread.currentThread().isMainThread){
//            callback()
//        }else{
//            dispatch_async(dispatch_get_main_queue(), {
//                callback()
//            })
//        }
//    }
    
    func addBubbleChat(data: SignalRUserSendMessagePopupDTO){
        if(_countMessageShowed == _maxMessageShow){
            _queueBubbleChat.insert(data, atIndex: 0)
        }else{
            self.showChatBubbleMessage(data)
        }
    }
    
    private func nextBubbleMessage(){
        if(_queueBubbleChat.count > 0){
            let lastIndex = _queueBubbleChat.count - 1
            let temp = _queueBubbleChat[lastIndex];
            self.showChatBubbleMessage(temp)
            _queueBubbleChat.removeAtIndex(lastIndex)
        }
    }
    
    private func getLine() -> CGFloat{
        if(Int(_lineFree) == _maxMessageShow){
            _lineFree = 1
        }
        else{
            _lineFree = _lineFree + 1
        }
        return _lineFree
    }
    
    private func showChatBubbleMessage(data: SignalRUserSendMessagePopupDTO){
        if(self.hidden == true) {return}
        
        //Khởi tạo view
        let viewBubble = NSBundle.mainBundle().loadNibNamed("AnimationContainerView", owner: self, options: nil)![1] as! BubbleMessageChatView
        var x1 = self.bounds.size.width
        //let y1 = viewBubble.bounds.size.height + (40 * self.getRandomNumber(1.0 , to: 3.0))
        //let y1 = viewBubble.bounds.size.height + CGFloat(40 * self.getLine())
        let y1 = self.frame.size.height - ((viewBubble.frame.size.height + self._bubbleChatPadding) * self.getLine())
        
        let startPoint = CGPoint(x: x1,y: y1)
        viewBubble.frame.origin = startPoint
        viewBubble.frame.size = CGSize(width: self.bounds.size.width, height: 20)
        viewBubble.imgAvatar.sd_setImageWithURL(NSURL(string:(data.userInfo?.avatar)!), placeholderImage: UIImage(named:"ic_default_avatar"))
        FunctionHelper.circleBorderImageView(viewBubble.imgAvatar)
        FunctionHelper.runSyncWithMainThread {
            self._countMessageShowed = self._countMessageShowed + 1
            self.addSubview(viewBubble)
            viewBubble.lblUserName.attributedText = data.actionAttributeText
            viewBubble.lblContent.attributedText = data.messageAttributeText
            viewBubble.lblContent.sizeToFit()
            viewBubble.lblContent.layer.borderWidth = 1
            viewBubble.lblContent.layer.borderColor =  UIColor(white:1.0, alpha: 0.85).CGColor
            viewBubble.lblContent.layer.cornerRadius = 10
            viewBubble.lblContent.layer.masksToBounds = true
            viewBubble.lblContent.layer.backgroundColor = UIColor(white:1.0, alpha: 0.8).CGColor
            viewBubble.layoutSubviews()
            x1 = -(viewBubble.lblContent.bounds.size.width + 42)
            
            let endPoint = CGPoint(x: x1, y: y1)
            UIView.animateWithDuration(10, delay: 0, options: .CurveLinear, animations: {
                viewBubble.frame.origin = endPoint
                }, completion: { (isFinished) in
                    self._countMessageShowed = self._countMessageShowed - 1
                    viewBubble.removeFromSuperview()
                    self.nextBubbleMessage()
            })
        }
    }
    
    func showAnimationImage(imgSource: YYImage, duration: Double, animationType: AnimationType = .Small){
        let _imgSize = imgSource.size
        let _imgView = YYAnimatedImageView()
        _imgView.contentMode = .ScaleToFill
        _imgView.image = imgSource
        //if(_imgSize.width >= self.animationMaxSize.width / 2){ //Large Image
        if animationType == .Large{
            if(self._countLargeAnimationShowing > 0){
                _queueLargeAnimation.insert((image: imgSource, duration: duration), atIndex: 0)
                return
            }
            
            let _width = imgSource.size.width > self.animationMaxSize.width ? self.animationMaxSize.width : imgSource.size.width
            let _height = _imgSize.height * (_width / _imgSize.width)
            let position = CGPoint(x : _width > self.animationMaxSize.width ? 0 :
                (self.animationMaxSize.width - _width) / 2,
                                   y: _height > self.animationMaxSize.height
                                    ? 0 : self.animationMaxSize.height - _height)
            //_imgView.frame = CGRect(origin: position, size: CGSize(width: _width, height: _height))
            
            FunctionHelper.runSyncWithMainThread({
                _imgView.frame = CGRect(origin: position, size: CGSize(width: _width, height: _height))
                self._countLargeAnimationShowing += 1
                self.addSubview(_imgView)
                self.performSelector(#selector(self.removeLargeImageView), withObject: _imgView, afterDelay: duration)
            })
        }else{ //Small Image
            if(self._countSmallAnimationShowing >= self._maxSmallAnimationShow){
                _queueSmallAnimation.insert((image: imgSource, duration: duration), atIndex: 0)
                return
            }
           
            FunctionHelper.runSyncWithMainThread({
                _imgView.frame.size = imgSource.size
                _imgView.frame.origin.x = FunctionHelper.getRandomNumber(20, to: (self.animationMaxSize.width - imgSource.size.width - 20))
                _imgView.frame.origin.y = FunctionHelper.getRandomNumber(20, to: (self.animationMaxSize.height - imgSource.size.height - 20))
                self._countSmallAnimationShowing += 1
                self.addSubview(_imgView)
                self.performSelector(#selector(self.removeSmallImageView), withObject: _imgView, afterDelay: duration)
            })
        }
    }
    
    func removeSmallImageView(view: UIImageView){
        view.image = nil
        view.removeFromSuperview()
        self._countSmallAnimationShowing -= 1
        self.performSelector(#selector(self.nextSmallImage), withObject: nil, afterDelay: 0.3)
    }
    
    func removeLargeImageView(view: UIImageView){
        view.image = nil
        view.removeFromSuperview()
        self._countLargeAnimationShowing -= 1
        self.performSelector(#selector(self.nextLargeImage), withObject: nil, afterDelay: 0.3)
    }
    
    func nextSmallImage(){
        if(_queueSmallAnimation.count > 0){
            let temp = _queueSmallAnimation.removeLast()
            self.showAnimationImage(temp.0, duration: temp.1,animationType: .Small)
        }
    }
    
    func nextLargeImage(){
        if(_queueLargeAnimation.count > 0){
            let temp = _queueLargeAnimation.removeLast()
            self.showAnimationImage(temp.0, duration: temp.1, animationType: .Large)
        }
    }
    
    
    
//    func removeGiftView(view: UIImageView){
//        view.animationImages = nil
//        view.removeFromSuperview()
//        self._countAnimationShowed = self._countAnimationShowed - 1
//        self.nextGiftAnimation()
//    }
//
//    func showInventoryAnimation(animation: YYImage, animationSize: CGSize, duration: Double){
//        //Check Queue
//        if(self._countInventoryShowed == 1){
//            _queueInventoryAnimation.insert((image: animation, size: animationSize, duration: duration), atIndex: 0)
//            return
//        }
//        
//        let animationView = YYAnimatedImageView()
//        animationView.contentMode = .ScaleAspectFit
//        animationView.image = animation
//        animationView.frame.size = CGSize(width: animationSize.width , height: animationSize.height)
//        animationView.frame.origin = self._inventoryAnimationStartPoint
//        
//        //Check X position by size
//        let startPointX = self.center.x
//        animationView.center = CGPoint(x: startPointX, y: self._inventoryAnimationStartPoint.y)
//        self.runSyncWithMainThread {
//            self._countInventoryShowed += 1
//            self.addSubview(animationView)
//            self.performSelector(#selector(self.removeInventoryView), withObject: animationView, afterDelay: duration)
//        }
//    }
//    
//    func removeInventoryView(view: UIImageView){
//        view.animationImages = nil
//        view.removeFromSuperview()
//        self._countInventoryShowed -= 1
//        self.performSelector(#selector(self.nextInventoryAnimation), withObject: nil, afterDelay: 0.3)
//    }

    func cleanUp(){
        for view in self.subviews{
            view.removeFromSuperview()
        }
        //Clean queues
        self._queueLargeAnimation.removeAll()
        self._queueSmallAnimation.removeAll()
    }
}
