//
//  PayCardTableViewCell.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/1/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class PayCardTableViewCell: UITableViewCell {

    @IBOutlet weak var lbCreatedDate: UILabel?
    @IBOutlet weak var btStatus: UIButton?
    @IBOutlet weak var lbPayName: UILabel?
    @IBOutlet weak var lbCardInfo: UILabel?
    @IBOutlet weak var lbPrice: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setDataPayment(model: PaymentLogModel)
    {
        var status:(ic_string: String, background: String , title: String)?
        switch (model.paymentLog?.status)! {
        case 0:
            status = ("ic_payment_processing", "bg_payment_processing", " Đang xử lý")
            break
        case 1:
            status = ("ic_payment_success", "bg_payment_success", " Thành công")
            break
        case 2:
            status = ("ic_payment_fail", "bg_payment_fail" , " Thất bại")
            break
        default:
            break
        }
        
        let date = NSDate(timeIntervalSince1970: (Double((model.paymentLog?.createDate)!) / 1000))
        lbCreatedDate!.text = FunctionHelper.dateToString(date, mode: 5)
        
        lbPayName!.text = "\((model.paymentType?.name)!)"
        btStatus?.setTitle(status!.title, forState: .Normal)
        btStatus?.setImage(UIImage(named: status!.ic_string), forState: .Normal)
        btStatus?.setBackgroundImage(UIImage(named: status!.background), forState: .Normal)
        
        if(model.paymentType?.Id == 1)
        {
            let cardInfo = (model.pay1PaymentLog?.Serial)! + "\n " + (model.pay1PaymentLog?.Pin)!
            lbCardInfo?.text = cardInfo
        }
        else
        {
            lbCardInfo?.text = "Không có thông tin"
        }

        let paymentValue = "\(FunctionHelper.formatNumberWithSeperator((model.paymentLog?.amount)!))đ\n\((model.paymentLog?.coin)!) Ruby"
        lbPrice!.text = paymentValue
    }

    
}
