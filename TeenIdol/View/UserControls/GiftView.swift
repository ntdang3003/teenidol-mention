	//
//  GiftView.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftView: UIView, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,
    UITableViewDelegate, UITableViewDataSource{


    @IBOutlet weak var colGift: UICollectionView?;
    @IBOutlet weak var dotPage: UIPageControl?;
    @IBOutlet weak var btnNumberSelection : UIButton?
    @IBOutlet weak var lbRuby:UILabel?;
    @IBOutlet weak var lblHeart:UILabel?;
    @IBOutlet weak var btnSendGift : UIButton?
    
    var rootViewController : UIViewController?
    var currentShowModel : ShowModel?
    
    var _data : Array<GiftModel> = []
    var _numberOfItemInSection = 0
    var _numberOfSection = 1
    var _tblNumberSelection : UITableView?
    var _selectedGift = GiftModel()
    var _selectedQuantity = 0
    var _popoverSelection : Popover?
    var _selectedIndex = NSIndexPath(forRow: 0, inSection: 0)
    var _cellSize = CGSize(width: 80, height: 70)
    var _selectedQuantityRow = 0
    private var _isLoading = false
    
    private var popoverOptions: [PopoverOption] = [
        .Type(.Up),
        .BlackOverlayColor(UIColor(white: 0.0, alpha: 0.0))
    ]
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("GiftView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        self.setupCollectionView()
        self.setupTableSelection()
        self.loadData()
        
        //self.lbRuby?.text = String(SettingsHelper.getCurrentUserRuby())
        self.updateUserCoin()
        self.btnSendGift?.showsTouchWhenHighlighted = true
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateUserCoin), name: UserRubyChangeNotificationName, object: nil)
    }
    
    func updateUserCoin(){
        self.lbRuby?.text = FunctionHelper.formatNumberWithSeperator(SettingsHelper.getCurrentUserRuby())
        self.lblHeart?.text = FunctionHelper.formatNumberWithSeperator(SettingsHelper.getCurrentUserHeart())
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupTableSelection(){
        self._tblNumberSelection = UITableView(frame: CGRect(x:0, y:0, width: 80.0,height: 95.0))
        
        self._tblNumberSelection?.registerClass(NumberSelectionViewCell.self, forCellReuseIdentifier: "NumberSelectionIndentifier")
        self._tblNumberSelection?.showsVerticalScrollIndicator = false
        self._tblNumberSelection?.separatorStyle = UITableViewCellSeparatorStyle.None
        self._tblNumberSelection?.dataSource = self
        self._tblNumberSelection?.delegate = self
        
        
    }
    
    func setupCollectionView(){
        colGift?.registerNib(UINib(nibName: "GiftCell", bundle: nil), forCellWithReuseIdentifier: "GiftCellIndentifier")
        let layout =  UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        layout.minimumLineSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        
        layout.itemSize = self._cellSize
        self.colGift?.showsVerticalScrollIndicator = false
        self.colGift?.showsHorizontalScrollIndicator = false
        self.colGift?.setCollectionViewLayout(layout, animated: false)
        self.colGift?.allowsSelection = true
        self.dotPage?.numberOfPages = 1
        self.dotPage?.currentPage = 0
        self.dotPage?.currentPageIndicatorTintColor = ColorAppDefault
        
    }
    
    func loadData(){
        GiftDAO.User_GetListGiftV2(0, pageSize: 200) { (result) in
            self._data.appendContentsOf(result.ListItem)
            self._numberOfItemInSection = 10
            self.colGift?.reloadData()
            
            //Giả lập tặng quà
            //let showInfo = self.currentShowModel
            //let scheduleId = 4
            //let toUserId = 5
            
//            for gift in self._data{
//                for quantity in gift.listGiftAnimations.ListItem{
//                    let popTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
//                    dispatch_after(popTime, dispatch_get_main_queue(), {() -> Void in
//                        GiftDAO.GiveGiftV2((gift.giftDTO?.Id)!, quantity: quantity.Quantity, scheduleId: 4, toUserId: 5) { (result) in
//                            if(result.ErrorCode == 0){
//                                let totalUsed = (self._selectedGift.giftDTO?.Price)! * self._selectedQuantity
//                                SettingsHelper.addCurrentUserRuby(-totalUsed)
//                                
//                            }else{
//                                FunctionHelper.showBannerAlertView(content: result.Message)
//                            }
//                            self.btnSendGift?.setTitle("Tặng", forState: .Normal)
//                            self.btnSendGift?.userInteractionEnabled = true
//                        }
//                    })
//                   
//                }
//            }
            
            
        }
    }
    
    
    //MARK: Collection View DataSource & Delegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //self._numberOfItemInSection = Int((self.frame.width) / self._cellSize.width)
        if(self._data.count <= 0) {return 1}
        let value =  Double(self._data.count) / Double(_numberOfItemInSection)
        let number = Int(ceil(value))
        self._numberOfSection = number
        self.dotPage?.numberOfPages = number
        return self._numberOfSection
        //return 1;
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return _numberOfItemInSection
        //return self._data.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = colGift?.dequeueReusableCellWithReuseIdentifier("GiftCellIndentifier", forIndexPath: indexPath) as! GiftCell
        let row = indexPath.row + (indexPath.section * self._numberOfItemInSection)
        if(row > self._data.count - 1){
            cell.invisibleCell()
            return cell
        }
        self.dotPage?.currentPage = indexPath.section
        cell.initWithData(_data[row])
        
        //Set Select
        if(self._selectedIndex.row == indexPath.row && self._selectedIndex.section == indexPath.section){
            cell.setCellSelected(true)
            self._selectedGift = _data[row]
            self.onGiftSelected()
        }else{
            cell.setCellSelected(false)
           
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(indexPath == _selectedIndex){
            return
        }
        let selectedCell = self.colGift?.cellForItemAtIndexPath(indexPath) as! GiftCell
        selectedCell.setCellSelected(true)
        
        let lastCellSelected = self.colGift?.cellForItemAtIndexPath(_selectedIndex) as? GiftCell
        if(lastCellSelected != nil){
            lastCellSelected?.setCellSelected(false)
        }
        _selectedIndex = indexPath
        let row = indexPath.row + (indexPath.section * self._numberOfItemInSection)
        self._selectedGift = self._data[row]
        self.onGiftSelected()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        self._cellSize  = CGSize(width: collectionView.frame.size.width / CGFloat(self._numberOfItemInSection / 2),height: collectionView.frame.size.height/2)
        return self._cellSize
    }
    
    //MARK : Table View DataSource + Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self._selectedGift.listGiftAnimations.ListItem.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.bounds.size.height / 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self._tblNumberSelection?.dequeueReusableCellWithIdentifier("NumberSelectionIndentifier", forIndexPath: indexPath) as! NumberSelectionViewCell
        cell.setData(self._selectedGift.listGiftAnimations.ListItem[indexPath.row])
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        _selectedQuantityRow = indexPath.row
        let giftAni = _selectedGift.listGiftAnimations.ListItem[indexPath.row]
        _selectedQuantity = giftAni.Quantity
        btnNumberSelection?.setTitle( "\(self._selectedQuantity)", forState: .Normal)
        _popoverSelection?.dismiss()
    }
    
    func onGiftSelected(){
        self._selectedQuantity = self._selectedGift.listGiftAnimations.ListItem[0].Quantity
        btnNumberSelection?.setTitle( "\(self._selectedQuantity)", forState: .Normal)
        self._tblNumberSelection?.reloadData()
    }
    
    @IBAction func touchedNumber(sender: AnyObject){
        self._popoverSelection = Popover(options: self.popoverOptions, showHandler: nil, dismissHandler: nil)
        self._popoverSelection?.show(self._tblNumberSelection!, fromView: self.btnNumberSelection!)
    }
    
    @IBAction func touchedSendGift(sender: AnyObject)
    {
        self.rootViewController?.checkLogin({ (loginState) in
            if(loginState == .Logged){
                if(self._isLoading) {return }
                self._isLoading = true
                
                let showInfo = self.currentShowModel
                let scheduleId = showInfo!.Schedule?.Id
                let toUserId = showInfo!.ListShowItem[0].StarUser.Id
                
                self.btnSendGift?.setTitle("...", forState: .Normal)
                //self.btnSendGift?.userInteractionEnabled = false
                GiftDAO.GiveGiftV2((self._selectedGift.giftDTO?.Id)!, quantity: self._selectedQuantity, scheduleId: scheduleId!, toUserId: toUserId) { (result) in
                    if(result.ErrorCode == 0){
                        let totalUsed = (self._selectedGift.giftDTO?.Price)! * self._selectedQuantity
                        SettingsHelper.addCurrentUserRuby(-totalUsed)
                        
                    }else{
                        FunctionHelper.showBannerAlertView(content: result.Message)
                    }
                    self.btnSendGift?.setTitle("Tặng", forState: .Normal)
                    self._isLoading = false
                    //self.btnSendGift?.userInteractionEnabled = true
                }
            }
        })
    }
    
    @IBAction func touchPayment(sender: AnyObject){
        
        if(self.rootViewController != nil){
            self.rootViewController?.checkLogin({ (loginState) in
                if(loginState == .Logged){
                    let con = DomainViewController(nibName: "DomainViewController", bundle: nil)
                    self.rootViewController?.navigationController?.pushViewController(con, animated: true)
                }
            })
        }
    }
    
    func cleanUp(){
        self.rootViewController = nil
    }
}

class NumberSelectionViewCell : UITableViewCell {
    var imgAnimation : UIImageView?
    var lblAnimationName : UILabel?
    var model : GiftAnimationDTO?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let width  = CGFloat(75)
        //self.contentView.frame = CGRect(x: 0, y: 0, width: width, height: 20)
        self.frame = CGRect(x: 0, y: 0, width: width, height: 20)
        if(imgAnimation == nil){
            imgAnimation = UIImageView(frame: CGRect(x: 5, y: 5, width: width / 3, height: width/3))
            imgAnimation?.contentMode = .ScaleToFill
            self.contentView.addSubview(imgAnimation!)
        }
        if(lblAnimationName == nil){
            let imgRect = imgAnimation?.bounds
            lblAnimationName = UILabel(frame: CGRect(x : (imgRect?.size.width)! + 10, y: 5,
                width: width - ((imgRect?.size.width)! + 10), height: (imgRect?.size.height)! ))
            lblAnimationName?.font = UIFont.boldSystemFontOfSize(12)
            lblAnimationName?.textAlignment = .Left
            lblAnimationName?.textColor = UIColor(red: 101/255, green: 117/255, blue: 117/255, alpha: 1)
            self.contentView.addSubview(lblAnimationName!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    func setData(data: GiftAnimationDTO){
        self.model = data
        let URL = NSURL(string: (self.model?.PhotoLink)!)
        if(URL != nil){
                self.imgAnimation?.sd_setImageWithURL(URL!)
        }
        self.lblAnimationName?.text = String((self.model?.Quantity)!)
    }
}
