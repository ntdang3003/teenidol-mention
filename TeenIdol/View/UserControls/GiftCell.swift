//
//  GiftCell.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SDWebImage
class GiftCell: UICollectionViewCell {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    @IBOutlet weak var imgGift:YYAnimatedImageView?
    @IBOutlet weak var imgIcon:YYAnimatedImageView?
    @IBOutlet weak var lbGiftName: UILabel?
    @IBOutlet weak var lbGiftCost: UILabel?
    @IBOutlet weak var borderGift: UIView?
    
    //var imgGift : UIImageView? = nil
    
    //var imgGiftAS : ASNetworkImageNode? = nil
    var model : GiftModel? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //borderGift?.layer.borderWidth = 1
       // borderGift?.layer.cornerRadius = 5
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.hidden = false
    }
    
    
    func setCellSelected(value: Bool){
        
        if(value){
            self.contentView.layer.borderColor = ColorAppDefault.CGColor
            //self.contentView.backgroundColor = UIColor.clearColor()
            self.contentView.layer.borderWidth = 2
            self.contentView.alpha = 1
        }else{
            self.contentView.layer.borderColor = UIColor.clearColor().CGColor
            //self.contentView.backgroundColor = UIColor.clearColor()
            self.contentView.layer.borderWidth = 1
            self.contentView.alpha = 1
        }
    }
    
    func invisibleCell(){
        self.hidden = true
    }
    
    func initWithData(data: GiftModel){
        self.model = data
        self.lbGiftName?.text = model?.giftDTO!.Name
        lbGiftCost?.attributedText = self.getRubyAttributeString(String((model?.giftDTO!.Price)!), fontSize: (lbGiftCost?.font.pointSize)!)
        let URL = NSURL(string: (model?.giftDTO!.PhotoGiftLink)!)
        if(URL != nil){
            self.imgGift?.sd_setImageWithURL(URL!, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        if(model?.giftDTO!.IconLink != ""){
            let temp = NSURL(string: (model?.giftDTO!.IconLink)!)
            if(temp != nil){
                imgIcon?.hidden = false
                imgIcon?.sd_setImageWithURL(temp!)
            }
        }
        else{
            self.imgIcon?.hidden = true
        }
    }
    
    func getRubyAttributeString(input: String, fontSize: CGFloat) -> NSAttributedString{
        let font = UIFont.italicSystemFontOfSize(fontSize)
        
        
        let image = UIImage(named: "ic_ruby")
        let inlineAttach = FunctionHelper.inlineTextAttachment(image!, size: CGSize(width: 9,height: 8), font: UIFont.systemFontOfSize(3))
        let imageAttribute = NSAttributedString(attachment: inlineAttach)
        let temp = NSMutableAttributedString(attributedString: imageAttribute)
        let rubyAtt = NSAttributedString(string: " \(input)", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor(red:187/255,
            green: 189/255,blue: 189/255,alpha: 1) ])
        temp.appendAttributedString(rubyAtt)
        return temp
    }
}
