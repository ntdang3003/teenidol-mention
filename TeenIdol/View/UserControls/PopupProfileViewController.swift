//
//  PopupProfileViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/12/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol  PopupProfileDelegate {
    func popupProfile_didTouchClose()
    func touchFollow(userId: Int64, isFollow: Bool)
    func touchOpenProfile(userId: Int64)
    func popupProfile_didTouchDisableChat(userId: Int64, isCanChat: Bool, isFacebookUser: Bool,
                                          facebookName: String)
    func popupProfile_didTouchKick(userId: Int64)
    func popupProfile_didTouchChat(userInfo: SignalRUserDTO)
    func popupProfile_didTouchReport(userId: Int64)
    
}
class PopupProfileViewController: UIViewController, UIAlertViewDelegate   {
    
    @IBOutlet weak var imageAvatar: UIImageView!;
    @IBOutlet weak var lbName:      UILabel!;
    
    @IBOutlet weak var btnQuickChat: UIButton!
    @IBOutlet weak var btnKick: UIButton!
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnRuby: UIButton!
    @IBOutlet weak var imgUserIcon: UIImageView!
    
    
    var data = DoResultDTO();
    var dataTopFan = FListResultDTO<UserModel>();
    
    private var userId:Int64 = 0;
    private var model : UserModel?
    private var _isLoadingData = false
    var delegate : PopupProfileDelegate?
    private var scheduleId : Int64 = 0
    
    var _isFollowed = false {
        didSet{
//            self.btnFollow.setTitle(_isFollowed ? "ĐÃ THEO DÕI" : "THEO DÕI", forState: .Normal)
//            self.btnFollow.setImage(UIImage(named: _isFollowed ? "ic_unfollow" : "ic_follow" ), forState: .Normal)
        }
    }
    
    private var _isCanChat = true{
        didSet{
            self.btnMute.setImage(UIImage(named:_isCanChat ? "ic_mute" : "ic_enable_chat") , forState: .Normal)
            self.btnMute.setTitle(_isCanChat ? "CHẶN CHAT" : "CHO CHAT", forState: .Normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI()
    {
        //self.modalPresentationStyle = .OverFullScreen
        FunctionHelper.circleBorderImageView(imageAvatar)
        //FunctionHelper.makeBorderView(viewBorder, borderWidth: 0, color: UIColor.clearColor(), cornerRadius: 25)
        
//        btnFollow.layer.borderWidth = 1
//        btnFollow.layer.borderColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1).CGColor
//        btnFollow.layer.cornerRadius = 2
//        btnFollow.titleLabel?.adjustsFontSizeToFitWidth = true
        
        btnQuickChat.layer.borderWidth = 1
        btnQuickChat.layer.borderColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1).CGColor
        btnQuickChat.layer.cornerRadius = 2
        
        btnKick.layer.cornerRadius = 2
        btnMute.layer.cornerRadius = btnKick.layer.cornerRadius
        
//        btnRuby.layer.cornerRadius = 10
//        btnRuby.layer.borderWidth = 1
//        btnRuby.layer.borderColor = UIColor(white: 0, alpha: 0.3).CGColor
        
        self.imageAvatar.userInteractionEnabled = true
        self.imageAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openProfile)))
        
        //colFans.registerClass(UserOnlineCollectionViewCell.self, forCellWithReuseIdentifier: "UserTopFanCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
        //self.loadDataFan()
    }
    
    func setUserInfo(userId: Int64, model: UserModel?, scheduleId : Int64)
    {
        self.userId = userId
        self.model = model
        self.scheduleId = scheduleId
    }
    
    func loadData()
    {
//        if(self.model != nil){
//            self.updateUI()
//            return
//        }
        if(self._isLoadingData == true) {return}
        self._isLoadingData = true
        
        UserDAO.User_GetDetailsV2(UserId: userId) { (result) in
            
            if result.ErrorCode == 0
            {
                self.model = result.DataObject as? UserModel
                self.updateUI()
            }
            self._isLoadingData = false
            
        }
    }
    
//    func loadDataFan()
//    {
//        if(self.dataTopFan.isLoadingData)
//        {
//            return
//        }
//        self.dataTopFan.isLoadingData = true
//        let weakSeft = self
//        
//        UserDAO.User_GetListUserFanV2(pageIndex: 0, pageSize: 3, str_search: "", num_userId: self.userId) { (result) in
//            weakSeft.dataTopFan.CountAllResult = result.CountAllResult
//            weakSeft.dataTopFan.ListItem = result.ListItem
//            
//            dispatch_async(dispatch_get_main_queue(), {
//                weakSeft.colFans.reloadData()
//            })
//            self.dataTopFan.isLoadingData = false
//        }
//    }
//    
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if dataTopFan.ListItem.count != 0
//        {
//            lbDescription.hidden = true
//            self.colFans.hidden = false
//        }
//        return dataTopFan.ListItem.count;
//    }
    
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1;
//    }
//    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        return getCollCell(collectionView, nameCell: "UserTopFanCell", indexPath: indexPath)
//    }
//    
//    func getCollCell(collMain: UICollectionView, nameCell: String, indexPath: NSIndexPath) -> UICollectionViewCell
//    {
//        let cell = collMain.dequeueReusableCellWithReuseIdentifier(nameCell, forIndexPath: indexPath) as! UserOnlineCollectionViewCell
//        cell.setData(dataTopFan.ListItem[indexPath.row])
//        return cell;
//    }
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
//        let padding : CGFloat = 2
//        let leftInset = CGFloat(((collectionView.frame.size.width - CGFloat(32 * self.dataTopFan.ListItem.count)) / 2) - padding)
//        
//        //let totalCellWidth = 32 * dataTopFan.ListItem.count
//        //let totalSpacingWidth = 32 * (dataTopFan.ListItem.count - 1)
//        
//        //let leftInset = (self.colFans.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
//       // let rightInset = leftInset
//        
//        return UIEdgeInsetsMake(0, leftInset, 0, leftInset)
//    }
    
    func updateUI()
    {
        var nameColor = self.lbName.textColor
        if let url = NSURL(string: (model!.userInfo?.AvatarPhoto)!.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!){
           self.imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
        if let url = NSURL(string: (model!.vipInfo?.photoLink)!){
            self.imgUserIcon.sd_setImageWithURL(url)
            if let color = SignalRHelper.getHexColor((model!.vipInfo?.hexColor)!,alpha: 1){
                nameColor = color
            }
        }
        self.lbName.attributedText = NSAttributedString(string: (model!.userInfo?.Name)!, attributes: [
            NSForegroundColorAttributeName : nameColor])
        if(model!.starDataInfo != nil){
            let ruby = " \(FunctionHelper.formatNumberWithSeperator(model?.userInfo?.GroupUserId == .Star ? (model!.starDataInfo?.coinGetFromGift)! :(model?.userInfo?.TotalCoinUsed)!))"
            btnRuby.setImage( UIImage(named: model!.userInfo?.GroupUserId == .Star ? "ic_star_point_popup" : "ic_ruby_popup" ) ,forState: .Normal)
            self.btnRuby.setTitle(ruby, forState: .Normal)
            self.btnRuby.sizeToFit()
        }
        _isFollowed = model!.followInfo?.Status == 1 ? true : false
        
        //check Facebook user
        if(self.userId != SettingsHelper.getUserID()){
            self.btnMute.enabled = true
            self.btnQuickChat.enabled = true
            if(self.model?.isFacebookUser() == true){
                self.btnQuickChat.enabled = false
                UserDAO.User_IsUserCanFacebookChat(self.userId, scheduleId: self.scheduleId, callback: { (result) in
                    self._isCanChat = result
                })
                
            }else{
                self.btnKick.enabled = true
                let signalRInstance = SignalRConnector.sharedInstance
                signalRInstance.isUserCanChat(self.scheduleId, userId: self.userId) { (result) in
                    self._isCanChat = result
                }
            }
            
            //Check Idol to enable follow
//            if(model!.userInfo?.GroupUserId == .Star){
//                self.btnFollow.enabled = true
//            }
        }
    }
    
    func openProfile()
    {
        if(self.delegate != nil)
        {
            self.delegate?.touchOpenProfile(userId)
        }
    }
    
    @IBAction func touchedClose(sender: AnyObject)
    {
        if(self.delegate != nil)
        {
            self.delegate?.popupProfile_didTouchClose()
        }
        
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func touchedFollow(sender: AnyObject)
    {
        
        if(self.delegate != nil)
        {
            // self.delegate?.touchFollow(userId,isFollow: _isFollowed)
            if(SettingsHelper.getUserID() == -1)
            {
                let alert = UIAlertView(title: "Thông báo", message: "Bạn cần đăng nhập để thực hiện thao tác.", delegate: self,cancelButtonTitle: "Đồng ý")
                alert.show()
            }
            else
            {
                UserDAO.User_FollowUser(FollowUserId: userId, IsFollow: !_isFollowed, callback: { (result) in
                    if( result.ErrorCode == 0)
                    {
                        self._isFollowed = !self._isFollowed;
                       // self.loadDataFan()
                    }
                    
                    FunctionHelper.showBannerAlertView(content: result.Message)
                })
            }
        }
    }
    
    @IBAction func touchedQuickChat(sender: AnyObject)
    {
        if(self.delegate != nil && self.model != nil){
            self.delegate?.popupProfile_didTouchChat(SignalRUserDTO(model: self.model!))
        }
    }
    
    @IBAction func touchedKick(sender: AnyObject)
    {
        self.showAlert("Bạn có muốn kích người này?", tag: 111)
    }
    
    @IBAction func touchedMute(sender: AnyObject)
    {
        self.showAlert("Bạn muốn \(self._isCanChat ? "chặn chat người này?" : "cho phép người này chat?     " )", tag: 222)
    }
    
    private func showAlert(content: String, tag: Int)
    {
        //let alertView = UIAlertView(title: "Xác nhận", message: content, delegate: self, cancelButtonTitle: "Đồng ý", otherButtonTitles: "Hủy")
        let alertView = UIAlertView(title: "Xác nhận", message: content, delegate: self, cancelButtonTitle: "Đồng ý", otherButtonTitles: "Hủy")
        alertView.tag = tag
        alertView.show()
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            if(alertView.tag == 111) //Kick
            {
                if(self.delegate != nil){
                    self.delegate?.popupProfile_didTouchKick((self.model?.userInfo?.Id)!)
                }
            }else{ //Mute
                if(self.delegate != nil){
                    self.delegate?.popupProfile_didTouchDisableChat((self.model?.userInfo?.Id)!, isCanChat: self._isCanChat, isFacebookUser: (self.model?.isFacebookUser())!,
                    facebookName: (self.model?.userInfo?.Name)!)
                }
            }
        }
    }
    
    @IBAction func btReport(sender: UIButton)
    {
        if(delegate != nil)
        {
            delegate?.popupProfile_didTouchReport((self.model?.userInfo?.Id)!)
        }
    }
}
