//
//  UserOnlineView.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/5/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol UserOnlineViewProtocol {
    func userOnline_didTouchUser(userId : Int64, model: UserModel)
}


class UserOnlineView: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var colMain: UICollectionView!
    var _showId : Int64 = -1
    var delegate : UserOnlineViewProtocol? = nil
    private static var _data = Array<UserModel>()
    private var _queueUser = [(id: Int64, model: UserModel?)]()
    private var isLoadingData = false
    private let _pageSize = 50
    private var _count = 0
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("UserOnlineView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        colMain.registerClass(UserOnlineCollectionViewCell.self, forCellWithReuseIdentifier: "UserOnlineIndentifier")
        colMain.backgroundColor = UIColor.clearColor()
    }
    
    func toNextUserBehavior(){
        if self._queueUser.count > 0{
            let temp = self._queueUser.removeFirst()
            if temp.id > 0 { //Delete State
                self.removeUserFromList(temp.id)
            }else{ //Add State
                self.addNewUserToList(temp.model!)
            }
        }
    }
    
    func addNewUserToList(data: UserModel) -> Void{
        dispatch_async(dispatch_get_main_queue()) { 
            if self.isLoadingData == true {
                self._queueUser.append((id: -1, model: data))
                return
            }
            self.isLoadingData = true
            //Find Index
            let index = UserOnlineView._data.count == 0 ? 0 : 1
            UserOnlineView._data.insert(data, atIndex: index)
            self.colMain.performBatchUpdates({ 
                self.colMain.insertItemsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)])
                 self._count += 1
                }, completion: { (isFinished) in
                    self.isLoadingData = false
                    self.toNextUserBehavior()
            })
            
        }
    }
    
    func removeUserFromList(userId: Int64) -> Void{
        dispatch_async(dispatch_get_main_queue()) {
            if self.isLoadingData == true {
                self._queueUser.append((id: userId, model: nil))
                return
            }
            self.isLoadingData = true
            //Find Index
            var index = -1
            for i in 0..<UserOnlineView._data.count{
                let temp = UserOnlineView._data[i]
                if(temp.userInfo?.Id == userId){
                    UserOnlineView._data.removeAtIndex(i)
                    index = i
                    break
                }
            }
            
            if index == -1 { return }
            self.colMain.performBatchUpdates({
                self.colMain.deleteItemsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)])
                self._count -= 1
                }, completion: {(isFinished) in
                self.isLoadingData = false
                self.toNextUserBehavior()
            })
        }
    }
    
    //MARK: Collection View Delegate & DataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return self._data.ListItem.count
        return self._count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        if(indexPath.row == self._data.ListItem.count - 1 &&
//            self._data.ListItem.count != self._data.CountAllResult){
//            self.loadData()
//        }
        let cell = colMain.dequeueReusableCellWithReuseIdentifier("UserOnlineIndentifier", forIndexPath: indexPath)
        as! UserOnlineCollectionViewCell
        cell.setData(UserOnlineView._data[indexPath.row])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let model = UserOnlineView._data[indexPath.row]
        if(self.delegate != nil){
            self.delegate?.userOnline_didTouchUser((model.userInfo?.Id)!, model: model)
        }
    }
    
    func cleanup(){
        UserOnlineView._data  = Array<UserModel>()
        self._count = UserOnlineView._data.count
        self._queueUser.removeAll()
        self.colMain.reloadData()
    }
    
    func loadData(){
        if(self.isLoadingData) {return}
        self.isLoadingData = true
        let _weakSelf = self;
        
        UserDAO.User_GetListUserInShow(self._showId, pageIndex: 0, pageSize: _pageSize) { (result) in
            UserOnlineView._data.appendContentsOf(result.ListItem)
           _weakSelf._count = UserOnlineView._data.count
            _weakSelf.colMain.reloadData()
            _weakSelf.isLoadingData = false
        }
    }
}

class UserOnlineCollectionViewCell : UICollectionViewCell{
    var _model : UserModel? = nil
    var _imgAvatar: UIImageView? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        _imgAvatar = UIImageView(frame: self.contentView.bounds)
        _imgAvatar?.layer.borderWidth = 1
        _imgAvatar?.layer.masksToBounds = false
        _imgAvatar?.layer.borderColor = UIColor(red: 219/255, green: 201/255, blue: 191/255, alpha: 1).CGColor
        _imgAvatar?.layer.cornerRadius = (_imgAvatar?.frame.height)! / 2
        _imgAvatar?.clipsToBounds = true
        self.contentView.addSubview(_imgAvatar!)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func setData(model: UserModel){
        _model = model
        _imgAvatar?.image = nil
        if let url = NSURL(string: FunctionHelper.getImageUrlBySize((model.userInfo?.AvatarPhoto)!, imageSize: (self._imgAvatar?.bounds.size)!)){
            _imgAvatar?.sd_setImageWithURL(url,placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
    }
}
