//
//  SeatContainerView.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class SeatContainerView: UIView, PopupBuySeatDelegate {

    var data = FListResultDTO<ShowItemModel>()
    var currentShowModel  :  ShowModel?
    private var _popupController : CNPPopupController?
    var rootViewController : UIViewController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("SeatViewContainer", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        self.setupTap()
    }
    
    func setupTap(){
        for i in 1...5{
            let seatView = self.viewWithTag(i) as? SeatView
            if(seatView != nil){
                seatView?.onTap({ (tapGestureRecognizer) in
                    self.rootViewController?.checkLogin({ (loginState) in
                        if(loginState == .Logged){
                            let data = seatView?.model
                            let con = PopupBuySeatViewController(nibName: "PopupBuySeatViewController", bundle: nil) as PopupBuySeatViewController
                            if(data != nil){
                                con.model = (data?.userShowItem)!
                            }else{
                                con.model = UserShowItemDTO()
                                con.model.index = i
                                con.model.scheduleId = (self.currentShowModel?.Schedule?.Id)!
                            }
                            con.delegate = self
                            self._popupController = CNPPopupController(contents: [con.view])
                            self._popupController!.mainController = con
                            self._popupController!.theme = CNPPopupTheme.defaultTheme()
                            //self._popupController!.theme.maxPopupWidth = con.view.bounds.size.width
                            self._popupController!.theme.maxPopupWidth = con.view.frame.size.width
                            self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                            self._popupController!.theme.popupStyle = .Centered
                            self._popupController!.presentPopupControllerAnimated(true)

                        }
                    })
                })
            }
        }
    }
    
    func popupBuySeat_onClose() {
        self._popupController?.dismissPopupControllerAnimated(true)
    }
    
    func loadData(){
        if(data.isLoadingData) {return}
        data.isLoadingData = true
        
        UserDAO.List_GetListUserBuySeatInShow((self.currentShowModel?.Schedule?.Id)!) { (result) in
            if(result.ListItem.count > 0){
                for index in 0..<result.ListItem.count{
                    let data = result.ListItem[index]
                    let seatIndex = self.getIndexFromJson((data.userShowItem?.info)!)
                    if(seatIndex > 0){
                        let seatView = self.viewWithTag(seatIndex) as? SeatView
                        if(seatView != nil){
                            data.userShowItem?.index = seatIndex
                            seatView?.setData(data)
                        }
                    }
                }
            }
            self.data.isLoadingData = false
        }
    }
    
    func updateSeat(data: ShowItemModel){
        if let seatView = self.viewWithTag(data.userShowItem!.index) as? SeatView{
            data.userShowItem?.scheduleId = (self.currentShowModel?.Schedule?.Id)!
            seatView.setData(data)
        }
    }
    
    func getIndexFromJson(json: String) -> Int{
        if let data = json.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false){
            let jsonData = JSON(data: data)
            if(jsonData != nil){
                return jsonData["Index"].intValue
            }
        }
        return -1
    }
    
    func cleanUp(){
        for i in 1...5{
            let seatView = self.viewWithTag(i) as? SeatView
            if(seatView != nil){
                seatView?.cleanUp()
            }
        }
    }
}
