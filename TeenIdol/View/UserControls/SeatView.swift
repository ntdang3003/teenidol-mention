//
//  SeatView.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class SeatView : UIView{
    
    @IBOutlet weak var imgSeat: UIImageView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: MarqueeLabel!
    @IBOutlet weak var imgBackgroundName: UIImageView!
    var model : ShowItemModel?
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("SeatView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        //viewTemp.frame = self.bounds
        FunctionHelper.circleBorderImageView(self.imgAvatar)
        lblName.text = "Trống"
    }
    
    func setData(data : ShowItemModel){
        self.model = data
        let urlSeat = NSURL(string: (data.userShowItem?.currentPhoto)!)
        if(urlSeat != nil){
            self.imgSeat.sd_setImageWithURL(urlSeat, placeholderImage: UIImage(named: "ic_seat"))
        }
        
        let  urlAvatar = NSURL(string: FunctionHelper.getImageUrlBySize((data.userInfo?.AvatarPhoto)!, imageSize: self.imgAvatar.frame.size))
        if(urlAvatar != nil){
            self.imgAvatar.sd_setImageWithURL(urlAvatar, placeholderImage:  UIImage(named: "ic_default_avatar"))
            
        }
        //self.imgBackgroundName.hidden = false
        //self.lblName.hidden = false
        self.lblName.text = self.model?.userInfo?.Name
    }
    
    func cleanUp(){
        self.imgAvatar.image = nil
        self.imgSeat.image = UIImage(named: "ic_seat")
        self.lblName.text = "Trống"
        //self.lblName.hidden = true
        //self.imgBackgroundName.hidden = true
    }
}
