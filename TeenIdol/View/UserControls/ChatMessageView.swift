//
//  ChatMessageView.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 4/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol ChatMessageDelegate {
    func chatMessage_didTouchedUser(userId: Int64, data: SignalRUserDTO?)
}

class ChatMessageView: UIView, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tbMain: UITableView!
    private let padding: CGFloat = 8.0
    private var _messageSizingCell : ChatMessageTableViewCell?
    private var dataSet : Array = Array<SignalRUserDoActionDTO>()
    private var _queueData  = Array<SignalRUserDoActionDTO>()
    private var _swapData  = Array<SignalRUserDoActionDTO>()
    private var _isAddingMessage = false
    private let _maxCountMessage = 150
    var delegate : ChatMessageDelegate?
    var _isAutoScroll = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("ChatMessageView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        
        tbMain.registerClass(ChatMessageTableViewCell.self, forCellReuseIdentifier: "ChatMessageIndentifier")
        tbMain.autoresizingMask = .FlexibleWidth
        self._messageSizingCell = ChatMessageTableViewCell()
        self._messageSizingCell?.txtContent = ChatMessageTextViewContainer(frame: CGRectZero, textContainer: nil)
    }
    
    //MARK: Table Delegate + DataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSet.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let data = dataSet[indexPath.row]
        if(data.actionTextHeight <= 0){
            self._messageSizingCell?.bounds = CGRect(x: 0, y: 0, width: self.tbMain.frame.size.width, height: self._messageSizingCell!.bounds.size.height)
            let height = (self._messageSizingCell?.setData(data).height)! + self.padding
            data.actionTextHeight = height
        }
        return data.actionTextHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tbMain.dequeueReusableCellWithIdentifier("ChatMessageIndentifier", forIndexPath: indexPath) as! ChatMessageTableViewCell
        let data = dataSet[indexPath.row]
        cell.contentView.frame.size = CGSize(width: self.tbMain.bounds.size.width,height: data.actionTextHeight)
        cell.setData(data)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.frame.size = CGSize(width: tableView.bounds.size.width, height: cell.bounds.size.height)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! ChatMessageTableViewCell
        if(self.delegate != nil && cell.model?.id > 0){
            self.delegate?.chatMessage_didTouchedUser((cell.model?.id)!, data: cell.model!.vipId == 100 ?  cell.model : nil)
        }
    }
    
    //MARK: Methods
    func cleanup(){
        self.dataSet  = []
        self.tbMain.reloadData()
        self.delegate = nil
        self._isAutoScroll = true
    }
    
    func addMessageToData(data: SignalRUserDoActionDTO){
        var _indexPath : NSIndexPath? = nil
        
        if(_isAddingMessage == true){
            _queueData.append(data)
            return
        }
        _isAddingMessage = true
        if(dataSet.count >= _maxCountMessage){
            //Lưu lại 15 tin nhắn gần nhất
            if(_swapData.count > 0){
                _swapData.removeAll()
            }
            for index in 1...15{
                let model = dataSet[dataSet.count - index]
                _swapData.insert(model, atIndex: 0)
            }
            
            dataSet.removeAll()
            dataSet.appendContentsOf(_swapData)
            dataSet.append(data)
            dataSet.appendContentsOf(_queueData)
            
            _swapData.removeAll()
            _queueData.removeAll()
            tbMain.reloadData()
             _indexPath = NSIndexPath(forRow: dataSet.count - 1,inSection: 0)
        }else{
            dataSet.append(data)
             _indexPath = NSIndexPath(forRow: dataSet.count - 1,inSection: 0)
            tbMain.beginUpdates()
            tbMain.insertRowsAtIndexPaths([_indexPath!], withRowAnimation:.None)
            tbMain.endUpdates()
        }
        _isAddingMessage = false
        if(_isAutoScroll){
            let _weakSelf = self
            dispatch_after(DISPATCH_TIME_NOW, dispatch_get_main_queue(), {
                if(_indexPath?.row == _weakSelf.dataSet.count - 1){
                    _weakSelf.tbMain.scrollToRowAtIndexPath(_indexPath!, atScrollPosition: .Top, animated: true)
                }
            })
        }
    }
    
    func addNewMessage(data: SignalRUserDoActionDTO){
        let _weakSelf = self
        self.runSyncWithMainThread({() in
            _weakSelf.addMessageToData(data)
        })
    }
    
    private func runSyncWithMainThread(callback: ()->Void){
        if(NSThread.isMainThread()){
            callback()
        }else{
            dispatch_after(DISPATCH_TIME_NOW,dispatch_get_main_queue(), {
                callback()
            })
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if(Int(distanceFromBottom) <= Int(height)){
            self._isAutoScroll = true
        }else{
            self._isAutoScroll = false
        }
    }
}

class ChatMessageTableViewCell : UITableViewCell{
    var txtContent : ChatMessageTextViewContainer? = nil
    var model : SignalRUserDTO?
    var size = CGSize(width: 0,height: 0)
    let minWidth : CGFloat = 100.0
    let minHeight : CGFloat =  20.0
    let padding : CGFloat = 5.0
    //var maxSize = CGSize(width: 0, height: 0)
    
    func getMaxSize() -> CGSize{
            return CGSize(width: self.bounds.width - 10, height: 300)
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.frame = CGRect(x: 0, y: 0, width: 310, height: 26)
        self.backgroundColor = UIColor.clearColor()
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize(width: minWidth, height: minHeight))
        layoutManager.addTextContainer(textContainer)
        self.txtContent = ChatMessageTextViewContainer()
        self.txtContent?.frame = CGRect(x: padding,y: padding,width: self.contentView.bounds.size.width,height: 20)
        self.txtContent?.autoresizingMask = .FlexibleWidth
        self.txtContent?.userInteractionEnabled = false
        self.contentView.addSubview(self.txtContent!)
        self.selectionStyle = .None
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(data: SignalRUserDoActionDTO) -> CGSize{
        self.model = data.userInfo
        self.txtContent?.attributedText = data.actionAttributeText
        self.contentView.layoutIfNeeded()
        
        var size = self.txtContent?.sizeThatFits(self.getMaxSize())
        
        if size!.height < minHeight {
            size!.height = minHeight
        }
        if size!.width < minWidth {
            size!.width = minWidth
        }
        self.txtContent!.frame = CGRectMake(padding,padding, size!.width, size!.height)
        var cornerRadius = (self.txtContent?.frame.size.height)! / 2
        if(cornerRadius > 24){
            cornerRadius = 24
        }
        self.txtContent?.layer.cornerRadius = cornerRadius
        return size!
    }
    
}

class ChatMessageTextViewContainer : UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.font = UIFont.systemFontOfSize(15.0)
        self.scrollEnabled = false
        self.editable = false
        self.textContainerInset = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 0
        self.layer.backgroundColor = UIColor(white:1.0, alpha: 0.75).CGColor
        self.layer.masksToBounds = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.font = UIFont.systemFontOfSize(15.0)
        self.scrollEnabled = false
        self.editable = false
        self.textContainerInset = UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0)
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 0
        self.layer.backgroundColor =  UIColor(white:1.0, alpha: 0.75).CGColor
        //self.layer.backgroundColor = UIColor(red: 219/255, green: 205/255, blue: 202/255, alpha: 1).CGColor
        self.layer.masksToBounds = false
    }

}

extension UITextView {
    func _firstBaselineOffsetFromTop() {
    }
    func _baselineOffsetFromBottom() {
    }
}
