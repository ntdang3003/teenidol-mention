//
//  PopupBuySeatViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/7/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol PopupBuySeatDelegate {
    func popupBuySeat_onClose()
}

class PopupBuySeatViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnPriceSelector: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    private var tblSeatSelector : UITableView?
    private var _popover: Popover?
    private var popoverOptions: [PopoverOption] = [
        .Type(.Down),
        .BlackOverlayColor(UIColor(white: 0.0, alpha: 0.0))
    ]
    
    var delegate: PopupBuySeatDelegate? = nil
    var model = UserShowItemDTO(){
        didSet{
        //    self.UpdateUI()
        }
    }
    private var _selectedShowItemPrice : ShowItemPriceDTO?
    private var data = FListResultDTO<ShowItemPriceDTO>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        if(self.data.isLoadingData){return}
        self.data.isLoadingData = true
        
        UserDAO.List_GetListShowItemPrice(0, pageSize: 3, minPrice: self.model.currentPrice) { (result) in
            self.data = result
            if(self.data.ListItem.count > 0){
                self._selectedShowItemPrice = self.data.ListItem[0]
                self.UpdateUI()
            }
            self.tblSeatSelector?.reloadData()
            self.data.isLoadingData = false
        }
    }
    
    func setupUI(){
        btnBuy.layer.cornerRadius = 5
        btnCancel.layer.cornerRadius = 5
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor(red: 233/255, green: 233/255, blue: 231/255, alpha: 1).CGColor
        btnPriceSelector.layer.cornerRadius = 5
        btnPriceSelector.layer.borderWidth = 1
        btnPriceSelector.layer.borderColor = UIColor(red: 233/255, green: 233/255, blue: 231/255, alpha: 1).CGColor
        self.setupTableSelection()
        
        _popover?.popoverColor = UIColor.lightGrayColor()
    }

    func setupTableSelection(){
        
       // self.view.bounds = CGRect(x: 0, y: 0, width: 280, height: 240)
        
        self.tblSeatSelector = UITableView(frame: CGRect(x:0, y:0, width: self.view.bounds.size.width/1.75,height: 120))
        
        self.tblSeatSelector?.registerClass(SeatSelectionTableViewCell.self, forCellReuseIdentifier: "SeatSelectionIndentifier")
        self.tblSeatSelector?.showsVerticalScrollIndicator = false
        self.tblSeatSelector?.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tblSeatSelector?.dataSource = self
        self.tblSeatSelector?.delegate = self
        self.tblSeatSelector?.layer.borderWidth = 1
        self.tblSeatSelector?.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.tblSeatSelector?.layer.cornerRadius = 10
    }
    
    private func UpdateUI(){
        let url = NSURL(string: (self._selectedShowItemPrice?.photoLink)!)
        
        if(url != nil){
            imgMain.sd_setImageWithURL(url!,placeholderImage: UIImage(named: "ic_seat"))
        }
        self.lblName.text = String("Ghế cấp độ \((self._selectedShowItemPrice?.Id)!)")
        self.btnPriceSelector.setTitle("\((self._selectedShowItemPrice?.price)!)", forState: .Normal)
    }
    
    //MARK : Table View DataSource + Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.ListItem.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //return tableView.bounds.size.height / 3
        return 40;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tblSeatSelector!.dequeueReusableCellWithIdentifier("SeatSelectionIndentifier", forIndexPath: indexPath) as! SeatSelectionTableViewCell
        cell.setData(self.data.ListItem[indexPath.row])
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self._selectedShowItemPrice = self.data.ListItem[indexPath.row]
        self._popover?.dismiss()
        self.UpdateUI()
    }
    
    @IBAction func touchedCancel(sender: UIButton)
    {
       // self.dismissViewControllerAnimated(true, completion: nil)
        if(self.delegate != nil){
            self.delegate?.popupBuySeat_onClose()
        }
    }
    
    @IBAction func touchedBuy(sender: UIButton)
    {
        if let itemPrice =  self._selectedShowItemPrice {
            JTProgressHUD.show()
            UserDAO.User_BuySeat(self.model.index, scheduleId: self.model.scheduleId, price: itemPrice.price, isEvent: false) { (result) in
                JTProgressHUD.hide()
                FunctionHelper.showBannerAlertView(content: result.Message)
                if(result.ErrorCode == 0){
                    if(self.delegate != nil){
                        self.delegate?.popupBuySeat_onClose()
                    }
                }
            }
        }
       
    }
    
    @IBAction func touchedPriceSelection(sender: AnyObject)
    {
        self._popover = Popover(options: self.popoverOptions, showHandler: nil, dismissHandler: nil)
        self._popover?.show(self.tblSeatSelector!, fromView: self.btnPriceSelector)
    }
    
}

class SeatSelectionTableViewCell : UITableViewCell {
    var imgSeat : UIImageView?
    var lblPrice : UILabel?
    var model : ShowItemPriceDTO?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let width  = CGFloat(75)
        let height = CGFloat(40)
        self.contentView.backgroundColor = UIColor.clearColor()
        self.contentView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        if(imgSeat == nil){
            imgSeat = UIImageView(frame: CGRect(x: 5, y: 0, width: width / 3, height: height))
            imgSeat?.contentMode = .ScaleToFill
            self.contentView.addSubview(imgSeat!)
        }
        if(lblPrice == nil){
            lblPrice = UILabel(frame: CGRect(x : 35, y: 0,
                width: width - 35, height: height))
            lblPrice?.font = UIFont.boldSystemFontOfSize(12)
            lblPrice?.textAlignment = .Left
            lblPrice?.textColor = UIColor(red: 101/255, green: 117/255, blue: 117/255, alpha: 1)
            self.contentView.addSubview(lblPrice!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setData(data: ShowItemPriceDTO){
        self.model = data
        let URL = NSURL(string: (self.model?.photoLink)!)
        if(URL != nil){
            self.imgSeat?.sd_setImageWithURL(URL!)
        }
        self.lblPrice?.text = String((self.model?.price)!)
    }
}
