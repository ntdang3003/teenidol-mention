//
//  IdolInfoTableViewCell.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/30/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol UserJoinGuildProtocol {
    func AcceptJoinGuild()
    func DeclineJoinGuild()
}

class IdolInfoTableViewCell: UITableViewCell, UIAlertViewDelegate
{
    @IBOutlet var imageAvatar: UIImageView!;
    @IBOutlet var lbName:UILabel!;
    @IBOutlet var lbCoin: UILabel!;
    @IBOutlet var footerLine: UIView!;
    @IBOutlet var imgLive: UIImageView!;
    @IBOutlet var imgLevel: UIImageView!
    @IBOutlet var lbIndexRank: UILabel?;
    @IBOutlet var conLevelWidth : NSLayoutConstraint!
    @IBOutlet var imgRuby: UIImageView!;
    @IBOutlet var lbPosition: UILabel!;
    
    @IBOutlet var constraint_actionForGuild: NSLayoutConstraint?;
    @IBOutlet var conContentWidth: NSLayoutConstraint!
    var delegateJoinGuild: UserJoinGuildProtocol?
    
    var isLive = false
    var userId:Int64 = -1
    
    var guildId: Int64 = 0
    var indexPath: NSIndexPath?
    
    private var userModel: UserModel? = nil
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        self.contentView.backgroundColor =  UIColor.whiteColor()
        
        FunctionHelper.circleBorderImageView(imageAvatar, cornerRadius: 21)
        if(lbIndexRank != nil)
        {
            FunctionHelper.makeBorderView(lbIndexRank!, borderWidth: 0, color: UIColor.clearColor(), cornerRadius: 0)
        }
    }
    
    // search
    func setData(userModel: UserModel)
    {
        userId = (userModel.userInfo?.Id)!;
        
        if  let url = NSURL(string: FunctionHelper.getImageUrlBySize((userModel.userInfo?.AvatarPhoto)!, imageSize: self.imageAvatar.frame.size)){
            imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        lbName.text = userModel.userInfo?.Name
        
        isLive = userModel.scheduleInfo?.Status == .Casting ? true : false
        self.userModel = userModel
        imgLive.hidden = !isLive
        
        if let urlLevel = NSURL(string: (userModel.groupLevel?.photo)!){
            imgLevel.sd_setImageWithURL(urlLevel, placeholderImage: UIImage(named: "ic_level_placeholder"))
        }
        
        lbCoin.text = FunctionHelper.formatNumberWithSeperator(userModel.userInfo?.GroupUserId == .Star ? (userModel.starDataInfo?.coinGetFromGift)!  : (userModel.userInfo?.TotalCoinUsed)!)
        imgRuby.image = userModel.userInfo?.GroupUserId == .Star ? KImage.StarIcon : KImage.UserIcon
    }
    
    func setDataRanking(model: GiveAwayModel, index: Int)
    {
        imgLive.hidden = true
        lbIndexRank!.hidden = false
        //imgRuby.hidden = true
        
        self.conLevelWidth.constant = 22.0
        self.layoutIfNeeded()
        
        lbName.text = model.senderUser?.Name
        lbCoin.text = FunctionHelper.formatNumberWithSeperator((model.totalPrice))
        
        lbIndexRank!.text = "\(index)"
        
        if let url = NSURL(string: (model.senderUser?.AvatarPhoto)!)
        {
            imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        if let urlLevel = NSURL(string: (model.senderUserLevel?.photo)!){
            imgLevel.sd_setImageWithURL(urlLevel, placeholderImage: UIImage(named: "ic_level_placeholder"))
        }
    }
    
    func setDataRankingGuild(model: GuildModel, index: Int)
    {
        lbName.text = model.guildInfo?.Name
        lbCoin.text = model.owner?.Name
        
        lbIndexRank!.text = "\(index)"
        
        if let url = NSURL(string: (model.guildInfo?.PhotoLink)!)
        {
            imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        if let urlLevel = NSURL(string: (model.currentLevel?.Photo)!){
            imgLevel.sd_setImageWithURL(urlLevel, placeholderImage: UIImage(named: "ic_level_placeholder"))
        }
        
        self.guildId = (model.guildInfo?.Id)!
    }
    
    func setDataMemberGuild(model: GuildMemberModel, index: Int)
    {
        lbName.text = model.userInfo?.Name
        lbCoin.text = FunctionHelper.formatNumberWithSeperator((model.userGuild?.ruby)!)
        lbPosition.text = model.guildPosition?.name;
        
        if let url = NSURL(string: (model.userInfo?.AvatarPhoto)!)
        {
            imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
        userId = (model.userInfo?.Id)!;
    }
    
    func setDataForUserJoinGuild(model: GuildMemberModel)
    {
        isAnimation = true
        lbName.text = model.userInfo?.Name
        lbCoin.text = FunctionHelper.formatNumberWithSeperator((model.userInfo?.TotalCoinUsed)!)
        if let url = NSURL(string: (model.userLevel?.photo)!)
        {
            imgLevel.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
        if let url = NSURL(string: (model.userInfo?.AvatarPhoto)!)
        {
            imageAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
    }
    var isAnimation = true
    var isAction:Bool = false
        {
        didSet{
            if(isAction == true)
            {
                aniShowAction(isAnimation)
            }
            else
            {
                aniHideAction(isAnimation)
            }
        }
    }
    
    func aniShowAction(isAniamtion: Bool)
    {
        constraint_actionForGuild?.constant = 120;
        if(!isAniamtion)
        {
            return
        }
        
        UIView.animateWithDuration(0.5) {
            self.layoutIfNeeded()
        }
    }
    
    func aniHideAction(isAniamtion: Bool)
    {
        constraint_actionForGuild?.constant = 0;
        if(!isAniamtion)
        {
            return
        }
        
        UIView.animateWithDuration(0.5) {
            self.layoutIfNeeded()
        }
    }
    
    func aniShowToturial()
    {
        constraint_actionForGuild?.constant = 35;
        //conContentWidth.con
        UIView.animateWithDuration(1.25, delay: 0.75, options: [UIViewAnimationOptions.Repeat, UIViewAnimationOptions.AllowUserInteraction, .Autoreverse] , animations: {
            self.layoutIfNeeded()
            }, completion: nil)
    }
    
    @IBAction func btAcceptGuild()
    {
        if(delegateJoinGuild != nil)
        {
            delegateJoinGuild?.AcceptJoinGuild()
        }
    }
    
    @IBAction func btDeclineGuild()
    {
        if(delegateJoinGuild != nil)
        {
            delegateJoinGuild?.DeclineJoinGuild()
        }
    }
    
    func getInfo() -> UserSMModel{
        let model = UserSMModel()
        model.UserInfo = self.userModel?.userInfo
        model.ShowInfo = self.userModel?.showInfo
        model.UserStarData = self.userModel?.starDataInfo
        model.imgBackgroud = imageAvatar.image
        return model;
    }
}

