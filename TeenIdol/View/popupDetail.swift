//
//  popupDetail.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/12/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class popupDetail: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate {
    
  //  @IBOutlet weak var lbMissionName: UIOutlinedLabel?;
   // @IBOutlet weak var lbLastHit: UILabel?;
    @IBOutlet weak var colProgressMission: UICollectionView?;
    @IBOutlet weak var btLevel1: UIButton?;
    @IBOutlet weak var btLevel2: UIButton?;
    @IBOutlet weak var btLevel3: UIButton?;
    @IBOutlet weak var btLevel4: UIButton?;
    @IBOutlet weak var btLevel5: UIButton?;
    @IBOutlet weak var btReward: UIButton?
    @IBOutlet weak var btCancel: UIButton?
    @IBOutlet weak var lblTotalStar: UILabel!
    
    var btLevels: [UIButton] = [];
    
    var delegate: popupEventProtocol? = nil;
    var scheduleId: Int64? = 0
    var starId: Int64? = 0;
    var data: [IdolQuestMissionGift] = [];
    var model: IdolQuestModel? = nil;
    var isLoading: Bool = false;
    var totalStar : Int = 0
    
    @IBOutlet weak var leftContraintBtReward: NSLayoutConstraint?;
    private let lblSizing = UILabel()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        colProgressMission?.dataSource = self
        colProgressMission?.delegate  = self
        colProgressMission?.registerNib(UINib(nibName: "IdolQuestGiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "giftCell")
        
        btLevels = [btLevel1!, btLevel2!, btLevel3!, btLevel4!, btLevel5!];
        
        lblSizing.frame = CGRect(x: 0, y: 0, width: 100, height: 35)
        lblSizing.textAlignment = .Center
        lblSizing.font = UIFont.systemFontOfSize(14.0)
        lblSizing.contentMode = .Left
        
//        lbMissionName!.type = .Continuous
//        lbMissionName!.animationCurve = .EaseInOut
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return data.count;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("giftCell", forIndexPath: indexPath) as! IdolQuestGiftCollectionViewCell;
        cell.setData(displayType: .Gift, data: data[indexPath.row])
        return cell;
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let padding : CGFloat = 2
        let staticWidth = 45
        var textWidth : CGFloat = 0.0
        for i in 0..<data.count{
            let model = data[i];
            lblSizing.text = "\(model.count)/\(model.max)"
            lblSizing.sizeToFit()
            textWidth += lblSizing.bounds.size.width
        }
        
        var leftInset = CGFloat(((collectionView.frame.size.width - CGFloat(textWidth + CGFloat(staticWidth * data.count))) / 2) - padding)
        if(leftInset < 0) {leftInset = 2}
        return UIEdgeInsetsMake(0, leftInset, 0, leftInset)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        //Check label width
        let model = data[indexPath.row]
        let str = "\(model.count)/\(model.max)"
        lblSizing.text = str
        lblSizing.sizeToFit()
        var _size = lblSizing.frame.size;
        _size.width += 80.0
        _size.height = 35
        return _size
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    func loadData()
    {
        if(isLoading)
        {
            return
        }
        isLoading = true
        
        EventDAO.User_CurrentIdolQuestInfo(starId!) { (result) in
            if(result.ErrorCode == 0 && result.DataObject != nil)
            {
                self.model = result.DataObject as? IdolQuestModel
                self.data = self.model!.giftInfo!
                
                
                self.updateUI()
                self.isLoading = false
            }
        }
    }
    
    func updateUI()
    {
//        self.lbMissionName?.text = model?.questInfo?.name
//        self.lbLastHit?.text = model?.userLastHitInfo?.name
        
        setLevel((model?.questInfo?.level)!);
        
        self.colProgressMission?.reloadData()
        self.lblTotalStar.text = "\(self.totalStar)"
        /*
        if(self.data.count == 1)
        {
            let center = (self.colProgressMission?.frame.size.width)! / 2 - 125/2;
            self.colProgressMission?.contentInset = UIEdgeInsets(top: 2, left: center, bottom: 0, right: 10)
        }
        else
        {
            self.colProgressMission?.contentInset = UIEdgeInsets(top: 2, left: 10, bottom: 0, right: 10)
        }
        */
        
        // layout for User
        if(starId != SettingsHelper.getUserID())
        {
            leftContraintBtReward?.constant = 77;
            btCancel?.hidden = true
        }
        else
        {
            leftContraintBtReward?.constant = 0;
            btCancel?.hidden = false
        }
    }
    
    func setLevel(level: Int)
    {
        for i in 0..<level
        {
            btLevels[i].setImage(UIImage(named: "ic_event_mission_unit_p"), forState: .Normal);
        }
        
        for i in level..<5
        {
            btLevels[i].setImage(UIImage(named: "ic_event_mission_unit"), forState: .Normal);
        }
    }
    
    @IBAction func btRewardInfo(sender: AnyObject)
    {
        if(delegate != nil)
        {
            delegate!.detail_rewardInfo()
        }
    }
    
    @IBAction func btCancel(sender: AnyObject)
    {
        if(delegate != nil)
        {
            let alert = UIAlertView(title: "Thông báo", message: "Bạn muốn hủy nhiệm vụ?", delegate: self, cancelButtonTitle: "Đồng ý", otherButtonTitles: "Không");
            alert.show();
        }
    }
    
    @IBAction func btClose(sender: AnyObject)
    {
        if(delegate != nil)
        {
            self.delegate?.detail_close()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0)
        {
            EventDAO.User_CancelIdolQuest(scheduleId!, callback: { (result) in
                if(result.ErrorCode == 0)
                {
                    self.delegate?.detail_cancel()
                }
                FunctionHelper.showBannerAlertView(content: result.Message)
            })
        }
        
    }
    
    
}
