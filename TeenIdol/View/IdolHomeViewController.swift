//
//  IdolHomeViewController.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 3/20/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

class IdolHomeViewController: UIViewController, MediaStreamerProtocol, UIAlertViewDelegate {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageBackground : UIImageView!
    
    private let currentUserInfo = SettingsHelper.getUserInfo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Trang chủ"
        if currentUserInfo == nil {
            let userId = SettingsHelper.getUserID()
            if(userId > 0){ //Logged in
                UserDAO.User_GetDetailsV2(UserId: userId, callback: { (result) in
                    dispatch_async(dispatch_get_main_queue()) {
                        if(result.ErrorCode == 0 && result.DataObject != nil){
                            if let userInfo = result.DataObject as? UserModel {
                                SettingsHelper.setUserInfo(userInfo)
                                self.labelName.text = "Chào mừng Idol \((userInfo.userInfo?.Name ?? "")) đến với show diễn"
                            }
                        }
                    }
                })
            }
        } else {
            self.labelName.text = "Chào mừng Idol \((currentUserInfo?.userInfo?.Name ?? "")) đến với show diễn"
        }
        self.setupRightMenuButton()
    }

    func setupRightMenuButton()
    {
        let btn = FunctionHelper.getNavigationBarButton("ic_tabbar_profile_white")
        btn.addTarget(self, action: #selector(touchedRight), forControlEvents: .TouchUpInside)
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(customView: btn), animated: true)
    }
    
    func touchedRight(sender: AnyObject)
    {
        self.checkLogin { (loginState) in
            if(loginState == .Logged || loginState == .Successed){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                if let vc = storyboard.instantiateViewControllerWithIdentifier("ProfileViewController") as? ProfileViewController{
                    vc.hidesBottomBarWhenPushed = true
                    //vc.navigationBarHidden = true
                    //vc.navigationBar.translucent = false
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }

    @IBAction func touchShow(sender: AnyObject) {
        self.showMediaStream()
    }
    
    func mediaStreamer_onClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func showMediaStream(){
        self.checkLogin { (loginState) in
            if(loginState == .Logged || loginState == .Successed){
                
                if(!(SettingsHelper.getUserInfo()?.isStreamer)!)
                {
                    let alertView = UIAlertView(title: "Xác nhận", message: "Bạn cần đăng ký làm Idol để stream.", delegate: self, cancelButtonTitle: "Hủy", otherButtonTitles: "Đồng ý")
                    alertView.show()
                }
                else
                {
                    if let _weakInstace = MediaStreamerViewController.shareInstance(){
                        _weakInstace.delegate = self;
                        _weakInstace.isStreamer = true
                        let navigate = UINavigationController(rootViewController: _weakInstace)
                        navigate.navigationBar.tintColor = UIColor.whiteColor()
                        navigate.navigationBar.translucent = false
                        navigate.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
                        navigate.toolbarHidden = true
                        self.presentViewController(navigate, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            let link = "https://docs.google.com/forms/d/e/1FAIpQLSeV8X9GuTTzwKHaIfaZCw4VpO-wiKyDgR0ZVRny5_f1qBCfyQ/viewform"
            let vc = WebViewController(title: "Đăng ký Idol", url: link)
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }

}
