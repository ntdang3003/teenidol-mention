//
//  PopupLixiRewardTableViewCell.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/9/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

class PopupLixiRewardTableViewCell: UITableViewCell {
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var viewCotainer : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.viewCotainer.layer.borderWidth = 0.5
//        self.viewCotainer.layer.borderColor = UIColor(red: 204/255, green: 153/255, blue: 5/255, alpha: 1).CGColor
//        self.viewCotainer.layer.cornerRadius = 5
    }

    func setData(rewardName  : String){
        self.labelName.text = rewardName
    }
}
