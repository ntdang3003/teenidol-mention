//
//  ExtesionEventViewController.swift
//  TeenIdol
//
//  Created by TFL Mac on 9/7/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

extension MediaStreamerViewController {
    
    class Event: NSObject, popupEventProtocol, CNPPopupControllerDelegate {
    
        var idolInfo: ShowItem?
        var showInfo: ShowModel?
        var lbEventLevel: UILabel?
        var imgOnEvent: UIImageView?
        var viewEventContainer: UIView?
        var imgGift: UIImageView?
        var _popupEventView: UIViewController?
        var _popupControllerEvent: CNPPopupController?
        var viewLeftInfoCointainer : UIView?
        
        func setupEventUI()
        {
            self.viewEventContainer!.layer.cornerRadius = 12
            self.viewEventContainer!.layer.backgroundColor = self.viewLeftInfoCointainer!.layer.backgroundColor
            
            self.viewEventContainer!.onTap { (tapGestureRecognizer) in
                self.loadDataEventMission(1)
            }
            
            imgGift!.userInteractionEnabled = true
            imgGift!.onTap({ (tapGestureRecognizer) in
                self.showPopupEventReward()
            })
            
            if(imgGift!.layer.animationForKey("SpinAnimation") == nil){
                let animation = CABasicAnimation(keyPath: "transform.rotation")
                animation.fromValue = NSNumber(float: -0.5)
                //animation.toValue = NSNumber(double: (2 * M_PI))
                animation.toValue  = NSNumber(float: 0.5)
                animation.duration = 0.8
                animation.repeatCount = Float(CGFloat.max)
                animation.autoreverses = true
                imgGift!.layer.addAnimation(animation, forKey: "SpinAnimation")
            }
        }
        
        func checkEnableEvent()
        {
            if let config = SettingsHelper.getConfig(){
                if(config.idolQuestEnable){
                    
                    // thang 09
                    viewEventContainer!.hidden = false
                    loadDataEventMission(0)
                }
            }
        }
        
        func updateViewEvent(level level: Int, isOnMisstion: Bool)
        {
            lbEventLevel!.text = "\(level)"
            imgOnEvent!.image = (isOnMisstion == true) ? UIImage(named: "ic_event_mission_unit_p") : UIImage(named: "ic_event_mission_unit")
        }
        
        func completeViewEvent(delayTime delayTime: NSTimeInterval)
        {
            UIView.animateWithDuration(delayTime, delay: 0,
                                       options: [.AllowUserInteraction, .Repeat, .Autoreverse], animations: {
                                        self.viewEventContainer?.backgroundColor = ColorAppDefault
                }, completion: nil)
        }
        
        func touchEvent()
        {
            // thang 09
            loadDataEventMission(1)
        }
        
        // showMsg: 0, touchEvent: 1
        func loadDataEventMission(mode: Int)
        {
            let isIdol = (SettingsHelper.getUserID() == idolInfo?.StarUser.Id) ? true : false
            
            EventDAO.User_CheckIdolQuest((idolInfo?.StarUser.Id)!) { (result) in
                if(result.ErrorCode == 0)
                {
                    let data = result.DataObject as! IdolQuestStateDTO
                    var onMission = false;
                    
                    switch(data.openQuestState)
                    {
                    case 1: // full stack
                        if(isIdol)
                        {
                            FunctionHelper.showBannerAlertView(content: "Bạn đã hoàn thành chuỗi nhiệm vụ hôm nay.")
                            onMission = false
                        }
                        break
                        
                    case 2: // can get mission
                        if(isIdol)
                        {
                            //let popup = self.showPopupAcceptEvent(data)
                            //popup?.updateUI(data)
                            
                            self.showPopupAcceptEvent(data)
                            onMission = false
                        }
                        else if(mode == 1 && !isIdol)
                        {
                            FunctionHelper.showBannerAlertView(content: "Idol chưa nhận nhiệm vụ.");
                        }
                        break
                        
                    case 3: // on mission
                        
                        if(data.isComplete!)
                        {
                            // hiệu ứng
                            self.imgGift?.hidden = false;
                        }
                        
                        if(!isIdol && mode == 0)
                        {
                            //FunctionHelper.showBannerAlertView(content: "Idol đang làm nhiệm vụ.");
                        }
                        else if(mode == 1)
                        {
                            self.showPopupEventDetail()
                        }
                        
                        onMission = true
                        break
                        
                    default:
                        break
                    }
                    
                    self.updateViewEvent(level: data.level, isOnMisstion: onMission)
                    
                }
            }
        }
        
        func showPopupAcceptEvent(data: IdolQuestStateDTO)
        {
            if(_popupEventView != nil ) { return }
            
            _popupEventView = popupAccept(nibName: "popupAccept", bundle: nil)
            let con = (_popupEventView as! popupAccept)
            con.starId = (idolInfo?.StarUser.Id)!
            con.data = data;
            con.delegate = self
            
            self._popupControllerEvent = CNPPopupController(contents: [con.view])
            self._popupControllerEvent!.mainController = con
            self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
            self._popupControllerEvent!.theme.popupStyle = .Centered
            self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
            self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self._popupControllerEvent!.presentPopupControllerAnimated(true)
            self._popupControllerEvent!.delegate = self
        }
        
        func showPopupEventDetail()
        {
            if(_popupEventView != nil) { return }
            
            _popupEventView = popupDetail(nibName: "popupDetail", bundle: nil)
            let con = (_popupEventView as! popupDetail)
            con.starId = (idolInfo?.StarUser.Id)!;
            con.scheduleId = (showInfo?.Schedule?.Id);
            con.delegate = self
            con.totalStar  = (self.showInfo?.TotalStar)!
            
            self._popupControllerEvent = CNPPopupController(contents: [con.view])
            self._popupControllerEvent!.mainController = con
            self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
            self._popupControllerEvent!.theme.popupStyle = .Centered
            self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
            self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self._popupControllerEvent!.presentPopupControllerAnimated(true)
            self._popupControllerEvent!.delegate = self
        }
        
        func showPopupEventReward()
        {
            if(_popupEventView != nil) { return }
            
            _popupEventView = popupRewardInfo(nibName: "popupRewardInfo", bundle: nil)
            let con = (_popupEventView as! popupRewardInfo)
            con.starId = (idolInfo?.StarUser.Id)!;
            con.delegate = self
            
            self._popupControllerEvent = CNPPopupController(contents: [con.view])
            self._popupControllerEvent!.mainController = con
            self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
            self._popupControllerEvent!.theme.popupStyle = .Centered
            self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
            self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self._popupControllerEvent!.presentPopupControllerAnimated(true)
            self._popupControllerEvent!.delegate = self
        }
        
        func popupControllerDidDismiss(controller: CNPPopupController!) {
            if(_popupEventView != nil)
            {
                _popupEventView = nil
            }
        }
        
        //MARK: ###
        //MARK: protocol
        
        func accept_close() {
            _popupControllerEvent?.dismissPopupControllerAnimated(true);
            _popupEventView = nil
        }
        
        func accept_mission(level: Int) {
            
            EventDAO.User_IdolGetQuest((showInfo?.Schedule?.Id)!, level: level) { (result) in
                if(result.ErrorCode == 0)
                {
                    self._popupControllerEvent?.dismissPopupControllerAnimated(true);
                    self.updateViewEvent(level: level, isOnMisstion: true)
                    self._popupEventView = nil
                    self.showPopupEventDetail()
                }
                else
                {
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            }
        }
        
        func detail_cancel() {
            _popupControllerEvent?.dismissPopupControllerAnimated(true)
            _popupEventView = nil
        }
        
        func detail_rewardInfo() {
            _popupControllerEvent?.dismissPopupControllerAnimated(true);
            _popupEventView = nil
            showPopupEventReward();
        }
        
        func detail_close() {
            _popupControllerEvent?.dismissPopupControllerAnimated(true);
            _popupEventView = nil
        }
        
        func reward_get() {
            JTProgressHUD.show()
            EventDAO.User_DoneIdolQuest((showInfo?.Schedule?.Id)!) { (result) in
                JTProgressHUD.hide()
                if(result.ErrorCode == 0)
                {
                    self._popupControllerEvent?.dismissPopupControllerAnimated(true)
                    self._popupEventView = nil
                    //self.viewEventContainer.layer.removeAllAnimations()
                }
                FunctionHelper.showBannerAlertView(content: result.Message)
            }
        }
        
        func reward_back()
        {
            _popupControllerEvent?.dismissPopupControllerAnimated(true);
            showPopupEventDetail();
        }
        
        func reward_close() {
            _popupControllerEvent?.dismissPopupControllerAnimated(true);
            _popupEventView = nil
        }

        
    }
}
