//
//  ProfileViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/10/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var profileView : UserProfileView?
    var userId : Int64 = SettingsHelper.getUserID();
    var isEnableBack = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(updateUserId), name: UserDidLoginNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.setupNavigationBar()
        self.setupTabBar()
        profileView = self.view as? UserProfileView
        profileView!.parentViewController = self
        //profileView!.setupUI(userId, isEnableBack: self.isEnableBack)
        profileView!.loadData()
        
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if(self.navigationController?.navigationBarHidden == false){
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
       
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
    }
    
    func setupTabBar(){
       UITabBar.appearance().tintColor = ColorAppDefault
    }
    
    func updateUserId(){
        self.userId = SettingsHelper.getUserID()
        if(self.profileView != nil){
            self.profileView?.loadData()
        }
    }
}
