//
//  PopupLamBanhCongThucCollectionViewCell.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/20/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

class PopupLamBanhCongThucCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageCongThuc: UIImageView!
//    @IBOutlet weak var labelTitle: UILabel!
//    @IBOutlet weak var labelCongThuc: UILabel!
//    @IBOutlet weak var buttonNau: UIButton!
    var delegate : ExchangeCellDelegate!
    
    @IBOutlet weak var labelHopSua: UILabel!
    @IBOutlet weak var labelHatDe: UILabel!
    @IBOutlet weak var labelHoaCo: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 10
        self.contentView.layer.cornerRadius = 10
        //buttonNau.layer.cornerRadius = 5
    }

    func setData(model: LixiRequireModel){
        
        
        
//        self.labelTitle.text = model.name
        if let url = NSURL(string: model.photo){
            self.imageCongThuc.sd_setImageWithURL(url)
        }
        
        for ct in model.listRequire {
            switch ct.unitTypeId {
            case 12:
                labelHatDe.text = "\(ct.lixiValue)"
            case 13:
                labelHopSua.text = "\(ct.lixiValue)"
            case 14:
                labelHoaCo.text = "\(ct.lixiValue)"
            default:
                break
            }
        }
    }
    
    @IBAction func touchNau(sender: AnyObject) {
        if delegate  != nil {
            self.delegate.exchangeCellDidGet(self)
        }
    }
}
