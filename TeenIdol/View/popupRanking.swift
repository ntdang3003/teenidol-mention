//
//  popupRanking.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/22/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class popupRanking: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var lbRuby: UILabel?;
    
    var showInfo: ShowModel?
    var data =  FListResultDTO<GiveAwayModel>()
    var totalRuby: (InShow: Int, InSchedule: Int)?;
    
    private var _scheduleId:Int64 = 0;
    
    override func viewDidLoad() {
        
        if(self.showInfo != nil){
            _scheduleId = (showInfo?.Schedule?.Id)!
        }
        
        setupUI()
        setupNavigationBar()
        setupLeftMenuButton()
        loadTotalRuby()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
 
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
        
        buttonBarView.removeFromSuperview()
        buttonBarView.frame = CGRectMake(0, 0, 200, 30)
        
        let selBar = buttonBarView.selectedBar.frame;
        buttonBarView.selectedBar.frame = CGRectMake(selBar.origin.x, 28, selBar.width, selBar.height);
        
        self.navigationItem.titleView = buttonBarView
        
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(leftMenuButton), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let btn: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func leftMenuButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupUI()
    {
        settings.style.buttonBarBackgroundColor = .clearColor()
        settings.style.buttonBarItemBackgroundColor = .clearColor()
        settings.style.selectedBarBackgroundColor = .whiteColor()
        settings.style.buttonBarItemFont = UIFont.boldSystemFontOfSize(16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .blackColor()
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 10
        settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
            newCell?.label.textColor = .whiteColor()
        }
        super.viewDidLoad()
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        let child_1 = RankingTableView(style: .Plain, itemInfo: "Trong show", scheduleId: _scheduleId )
//        child_1.showInfo = showInfo
//        let child_2 = RankingTableView(style: .Plain, itemInfo: "Tất cả", scheduleId: -1)
//        child_2.showInfo = showInfo
        let child_1 = RankingTableView(nibName:nil, bundle:nil, itemInfo: "Trong show", scheduleId: _scheduleId )
        child_1.showInfo = showInfo
        let child_2 = RankingTableView(nibName:nil, bundle:nil, itemInfo: "Tất cả", scheduleId: -1)
        child_2.showInfo = showInfo
        return [child_1, child_2]
    }
    
    func updateRubyUI()
    {
//        dispatch_async(dispatch_get_main_queue(), {
//            let inShow = FunctionHelper.formatNumberWithSeperator( (self.totalRuby?.InShow)!)
//            let inSchedule = FunctionHelper.formatNumberWithSeperator( (self.totalRuby?.InSchedule)!)
//            self.lbRuby?.text = (self._scheduleId == -1) ? inShow : inSchedule ;
//        })
        let inShow = FunctionHelper.formatNumberWithSeperator( (self.totalRuby?.InShow)!)
        let inSchedule = FunctionHelper.formatNumberWithSeperator( (self.totalRuby?.InSchedule)!)
        self.lbRuby?.text = (self._scheduleId == -1) ? inShow : inSchedule ;

    }
    
    func loadTotalRuby(){
        let showId = showInfo?.ShowInfo?.Id
        UserDAO.User_GetTotalCoinUsed(_scheduleId, showId: showId!) { (result) in
            self.totalRuby = result;
            self.updateRubyUI()
        }
    }

}
