//
//  PopupLixiExchangeTableViewCell.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/6/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

protocol ExchangeCellDelegate {
    func exchangeCellDidGet(cell: PopupLamBanhCongThucCollectionViewCell)
}

class PopupLixiExchangeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonExchange: UIButton!
    @IBOutlet weak var viewCotainer: UIView!
    @IBOutlet weak var labelName: UILabel!
    var delegate : ExchangeCellDelegate!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewCotainer.layer.borderWidth = 0.5
        self.viewCotainer.layer.borderColor = UIColor(red: 204/255, green: 153/255, blue: 5/255, alpha: 1).CGColor
        self.viewCotainer.layer.cornerRadius = 5
        buttonExchange.layer.cornerRadius = 5
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model: LixiRequireModel){
        let total = FunctionHelper.formatNumberWithSeperator(model.listRequire[0].lixiValue)
        self.buttonExchange.setTitle(total, forState: .Normal)
        self.labelName.text = model.name
    }
    
    @IBAction func touchGetLixi(sender: AnyObject) {
        if self.delegate != nil {
            //self.delegate.exchangeCellDidGet(self)
        }
    }
}
