//
//  rankingTableViewCell.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/22/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class rankingTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBorderAvata: UIImageView?;
    @IBOutlet weak var imgAvatar: UIImageView?;
    @IBOutlet weak var lbName: UILabel?;
    @IBOutlet weak var imgRanking: UIImageView?;
    @IBOutlet weak var imgLevel: UIImageView?;
    @IBOutlet weak var lbRuby: UILabel?;
    @IBOutlet weak var imgRuby: UIImageView?
    @IBOutlet weak var lbLeader: UILabel?
    @IBOutlet weak var width_ImgLevel: NSLayoutConstraint?
    @IBOutlet weak var view_IdolLive: UIView?
    
    var guildId: Int64 = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .None
        FunctionHelper.circleBorderImageView(imgAvatar!,color: ColorAppDefault.CGColor, borderWidth: 3);
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: GiveAwayModel, index: Int)
    {
        lbName?.text = data.senderUser?.Name
        lbRuby!.text = FunctionHelper.formatNumberWithSeperator((data.totalPrice))
        
        if let url  = NSURL(string: (data.senderUser?.AvatarPhoto)!){
            imgAvatar?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        if let url  = NSURL(string: (data.senderUserLevel?.photo)!){
            imgLevel?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_level_user_placeholder"))
        }
        
        let strIndex = "\(index + 1)";
        let imgBorder = "ic_event_rank" + strIndex;
        imgBorderAvata?.image = UIImage(named: imgBorder)
        let imgRankTop = "ic_ranktop" + strIndex;
        imgRanking?.image = UIImage(named: imgRankTop)
        
        //Check color
        if(index == 1){
            imgAvatar?.layer.borderColor = UIColor(red: 255, green: 102, blue: 0).CGColor
        } else if (index == 2){
            imgAvatar?.layer.borderColor = UIColor(red: 41, green: 167, blue: 167).CGColor
        }
        
    }
    
    func setDataRankingGuild(data: GuildModel, index: Int)
    {
        lbName?.text = data.guildInfo!.Name
        lbLeader?.text = data.owner?.Name
        //lbRuby!.text = FunctionHelper.formatNumberWithSeperator(data.guildInfo!.Ruby)
        
        if let url  = NSURL(string: (data.guildInfo?.PhotoLink)!){
            imgAvatar?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        if let url  = NSURL(string: (data.currentLevel?.Photo)!){
            imgLevel?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_level_user_placeholder"))
        }
        
        let strIndex = "\(index + 1)";
        let imgBorder = "ic_guild_rank_top_" + strIndex;
        imgBorderAvata?.image = UIImage(named: imgBorder)
        
        self.guildId = (data.guildInfo?.Id)!
    }
    
    // danh sach mỹ nhân
    func setDataIdolGuild(data: GuildMemberModel)
    {
        lbName?.text = data.userInfo?.Name
        lbRuby?.text = FunctionHelper.formatNumberWithSeperator((data.userInfo?.TotalCoinUsed)!)
        
        if let url = NSURL(string: (data.userInfo?.AvatarPhoto)!)
        {
            imgAvatar?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
    }
    
    func setDataUserTopGuild(data: GuildMemberModel)
    {
        
        lbName?.text = data.userInfo?.Name
        lbRuby?.text = FunctionHelper.formatNumberWithSeperator((data.userGuild!.ruby))
        lbLeader?.text = data.guildPosition?.name
        
        let imgBorder = "ic_event_rank1";
        imgBorderAvata?.image = UIImage(named: imgBorder)
        
        if let url = NSURL(string: (data.userInfo?.AvatarPhoto)!)
        {
            imgAvatar?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
    }
    
}
