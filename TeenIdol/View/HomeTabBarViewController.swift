//
//  HomeTabBarViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/4/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController, UITabBarControllerDelegate,MediaStreamerProtocol, UIAlertViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setupSubView()
        self.setupTabTwo()
//        self.setupNavigationBar()
        self.setupTabBar()
        self.delegate = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        //Add Google Analytics
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Home View Controller")
        
        let builder = GAIDictionaryBuilder.createScreenView().build()
        tracker.send(builder as [NSObject : AnyObject])
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
    }
    
    //Profile button
    func setupTabTwo()
    {
//        let btn = FunctionHelper.getNavigationBarButton("ic_tabbar_profile_white")
//        btn.addTarget(self, action: #selector(self.touchedRight), forControlEvents: .TouchUpInside)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let tabTwo = storyboard.instantiateViewControllerWithIdentifier("UserProfileNavigation") as? UINavigationController {
            let tabTwoItem = UITabBarItem(title: "Profile", image: UIImage(named: "ic_tabbar_profile_white"), selectedImage: UIImage(named: "ic_tabbar_profile_white"))
            tabTwo.tabBarItem = tabTwoItem
            
            self.addChildViewController(tabTwo)
        }
        
//        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(customView: btn), animated: true)
    }
    
//    @objc private func touchedRight(sender: AnyObject)
//    {
//        self.checkLogin { (loginState) in
//            if(loginState == .Logged || loginState == .Successed){
//                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                if let vc = storyboard.instantiateViewControllerWithIdentifier("ProfileViewController") as? ProfileViewController{
//                    vc.hidesBottomBarWhenPushed = true
//                    //vc.navigationBarHidden = true
//                    //vc.navigationBar.translucent = false
//                    self.navigationController?.pushViewController(vc, animated: true)
//                }
//                //let vc = storyboard.instantiateViewControllerWithIdentifier("someViewController") as! UIViewController
//                //self.presentViewController(vc, animated: true, completion: nil)
//                
//                
//            }
//        }
//    }
    
    func setupTabBar(){
        UITabBar.appearance().tintColor = ColorAppDefault
        self.tabBar.backgroundImage = UIImage(named: "bg_tabbar")
    }
    
    func setupSubView(){
        let streamImg = UIImage(named: "ic_home_stream")
        let buttonView = UIButton()
        buttonView.setImage(streamImg!, forState: .Normal)
        buttonView.addTarget(self, action: #selector(showMediaStream), forControlEvents: .TouchUpInside)
        buttonView.frame = CGRect(x: 0,y: 0,width: 60,height: 60)
        buttonView.center = CGPoint(x: self.tabBar.frame.size.width/2, y: 15)
        self.tabBar.addSubview(buttonView)
    }
    func showMediaStream(){
        self.checkLogin { (loginState) in
            if(loginState == .Logged || loginState == .Successed){
              
                if(!(SettingsHelper.getUserInfo()?.isStreamer)!)
                {
                    let alertView = UIAlertView(title: "Xác nhận", message: "Bạn cần đăng ký làm Idol để stream.", delegate: self, cancelButtonTitle: "Hủy", otherButtonTitles: "Đồng ý")
                    alertView.show()
                }
                else
                {
                    if let _weakInstace = MediaStreamerViewController.shareInstance(){
                        _weakInstace.delegate = self;
                        _weakInstace.isStreamer = true
                        let navigate = UINavigationController(rootViewController: _weakInstace)
                        navigate.navigationBar.tintColor = UIColor.whiteColor()
                        navigate.navigationBar.translucent = false
                        navigate.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
                        navigate.toolbarHidden = true
                        self.presentViewController(navigate, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 1){
            let link = "https://docs.google.com/forms/d/e/1FAIpQLSeV8X9GuTTzwKHaIfaZCw4VpO-wiKyDgR0ZVRny5_f1qBCfyQ/viewform"
            let vc = WebViewController(title: "Đăng ký Idol", url: link)
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    func mediaStreamer_onClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        let index = tabBarController.viewControllers?.indexOf(viewController)
        if(index == 9){
            showMediaStream()
            return false
        }
        return true
    }
}
