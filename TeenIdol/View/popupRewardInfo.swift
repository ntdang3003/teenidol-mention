//
//  popupRewardInfoViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class popupRewardInfo: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var btGetReward: UIButton?;
    @IBOutlet weak var colIdolReward: UICollectionView?;
    @IBOutlet weak var colUserReward: UICollectionView?;
    
    var delegate: popupEventProtocol? = nil;
    
    var idolRewards: [IdolQuestMissionGift]? = [];
    var userRewards: [IdolQuestMissionGift]? = [];
    var starId: Int64 = 0;
     private let lblSizing = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        colIdolReward?.delegate = self
        colIdolReward?.dataSource = self
        
        colUserReward?.delegate = self
        colUserReward?.dataSource = self
        
        colUserReward?.registerNib(UINib(nibName: "IdolQuestGiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "giftCell")
        colIdolReward?.registerNib(UINib(nibName: "IdolQuestGiftCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "giftCell")
        lblSizing.frame = CGRect(x: 0, y: 0, width: 100, height: 35)
        lblSizing.textAlignment = .Center
        lblSizing.font = UIFont.systemFontOfSize(14.0)
        lblSizing.contentMode = .Left

    }
    
    override func viewWillAppear(animated: Bool) {
        LoadData()
    }
    
    func LoadData()
    {
        JTProgressHUD.show()
        EventDAO.User_GetRewardInfo(starId) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
                let model = result.DataObject as! IdolQuestModel
                self.idolRewards = model.listIdolReward
                self.userRewards = model.listUserReward
                
                self.UpdateUI(model);
            }
            else
            {
                FunctionHelper.showBannerAlertView(content: result.Message);
            }
        }
    }
    
    func UpdateUI(data: IdolQuestModel)
    {
        /*
        if(self.idolRewards!.count == 1)
        {
            let center = (self.colIdolReward?.frame.size.width)! / 2 - 125/2;
            self.colIdolReward?.contentInset = UIEdgeInsets(top: 2, left: center, bottom: 0, right: 10)
        }
        else
        {
            self.colIdolReward?.contentInset = UIEdgeInsets(top: 2, left: 10, bottom: 0, right: 10)
        }
        
        if(self.userRewards!.count == 1)
        {
            let center = (self.colUserReward?.frame.size.width)! / 2 - 125/2;
            self.colUserReward?.contentInset = UIEdgeInsets(top: 2, left: center, bottom: 0, right: 10)
        }
        else
        {
            self.colUserReward?.contentInset = UIEdgeInsets(top: 2, left: 10, bottom: 0, right: 10)
        }
        */
        colIdolReward?.reloadData()
        colUserReward?.reloadData()
        
        if(starId != SettingsHelper.getUserID())
        {
            btGetReward?.setTitle("  Tiếp tục nhiệm vụ", forState: .Normal)
        }
        else
        {
            btGetReward?.setTitle("  Nhận thưởng", forState: .Normal)
            if(data.idolQuestState == 2)
            {
                btGetReward?.enabled = true
            }
            else
            {
                btGetReward?.enabled = false
            }
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == colIdolReward)
        {
            return self.idolRewards!.count + 1;
        }
        return self.userRewards!.count;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("giftCell", forIndexPath: indexPath) as! IdolQuestGiftCollectionViewCell;
        if(collectionView == colIdolReward)
        {
            let count = self.idolRewards!.count
            if(count > 0 && indexPath.row != (count)){
                cell.setData(displayType: .Idol, data: idolRewards![indexPath.row])
            }
            return cell;
        }
        cell.setData(displayType: .User, data: userRewards![indexPath.row])
        return cell;
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
//        let padding : CGFloat = 2
//        let staticWidth = 45
//        var textWidth : CGFloat = 0.0
//        var leftInset :CGFloat = 2
//        if(collectionView == colIdolReward){
//            for i in 0..<idolRewards!.count{
//                let model = idolRewards![i];
//                lblSizing.text = "\(model.max)"
//                lblSizing.sizeToFit()
//                textWidth += lblSizing.bounds.size.width
//            }
//            leftInset = CGFloat(((collectionView.frame.size.width - CGFloat(textWidth + CGFloat(staticWidth * idolRewards!.count))) / 2) - padding)
//        }else{
//            for i in 0..<userRewards!.count{
//                let model = userRewards![i];
//                lblSizing.text = "\(model.Description)"
//                lblSizing.sizeToFit()
//                textWidth += lblSizing.bounds.size.width
//            }
//            leftInset = CGFloat(((collectionView.frame.size.width - CGFloat(textWidth + CGFloat(staticWidth * userRewards!.count))) / 2) - padding)
//        }
//        
//        if(leftInset < 0) {leftInset = 2}
//        return UIEdgeInsetsMake(0, leftInset, 0, leftInset)
//    }
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        
//        //Check label width
//        var str = " + ?"
//        if(collectionView == colIdolReward)
//        {
//            let count = (idolRewards?.count)!
//            if (count > 0 && indexPath.row != count){
//                let model =
//                    idolRewards![indexPath.row]
//                str = "\(model.max)"
//            }
//        }else{
//            let model =
//                userRewards![indexPath.row]
//           // str = "\(model.count)"
//            str = model.Description
//        }
//        
//        lblSizing.text = str
//        lblSizing.sizeToFit()
//        var _size = lblSizing.frame.size;
//        _size.width += 54.0
//        _size.height = 35
//        return _size
//    }
    
    @IBAction func btGetReward(sender: AnyObject)
    {
        if(delegate != nil)
        {
            if(starId != SettingsHelper.getUserID())
            {
                delegate?.reward_close()
            }
            else
            {
                delegate?.reward_get()
            }
        }
    }
    
    @IBAction func btClose(sender: AnyObject)
    {
        if(delegate != nil)
        {
            delegate?.reward_close()
        }
    }
    
    
    /*
     
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
     
    */

}
