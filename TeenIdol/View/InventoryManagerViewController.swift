//
//  InventoryManagerViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/28/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class InventoryManagerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    @IBOutlet weak var colMain : UICollectionView!
    private var data = FListResultDTO<AnimationInventoryModel>()
    private var _popupController : CNPPopupController?
    private var _selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colMain.registerNib(UINib(nibName: "InventoryManagerCollectionViewCell", bundle:  nil), forCellWithReuseIdentifier: "InventoryManagerIndentifier")
        self.setupLeftMenuButton()
        self.title = "Trang bị"

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.cleanUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupLeftMenuButton(){
        let button = FunctionHelper.getNavigationBarButton("ic_arrow_left")
        button.addTarget(self, action: #selector(leftButtonPress), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem( UIBarButtonItem(customView:
            button), animated: true)
    }
    
    func leftButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func cleanUp(){
        self.data = FListResultDTO<AnimationInventoryModel>()
        self._selectedIndex = 0
    }
    
    func loadData(){
        if(data.isLoadingData) {
            return
        }
        data.isLoadingData = true
        if(self.data.iPageIndex == 0){
            JTProgressHUD.show()
        }
        UserDAO.User_GetListAnimationItem(SettingsHelper.getUserID(), pageIndex: self.data.iPageIndex, pageSize: 15) { (result) in
            JTProgressHUD.hide()
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            self.data.isLoadingData = false
            self.data.iPageIndex += 1
            self.colMain.reloadData()
        }
    }
    
    

    //MARK: Collection DataSource & Delegate
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if(kind == UICollectionElementKindSectionFooter){
            if(self.data.CountAllResult != self.data.ListItem.count){
                let indicator = UICollectionReusableView()
                indicator.addSubview(UIActivityIndicatorView())
                return indicator
            }
        }
        return UICollectionReusableView()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.ListItem.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == self.data.ListItem.count - 1){
            self.loadData()
        }
        
        let cell = colMain.dequeueReusableCellWithReuseIdentifier("InventoryManagerIndentifier", forIndexPath: indexPath) as! InventoryManagerCollectionViewCell
        let data = self.data.ListItem[indexPath.row]
        cell.setData(data)
        
        if(data.animationInventory?.isDefault == true){
            self._selectedIndex = indexPath.row
            //self._selectedCell = cell
            cell.setCellSelected(true)
        }else{
            cell.setCellSelected(false)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let cell = collectionView.cellForItemAtIndexPath(indexPath) as? InventoryManagerCollectionViewCell {
            if cell.state == .Invalid{
                let vc = InventoryShopViewController(nibName:  "InventoryShopViewController", bundle: nil)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
//                let con = PopupBuyAnimationItemViewController(nibName: "PopupBuyAnimationItemViewController", bundle: nil) as PopupBuyAnimationItemViewController
//                con.delegate = self
//                self._popupController = CNPPopupController(contents: [con.view])
//                self._popupController!.mainController = con
//                self._popupController!.theme = CNPPopupTheme.defaultTheme()
//                //self._popupController!.theme.maxPopupWidth = con.view.bounds.size.width
//                self._popupController!.theme.maxPopupWidth = 280.0
//                self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//                self._popupController!.theme.popupStyle = .Centered
//                self._popupController!.presentPopupControllerAnimated(true)
//                con.model = AnimationItemModel(inventoryModel: self.data.ListItem[indexPath.row]);
                return false
            }
        }
        return true
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(self._selectedIndex == indexPath.row){ return }
        
        let data = self.data.ListItem[indexPath.row]
        JTProgressHUD.show()
        UserDAO.User_ChoseAnimationDefaultItem((data.animationItem?.Id)!, rootCategoryId: (data.itemCategory?.Id)!, callback: { (result) in
            if(result.ErrorCode == 0){
                self.data = FListResultDTO<AnimationInventoryModel>()
                self.loadData()
            }
            FunctionHelper.showBannerAlertView(content: result.Message)
        })
//        self._selectedIndex = indexPath.row
//        self._selectedCell?.setCellSelected(false)
//        self._selectedCell = collectionView.cellForItemAtIndexPath(indexPath) as! InventoryManagerCollectionViewCell
        
    }
}
