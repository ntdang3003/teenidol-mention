//
//  PopupLamBanhCollectionViewCell.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/20/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

class PopupLamBanhCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageNguyenLieu: UIImageView!
    @IBOutlet weak var labelCount: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.layer.cornerRadius = 5
    }

    func setData(model: NguyenLieuModel){
        self.labelCount.text = "\(model.userGranary.lixiValue)"
        self.labelName.text = model.name
        if let url = NSURL(string: model.photo) {
            self.imageNguyenLieu.sd_setImageWithURL(url)
        }
    }
}
