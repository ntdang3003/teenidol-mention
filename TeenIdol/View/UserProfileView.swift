//
//  UserProfileView.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import FBSDKCoreKit

@objc protocol UserProfileHeaderProtocol {
    optional func userHeader_onTouchedEditProfile()
    optional func userHeader_onTouchedBack()
    optional  func userHeader_onTouchedFacebook()
    optional func userHeader_onTouchedMoney()
    optional func userHeader_onTouchedAvatar()
    optional func userHeader_onTouchedLogout()
}

class UserProfileHeaderView : UIView{
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblRuby: UILabel!
    @IBOutlet weak var imgRuby: UIImageView!
    
    private var _currentUserInfo : UserModel? = nil
    private var _urlFB: (canOpen: Bool, url: String)? = nil
    private var _isMine = false
    
    var delegate : UserProfileHeaderProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        FunctionHelper.circleBorderImageView(imgAvatar,color: UIColor.clearColor().CGColor,borderWidth: 2, cornerRadius: 45.0)
        self.imgAvatar.userInteractionEnabled = true
        self.imgAvatar.onTap { (tapGestureRecognizer) in
            self.editAvatar()
        }
    }
    
    func setUserInfo(userId: Int64, userInfoModel: UserModel){
        self._currentUserInfo = userInfoModel
        self.updateUI()
    }
    
    
    func getUrlFB(accountInfo: UserModel) -> (canOpen: Bool, url: String)
    {
        
        let socialData = accountInfo.socialDataInfo
        var canOpenFB = false;
        var urlFB = ""
        if(!((socialData?.facebookToken.isEmpty)!) || !((accountInfo.starDataInfo?.facebook.isEmpty)!))
        {
            canOpenFB = true
            if( UIApplication.sharedApplication().canOpenURL(NSURL(string: "fb://")!) && !((socialData?.facebookToken.isEmpty)!))
            {
                urlFB = String("fb://profile/",socialData?.facebookId)
            }
            else
            {
                urlFB = (accountInfo.starDataInfo?.facebook)!;
            }
        }
        else
        {
            canOpenFB = false
        }
        
        return(canOpenFB,urlFB)
    }

    
    func updateUI()
    {
        if let accountInfo = _currentUserInfo{
            if let url = NSURL(string: (accountInfo.userInfo?.AvatarPhoto)!){
                self.imgAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
            }
            self.lblName.text = accountInfo.userInfo?.Name

            self.lblRuby.text = FunctionHelper.formatNumberWithSeperator((accountInfo.userInfo?.GroupUserId == .Star ?  accountInfo.starDataInfo?.coinGetFromGift : accountInfo.userInfo?.TotalCoinUsed)! )
        }
    }
    
        
    
    @IBAction func touchedBack(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.userHeader_onTouchedBack!()
        }
    }
    
    @IBAction func touchedFacebook(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.userHeader_onTouchedFacebook!()
        }
    }
    
    @IBAction func touchedEditProfile(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.userHeader_onTouchedEditProfile!()
        }
    }
    
    @IBAction func touchedMoney(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.userHeader_onTouchedMoney!()
        }
    }
    
    @IBAction func touchedLogout(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.userHeader_onTouchedLogout!()
        }
    }
    
    func editAvatar()
    {
        if(self.delegate != nil && self.delegate?.userHeader_onTouchedAvatar != nil ){
            self.delegate?.userHeader_onTouchedAvatar!()
        }
    }
    
}

class UserProfileView: UIView, UITableViewDelegate, UIAlertViewDelegate, UserProfileHeaderProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate  {

    @IBOutlet weak var parentViewController: ProfileViewController?
    
    var viewHeader: UserProfileHeaderView?
    var followController: FollowViewController!
    var data: DoResultDTO? = nil;
    var currentUserId:Int64 = -1
    var currentUserInfo : UserModel?
    
    var _isMine = true
    
    var _popupController : CNPPopupController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("UserProfileView", owner: self, options: nil)![0] as! UIView
        self.addSubview(viewTemp)
        viewTemp.frame = self.bounds
        viewHeader = NSBundle.mainBundle().loadNibNamed("UserProfileHeaderView", owner: UserProfileHeaderView.self, options: nil)![0] as? UserProfileHeaderView
        viewHeader!.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        viewHeader!.delegate = self
        viewHeader?.frame = viewTemp.bounds
        viewTemp.addSubview(viewHeader!)
    }
    
    //MARK: - Header Delegate
    func userHeader_onTouchedEditProfile() {
        let vc =  ProfileEditViewController(nibName: "ProfileEditViewController", bundle: nil)
        self.parentViewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func userHeader_onTouchedMoney() {
        let vc = DomainViewController(nibName: "DomainViewController", bundle: nil)
        parentViewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func userHeader_onTouchedLogout() {
        let alert = UIAlertView(title: "LiveIdol", message: "Bạn muốn đăng xuất?", delegate: self, cancelButtonTitle: "Đồng ý", otherButtonTitles: "Hủy")
        alert.show()
    }
    
    func userHeader_onTouchedBack() {
        parentViewController?.navigationController?.popViewControllerAnimated(true)
    }
    
    func userHeader_onTouchedAvatar() {
        openAlbum()
    }
    
    
    
    // Open Album
    
    func openAlbum()
    {
        let pickVC = UIImagePickerController()
        pickVC.delegate = self
        pickVC.sourceType = .PhotoLibrary
        pickVC.allowsEditing = true
        parentViewController!.presentViewController(pickVC, animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        parentViewController!.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        let photo64 = UIImagePNGRepresentation(image)?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        
        // loading
        
        UserDAO.User_UploadPhoto(BasePhoto64: photo64!) { (result) in
            
            if(result.ErrorCode == 0)
            {
                SettingsHelper.getUserInfo()?.userInfo = result.DataObject as? UserDTO
                self.UpdateUI()
                
                // update info
                UserDAO.User_UpdateTeenIdolAccount(UserInfo: (SettingsHelper.getUserInfo()?.userInfo)!) { (result) in
                    
                    FunctionHelper.showBannerAlertView("LiveIdol", content: result.Message, callback: nil)
                }
            }
            
            FunctionHelper.showBannerAlertView("Thông báo", content: result.Message, callback: nil)
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //MARK: Setup
    
    func loadData()
    {
        data = DoResultDTO();
        if(self._isMine){
            self.currentUserInfo = SettingsHelper.getUserInfo()
        }
        if(self.currentUserInfo != nil){
            self.UpdateUI()
        }
        else{
            JTProgressHUD.show()
            UserDAO.User_GetDetailsV2(UserId: currentUserId, callback: { (result) in
                JTProgressHUD.hide()
                if(result.ErrorCode == 0){
                    self.currentUserInfo = result.DataObject as? UserModel
                    self.UpdateUI()
                }
            })
        }
    }

    func UpdateUI()
    {
        viewHeader?.setUserInfo((currentUserInfo?.userInfo?.Id)!, userInfoModel: currentUserInfo!)
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0)
        {
            SignOut()
        }
    }
    
    func SignOut()
    {
        if(SettingsHelper.getUserID() > 0)
        {
            UserDAO.User_LogoutTeenIdol()
            SettingsHelper.signOut()
            FunctionHelper.showBannerAlertView(content: "Bạn vừa đăng xuất.")
            self.userHeader_onTouchedBack()
            NSNotificationCenter.defaultCenter().postNotificationName(UserDidLogoutNotification, object: nil)
        }
    }
}

class ProfileFunctionTableViewCell : UITableViewCell {
    
}
