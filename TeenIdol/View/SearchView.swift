//
//  SearchView.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/10/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class SearchView: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate,
MediaStreamerProtocol, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet var tbSearch: UITableView!
    @IBOutlet var txtSearch: UITextField!
    var data = FListResultDTO<UserModel>()
    var keyboardSize: CGRect?
    var _isKeyboardShowed: Bool?;
    
    private var touchTapHideKeyboard: UITapGestureRecognizer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
    }
    
    override func viewDidLoad() {
        
        self.setupObserver()
        
        self.tbSearch.dataSource = self;
        self.tbSearch.delegate = self;
        
        tbSearch.emptyDataSetSource = self
        tbSearch.emptyDataSetDelegate = self
        
        self.tbSearch.registerNib(UINib.init(nibName: "IdolInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "IdolCell")
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 21, height: 16))
        
        let imageView = UIImageView(frame: CGRect(x: 5, y: 0, width: 16, height: 16))
        imageView.image = UIImage(named: "ic_search_black")
        imageView.contentMode = .Center
        leftView.addSubview(imageView)
        txtSearch.leftView = leftView
        txtSearch.leftViewMode = .Always
    }
    
    deinit{
        self.removeObserver()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBarHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.hidesBarsOnTap = false
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Search View Controller")
        let builder = GAIDictionaryBuilder.createScreenView().build()
        tracker.send(builder as [NSObject : AnyObject])
        txtSearch.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.removeObserver()
    }
    
    func touchHideKeyboard()
    {
        txtSearch.resignFirstResponder()
        self.tbSearch.removeGestureRecognizer(touchTapHideKeyboard!)
    }
    
    func setupObserver()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboarWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboarWillHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func removeObserver(){
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func keyboarWillShow(aNotification: NSNotification){
        keyboardSize = (aNotification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        _isKeyboardShowed = true
    }
    
    func keyboarWillHide(aNotification: NSNotification){
        _isKeyboardShowed = false
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let point = touch.locationInView(self.view)
        if(_isKeyboardShowed!){
            if(point.y < keyboardSize?.origin.y){
                self.txtSearch.resignFirstResponder()
            }
        }
    }

    
    @IBAction func txtSearchChanged(sender: UITextField) {
        self.data.iPageIndex = 0
        if(sender.text?.characters.count >= 2){
            clearSource()
//            touchTapHideKeyboard = UITapGestureRecognizer(target: self, action: #selector(touchHideKeyboard))
//            self.tbSearch.addGestureRecognizer(touchTapHideKeyboard!)
            self.performSelector(#selector(loadData), withObject: nil, afterDelay: 1.0)
        }
    }
    
    
    func clearSource()
    {
        self.data.ListItem.removeAll()
        self.tbSearch.reloadData()
    }
    
    func loadData()
    {
        if(txtSearch.text?.characters.count > 1)
        {
            if (self.data.isLoadingData) {return}
            self.data.isLoadingData = true
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            UserDAO.User_GetListUserV2(PageIndex: self.data.iPageIndex, PageSize: 10, Str_Search: txtSearch.text!) { (result) in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                self.data.CountAllResult = result.CountAllResult
                self.data.ListItem.appendContentsOf(result.ListItem)
                
                if(self.data.iPageIndex == 0)
                {
                    self.tbSearch.reloadData()
                }
                else
                {
                    var arrIndex = Array<NSIndexPath>()
                    let currentRow = self.data.ListItem.count - result.ListItem.count
                    for index in 0..<result.ListItem.count{
                        arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                    }
                    self.tbSearch.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Fade)
                }
                self.tbSearch.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.CountAllResult == self.data.ListItem.count)
                self.data.iPageIndex = self.data.iPageIndex + 1
                self.data.isLoadingData = false
            }    
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        if(self.data.CountAllResult != self.data.ListItem.count &&  indexPath.row == self.data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("IdolCell", forIndexPath: indexPath) as! IdolInfoTableViewCell
        cell.setData(data.ListItem[indexPath.row])
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! IdolInfoTableViewCell
        let model = self.data.ListItem[indexPath.row]
        SettingsHelper.setCurrentShowIndex(indexPath.row)
        let _weakSelf = self
        
        //if(cell.isLive)
        if(true)
        {

        dispatch_async(dispatch_get_main_queue(), {
            let _weakInstace = MediaStreamerViewController.shareInstance()
            if(_weakInstace != nil){
                _weakInstace?.isStreamer = false
                _weakInstace?._imgBackground = cell.imageAvatar?.image
                _weakInstace!.setShowInfo(Int64((model.showInfo?.Id)!), urlStreamer: (model.showInfo?.ServerIP)!, streamKey: (model.showInfo?.StreamKey)!)
                //_weakInstace!.setShowInfo(Int64((model.ShowInfo?.Id)!), urlStreamer: "rtmp://210.245.18.43/live", streamKey: (model.ShowInfo?.StreamKey)!)
                _weakInstace!.delegate = _weakSelf
                
            }
            let navigate = UINavigationController(rootViewController: _weakInstace!)
            navigate.navigationBar.tintColor = UIColor.whiteColor()
            navigate.navigationBar.translucent = false
            navigate.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
            navigate.toolbarHidden = true
            _weakSelf.presentViewController(navigate, animated: true, completion: nil)
        })
        }
//        else
//        {
//            let story = UIStoryboard(name: "Main", bundle: nil)
//            let vc = story.instantiateViewControllerWithIdentifier("ProfileViewController") as! ProfileViewController
//            vc.userId = (model.userInfo?.Id)!;
//            vc.isEnableBack = true
//            self.navigationController?.pushViewController(vc, animated: true)
//            //self.presentViewController(vc, animated: true, completion: nil)
//        }
    }
    
    func mediaStreamer_onClose() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btCancel(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    // MARK: Delegate EmptyDataSet
    
    func titleForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        let attStr = NSAttributedString(string: "Không tìm thấy Idol", attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(18)])
        
        return attStr
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        return nil
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView) -> UIColor? {
        return UIColor.whiteColor()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_live_idol");
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView) -> Bool {
        if(data.ListItem.count == 0 && txtSearch.text?.characters.count != 0)
        {
            return true;
        }
        return false
    }
}
