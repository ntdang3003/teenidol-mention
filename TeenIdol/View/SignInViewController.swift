//
//  SignInViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/22/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import FBSDKLoginKit

enum SignInState : Int {
    case Closed = 0
    case Successed = 1
    case Failed = 2
    case Logged = 3
}

class SignInView: UIView, UITextFieldDelegate{
    
    @IBOutlet weak var imgUserName: UIImageView!
    @IBOutlet weak var imgPassword: UIImageView!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtUserPass: UITextField!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var rootViewController : UIViewController!
    
    var callback  : ((SignInState) -> Void)? = nil
    private var _isShowingKeyboard  = false
    private var _keyboardFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("SignInViewController", owner: self, options: nil)![0] as! UIView
        
        viewTemp.frame = self.bounds
        self.addSubview(viewTemp)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let viewTemp = NSBundle.mainBundle().loadNibNamed("SignInViewController", owner: self, options: nil)![0] as! UIView
        //viewTemp.translatesAutoresizingMaskIntoConstraints = true
        viewTemp.frame = self.bounds
        self.addSubview(viewTemp)
    }
    
    convenience init(frame: CGRect,rootViewController: UIViewController?, callback: ((SignInState) -> Void)?, closeEnable: Bool = true) {
        self.init(frame: frame)
        self.callback = callback
        self.rootViewController = rootViewController
       // self.btnClose.hidden = !closeEnable
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
        
        self.txtUserName.delegate = self
        self.txtUserPass.delegate = self
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let point = touch.locationInView(self)
        if(_isShowingKeyboard){
            if(point.y < _keyboardFrame.origin.y){
                self.endEditing(true)
            }
        }
    }
    
    func keyboardWillShow(aNotification: NSNotification){
        _keyboardFrame = (aNotification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        _isShowingKeyboard = true
    }
    
    func keyboardWillHide(aNotification: NSNotification){
        _isShowingKeyboard = false
    }
    
    deinit{
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if(textField == self.txtUserName){
            self.txtUserPass.becomeFirstResponder()
        }else{
            self.txtUserPass.resignFirstResponder()
        }
        return true
    }
    
    
    @IBAction func touchedClose(sender: AnyObject){
        if(self.callback != nil){
            callback!(.Closed)
        }
        self.rootViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func touchedForgetPassword(sender: AnyObject){
        
    }
    
    @IBAction func touchedSignUp(sender: AnyObject){
        let signupCon = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("SignUpViewControllerIndentifier") as! SignUpViewController
        self.rootViewController!.presentViewController(signupCon, animated: true, completion: nil)
    }
    
    private func loadDetail(userId: Int64){
        UserDAO.User_GetDetailsV2(UserId: userId, callback: { (resultDetail) in
            JTProgressHUD.hide()
            if(resultDetail.ErrorCode == 0){
                let model = resultDetail.DataObject as? UserModel
                SettingsHelper.setUserInfo(model!)
                let token = SettingsHelper.getDeviceToken()
                if(token.isEmpty == false){
                    UserDAO.User_InsertDeviceToken(token)
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName(UserDidLoginNotification, object:nil)
//                self.rootViewController.dismissViewControllerAnimated(true, completion: {
//                    if(self.callback != nil){
//                        self.callback!(SignInState.Successed)
//                    }
//                })
            }else{
                FunctionHelper.showBannerAlertView("LiveIdol", content: resultDetail.Message, callback: nil)
                if(self.callback != nil){
                    self.callback!(SignInState.Failed)
                }
                
            }
        })
    }
    
    @IBAction func touchLoginFacebook(sender: AnyObject) {
        let login = FBSDKLoginManager()
        //login.logInWithPublishPermissions(["public_profile","email","publish_actions"], fromViewController: rootViewController) { (result, error) in
        login.logInWithReadPermissions(["public_profile","email"], fromViewController: rootViewController) { (result, error) in
            if(error != nil){
                 FunctionHelper.showBannerAlertView("LiveIdol", content: "Có lỗi trong quá trình đăng nhập facebook!", callback: nil)
                if(self.callback != nil){
                    self.callback!(SignInState.Failed)
                }
            }
            else if (result.isCancelled){
                FunctionHelper.showBannerAlertView("LiveIdol", content: "Bạn không chấp nhận quyền từ facebook!", callback: nil)
                if(self.callback != nil){
                    self.callback!(SignInState.Failed)
                }
            }else{
                let userSocialData = UserSocialDataDTO()
                userSocialData.facebookId = Int64(result.token.userID)!
                userSocialData.facebookToken = result.token.tokenString
                JTProgressHUD.show()
                UserDAO.User_LoginByFacebook(userSocialData.facebookToken, callback: { (resultFacebook) in
                    
                    if let session = resultFacebook.DataObject as? UserSessionDTO where resultFacebook.ErrorCode <= 0{
                        SettingsHelper.setUserTokenKey(session.tokenKey)
                        SettingsHelper.setUserID(session.Id)
                        self.loadDetail(session.Id)
                    }
                    else{
                        JTProgressHUD.hide()
                        FunctionHelper.showBannerAlertView("LiveIdol", content: resultFacebook.Message, callback: nil)
                        if(self.callback != nil){
                            self.callback!(SignInState.Failed)
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func touchLoginTeenidol(sender: AnyObject)
    {
        //Kiểm tra dữ liệu đầu vào
        
        let strUserName = txtUserName.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if(strUserName?.isEmpty == true){
            FunctionHelper.showBannerAlertView("LiveIdol", content:"Tên đăng nhập rỗng.", callback: nil)
            return
        }
        
        let strPassword = txtUserPass.text?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        
        if(strPassword?.isEmpty == true){
            FunctionHelper.showBannerAlertView("LiveIdol", content:"Mật khẩu rỗng.", callback: nil)
            return
        }
        
        if(!SettingsHelper.isInternet())
        {
            FunctionHelper.showBannerAlertView(content: "Không có kết nối mạng.");
            return
        }
        
        JTProgressHUD.show()
        
        UserDAO.Star_LoginTeenIdol(strUserName!, password: strPassword!, ipAddress: "0.0.0.0", token: "") { (result) in
            if(result.ErrorCode <= 0)
            {
                let session = result.DataObject as! UserSessionDTO;
                SettingsHelper.setUserID(session.Id)
                SettingsHelper.setUserTokenKey(session.tokenKey)
                self.loadDetail(session.Id)
            }
            else
            {
                JTProgressHUD.hide()
                FunctionHelper.showBannerAlertView("LiveIdol", content: result.Message, callback: nil)
                if(self.callback != nil){
                    self.callback!(SignInState.Failed)
                }
            }
        }
    }
    
    @IBAction func btBack(sender: AnyObject)
    {
        self.rootViewController.navigationController?.popViewControllerAnimated(true)
    }
}


