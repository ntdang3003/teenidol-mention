//
//  HomeTableViewCell.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/22/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelRuby: UILabel!
    @IBOutlet weak var imageMain: UIImageView!
    @IBOutlet weak var conIconLiveWidth: NSLayoutConstraint!
    @IBOutlet weak var labelState: UILabel!
    @IBOutlet weak var labelShowName: UILabel!
    @IBOutlet weak var imageThuViec: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageAvatar.layer.cornerRadius = imageAvatar.frame.size.height / 2
        imageAvatar.layer.masksToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(model: UserSMModel){
        var strName = ""
        if let starInfo = model.UserInfo {
            strName = starInfo.Name
        }else{
            strName = (model.ShowInfo?.Name)!
        }
        self.labelName.text = strName
        self.labelRuby.text = FunctionHelper.formatNumberWithSeperator(model.UserStarData!.coinGetFromGift)
        self.labelShowName.text = (model.ShowInfo?.Name)!
        
        if let avatarURL = NSURL(string: FunctionHelper.getImageUrlBySize((model.UserInfo?.AvatarPhoto)!, imageSize: self.imageAvatar.frame.size)){
            self.imageAvatar.sd_setImageWithURL(avatarURL, placeholderImage: KImage.AvatarDefault)
        }
        if let mainURL = NSURL(string: FunctionHelper.getImageUrlBySize((model.ShowInfo?.Photo)!, imageSize: self.imageMain.frame.size)){
            self.imageMain.sd_setImageWithURL(mainURL, placeholderImage:  UIImage(named: "bg_home_imageloader"))
        }
        
        if model.schedule?.Status == .Casting {
           labelState.attributedText = self.getOnlineUserAttributeString(String((model.ShowInfo?.OnlineUser)!), fontSize: 14)
        }else{
            conIconLiveWidth.constant = 0
            labelState.attributedText = self.getShowOfflineAttributeString(FunctionHelper.dateToString((model.schedule?.StartTime)!, mode: 2), fontSize: 14)
            self.layoutIfNeeded()
        }
        imageThuViec.hidden = !model.sampleStar
    }
    
    private func getOnlineUserAttributeString(input: String, fontSize: CGFloat) -> NSAttributedString{
        let font = UIFont.systemFontOfSize(fontSize)
        let image = UIImage(named: "ic_onlineuser")
        let inlineAttach = FunctionHelper.inlineTextAttachment(image!, size: CGSize(width: 12,height: 12), font: UIFont.systemFontOfSize(5))
        let imageAttribute = NSAttributedString(attachment: inlineAttach)
        let rs = NSMutableAttributedString()
        rs.appendAttributedString(imageAttribute)
        let onlineUserAtt = NSAttributedString(string: " \(input)", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)])
        rs.appendAttributedString(onlineUserAtt)
        return rs
    }
    
    private func getShowOfflineAttributeString(input: String, fontSize: CGFloat) -> NSAttributedString{
        let font = UIFont.systemFontOfSize(fontSize)
        let image = UIImage(named: "ic_home_clock")
        let inlineAttach = FunctionHelper.inlineTextAttachment(image!, size: CGSize(width: 14,height: 14), font: UIFont.systemFontOfSize(7))
        let imageAttribute = NSAttributedString(attachment: inlineAttach)
        let rs = NSMutableAttributedString()
        rs.appendAttributedString(imageAttribute)
        let onlineUserAtt = NSAttributedString(string: " \(input)", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 1.0)])
        rs.appendAttributedString(onlineUserAtt)
        return rs
        
    }
}
