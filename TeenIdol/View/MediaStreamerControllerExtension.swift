//
//  MediaStreamerControllerExtension.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/6/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import Foundation

extension MediaStreamerViewController: PopupExchangeDelegate {
    
    func loadDataEventMission(mode: Int) {
        let isIdol = (SettingsHelper.getUserID() == _idolInfo?.StarUser.Id) ? true : false
        if isIdol == true { return }
        let lixiExchangeViewController = PopupExchangeLixiViewController(nibName: "PopupExchangeLixiViewController", bundle: nil)
        lixiExchangeViewController.delegate = self
        lixiExchangeViewController.starId = (_idolInfo?.StarUser.Id)!
        lixiExchangeViewController.scheduleId = (self._showInfo?.Schedule?.Id)!
        self._popupControllerEvent = CNPPopupController(contents: [lixiExchangeViewController.view])
        self._popupControllerEvent!.mainController = lixiExchangeViewController
        self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
        self._popupControllerEvent!.theme.popupStyle = .Centered
        self._popupControllerEvent!.theme.maxPopupWidth = lixiExchangeViewController.view.frame.size.width
        self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupControllerEvent!.presentPopupControllerAnimated(true)
        self._popupControllerEvent!.delegate = self
    }
    
    func popupControllerWillDismiss(controller: CNPPopupController!) {
//        if let vc = controller.mainController as? PopupLixiRewardViewController {
//            let images = vc.model.listReward.map({$0.photo})
//            
//        }
    }
    
    func loadIdolTotalGranary() {
        EventDAO.Event_GetIdolTotalGranary((_idolInfo?.StarUser.Id)!, callback: { (result) in
            if result.ListItem.count > 0 {
                let model = result.ListItem[0]
                self.lbEventLevel.text = FunctionHelper.formatNumberWithSeperator(model.total)
            }
        })
    }
    
    func popupExchangeOnClose() {
       self._popupControllerEvent?.dismissPopupControllerAnimated(true)
    }
    
    func popupExchangeOnGetReward(model: GranaryRewardModel) {
        self._popupControllerEvent?.dismissPopupControllerAnimated(true)
        
        //Present Reward Detail Popup
        let lixiReward = PopupLixiRewardViewController(nibName: "PopupLixiRewardViewController", bundle: nil)
        lixiReward.model = model
        
        self._popupControllerEvent = CNPPopupController(contents: [lixiReward.view])
        self._popupControllerEvent!.mainController = lixiReward
        self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
        self._popupControllerEvent!.theme.popupStyle = .Centered
        self._popupControllerEvent!.theme.maxPopupWidth = lixiReward.view.frame.size.width
        self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupControllerEvent!.presentPopupControllerAnimated(true)
        self._popupControllerEvent!.delegate = self
        lixiReward.popupParent = self._popupControllerEvent
    }
}

//    func loadDataEventMission(mode: Int)
//    {
//        let isIdol = (SettingsHelper.getUserID() == _idolInfo?.StarUser.Id) ? true : false
//
//        EventDAO.User_CheckIdolQuest((_idolInfo?.StarUser.Id)!) { (result) in
//            if(result.ErrorCode == 0)
//            {
//                let data = result.DataObject as! IdolQuestStateDTO
//                var onMission = false;
//
//                switch(data.openQuestState)
//                {
//                case 1: // full stack
//                    if(isIdol)
//                    {
//                        FunctionHelper.showBannerAlertView(content: "Bạn đã hoàn thành chuỗi nhiệm vụ hôm nay.")
//                        onMission = false
//                    }
//                    break
//
//                case 2: // can get mission
//                    if(isIdol)
//                    {
//                        //let popup = self.showPopupAcceptEvent(data)
//                        //popup?.updateUI(data)
//
//                        self.showPopupAcceptEvent(data)
//                        onMission = false
//                    }
//                    else if(mode == 1 && !isIdol)
//                    {
//                        FunctionHelper.showBannerAlertView(content: "Idol chưa nhận nhiệm vụ.");
//                    }
//                    break
//
//                case 3: // on mission
//
//                    if(data.isComplete!)
//                    {
//                        // hiệu ứng
//                        self._imgGift?.hidden = false;
//                    }
//
//                    if(!isIdol && mode == 0)
//                    {
//                        //FunctionHelper.showBannerAlertView(content: "Idol đang làm nhiệm vụ.");
//                    }
//                    else if(mode == 1)
//                    {
//                        self.showPopupEventDetail()
//                    }
//
//                    onMission = true
//                    break
//
//                default:
//                    break
//                }
//                self.updateViewEvent(data.level, isOnMisstion: onMission)
//            }
//        }
//    }
