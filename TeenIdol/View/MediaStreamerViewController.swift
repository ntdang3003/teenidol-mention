//
//  MediaStreamerViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/25/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SDWebImage
import FBSDKShareKit
import Instructions

enum VideoQuality : Int {
    case Low = 360
    case Medium = 450
    //case High = 720
    func miniumBandwidthRequirement() -> Double{
        switch self {
        case Low:
            return 450
        default:
            return 800
        }
    }
    static func allValues() -> Array<VideoQuality>{
            return  [Medium, Low]
    }

}

protocol MediaStreamerProtocol {
    func mediaStreamer_onClose()
}

extension MediaStreamerViewController { //Quanlity Selector extension
    func labelWith(frame: CGRect, quality: Int) -> UILabel {
        let _label = UILabel(frame: frame)
        _label.textColor = UIColor.lightGrayColor()
        _label.textAlignment = .Center
        _label.text = "\(quality)p"
        _label.tag = quality
        _label.userInteractionEnabled = true
        
        let _tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLabelQuality))
        _label.addGestureRecognizer(_tapGesture)
        return _label
    }
    
    func handleLabelQuality(sender: UITapGestureRecognizer){
        if let label = sender.view as? UILabel{
            guard label.tag != self._qualityCurrent.rawValue else { return }
            self._qualityCurrent = VideoQuality(rawValue: label.tag)!
            self._popoverQuanlity?.dismiss()
        }
       
    }
    
    func setupQuanlitySelector(){
        let _labelHeight : CGFloat = 30.0
        self._quanlitySelectorView = UIView(frame: CGRect(x: 0, y: 0, width:  75.0, height:  _labelHeight * CGFloat(self.cameraQuality.count)))
        self._quanlitySelectorView?.backgroundColor = UIColor.blackColor()
        
        
        for i in 0 ..< self.cameraQuality.count {
            let quality = self.cameraQuality[i].rawValue
            let label = self.labelWith(CGRect(x: 0, y: _labelHeight *  CGFloat(i) , width: self._quanlitySelectorView!.frame.size.width, height: _labelHeight), quality: quality)
            if quality == self._qualityCurrent.rawValue{
                label.textColor = UIColor.whiteColor()
                label.font = UIFont.boldSystemFontOfSize(label.font.pointSize + 1)
            }
            self._quanlitySelectorView?.addSubview(label)
        }
    }
    
    func changeVideoQuantity(){
        if let session = self.cameraSession{
            //session.captureDevicePosition = AVCaptureDevicePosition.Back
            session.delegate = nil
            session.running = false
            
            let lastInfoCamera = (state : session.state, isBeauty: session.beautyFace, cameraPosition : session.captureDevicePosition)
            if lastInfoCamera.state != .Stop && lastInfoCamera.state != .Error{
                session.stopLive()
            }
            
            //Change Video Quatity
            switch self._qualityCurrent.rawValue {
            case 360:
                self.cameraAudioConfig = LFLiveAudioConfiguration.defaultConfigurationForQuality(.Low)
                self.cameraVideoConfig = LFLiveVideoConfiguration.defaultConfigurationForQuality(.Low)
                break
            default:
                self.cameraAudioConfig = LFLiveAudioConfiguration.defaultConfigurationForQuality(.Medium)
                self.cameraVideoConfig = LFLiveVideoConfiguration.defaultConfigurationForQuality(.Medium)
            }
            
            self.cameraSession = LFLiveSession(audioConfiguration: self.cameraAudioConfig!, videoConfiguration: self.cameraVideoConfig!)
            self.cameraSession?.delegate = self
            self.cameraSession?.preView = self.viewPreview
            self.cameraSession?.beautyFace = lastInfoCamera.isBeauty
            if lastInfoCamera.cameraPosition != .Front{
                self.cameraSession?.captureDevicePosition = AVCaptureDevicePosition.Back
            }
            self.cameraSession?.running = true
            if lastInfoCamera.state != .Stop && lastInfoCamera.state != .Error{
                self.cameraSession?.startLive(self.cameraLiveStreamInfo!)
            }
        }
    }
}

//MARK: Player Network Checker
extension MediaStreamerViewController {
    func startCheckNetworkTimer(){
        if self.playerNetworkChecker != nil {
            self.playerNetworkChecker.invalidate()
        }
        self.playerNetworkChecker = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.checkStreamerVideoUpload), userInfo: nil, repeats: true)
    }
    
    func stopCheckNetworkTimer(){
        if self.playerNetworkChecker != nil {
            self.playerNetworkChecker.invalidate()
            self.playerNetworkChecker = nil
        }
    }
    
    func checkStreamerVideoUpload(){
        if self.isCheckingNetwork == true {return}
        self.isCheckingNetwork = true
        UserDAO.Stream_GetStreamInfo(self._strStreamerKey!) { (result) in
            if result.ErrorCode == 0 {
                if let model = result.DataObject as? StreamInfoDTO {
                    if model.videoUpload <= 0 { //Network Problem
                        self.showPlayerLoading()
                    }else{
                        self.hidePlayerLoading()
                    }
                }
            }
            self.isCheckingNetwork = false
        }
    }
    
    func showPlayerLoading(){
        self.playerLoadingActivity.startAnimating()
    }
    
    func hidePlayerLoading(){
        self.playerLoadingActivity.stopAnimating()
    }
}

class MediaStreamerViewController: UIViewController,LFLiveSessionDelegate,TFLChatInputDelegate,UserOnlineViewProtocol,PopupProfileDelegate,FBSDKSharingDelegate,StreamBeginDelegate,StreamEndProtocol,ChatMessageDelegate,
SignalRConnectorDelegate, FollowTableViewProtocol,YTPlayerViewDelegate, UIAlertViewDelegate, popupEventProtocol, CNPPopupControllerDelegate, popupReportProtocol
{
    
    @IBOutlet weak var imgIdolAvatar: UIImageView!
    @IBOutlet weak var lblIdolName: UILabel!
    @IBOutlet weak var lblStreamStatus : UILabel!
    @IBOutlet weak var lblUserOnline: UILabel!
    @IBOutlet weak var lblIdolRuby: UILabel!
    @IBOutlet weak var lbEventLevel: UILabel!
    @IBOutlet weak var imgOnEvnent: UIImageView!
    
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSwapCamera: UIButton!
    @IBOutlet weak var btnFlash: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var btnChangeVideoQuatity: UIButton!
    
    @IBOutlet weak var viewMainContainer : UIView!
    @IBOutlet weak var viewUserOnline: UserOnlineView!
    @IBOutlet weak var viewChatViewer: ChatMessageView!
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var viewChatInput: TFLChatInputView!
    @IBOutlet weak var viewButtonsContainer : UIView!
    @IBOutlet weak var viewAnimationContainer : AnimationContainerView!
    @IBOutlet weak var viewIdolInfoCointainer : UIView!
    @IBOutlet weak var viewRubyContainer : UIView!
    @IBOutlet weak var viewLeftInfoCointainer : UIView!
    @IBOutlet weak var viewSwipePanReconize : UIView!
    @IBOutlet weak var viewSeatContainer : SeatContainerView!
    @IBOutlet var _imgBackgroud : UIImageView?
    
    @IBOutlet weak var conChatInputHeight: NSLayoutConstraint!
    @IBOutlet weak var conGiftViewToTop: NSLayoutConstraint!
    @IBOutlet weak var conTopLayerToLeft: NSLayoutConstraint!
    @IBOutlet weak var conTopLayerToRight: NSLayoutConstraint!
    @IBOutlet weak var conSwipeViewToTop: NSLayoutConstraint!
    @IBOutlet weak var conSwipeViewToBottom: NSLayoutConstraint!
    @IBOutlet weak var conIdolInfoToTop: NSLayoutConstraint!
    @IBOutlet weak var conSeatViewToTop: NSLayoutConstraint!
    @IBOutlet weak var conChatContainerToBottom: NSLayoutConstraint!
    
    @IBOutlet weak var playerLoadingActivity : UIActivityIndicatorView!
    
    
    private var _popupController : CNPPopupController?
    internal var _popupControllerEvent : CNPPopupController?;
    private var _popupEventView: UIViewController!
    
    private var _eventModel: IdolGiftShowModel?;
    
    var panGestureMain : UIPanGestureRecognizer? = nil
    var _swipeGestureLeft : UISwipeGestureRecognizer? = nil
    var _swipeGestureRight : UISwipeGestureRecognizer? = nil
    private var _showId: Int64 = -1;
    private var _showName  = ""
    internal var _scheduleId : Int64 = -1
    
    private var _strStreamServer : String? = ""
    private var _strStreamerKey: String? = ""
    internal var _showInfo: ShowModel? = nil
    internal var _idolInfo: ShowItem? = nil
    private var _targetUser : SignalRUserDTO? = nil
//    private var _playerSession: IJKFFMoviePlayerController?
//    private var playerNotificationMangager = IJKNotificationManager()
    private var playerNetworkChecker : NSTimer!
    private var isCheckingNetwork : Bool = false
    private var networkCheckingCount = 0
    
    private var _scheduleStatus : ScheduleStatus = .Casting
    private var _youtubePlayer : YTPlayerView?
    
    
    private var _isKeyboardShowed = false
    private var _isChatInputShowed = false
    private var _isGiftViewShowed = false
    private var _isLoading = false
    private var _isNeedToReSetup = true
    private var _isForceStop = false
    private var _reconnectTimer : NSTimer? = nil
    private let _heartSize: CGFloat = 24
    private var _burstTimer : NSTimer?
    private var _autoBurstTimer : NSTimer?
    private var _heartViewPosition = CGPoint(x: 0,y:0)
    private var _isSettingUp = false
    var _imgBackground : UIImage?
    var isStreamer = false
    private var _isFlashEnable = false
    
    private var _centerPanBegin = CGPoint(x: 0,y: 0)
    private var _panDirection : UISwipeGestureRecognizerDirection?
    private var _deltaSwipe : CGFloat = 0
    private var _defaultSwipeValue : CGFloat = 0
    
    private var _signalRConnector : SignalRConnector? = nil
    private var _internetChecker : Reachability?
    private var _signalRQueue : dispatch_queue_t?
    private var _quanlitySelectorView : UIView?
    private var _qualityCurrent = VideoQuality(rawValue: 450)! {
        didSet{
            self.changeVideoQuantity()
        }
    }
    
    private var _popoverQuanlity : Popover?
    
    private var cameraSession : LFLiveSession?
    private var cameraAudioConfig : LFLiveAudioConfiguration?
    private var cameraVideoConfig : LFLiveVideoConfiguration?
    private var cameraLiveStreamInfo : LFLiveStreamInfo?
    private var cameraQuality = Array<VideoQuality>()
    
    var delegate : MediaStreamerProtocol? = nil
    static var _shareInstance : MediaStreamerViewController? = nil
    class func shareInstance() -> MediaStreamerViewController?{
        if(_shareInstance != nil){
           // _shareInstance?.cleanUp()
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            _shareInstance = storyboard.instantiateViewControllerWithIdentifier("MediaStreamerIndentifier") as?
            MediaStreamerViewController
            
        }
        NSNotificationCenter.defaultCenter().addObserver(_shareInstance!, selector: #selector(userDidLogin), name: UserDidLoginNotification, object: nil)
        
        return _shareInstance!
    }
    
    //MARK: View methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        _internetChecker = SettingsHelper.getInternetChecker()
        _signalRQueue = dispatch_queue_create("com.signalrProcessing.queue", DISPATCH_QUEUE_SERIAL)
        self.setupUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.sharedApplication().idleTimerDisabled = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if(self._imgBackgroud != nil){
            self._imgBackgroud?.image = self._imgBackground
        }
        
        let strName = self.isStreamer
            ? "Streamer \((SettingsHelper.getUserInfo()?.userInfo?.Name)!)"
            :  "Player \(self._showId)"
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: strName)
        
        let builder = GAIDictionaryBuilder.createScreenView()
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.sharedApplication().idleTimerDisabled = false
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        _internetChecker?.stopNotifier()
        self.removeObserver()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       
        if(_internetChecker?.isReachable() == true){
            if(self.isNeedToSetup()){
                if(self.isStreamer){
                    self.setupStreamer()
                }
                else{
                    self.setupPlayer()
                   //self.setupHeartBuster()
                }
                self._isNeedToReSetup = false
            }
        }else{
            FunctionHelper.showBannerAlertView(content: "Không có kết nối Internet")
        }
        do {
            try _internetChecker?.startNotifier()
        }catch{
            FunctionHelper.showBannerAlertView(content: "Start Notifier failed")
        }
        
        self.setupObserver()
    }
    
    //Mark: Ranking in Show
    
    func showRankingViewController()
    {
        let con = popupRanking(nibName: "popupRanking", bundle: nil)
        con.showInfo = _showInfo!
        
        self.navigationController?.pushViewController(con, animated: true)
    }
    
    //MARK: PopupEvent
    
    func updateViewEvent(level: Int, isOnMisstion: Bool)
    {
        self.lbEventLevel.text = "\(level)"
        self.imgOnEvnent.image = (isOnMisstion == true) ? UIImage(named: "ic_event_active") : UIImage(named: "ic_event_disable")
    }
    
    func showPopupAcceptEvent(data: IdolQuestStateDTO)
    {
        if(_popupEventView != nil ) { return }
        
        _popupEventView = popupAccept(nibName: "popupAccept", bundle: nil)
        let con = (_popupEventView as! popupAccept)
        con.starId = (_idolInfo?.StarUser.Id)!
        con.data = data;
        con.delegate = self
        
        self._popupControllerEvent = CNPPopupController(contents: [con.view])
        self._popupControllerEvent!.mainController = con
        self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
        self._popupControllerEvent!.theme.popupStyle = .Centered
        self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupControllerEvent!.presentPopupControllerAnimated(true)
        self._popupControllerEvent!.delegate = self
    }
    
    func showPopupEventDetail()
    {
        if(_popupEventView != nil) { return }
        
        _popupEventView = popupDetail(nibName: "popupDetail", bundle: nil)
        let con = (_popupEventView as! popupDetail)
        con.starId = (_idolInfo?.StarUser.Id)!;
        con.scheduleId = (_showInfo?.Schedule?.Id);
        con.delegate = self
        con.totalStar = (self._showInfo?.TotalStar)!
        
        self._popupControllerEvent = CNPPopupController(contents: [con.view])
        self._popupControllerEvent!.mainController = con
        self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
        self._popupControllerEvent!.theme.popupStyle = .Centered
        self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupControllerEvent!.presentPopupControllerAnimated(true)
        self._popupControllerEvent!.delegate = self
    }
    
    func showPopupEventReward()
    {
        if(_popupEventView != nil) { return }
        
        _popupEventView = popupRewardInfo(nibName: "popupRewardInfo", bundle: nil)
        let con = (_popupEventView as! popupRewardInfo)
        con.starId = (_idolInfo?.StarUser.Id)!;
        con.delegate = self
        
        self._popupControllerEvent = CNPPopupController(contents: [con.view])
        self._popupControllerEvent!.mainController = con
        self._popupControllerEvent!.theme = CNPPopupTheme.defaultTheme()
        self._popupControllerEvent!.theme.popupStyle = .Centered
        self._popupControllerEvent!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupControllerEvent!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupControllerEvent!.presentPopupControllerAnimated(true)
        self._popupControllerEvent!.delegate = self
    }
    
    func popupControllerDidDismiss(controller: CNPPopupController!) {
        if(_popupEventView != nil)
        {
            _popupEventView = nil
        }
    }
    
    //MARK: ###
    //MARK: protocol
    
    func accept_close() {
        _popupControllerEvent?.dismissPopupControllerAnimated(true);
        _popupEventView = nil
    }
    
    func accept_mission(level: Int) {
    
        EventDAO.User_IdolGetQuest((_showInfo?.Schedule?.Id)!, level: level) { (result) in
            if(result.ErrorCode == 0)
            {
                self._popupControllerEvent?.dismissPopupControllerAnimated(true);
                self.updateViewEvent(level, isOnMisstion: true)
                self._popupEventView = nil
                self.showPopupEventDetail()
            }
            else
            {
                FunctionHelper.showBannerAlertView(content: result.Message)
            }
        }
    }
    
    func detail_cancel() {
        _popupControllerEvent?.dismissPopupControllerAnimated(true)
        _popupEventView = nil
    }
    
    func detail_rewardInfo() {
        _popupControllerEvent?.dismissPopupControllerAnimated(true);
        _popupEventView = nil
        showPopupEventReward();
    }
    
    func detail_close() {
        _popupControllerEvent?.dismissPopupControllerAnimated(true);
         _popupEventView = nil
    }
    
    func reward_get() {
        JTProgressHUD.show()
        EventDAO.User_DoneIdolQuest((_showInfo?.Schedule?.Id)!) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
                self._popupControllerEvent?.dismissPopupControllerAnimated(true)
                self._popupEventView = nil
                //self.viewEventContainer.layer.removeAllAnimations()
            }
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    func reward_back()
    {
        _popupControllerEvent?.dismissPopupControllerAnimated(true);
        showPopupEventDetail();
    }
    
    func reward_close() {
        _popupControllerEvent?.dismissPopupControllerAnimated(true);
        _popupEventView = nil
    }
    
    //MARK: ------------
    
    func reachabilityChanged(note: NSNotification){
        if(_internetChecker?.isReachable() == true){
            if(self.isNeedToSetup()){ //Setup If Need
                if(self.isStreamer){
                    self.setupStreamer()
                }
                else{
                    self.setupPlayer()
                    //self.setupHeartBuster()
                }
                self._isNeedToReSetup = false
            }else{ //Start Player / Streamer again
                if(self.isStreamer){
//                    if(self._cameraSession != nil){
//                        self._cameraSession?.startRtmpSessionWithURL(self._strStreamServer, andStreamKey: self._strStreamerKey)
//                    }
                    if let session = self.cameraSession, let streamInfo = self.cameraLiveStreamInfo{
                        session.startLive(streamInfo)
                    }
                    
                }else{
//                    if(self._playerSession != nil){
//                        self._playerSession?.play()
//                    }
                }
            }
        }else{
            FunctionHelper.showBannerAlertView(content: "Không có kết nối Internet")
            if(isStreamer){ //Stop streamer
//                if(self._cameraSession != nil){
//                    self.forceDisconnect()
//                }
                if let session = self.cameraSession{
                    if session.state == .Start{
                       session.stopLive()
                    }
                }
            }else{
//                if(self._playerSession != nil){
//                    self._playerSession?.stop()
//                }
            }
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("MediaStreamer Memory Warning")
    }
    
    //MARK: Setup Methods
    private func isNeedToSetup() -> Bool{
        return self._isNeedToReSetup
    }
    
    private func setupUI(){
        //Setup Ruby Container
        self.viewLeftInfoCointainer.layer.cornerRadius  = 21
        self.viewLeftInfoCointainer.layer.backgroundColor = UIColor(white: 0.3, alpha: 0.5).CGColor
        
        self.viewRubyContainer.layer.cornerRadius = 12
        self.viewRubyContainer.layer.backgroundColor = self.viewLeftInfoCointainer.layer.backgroundColor
//        self.viewRubyContainer.onTap { (tapGestureRecognizer) in
//            self.showRankingViewController()
//        }
        
        let viewChatViewerWidthConstrains = NSLayoutConstraint(item: self.viewChatViewer,
                                                               attribute: NSLayoutAttribute.Width,
                                                               relatedBy: NSLayoutRelation.Equal,
                                                               toItem: nil,
                                                               attribute: NSLayoutAttribute.NotAnAttribute,
                                                               multiplier: 1,
                                                               constant: self.view.frame.size.width - 55)
        self.viewChatViewer.addConstraint(viewChatViewerWidthConstrains)
        self.setupBlurBackground()
    }
    
    private func setupObserver(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboarWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboarWillHide), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reachabilityChanged), name: ReachabilityChangedNotification, object: _internetChecker)
       // NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(userDidLogin), name: UserDidLoginNotification, object: nil)
        //Check If Not Login
//        if SettingsHelper.getUserID() <= 0{
//            
//        }
       

    }
    
    private func removeObserver(){
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        //NSNotificationCenter.defaultCenter().removeObserver(self, name: UserDidLoginNotification, object: nil)
    }

    private func setupObserverRTMP()
    {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(appInactive), name: UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(appActive), name: UIApplicationWillEnterForegroundNotification, object: nil)
    }
    
    private func removeObserverRTMP()
    {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationWillEnterForegroundNotification, object: nil)
    }
    
    private func setupBlurBackground(){
        let blur = UIBlurEffect(style: .Light)
        let effectView = UIVisualEffectView(effect: blur)
        self._imgBackgroud?.addSubview(effectView)
        effectView.frame = self.view.bounds
    }
    
    private func setupSignalR(){
        self._signalRConnector = SignalRConnector.sharedInstance
        if(self._signalRConnector != nil){
            self._signalRConnector!.delegate = self
            self._signalRConnector!.connectToServer()
        }
    }
    
    private func setupStreamer(){
       
        // self._showName = (SettingsHelper.getUserInfo()?.userInfo?.Name)!
        //        self._strStreamServer = "rtmp://210.245.18.48/live/"
        //        self._strStreamerKey = "dang123?pass=tfl"
        //        self._showId = 6
        //       let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        //        dispatch_async(queue) {
        //            self.setupSignalR()
        //        }
        
        let url = NSURL(string: (SettingsHelper.getUserInfo()?.userInfo?.AvatarPhoto)!)
        if(url != nil){
            self._imgBackgroud?.sd_setImageWithURL(url!)
        }
        
        do{
            let audioSession = AVAudioSession.sharedInstance()
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            
            let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
            if(authStatus == .Authorized){
                self.setupCameraSession()
                self.showStreamBeginInputView()
            } else if (authStatus == .Denied){
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted) in
                    if(granted == true){
                        self.setupCameraSession()
                        self.showStreamBeginInputView()
                    }else{
                        dispatch_sync(dispatch_get_main_queue(), { 
                            self.requestCameraPermission()
                        })
                    }
                })
            } else if (authStatus == .Restricted){
                
                let alert = UIAlertView(title: "Lỗi", message: "Không thể thiết lập camera. Xin thử lại.", delegate: nil, cancelButtonTitle: "Xong")
                alert.show()
                
            } else if (authStatus == .NotDetermined){
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted) in
                    if(granted == true){
                        self.setupCameraSession()
                        self.showStreamBeginInputView()
                    }else{
                        dispatch_sync(dispatch_get_main_queue(), {
                            self.requestCameraPermission()
                        })
                    }
                })
            }else{
                let alert = UIAlertView(title: "LiveIdol", message: "Có lỗi xảy ra. Xin thử lại.", delegate: nil, cancelButtonTitle: "Xong")
                alert.show()
            }
        }
        catch{
            let alert = UIAlertView(title: "LiveIdol", message: "Có lỗi xảy ra. Xin thử lại.", delegate: nil, cancelButtonTitle: "Xong")
            alert.show()
        }
    }
    
    private func setupPlayer(){
        self.viewPreview.hidden = false
        self._isSettingUp = true
        self.lblStreamStatus.hidden = true
        do{
            let audioSession = AVAudioSession.sharedInstance()
            try audioSession.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch{
            let alert = UIAlertView(title: "LiveIdol", message: "Có lỗi khi play video. Xin thử lại.", delegate: nil, cancelButtonTitle: "Xong")
            alert.show()
        }
        if let serverIP = self._strStreamServer, let streamKey = self._strStreamerKey {
            let URL = "\(serverIP)\(streamKey)"
            self.setupPlayerSession(URL)
            self.setupObserverRTMP()
        }
        
        self.setupGesture()
        self._isNeedToReSetup = false
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.setupSignalR()
            //Reload User Info
            UserDAO.User_GetDetailsV2(UserId: SettingsHelper.getUserID(), callback: { (resultDetail) in
                if(resultDetail.ErrorCode == 0){
                    let model = resultDetail.DataObject as? UserModel
                    SettingsHelper.setUserInfo(model!)
                }
            })
        }
       
    }
    
    private func setupChoosePlayerByScheduleState(){
        if(self._scheduleStatus == .Casting){
            return
        }
        else{
            if let URL = self._showInfo?.ShowInfo?.LinkVideo{
                self.setupYoutubePlayer(URL)
            }
            dispatch_async(dispatch_get_main_queue()){
                self.cleanUpRTMPPlayer()
            }
        }
    }
    
   func userDidLogin(){
        //Re Join Show
        UserDAO.User_ParticipateInShow(self._showId, connectionId: (self._signalRConnector?.connectionId)!) { (result) in
            if(result.ErrorCode == 0){
                
            }else{
                FunctionHelper.showBannerAlertView(content: result.Message)
            }
        }
    }
    
    private func setupDefaultSubviews(){
        
        self.setupIdolInfo()
        self.setupUserOnline()
        self.setupAnimationContainer()
        self.setupChatContainer()
        self.setupButtonsContainer()
        self.setupSeatContainer()
        self.setupHeartBuster()
        if(!self.isStreamer){
            self.setupChoosePlayerByScheduleState()
            
        }
        
        self._isSettingUp = false
    }
    
    private func setupUserOnline(){
        self.viewUserOnline._showId = self._showId
        self.viewUserOnline.delegate = self
        self.viewUserOnline.loadData()
    }
    
    private  func setupAnimationContainer(){
        self.viewAnimationContainer.hidden = false
        self.viewAnimationContainer.animationMaxSize = self.viewAnimationContainer.frame.size
    }
    
    private func setupSeatContainer(){
        self.viewSeatContainer.rootViewController = self
        self.viewSeatContainer.currentShowModel = self._showInfo
        self.viewSeatContainer.loadData()
    }
    
    private  func setupChatContainer(){
        self.viewChatViewer.hidden = false
        viewChatInput.delegate = self
        self.viewChatViewer.delegate = self
        var strWelcom = ""
        if(self.isStreamer){
            strWelcom = "Chào mừng \((self._showInfo?.ShowInfo?.Name)!) đến với LiveIdol nhé"
//            if let config = SettingsHelper.getConfig(){
//                strWelcom = config.mobileSlogan
//            }else{
//                
//            }
//            
        }else{
           strWelcom = self._scheduleStatus == .Casting ? "Chào mừng bạn đến với show diễn của \((self._idolInfo?.StarUser.Name)!).": "Bạn đang xem offline. Idol sẽ diễn vào lúc \(FunctionHelper.dateToString((self._showInfo!.Schedule?.StartTime)!, mode: 3)) nhé."
        }
        
        let welcomeMessage = SignalRUserDoActionDTO()
        welcomeMessage.actionAttributeText = NSMutableAttributedString(string: strWelcom, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15),
            NSForegroundColorAttributeName : ColorAppDefault])
        self.viewChatViewer.addMessageToData(welcomeMessage)
    }
    
    private func setupButtonsContainer(){
        self.viewButtonsContainer.hidden = false
        self.btnFlash.hidden = !self.isStreamer
        self.btnSwapCamera.hidden = !self.isStreamer
        self.btnFilter.hidden = !self.isStreamer
        self.btnChangeVideoQuatity.hidden = self.btnFilter.hidden
    }
    
    private func setupGesture(){
        self.viewLeftInfoCointainer.onTap { (tapGestureRecognizer) in
            self.showUserInfoPopup((self._idolInfo?.StarUser.Id)!, model: nil)
        }
        //self.viewAnimationContainer.onPan { (panGestureRecognizer) in
        self.viewSwipePanReconize.onPan { (panGestureRecognizer) in
            if(self.isStreamer == true) {return}
            
            let transition = panGestureRecognizer.translationInView(self.viewSwipePanReconize)
            let velocity = panGestureRecognizer.velocityInView(self.viewSwipePanReconize)
            if(panGestureRecognizer.state == .Began){
                let absX = abs(velocity.x)
                let absY = abs(velocity.y)
                if absX >= absY{ //Pan left or right
                    if(velocity.x > 0) { //Pan right
                        //Main View Hided => Return
                        if(self.conTopLayerToLeft.constant == self.view.frame.size.width){
                            return
                        }
                        self._panDirection = UISwipeGestureRecognizerDirection.Right
                        self._deltaSwipe = self.conTopLayerToLeft.constant
                    }else{//Pan left
                        //Main View Showed => Return
                        if(self.conTopLayerToLeft.constant == 0){
                            return
                        }
                        self._panDirection = UISwipeGestureRecognizerDirection.Left
                        self._deltaSwipe = self.view.frame.size.width
                    }
                }else{ //Pan up or down
                    self._deltaSwipe = self.conSwipeViewToTop.constant
                    if(velocity.y > 0){ //Pan down
                        self._panDirection = UISwipeGestureRecognizerDirection.Down
                    }else{ //Pan down
                        self._panDirection = UISwipeGestureRecognizerDirection.Up
                        
                    }
                }
                
            } else if (panGestureRecognizer.state == .Changed){
                if(self._panDirection == nil) {return}
                self._centerPanBegin = transition
                
                switch self._panDirection!{
                case UISwipeGestureRecognizerDirection.Right:
                    self.conTopLayerToLeft.constant =  self._deltaSwipe  + self._centerPanBegin.x
                    self.conTopLayerToRight.constant =  -(self.conTopLayerToLeft.constant)
                    break
                case UISwipeGestureRecognizerDirection.Left:
                    let temp = abs(self._centerPanBegin.x)
                    self.conTopLayerToLeft.constant = self._deltaSwipe - temp
                    self.conTopLayerToRight.constant =  -(self.conTopLayerToLeft.constant)
                    self.viewSwipePanReconize.layoutIfNeeded()
                    break
                case UISwipeGestureRecognizerDirection.Up:
                    self.conSwipeViewToTop.constant = self._deltaSwipe - abs(self._centerPanBegin.y)
                    self.conSwipeViewToBottom.constant = -(self.conSwipeViewToTop.constant)
                    //self.view.layoutIfNeeded()
                    break
                default:
                    self.conSwipeViewToTop.constant = self._deltaSwipe + abs(self._centerPanBegin.y)
                    self.conSwipeViewToBottom.constant = -(self.conSwipeViewToTop.constant)
                    //self.view.layoutIfNeeded()
                    break
                }
            }else if(panGestureRecognizer.state == .Ended){
                if self._panDirection == nil {return}
                
                switch self._panDirection!{
                case  UISwipeGestureRecognizerDirection.Right:
                    if(abs(self._centerPanBegin.x) < 80){
                        self.conTopLayerToLeft.constant = 0
                        self.conTopLayerToRight.constant = 0
                    }else {
                        //Hide Main View
                        self.conTopLayerToLeft.constant = self.view.frame.size.width
                        self.conTopLayerToRight.constant = -(self.conTopLayerToLeft.constant)
                        self.viewAnimationContainer.hidden = true
                    }
                    UIView.animateWithDuration(0.3) {
                        self.viewSwipePanReconize.layoutIfNeeded()
                    }
                    break
                case UISwipeGestureRecognizerDirection.Left:
                    if(abs(self._centerPanBegin.x) < 80) {
                        self.conTopLayerToRight.constant = self._deltaSwipe
                        self.conTopLayerToLeft.constant = -(self._deltaSwipe)
                    }else{
                        //Show Main View
                        self.conTopLayerToRight.constant = 0
                        self.conTopLayerToLeft.constant = 0
                        self.viewAnimationContainer.hidden = false
                    }
                    UIView.animateWithDuration(0.3) {
                        self.viewSwipePanReconize.layoutIfNeeded()
                    }
                    break
                default:
                    if(abs(self._centerPanBegin.y) > 150) {
                        self.conSwipeViewToTop.constant = self.viewSwipePanReconize.frame.size.height * (self._panDirection == .Down ? 1 : -1)
                        self.conSwipeViewToBottom.constant = -self.conSwipeViewToTop.constant
                        
                        UIView.animateWithDuration(0.35, animations: {
                            self.view.layoutIfNeeded()
                            }, completion: { (isFinished) in
                                self.conSwipeViewToTop.constant = 0
                                self.conSwipeViewToBottom.constant = 0
                                self.viewSwipePanReconize.layoutIfNeeded()
                                if let model = self._panDirection == .Down ? SettingsHelper.sharedInstance.getPreviousShow() : SettingsHelper.sharedInstance.getNextShow() {
                                    self.reloadWithModel(model)
                                }
                                self._panDirection = nil

                        })
                    }else{
                        self.conSwipeViewToTop.constant = 0
                        self.conSwipeViewToBottom.constant = 0
                        UIView.animateWithDuration(0.3, animations: {
                            self.viewSwipePanReconize.layoutIfNeeded()
                            }, completion:  {  (isFinished) in
                                self._panDirection = nil
                        })
                    }
                    break
                }
                
            }
        }
    }

    private func setupIdolInfo(){
        self.viewIdolInfoCointainer.hidden = false
        self.viewRubyContainer.hidden = false
        
        self.updateUserOnlineUI()
        
        
        FunctionHelper.circleBorderImageView(imgIdolAvatar)
      
        if(_idolInfo != nil){
            lblIdolName.text = _idolInfo?.StarUser.Name
            //lblIdolRuby.text = String((_idolInfo?.UserStarData?.coinGetFromGift)!)
            lblIdolRuby.text = FunctionHelper.formatNumberWithSeperator((_idolInfo?.UserStarData?.coinGetFromGift)!)
            if let url = NSURL(string: FunctionHelper.getImageUrlBySize((_idolInfo?.StarUser?.AvatarPhoto)!, imageSize: imgIdolAvatar.frame.size)){
                imgIdolAvatar.sd_setImageWithURL(url)
            }
            
            //Check image background
            if let url = NSURL(string: FunctionHelper.getImageUrlBySize((_showInfo?.ShowInfo?.Photo)!, imageSize: _imgBackgroud!.frame.size)) where _imgBackgroud?.image == nil{
                self._imgBackgroud!.sd_setImageWithURL(url)
            }
        }
    }
    
    private func setupCameraSession(){
        dispatch_async(dispatch_get_main_queue()) { 
            self.cameraQuality = VideoQuality.allValues()
            self.lblStreamStatus.hidden = false
            self.setStreamStatus("Ngắt", state: .Stop)
            self.viewPreview.hidden = false
            
            self.cameraAudioConfig = LFLiveAudioConfiguration.defaultConfigurationForQuality(.Medium)
            self.cameraVideoConfig = LFLiveVideoConfiguration.defaultConfigurationForQuality(.Medium)            
            if self.cameraSession == nil {
                self.cameraSession =  LFLiveSession(audioConfiguration: self.cameraAudioConfig, videoConfiguration: self.cameraVideoConfig)
                self.cameraSession?.delegate = self
                self.cameraSession?.showDebugInfo = true
            }
            if let session = self.cameraSession{
                session.preView = self.viewPreview
                session.running = true
            }
        }
    }
    
    private func requestCameraPermission(){
        let alertController = UIAlertController(title: "Thông báo",
                                                message: "Bạn chưa cấp quyền truy cập camera. Xin cấp quyền trong phần cài đặt.",
                                                preferredStyle: .Alert)
        let settingsAction = UIAlertAction(title: "Cài đặt", style: .Default) { (alertAction) in
            if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.sharedApplication().openURL(appSettings)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Hủy", style: .Cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    private func setupYoutubePlayer(inputURL: String){
        
        if let videoId = FunctionHelper.getYoutubeVideoIdFromURL(inputURL){
            self._youtubePlayer = YTPlayerView(frame: self.viewPreview.frame)
            
            dispatch_async(dispatch_get_main_queue()){
                self.viewPreview.addSubview(self._youtubePlayer!)
                self._youtubePlayer!.delegate = self
                //Check is playlist
                let isPlaylist = inputURL.containsString("list")
                var params : [NSObject : AnyObject] = ["playsinline" : 1,
                                                       "autoplay" : 1,
                                                       "controls" : 0,
                                                       "vq" : "auto"]
                if(isPlaylist){
                    params["listType"] = "playlist"
                    self._youtubePlayer!.loadWithPlaylistId(videoId, playerVars: params)
                }else{
                    self._youtubePlayer!.loadWithVideoId(videoId,playerVars: params)
                }
            }
        }
    }
    
    private func setupPlayerSession(inputURL: String){
        //self.viewPreview.hidden = false
//        self.lblStreamStatus.hidden = true
//        if let url = NSURL(string: inputURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!){
//            dispatch_async(dispatch_get_main_queue()) {
//                let options = IJKFFOptions.optionsByDefault()
//                options.setPlayerOptionIntValue(0, forKey: "timeout")
//                options.setPlayerOptionIntValue(0, forKey: "packet-buffering")
//                options.setFormatOptionIntValue(50000, forKey: "analyzeduration")
//                
//                //options.setPlayerOptionValue("1024", forKey: "max-buffer-size")
//                if(self._playerSession == nil){
//                    self._playerSession = IJKFFMoviePlayerController(contentURL:url, withOptions: options)
//                    self._playerSession!.view.frame = self.viewPreview.bounds
//                    self._playerSession!.scalingMode = .AspectFill
//                    self._playerSession?.shouldAutoplay = true
//                    self._playerSession!.prepareToPlay()
//                    IJKFFMoviePlayerController.setLogLevel(k_IJK_LOG_SILENT)
//                    self.showPlayerLoading()
//                }
//                self.viewPreview.addSubview((self._playerSession?.view)!)
//                NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.playerDidChangeState), name: IJKMPMoviePlayerPlaybackStateDidChangeNotification, object: self._playerSession)
//            }
//        }
        //let url = NSURL(string: "rtmp://210.245.18.48/live/dang123456")
        //let url = NSURL(string:"rtmp://mftsrhzg.cdnviet.com/kdswhts/_definst_/rooms_44019720/ychat_44019720")
    }
    
    private func setupHeartBuster(){
        
        self._autoBurstTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: #selector(sendAutoHeart), userInfo: nil, repeats: true)        
        self._heartViewPosition = self.viewMainContainer.convertRect(self.btnClose.frame, fromView: self.viewButtonsContainer).origin
    }
    
    internal func sendAutoHeart(){
        self.showTheLove(1)
    }
    
    func setShowInfo(showId: Int64, urlStreamer: String?, streamKey: String?) -> Bool{
        let rs = true;
        _showId = showId;
        _strStreamServer = urlStreamer
        _strStreamerKey = streamKey
        return rs;
    }
    
    //MARK: Popup Profile Delegate
    internal func popupProfile_didTouchClose() {
        self._popupController?.dismissPopupControllerAnimated(true);
    }
    
    internal func popupProfile_didTouchKick(userId: Int64) {
        self._popupController?.dismissPopupControllerAnimated(true);
        self.checkLogin { (loginState) in
            if(loginState == .Logged){
                UserDAO.User_KickUser(userId, scheduleId: (self._showInfo?.Schedule?.Id)!) { (result) in
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            }
        }
    }
    
    internal func popupProfile_didTouchChat(userInfo: SignalRUserDTO) {
        self._popupController?.dismissPopupControllerAnimated(true);
        self._targetUser = userInfo
        self.viewChatInput.setTargetUserName(self._targetUser?.name)
        self.touchedChat(nil)
    }
    
    internal func popupProfile_didTouchDisableChat(userId: Int64, isCanChat: Bool, isFacebookUser: Bool, facebookName : String) {
        self._popupController?.dismissPopupControllerAnimated(true);
        self.checkLogin { (loginState) in
            if(loginState == .Logged){
                if(isFacebookUser){
                    UserDAO.User_BanFacebookLiveChat(userId, scheduleId: (self._showInfo?.Schedule?.Id)!, userName: facebookName, isCanChat:  !isCanChat, callback: { (result) in
                         FunctionHelper.showBannerAlertView(content: result.Message)
                    })
                }else{
                    UserDAO.User_DisableChat(userId, scheduleId: (self._showInfo?.Schedule?.Id)!, isCanChat: !isCanChat) { (result) in
                        FunctionHelper.showBannerAlertView(content: result.Message)
                    }
                }
            }
        }
    }
    
    internal func popupProfile_didTouchReport(userId: Int64) {
        self._popupController?.dismissPopupControllerAnimated(true)
        self.checkLogin { (state) in
            if(state == .Logged){
                let base64photo = self.captureScreen()
                self.showPopupReport(base64photo,userId: userId)
            }
        }
        
    }
    
    //MARK: Stream Begin/End Delegate
    func streamBegin_didBegin(name: String, facebookToken: String) {
        
        self._showName = name.isEmpty ? (SettingsHelper.getUserInfo()?.userInfo?.Name)! : name
        self.dismissViewControllerAnimated(true) {
//            let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            dispatch_async(queue) {
//                self.streamBegin(facebookToken)
//            }
            dispatch_async(dispatch_get_main_queue(), { 
                self.streamBegin(facebookToken)
            })
            
        }
    }
    
    func cleanUpCameraSession(){
        if let session = self.cameraSession{
            session.stopLive()
            session.delegate = nil
        }
        self.cameraSession = nil
        self.cameraAudioConfig = nil
        self.cameraVideoConfig = nil
        self.cameraLiveStreamInfo = nil
    }
    
    func streamBegin_didClose() {
        self.dismissViewControllerAnimated(true) {
//            if(self._cameraSession != nil){
//                self._cameraSession?.delegate = nil
//                self._cameraSession?.endRtmpSession()
//                self._cameraSession?.cleanUp()
//                self._cameraSession = nil
//                self._isNeedToReSetup = true
//            }
            self.cleanUpCameraSession()
            self._isNeedToReSetup = true
            NSNotificationCenter.defaultCenter().removeObserver(self, name: UserDidLoginNotification, object: nil)
            if(self.delegate != nil){
                self.delegate?.mediaStreamer_onClose()
            }
        }
    }
    
    func streamEnd_didTouchHome(facebookToken: String) {
        self.dismissViewControllerAnimated(true) {
            if(self.delegate != nil){
                self.delegate?.mediaStreamer_onClose()
            }
        }
    }
    
    //MARK: Player Session Delegate
//    func playerDidChangeState(notification: NSNotification){
//        switch  self._playerSession!.playbackState {
//        case IJKMPMoviePlaybackState.Stopped:
//            break
//        case IJKMPMoviePlaybackState.Playing:
//            self.hidePlayerLoading()
//            self.startCheckNetworkTimer()
//            break
//        case IJKMPMoviePlaybackState.Paused:
//            break
//        case IJKMPMoviePlaybackState.Interrupted:
//            break
//        case IJKMPMoviePlaybackState.SeekingForward, IJKMPMoviePlaybackState.SeekingBackward:
//            break
//        }
//    }
//    
    
    //MARK: Touch + Swipe Delegate
    func swipeHandler(sender: UISwipeGestureRecognizer){
        if (sender.direction == .Left) {
            self.viewAnimationContainer.hidden = false
            self.conTopLayerToLeft.constant = 0
            self.conTopLayerToRight.constant = 0
        }
        else if (sender.direction == .Right){
            self.viewAnimationContainer.hidden = true
            self.conTopLayerToLeft.constant =  self.view.bounds.size.width
            self.conTopLayerToRight.constant =  -self.view.bounds.size.width
        }
        UIView.animateWithDuration(0.35) {
            self.view.layoutIfNeeded()
        }
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let point = touch.locationInView(self.view)
        if(_isChatInputShowed || _isKeyboardShowed){
            if(point.y < viewChatInput.frame.origin.y){
                hideChatInput()
            }
        }
        
        //Ẩn view tặng quà
//        if(_isGiftViewShowed){
//            //Kiểm tra touch có ở ngoài phạm vi
//            if(point.y < self.viewGiftContainer.frame.origin.y){
//                hideGiftViewContainer()
//            }
//        }
        
        if(self.viewSeatContainer.hidden == false){
            if(point.y < self.viewSeatContainer.frame.origin.y){
                hideSeatViewContainer()
            }
        }
    }
    
    //MARK: Helpers
    func showStreamBeginInputView(){
        let con = StreamBeginViewController(nibName: "StreamBeginViewController", bundle: nil)
        con.delegate = self
        con.modalPresentationStyle = .OverCurrentContext
        self.presentViewController(con, animated: true, completion: nil)
        
    }
    
    private func streamBegin(facebookToken: String){
        var connectionType = "Unknown"
        if let checker = self._internetChecker {
            connectionType = checker.isReachableViaWiFi() ? "Wifi" : "WWan" 
        }
        UserDAO.StreamBegin(self._showName, facebookToken: facebookToken, width: 450, height: 800, connectionType: connectionType) { (result) in
            if(result.ErrorCode == 0){
                //Kết nối signalR
                let data = result.DataObject as! BeginShowModel
                self._showId = data.showId
                self._scheduleId =  data.scheduleId
                //self._strStreamerKey = "dang123456?pass=tfl"
                //self._strStreamServer = "rtmp://210.245.18.48/live/"
                //data.streamKey.appendContentsOf("?pass=tfl")
                self._strStreamerKey = data.streamKey
                self._strStreamServer = data.streamServer
                self.setupSignalR()
                if let session = self.cameraSession{
                    self.cameraLiveStreamInfo = LFLiveStreamInfo()
                    self.cameraLiveStreamInfo?.url = "\(self._strStreamServer!)\(self._strStreamerKey!)"
                    session.startLive(self.cameraLiveStreamInfo!)
                }
                //self._cameraSession!.startRtmpSessionWithURL(self._strStreamServer, andStreamKey:self._strStreamerKey)
                
            }else{
                FunctionHelper.showBannerAlertView("LiveIdol", content: result.Message, callback:nil)
            }
        }
    }
    
    private func streamEnd(){
        UserDAO.StreamEnd(self._scheduleId) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0 && result.DataObject != nil){
                let data = result.DataObject as! EndShowModel
//                let con = StreamEndViewController(nibName: "StreamEndViewController", bundle: nil)
//                con.delegate = self
//                con.modalPresentationStyle = .OverCurrentContext
//                self.presentViewController(con, animated: true, completion: nil)
//                con.setInfo((self._idolInfo?.StarUser.AvatarPhoto)!, data: data)
                self.showEndStreamController((self._idolInfo?.StarUser.AvatarPhoto)!, model: data)
            }else{
                if(self.delegate != nil){
                    self.delegate?.mediaStreamer_onClose()
                }
            }
        }
    }
    
    private func showEndStreamController(photo: String, model: EndShowModel){
        let con = StreamEndViewController(nibName: "StreamEndViewController", bundle: nil)
        con.delegate = self
        con.setInfo(photo, data: model)
        con.modalPresentationStyle = .OverCurrentContext
        self.presentViewController(con, animated: true, completion: nil)
        //con.setInfo(photo, data: model)
    }
    
    private func showUserInfoPopup(userId: Int64, model: UserModel?){
        let con = PopupProfileViewController(nibName: "PopupProfileViewController", bundle: nil)
        con.setUserInfo(userId, model: model, scheduleId: (self._showInfo?.Schedule?.Id)!)
        con.delegate = self
        self._popupController = CNPPopupController(contents: [con.view])
        self._popupController!.mainController = con
        self._popupController!.theme = CNPPopupTheme.defaultTheme()
        self._popupController!.theme.popupStyle = .Centered
        self._popupController!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupController!.presentPopupControllerAnimated(true)
        
    }
    
    private func reloadWithModel(data: (model: UserSMModel, image: UIImage?)){
        if(self.isStreamer == true || self._isSettingUp == true) {return}
        self.cleanUp()
        self.setShowInfo(Int64((data.model.ShowInfo?.Id)!), urlStreamer: (data.model.ShowInfo?.ServerIP)!, streamKey: (data.model.ShowInfo?.StreamKey)!)
        if let url = NSURL(string:
            FunctionHelper.getImageUrlBySize((data.model.UserInfo?.AvatarPhoto)!, imageSize: CGSize(width: 320, height: 480))){
            self._imgBackgroud?.sd_setImageWithURL(url)
        }
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(userDidLogin), name: UserDidLoginNotification, object: nil)
        //self._imgBackgroud?.image =  data.image
        self.setupPlayer()
        //self.setupHeartBuster()
    }
    
    func updateUserOnlineUI(){
        dispatch_async(dispatch_get_main_queue()) {
            self.lblUserOnline.text = String((self._showInfo?.ShowInfo?.OnlineUser)!)
        }
    }
    
    func cleanUp(){
//        if(self._cameraSession != nil){
//            self._cameraSession?.delegate = nil
//            self._cameraSession?.endRtmpSession()
//            self._cameraSession?.cleanUp()
//            self._cameraSession = nil
//        }
        self.cleanUpCameraSession()
        self.cleanUpRTMPPlayer()
        self.cleanUpYoutubePlayer()
        
        if(self._signalRConnector != nil){
            self._signalRConnector?.disconnectToServer()
            self._signalRConnector = nil
        }
        
        self.viewChatViewer.cleanup()
        self.viewUserOnline.cleanup()
        self.viewAnimationContainer.cleanUp()
        self.viewSeatContainer.cleanUp()
        self.viewChatInput.cleanUp()
        
        self.viewAnimationContainer.hidden = true
        self.viewIdolInfoCointainer.hidden = true
        self.viewButtonsContainer.hidden = true
        self.viewRubyContainer.hidden = true
        self.viewPreview.hidden = true
        
        if(self.isStreamer){
            //self.streamEnd()
        }
        else
        {
            if(self._autoBurstTimer != nil){
                self._autoBurstTimer?.invalidate()
                self._autoBurstTimer = nil
            }
        }
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserDidLoginNotification, object: nil)
        self._targetUser = nil
        self._isNeedToReSetup = true
    }
    
    func cleanUpRTMPPlayer(){
//        if(self._playerSession != nil){
//            self.stopCheckNetworkTimer()
//            if(self._playerSession?.isPlaying() == true){
//                self._playerSession?.stop()
//            }
//            self._playerSession?.shutdown()
//            NSNotificationCenter.defaultCenter().removeObserver(self, name: IJKMPMoviePlayerPlaybackStateDidChangeNotification, object: self._playerSession)
//            self._playerSession?.view.removeFromSuperview()
//            self._playerSession = nil
//            self.hidePlayerLoading()
//        }
    }
    
    func cleanUpYoutubePlayer(){
        if(self._youtubePlayer != nil){
            self._youtubePlayer?.stopVideo()
            self._youtubePlayer?.removeWebView()
           // self._youtubePlayer?.removeFromSuperview()
            self._youtubePlayer = nil
        }
    }
    
    func showTheLove(count: Int) {
        for _ in 1...count{
            let heart = HeartView(frame: CGRectMake(0, 0, self._heartSize, self._heartSize))
            heart.userInteractionEnabled = false
            self.viewMainContainer.addSubview(heart)
            heart.center = self._heartViewPosition
            heart.animateInView(self.viewMainContainer)
        }
    }
    
    func getRandomNumber(from : CGFloat, to: CGFloat) -> CGFloat{
        return (from + CGFloat(arc4random()) % (to-from+1))
    }
    
    func joinShow(connectionId: String){
        //_showId = 6
        let _weakSelf = self

        UserDAO.User_ParticipateInShow(_showId, connectionId: connectionId) { (result) in
            if(result.ErrorCode == 0 && result.DataObject != nil){
                if let _showInfo = result.DataObject as? ShowModel {
                    if _showInfo.ShowInfo?.Id != _weakSelf._showId && self.isStreamer == false { return }
                    _weakSelf._showInfo = _showInfo
                    _weakSelf._scheduleStatus = (_weakSelf._showInfo?.Schedule?.Status)!
                    if(_weakSelf._showInfo?.ListShowItem.count  > 0){
                        _weakSelf._idolInfo = _weakSelf._showInfo?.ListShowItem[0]
                    }
                    //Check show status
                    dispatch_after(DISPATCH_TIME_NOW, dispatch_get_main_queue(), {
                        _weakSelf.setupDefaultSubviews()
                        
                        if(SettingsHelper.getUserID() > 0){
                            UserDAO.User_GetAnimationItemJoinShow(SettingsHelper.getUserID(), callback: { (animationResult) in
                                if(result.ErrorCode == 0 && animationResult.DataObject != nil){
                                    let _animationModel = animationResult.DataObject as! AnimationItemModel
                                    if(_animationModel.animationItem != nil && _animationModel.animationItem?.gifLinkMobile != ""){
                                        _weakSelf.downloadAndShowAnimationImage((_animationModel.animationItem?.gifLinkMobile)!, duration: (_animationModel.animationItem?.timeShowMobile)!,animationType : .Large)
                                    }
                                }
                            })
                        }
                    })
                }
            }else{
                dispatch_after(DISPATCH_TIME_NOW, dispatch_get_main_queue(), {
                    _weakSelf.cleanUp()
                    
                    let alertView = UIAlertView(title: "Thông báo", message: result.Message, delegate: nil, cancelButtonTitle: "Thoát")
                    alertView.show()
                    _weakSelf.dismissViewControllerAnimated(true, completion:  nil)
                })
            }
        }
    }
    
    func keyboarWillShow(aNotification: NSNotification){
        let keyboardSize = (aNotification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size
        
        conChatInputHeight.constant = keyboardSize.height + 65;
        self.view.layoutIfNeeded()
        viewChatInput.keyboardSize  = keyboardSize
        self.showChatInput()
        _isKeyboardShowed = true
        viewChatInput.isShowingEmoji = false
    }
    
    func keyboarWillHide(aNotification: NSNotification){
        _isKeyboardShowed = false
        if(self.viewChatInput.isShowingEmoji){return}
        hideChatInput()
    }
    
    func showChatInput(){
        //conChatInputHeight.constant = viewChatInput.totalSize().height
        self.conIdolInfoToTop.constant = -(self.conChatInputHeight.constant - 80)
        self.conChatContainerToBottom.constant = (self.conChatInputHeight.constant + 5)
        UIView.animateWithDuration(0.3) {
            self.view.layoutIfNeeded()
        }
        _isChatInputShowed = true
        viewChatViewer.userInteractionEnabled = false
        self.btnClose.hidden = true
        
    }
    
    func hideChatInput(){
        if(conChatInputHeight.constant > viewChatInput.keyboardSize.height){
            
            conChatInputHeight.constant = 65
            self.conIdolInfoToTop.constant = 15
            self.conChatContainerToBottom.constant = 40
            UIView.animateWithDuration(0.3) {
                self.view.layoutIfNeeded()
            }
            viewChatInput.isShowingEmoji = false;
        }
        self.view.endEditing(true)
        _isChatInputShowed = false
        viewChatInput.hidden = true
        viewChatViewer.userInteractionEnabled = true
        self.btnClose.hidden = false
    }
    
    func hideGiftViewContainer(){
        self.conGiftViewToTop.constant = self.view.bounds.size.height
        UIView.animateWithDuration(0.3, animations: { 
             self.view.layoutIfNeeded()
            }) { (isFinished) in
                self.endShowFooter()
                self._isGiftViewShowed = false
        }
    }
    
    func showSeatContainer(){
        self.beginShowFooter()
        self.conSeatViewToTop.constant = self.view.bounds.size.height - self.viewSeatContainer.bounds.size.height
        self.viewSeatContainer.hidden = false
        UIView.animateWithDuration(0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hideSeatViewContainer(){
        self.conSeatViewToTop.constant = self.view.bounds.size.height
        UIView.animateWithDuration(0.3, animations: {
            self.view.layoutIfNeeded()
        }) { (isFinished) in
            self.viewSeatContainer.hidden = true
            self.endShowFooter()
        }
        
    }

    
    func beginShowFooter(){
        self.viewChatViewer.hidden = true
        self.btnClose.hidden = true
        self.viewButtonsContainer.hidden = true
    }
    
    func endShowFooter(){
        self.viewChatViewer.hidden = false
        self.btnClose.hidden = false
        self.viewButtonsContainer.hidden = false
    }
    
    //func setStreamStatus(string : String, state: CameraHelperRTMPSessionState){
    func setStreamStatus(string : String, state: LFLiveState){
        self.lblStreamStatus.attributedText = self.getStatusAttributeString(string, fontSize: self.lblStreamStatus.font.pointSize, state: state)
    }
    //func getStatusAttributeString(input: String, fontSize: CGFloat, state: CameraHelperRTMPSessionState) -> NSAttributedString{
    func getStatusAttributeString(input: String, fontSize: CGFloat, state: LFLiveState) -> NSAttributedString{
        let font = UIFont.systemFontOfSize(fontSize)
        let image = UIImage(named: state == LFLiveState.Start ? "ic_streamstatus_connected" : "ic_streamstatus_disconnected")
        let inlineAttach = FunctionHelper.inlineTextAttachment(image!, size: CGSize(width: 12,height: 11), font: UIFont.systemFontOfSize(5))
        let imageAttribute = NSAttributedString(attachment: inlineAttach)
        let rs = NSMutableAttributedString()
        rs.appendAttributedString(imageAttribute)
        let color = state == LFLiveState.Start
            ? UIColor(red: 115/255, green: 175/255, blue: 0/255, alpha: 1.0)
            : UIColor(red: 212/255, green: 0/255, blue: 0/255, alpha: 1.0)
        let onlineUserAtt = NSAttributedString(string: " \(input)", attributes: [NSFontAttributeName : font, NSForegroundColorAttributeName: color])
        
        rs.appendAttributedString(onlineUserAtt)
        return rs
    }
    
    func forceDisconnect(){
        self._isForceStop = true
        if let session = self.cameraSession{
           session.stopLive()
        }
//        if(_cameraSession != nil){
//            _cameraSession!.endRtmpSession()
//        }
        self.stopTimer()
    }
    
    func stopTimer(){
        if(self._reconnectTimer != nil){
            self._reconnectTimer?.invalidate()
            self._reconnectTimer = nil
        }
    }
    
    func downloadAndShowAnimationImage(animationLink: String, duration: Double, animationType: AnimationType = .Small){
        if let url = NSURL(string: animationLink){
            let downloader = SDWebImageDownloader.sharedDownloader()
            downloader.downloadImageWithURL(url, options:.UseNSURLCache, progress: nil, completed: { (image, data, error, isFinished) in
                if(error == nil && image != nil){
                    //self.viewAnimationContainer.showAnimation(image!, giftSize: CGSize(width: (image?.size.width)!,height: (image?.size.height)!), duration: duration, type: animationType)
                    
                    let _yyImage = YYImage(data: data)
                    self.viewAnimationContainer.showAnimationImage(_yyImage!, duration: duration, animationType: animationType)
                }
            })
        }
    }
    
    //MARK: Youtube Delegate
    func playerView(playerView: YTPlayerView, didChangeToState state: YTPlayerState) {
        
    }
    
    func playerViewDidBecomeReady(playerView: YTPlayerView) {
        if(self._youtubePlayer != nil){
            self._youtubePlayer!.playVideo()
        }
    }
    
    //MARK: SignalR Delegate
    func signalR_DidConnect(connectionId: String) {
        self.joinShow(connectionId)
    }
    
    func signalR_DidReceiveAlert(data: JSON, code: SignalRAlertCode) {
        let weakSelf = self
        
        //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        dispatch_barrier_sync(weakSelf._signalRQueue!) {
            var dto : SignalRUserDoActionDTO? = nil
            switch code {
            case .PublicMessage:
                dto = SignalRUserSendMessageDTO(jsondata: data, code: code)
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                }
                break
                
            case .CoinMessage:
                let bubble = SignalRUserSendMessagePopupDTO(jsondata: data)
                self.viewAnimationContainer.addBubbleChat(bubble)
                break
                
            case .JoinShow:
                let userId = data["userId"].int64Value
                UserDAO.User_GetUserInfoInShow(userId, callback: { (result) in
                    if(result.ErrorCode == 0){
                        let _model = result.DataObject as! UserModel
                        weakSelf.viewUserOnline.addNewUserToList(_model)
                        UserDAO.User_GetAnimationItemJoinShow((_model.userInfo?.Id)!, callback: { (animationResult) in
                            var _animationModel : AnimationItemModel?
                            if (animationResult.ErrorCode == 0 && animationResult.DataObject != nil){
                                  _animationModel = animationResult.DataObject as? AnimationItemModel
                            }
                            dto = SignalRUserJoinShowDTO(model: _model, animationModel: _animationModel)
                            if(dto != nil){
                                weakSelf.viewChatViewer.addNewMessage(dto!)
                            }
                            if let animation = _animationModel{
                                if(animation.animationItem != nil && animation.animationItem?.gifLinkMobile != ""){weakSelf.downloadAndShowAnimationImage((animation.animationItem?.gifLinkMobile)!, duration: (animation.animationItem?.timeShowMobile)!,
                                    animationType : .Large)
                                }
                            }
                        })
                    }
                })
                weakSelf._showInfo?.increaseUserOnline(1)
                weakSelf.updateUserOnlineUI()
                break
                
            case .ExitShow:
                let userId = data["userId"].int64Value
                if(userId > 0){
                    weakSelf.viewUserOnline.removeUserFromList(userId)
                }
                if(weakSelf._showInfo?.ShowInfo?.OnlineUser > 0){
                    weakSelf._showInfo?.decreaseUserOnline(1)
                    weakSelf.updateUserOnlineUI()
                }
                break
            case .GiveGift:
                dto  = SignalRUserGiveGiftDTO(jsondata: data)
                if(dto != nil){
                    
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                    let temp = dto as! SignalRUserGiveGiftDTO
                    let animationType = AnimationType(rawValue: temp.giftInfo!.type) ?? AnimationType.Small
                    weakSelf.downloadAndShowAnimationImage( (temp.giftInfo?.photoGif)!, duration: (temp.giftInfo?.animationGifTime)!, animationType: animationType)
                    dispatch_async(dispatch_get_main_queue(), {
                        weakSelf.showTheLove(1)
                    })
                }
                break
            case .UpdateStarInfo:
                let model = SignalRStarDTO(jsondata: data)
                if(model.id > 0){
                    dispatch_async(dispatch_get_main_queue(), {
                        self.lblIdolRuby.text = FunctionHelper.formatNumberWithSeperator(model.coinGet)
                    })
                }
                break
                
            case .BuySeat:
                dto = SignalRUserBuySeatDTO(jsonData: data)
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                    dispatch_async(dispatch_get_main_queue(), {
                        weakSelf.viewSeatContainer.updateSeat(ShowItemModel(signalRData: dto as! SignalRUserBuySeatDTO))
                    })
                    
                }
                break
            case .EndShow:
                UserDAO.User_GetStreamInfo((weakSelf._showInfo?.Schedule?.Id)!, callback: { (result) in
                    if(result.ErrorCode == 0 && result.DataObject != nil){
                        dispatch_async(dispatch_get_main_queue(), { 
                            self.cleanUp()
                            let data = result.DataObject as! EndShowModel
                            self.showEndStreamController((weakSelf._idolInfo?.StarUser.AvatarPhoto)!, model: data)
                        })
                    }
                })
                break;
            case .KickUser, .EnablePublicChat, .DisablePublicChat, .DisableChatFacebook:
                dto = SignalRUserDoActionToTargetDTO(jsondata: data, code: code)
                dto?.generateActionText()
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                    if(dto!.code == .KickUser){
                        let kickDTO = dto as! SignalRUserDoActionToTargetDTO
                        if(kickDTO.targetUser?.id == SettingsHelper.getUserID()){
                            
                            dispatch_async(dispatch_get_main_queue(), {
                                let alertView = UIAlertView(title: "Thông báo", message: "Bạn đã bị kick ra khỏi phòng.", delegate: self, cancelButtonTitle: "Xong")
                                alertView.delegate = self
                                alertView.tag = 999
                                alertView.show()
                            })
                        }
                    }
                }
            case .GiveHeart:
                dto  = SignalRUserGiveHeartDTO(jsonData: data)
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                }
                dispatch_async(dispatch_get_main_queue(), {
                    self.showTheLove(3)
                })
                break
                
            case .StartShow:
                //Change Show State
                self._showInfo?.Schedule?.Status = .Casting
                self.setupChoosePlayerByScheduleState()
                break;
                
            case .IdolGetReward:
                dto = SignalRStarGetRewardDTO(jsondata: data)
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                }
                let temp = dto as! SignalRStarGetRewardDTO
                if(temp.giftInfo?.photoGif.isEmpty == false){
                    weakSelf.downloadAndShowAnimationImage( (temp.giftInfo?.photoGif)!, duration: (temp.giftInfo?.animationGifTime)!)
                
                }
                break;
                
            case .UserGetReward:
                dto = SignalRUserGetIdolQuestReward(jsondata: data)
                if(dto != nil){
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                }
                break
                
            case .IdolAcceptMission:
                dispatch_async(dispatch_get_main_queue(), {
                    self.updateViewEvent(data.intValue, isOnMisstion: true);
                })
                break
                
            case .IdolCancelMission:
                dispatch_async(dispatch_get_main_queue(), {
                    self.updateViewEvent(data.intValue, isOnMisstion: false);
                })
                break
            
            case .LixiGift:
                //dto  = SignalRUserGiveGiftDTO(jsondata: data)
                dto = SignalRUserGetLixiDTO(jsondata: data)
                if(dto != nil){
                    
                    weakSelf.viewChatViewer.addNewMessage(dto!)
                    let temp = dto as! SignalRUserGetLixiDTO
                    let animationType = AnimationType(rawValue: temp.giftInfo!.type) ?? AnimationType.Small
                    weakSelf.downloadAndShowAnimationImage( (temp.giftInfo?.photoGif)!, duration: (temp.giftInfo?.animationGifTime)!, animationType: animationType)
                }
            default:
                break
            }
        }
    }
    
    //MARK: AlertView Delegate
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(alertView.tag == 999){
            //Kick user
            self.touchClose(nil)
        }
    }
    
    func chatMessage_didTouchedUser(userId: Int64, data: SignalRUserDTO?) {
        var model : UserModel? = nil
        if(data != nil){
            model = UserModel(data: data!)
        }
        self.showUserInfoPopup(userId, model: model)
    }
    
    //MARK: Chat Input Delegate
    func chatInput_didTouchLeftButton() {
        if(!_isChatInputShowed){
            showChatInput()
            viewChatInput.isShowingEmoji = true
        }else{
            viewChatInput.isShowingEmoji = _isKeyboardShowed
            if(_isKeyboardShowed){
                self.viewChatInput.resignFirstReponser()
            }else{
                self.viewChatInput.becomeFirstReponser()
            }
        }
    }
    
    func chatInput_didTouchRightButton() {
        let message  = self.viewChatInput.plainText
        if(message.isEmpty){return}
        self.checkLogin { (loginState) in
            if(loginState == .Logged || loginState == .Successed){
                var targetUserId : Int64 = -1
                if(self._targetUser != nil){
                    targetUserId = (self._targetUser?.id)!
                }
                if(self.viewChatInput.isEnableBubbleChat){
                    UserDAO.User_SendCoinChat(targetUserId, scheduleId: (self._showInfo?.Schedule?.Id)!, message: message, callback: { (result) in
                        if(result.ErrorCode == 0){
                            //SettingsHelper.addCurrentUserRuby(-3);
                            SettingsHelper.addCurrentUserRuby(result.Config * (-1))
                        }else{
                            FunctionHelper.showBannerAlertView(content: result.Message)
                        }
                    })
                }
                else{
                    //Tạo đối tượng
                    let user = SignalRUserDTO(model: SettingsHelper.getUserInfo()!)
                    self._signalRConnector?.sendShowMessage(user, message: message, scheduleId: (self._showInfo?.Schedule?.Id)!,targetUser: self._targetUser, callback: { (doRs) in
                        if(doRs.ErrorCode != 0){
                            FunctionHelper.showBannerAlertView(content: doRs.Message)
                        }
                    })
                }
            }
        }
    }
    
    func chatInput_didRemoveTargetUser() {
        self._targetUser = nil
    }
    
    func chatInput_didChangedToSize(newSize: CGSize) {
        conChatInputHeight.constant = newSize.height;
    }
    
    //MARK: CameraSession Delegate
    
    func liveSession(session: LFLiveSession?, liveStateDidChange state: LFLiveState) {
        var _isPlay = false
        dispatch_async(dispatch_get_main_queue(), {
            switch state {
            case LFLiveState.Ready:
                self.setStreamStatus("Đang kết nối", state: state)
                _isPlay = true
                break;
            case LFLiveState.Pending:
                self.setStreamStatus("Đang kết nối", state: state)
                _isPlay = false
                break;
            case LFLiveState.Refresh:
                self.setStreamStatus("Đang kết nối", state: state)
                _isPlay = false
                break;
            case LFLiveState.Start:
                self.setStreamStatus("Tốt", state: state)
                _isPlay = true
                //self.startNetworkChecker()
                break;
            case LFLiveState.Error:
                self._isForceStop = true
                self.cameraSession?.stopLive()
                self.setStreamStatus("Ngắt", state: state)
                break;
            case LFLiveState.Stop:
                if self._isForceStop == false{
                    self.cameraSession?.startLive(self.cameraLiveStreamInfo!)
                }else{
                    self.setStreamStatus("Ngắt", state: state)
                    //self.stopNetworkChecker()
                }
                break
            }
        })
        
    }
    
    func liveSession(session: LFLiveSession?, errorCode: LFLiveSocketErrorCode) {
        switch errorCode{
        case .ReConnectTimeOut:
            //Reinit Stream URL
            dispatch_async(dispatch_get_main_queue(), {
                self._isForceStop = true
                self.cameraSession?.stopLive()
                
                //self.cameraSession?.startLive(self.cameraLiveStreamInfo!)
            })
        default:
            break
        }
    }

    //MARK: User Online Delegate
    func userOnline_didTouchUser(userId: Int64, model: UserModel) {
        self.showUserInfoPopup(userId, model: model)
    }
    
    func touchedIdolAvatar(){
        if(self._idolInfo != nil){
            let model = UserModel()
            model.userInfo = self._idolInfo?.StarUser
            model.starDataInfo = self._idolInfo?.UserStarData
            model.followInfo = self._idolInfo?.FollowInfo
            //model.userInfo?.TotalCoinUsed = (self._idolInfo?.UserStarData?.coinGetFromGift)!
            self.showUserInfoPopup((model.userInfo?.Id)!, model: model)
        }
    }
    
    func touchFollow(userId: Int64, isFollow: Bool) {
        self.checkLogin { (result) in
            if( result == .Logged)
            {
                UserDAO.User_FollowUser(FollowUserId: userId, IsFollow: isFollow) { (result1) in
                    if(result1.ErrorCode != 0)
                    {
                        //self.updateUI()
                        FunctionHelper.showBannerAlertView(content: result1.Message)
                        //self.btnFollow.titleLabel?.text = _isFollowed ? "Bỏ theo dõi" : "Theo dõi"
                    }
                }
            }
        }
    }
    
    func touchOpenProfile(userId: Int64)
    {
        if(self.isStreamer) {return}
        self._popupController?.dismissPopupControllerAnimated(true);
        let userDetail = UserDetailViewController(nibName: "UserDetailViewController", bundle: nil)
        userDetail.userId = userId
        self.navigationController?.pushViewController(userDetail, animated: true)
    }
    
    //MARK: Events
    
    @IBAction func touchSeat(sender: AnyObject?){
       showSeatContainer()
    }
    
    @IBAction func touchClose(sender: AnyObject?){
        self.cleanUp()
        self.removeObserverRTMP()
        if(self.delegate != nil && !self.isStreamer){
            //self.delegate?.mediaStreamer_onClose()
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }else{
            JTProgressHUD.show()
            self.streamEnd()
        }
    }
    
    @IBAction func touchPlay(sender: AnyObject) {
        if let session = self.cameraSession, let streamInfo = self.cameraLiveStreamInfo{
            switch session.state {
            case .Pending, .Start, .Ready:
                self._isForceStop = true
                session.stopLive()
                break
            default:
                session.startLive(streamInfo)
            }
        }
    }
    
    @IBAction func touchedFlash(sender: AnyObject) {
        if let session = self.cameraSession {
            session.torch = !_isFlashEnable
            _isFlashEnable  = !_isFlashEnable
             self.btnFlash.setImage(UIImage(named: _isFlashEnable ? "ic_detail_flash_deactived" : "ic_detail_flash_actived"), forState: .Normal)
        }
    }
    
    @IBAction func touchedSwapCamera(sender: AnyObject)
    {
        if let session = self.cameraSession {
            let devicePositon = session.captureDevicePosition;
            session.captureDevicePosition = (devicePositon == AVCaptureDevicePosition.Back) ? AVCaptureDevicePosition.Front : AVCaptureDevicePosition.Back;
        }
    }
    
    @IBAction func touchedFilter(sender: AnyObject){
        if let session = self.cameraSession {
            session.beautyFace = !session.beautyFace;
            self.btnFilter.setImage(UIImage(named: session.beautyFace ? "ic_detail_filter_actived" :  "ic_detail_filter_deactived"  ), forState: .Normal)
        }

//        let filterEnable = _cameraSession!.enableFilter()
//        self.btnFilter.setImage(UIImage(named: filterEnable ? "ic_detail_filter_deactived" :  "ic_detail_filter_actived"  ), forState: .Normal)
    }
    
    
    @IBAction  func touchedShareFacebook(sender: AnyObject) {
        JTProgressHUD.show()
        let url = NSURL(string: "https://teenidol.vn/show/\(self._showId)")
        let content = FBSDKShareLinkContent()
        content.contentURL = url
        content.contentTitle = self._showInfo?.ShowInfo?.Name
        content.contentDescription = self._showInfo?.ShowInfo?.Description
        content.imageURL = NSURL(string: (self._showInfo?.ShowInfo?.Photo)!)
        FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: self)
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        JTProgressHUD.hide()
        FunctionHelper.showBannerAlertView(content: "Có lỗi khi chia sẽ Facebook. Xin thử lại")
    }
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        JTProgressHUD.hide()
        if(results.count > 0){
            let postId : String = results.stringForKey("postId")
            if(!postId.isEmpty){
                //TODO: Call service
            }
        }
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        JTProgressHUD.hide()
    }
    
    @IBAction func touchedFollow(sender: AnyObject) {
        self.checkLogin { (loginState) in
        if(loginState == .Logged || loginState == .Successed){
            let isFollow = self._idolInfo?.FollowInfo.Status == 1 ? true : false
            
            UserDAO.User_FollowUser(FollowUserId: (self._idolInfo?.StarUser.Id)!, IsFollow: !isFollow) { (result) in
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            }
        }
    }
    
    @IBAction func touchedChat(sender: AnyObject?) {
        viewChatInput.hidden = false
        viewChatInput.becomeFirstReponser()
    }
    
    @IBAction func touchedChangeQuatity(sender: AnyObject) {
        self.setupQuanlitySelector()
        let options  : [PopoverOption] = [
            .Type(.Up),
            .BlackOverlayColor(UIColor(white: 0.0, alpha: 0.7)),
            .Color(UIColor.blackColor())
        ]
        self._popoverQuanlity = Popover(options:options, showHandler: nil, dismissHandler: nil)
        self._popoverQuanlity?.show(self._quanlitySelectorView!, fromView: self.btnChangeVideoQuatity)
    }
    
    @IBAction func touchSendHeart(sender: AnyObject) {
        self.checkLogin { (loginState) in
            if(loginState == .Logged){
                if(self._isLoading) {return}
                self._isLoading = true
                UserDAO.User_GiveFreeCoin((self._showInfo?.Schedule?.Id)!, starId: (self._idolInfo?.StarUser.Id)!, callback: { (result) in
                    self._isLoading = false
                    if(result.ErrorCode == 0){
                        SettingsHelper.addCurrentUserHeart(-10)
                    }else{
                        FunctionHelper.showBannerAlertView(content: result.Message)
                    }
                })
            }
        }
    }
    
    //MARK: load stream other idol live
    func Follow_onSelectedLive(viewcontroller: UIViewController, userinfo: UserSMModel) {
        
        self.reloadWithModel((model: userinfo, image: userinfo.imgBackgroud))
    }
    
    func captureScreen() -> String
    {
        
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, 0);
        
        self.view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        let imageData:NSData = UIImagePNGRepresentation(image)!
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        return imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength);
    }
    
    // MARK: report 
    
    func showPopupReport(base64:String, userId: Int64)
    {
        let con = PopupReportViewController(nibName: "PopupReportViewController", bundle: nil)
        con.delegate = self
        con.setData(base64photo: base64, starId: userId)
        self._popupController = CNPPopupController(contents: [con.view])
        self._popupController!.mainController = con
        self._popupController!.theme = CNPPopupTheme.defaultTheme()
        self._popupController!.theme.popupStyle = .Centered
        self._popupController!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupController!.presentPopupControllerAnimated(true)
    }
 
    //MARK: Delegate report
    func popupReport_Submit(base64:String,starId: Int64, note: String) {
        var connectionType = "Unknown"
        if let checker = self._internetChecker {
            connectionType = checker.isReachableViaWiFi() ? "Wifi" : "WWan"
        }
        UserDAO.User_ReportIdol(Base64photo: base64, starId: starId , note: note, platform: 2, connectionType: connectionType) { (result) in
            if(result.ErrorCode == 0)
            {
                self._popupController?.dismissPopupControllerAnimated(true)
            }
            
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    func popupReport_Cancel() {
        _popupController?.dismissPopupControllerAnimated(true)
    }
    
    // MARK: Delegate app active & inactive
    func appActive()
    {
        if let serverIP = self._strStreamServer, let streamKey = self._strStreamerKey {
            let URL = "\(serverIP)\(streamKey)"
            self.setupPlayerSession(URL)
        }
    }
    
    func appInactive()
    {
        cleanUpRTMPPlayer()
    }
}

extension LFLiveState : CustomStringConvertible {
    public var description: String {
        switch self {
        case .Ready:
            return "Ready"
        case .Pending:
            return "Pending"
        case .Start:
            return "Start"
        case .Stop:
            return "Stop"
        case .Error:
            return "Error"
        default:
            return "Refresh"
        }
    }
}

extension LFLiveSocketErrorCode : CustomStringConvertible {
    public var description: String {
        switch self {
        case .PreView:
            return "PreView"
        case .GetStreamInfo:
            return "GetStreamInfo"
        case .ConnectSocket:
            return "ConnectSocket"
        case .Verification:
            return "Verification"
        default:
            return "ReConnectTimeOut"
        }
    }
}
