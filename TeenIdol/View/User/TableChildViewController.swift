//
//  TableChildTableViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/1/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class TableChildViewController: UITableViewController, IndicatorInfoProvider, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    let cellIdentifier_payCard = "payCell"
    let cellIdentifier_Spent = "spentCell"
    var itemInfo = IndicatorInfo(title: "View")
    
    var data = FListResultDTO<LogModel>()
    
    var type = CoinLogType.Payment
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, itemInfo: IndicatorInfo, type: CoinLogType) {
        self.itemInfo = itemInfo
        self.type = type
        super.init(nibName:nibNameOrNil, bundle:nibBundleOrNil)
        
    }
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo, type: CoinLogType) {
        self.itemInfo = itemInfo
        self.type = type
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.separatorStyle = .None
        tableView.registerNib(UINib(nibName: "PayCardTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier_payCard)
        tableView.registerNib(UINib(nibName: "PayLogTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier_Spent)
        
        setupPutToRefresh()
        loadData()
    }
    
    deinit{
        if(self.tableView != nil){
            self.tableView.dg_removePullToRefresh()
        }
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tableView.backgroundColor!
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<LogModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tableView.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }

    func loadData()
    {
        
        if(data.isLoadingData) { return }
        data.isLoadingData = true
        if(self.data.iPageIndex == 0){
            JTProgressHUD.show()
        }
        
        UserDAO.ListUserSpent(self.data.iPageIndex, pageSize: 10, type: type.rawValue, startTime: 0, endTime: 0) { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            
            if(result.iPageIndex == 0)
            {
                self.tableView.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0...result.ListItem.count - 1{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            
            self.data.iPageIndex = self.data.iPageIndex + 1
            self.data.isLoadingData = false
            self.tableView.dg_stopLoading()
        }
    }
    

    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count
    }

    // MARK: - IndicatorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == self.data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        if(type.rawValue == 1)
        {
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier_Spent, forIndexPath: indexPath) as! PayLogTableViewCell
            cell.setData(data.ListItem[indexPath.row] as! CoinLogModel)
            return cell
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier_payCard, forIndexPath: indexPath) as! PayCardTableViewCell
        cell.setDataPayment(data.ListItem[indexPath.row] as! PaymentLogModel)
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(type == CoinLogType.Spent)
        {
            return 50;
        }
        return 95;
    }
    
    // MARK: Delegate EmptyDataSet
    
    func titleForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        let attStr = NSAttributedString(string: "Không có thông tin", attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(18)])
        
        return attStr
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        var text = ""
        if(type == CoinLogType.Payment)
        {
            text = "Tất cả lịch sử giao dịch"
        }
        else
        {
            text = "Tặng quà cho Idol nhé"
        }
        let attStr = NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.systemFontOfSize(15)])
        
        return attStr
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView, forState state: UIControlState) -> NSAttributedString? {
        
        var text = ""
        if(type == CoinLogType.Payment)
        {
            text = "Nạp tiền"
        }
        
        let attStr = NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(15)])
        return attStr
    }
    
    func buttonBackgroundImageForEmptyDataSet(scrollView: UIScrollView, forState state: UIControlState) -> UIImage? {
        if(type == CoinLogType.Payment)
        {
            return UIImage(named: "bg_signin_button")
        }
        return nil
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView) {
        if(type == CoinLogType.Payment)
        {
            let vc = DomainViewController(nibName: "DomainViewController", bundle: nil)
            navigationController?.pushViewController(vc, animated: true);
        }
    }
    
    /*
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView, forState state: UIControlState) -> NSAttributedString? {
        return NSAttributedString(string: "Tải lại")
    }
    */
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView) -> UIColor? {
        return UIColor.whiteColor()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_live_idol");
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView) -> Bool {
        if(data.ListItem.count == 0)
        {
            return true;
        }
        return false
    }
    
}
