//
//  PopupReportViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/31/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol popupReportProtocol {
    func popupReport_Submit(base64:String, starId: Int64, note: String)
    func popupReport_Cancel()
}

class PopupReportViewController: UIViewController {

    @IBOutlet weak var txtFieldNote: UITextView?
    var delegate: popupReportProtocol?
    private var starId: Int64 = 0
    private var base64: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtFieldNote!.frame = CGRectInset(self.txtFieldNote!.frame, 0, -2)
        FunctionHelper.makeBorderView(txtFieldNote!, borderWidth: 1, color: UIColor.lightGrayColor(), cornerRadius: 5)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setData(base64photo base64: String, starId: Int64)
    {
        self.base64 = base64
        self.starId = starId
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func btSend()
    {
        if(delegate != nil)
        {
            delegate?.popupReport_Submit(base64,starId: starId, note: (txtFieldNote?.text)!)
        }
    }
    
    @IBAction func btCancel()
    {
        delegate?.popupReport_Cancel()
    }
    
}
