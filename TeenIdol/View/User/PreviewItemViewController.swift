//
//  PreviewItemViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/15/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SDWebImage

protocol PreviewItemProtocol {
    func PreviewItem_Close()
}

class PreviewItemViewController: UIViewController {
    
    @IBOutlet weak var imgItem: UIImageView?
    
    var urlGiftLink: String?
    var delegate: PreviewItemProtocol?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.bringSubviewToFront()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        loadData()
    }
    
    func loadData()
    {
        if let url = NSURL(string: urlGiftLink!){
            let downloader = SDWebImageDownloader.sharedDownloader()
            downloader.downloadImageWithURL(url, options:.UseNSURLCache, progress: nil, completed: { (image, data, error, isFinished) in
                if(error == nil && image != nil){
                    //self.viewAnimationContainer.showAnimation(image!, giftSize: CGSize(width: (image?.size.width)!,height: (image?.size.height)!), duration: duration, type: animationType)
                    
                    let _yyImage = YYImage(data: data)
                    self.scaleImage(_yyImage!)
                }
            })
        }

    }
    
    func scaleImage(imgSource: YYImage)
    {
        let animationMaxSize = self.view.frame
        let _imgSize = imgSource.size
        let _imgView = YYAnimatedImageView()
        _imgView.contentMode = .ScaleToFill
        _imgView.image = imgSource
        
        let _width = imgSource.size.width > animationMaxSize.width ? animationMaxSize.width : imgSource.size.width
        let _height = _imgSize.height * (_width / _imgSize.width)
        /*
        let position = CGPoint(x : _width > animationMaxSize.width ? 0 :
            (animationMaxSize.width - _width) / 2,
                               y: _height > animationMaxSize.height
                                ? 0 : animationMaxSize.height - _height)*/
        //let position = self.view.center
        _imgView.frame = CGRect(origin: CGPoint(x:0,y:0), size: CGSize(width: _width, height: _height))
        _imgView.center = self.view.center
        
        FunctionHelper.runSyncWithMainThread({
            //self.imgItem = _imgView
            self.view.addSubview(_imgView)
            self.view.sendSubviewToBack(_imgView)
            //self.performSelector(#selector(self.removeLargeImageView), withObject: _imgView, afterDelay: duration)
        })
    }
    
    @IBAction func btClose(sender: UIButton)
    {
        if(delegate != nil)
        {
            self.dismissViewControllerAnimated(true, completion: nil)
            self.delegate?.PreviewItem_Close()
        }
    }
}
