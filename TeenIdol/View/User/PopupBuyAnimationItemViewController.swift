//
//  PopupBuyAnimationItemViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol PopupBuyAnimationItemDelegate {
    func touchedBuy(itemId: Int, purchaseId: Int)
    func touchedClose()
    func touched_viewItem(giftLink: String)
}

class PopupBuyAnimationItemViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UIButton!
    @IBOutlet weak var btnBuy: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var viewItemAnimation: UIView!
    
    var tblItemPurchaseSelection : UITableView?
    var _popoverItemPurchase: Popover?;
    private var popoverOptions: [PopoverOption] = [
        .Type(.Down),
        .BlackOverlayColor(UIColor(white: 0.0, alpha: 0.0))
    ]
    
    var delegate: PopupBuyAnimationItemDelegate? = nil
    
    var model : AnimationItemModel = AnimationItemModel(){
        didSet{
            self.UpdateUI()
        }
    }
    
    var selectedItemPurchase : ItemPurchaseDTO?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI(){
        btnBuy.layer.cornerRadius = 5
        btnCancel.layer.cornerRadius = 5
        btnCancel.layer.borderWidth = 1
        btnCancel.layer.borderColor = UIColor(red: 233/255, green: 233/255, blue: 231/255, alpha: 1).CGColor
        lblPrice.layer.cornerRadius = 5
        lblPrice.layer.borderWidth = 1
        lblPrice.layer.borderColor = UIColor(red: 233/255, green: 233/255, blue: 231/255, alpha: 1).CGColor
        self.setupTableSelection()
        
        self.lblPrice.titleLabel?.font = UIFont.boldSystemFontOfSize(13)
        self.lblPrice.addTarget(self, action: #selector(touchedPriceSelection), forControlEvents: .TouchUpInside)
        
        _popoverItemPurchase?.popoverColor = UIColor.lightGrayColor()
        
        viewItemAnimation.userInteractionEnabled = true
        viewItemAnimation.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(touched_viewitem)))
    }
    func setupTableSelection(){
        
        self.view.bounds = CGRect(x: 0, y: 0, width: 280, height: 240)
        
        self.tblItemPurchaseSelection = UITableView(frame: CGRect(x:0, y:0, width: self.view.bounds.size.width/1.75,height: self.view.bounds.size.height / 1.5))
        
        self.tblItemPurchaseSelection?.registerClass(ItemPurchaseSelection.self, forCellReuseIdentifier: "ItemPurchaseIndentifier")
        self.tblItemPurchaseSelection?.showsVerticalScrollIndicator = false
        self.tblItemPurchaseSelection?.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tblItemPurchaseSelection?.dataSource = self
        self.tblItemPurchaseSelection?.delegate = self
    }
    
    private func UpdateUI(){
        let url = NSURL(string: (self.model.animationItem?.photoLink)!)
        
        if(url != nil){
            imgMain.sd_setImageWithURL(url!)
        }
        self.lblName.text = self.model.animationItem?.name
        
        if(self.model.listItemPurchases.count > 0){
            self.selectedItemPurchase = self.model.listItemPurchases[0]
            self.tblItemPurchaseSelection?.reloadData()
            
            self.lblPrice.setTitle(self.selectedItemPurchase?.PriceToStr, forState: .Normal)
            self.lblTime.text = "\((self.selectedItemPurchase?.NumberOfDayExprire)!) ngày"
        }

    }
    
    //MARK : Table View DataSource + Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.model.listItemPurchases.count)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        //return tableView.bounds.size.height / 3
        return 25;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = self.tblItemPurchaseSelection!.dequeueReusableCellWithIdentifier("ItemPurchaseIndentifier", forIndexPath: indexPath) as! ItemPurchaseSelection
        cell.setData(self.model.listItemPurchases[indexPath.row])
        //cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedItemPurchase = self.model.listItemPurchases[indexPath.row]
        self._popoverItemPurchase?.dismiss()
        
        self.lblPrice.setTitle(self.selectedItemPurchase?.PriceToStr, forState: .Normal)
        self.lblTime.text = "\((self.selectedItemPurchase?.NumberOfDayExprire)!) ngày"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCancel(sender: UIButton)
    {
        if(delegate != nil)
        {
            self.delegate!.touchedClose();
        }
    }
    
    @IBAction func btnBuy(sender: UIButton)
    {
        if(delegate != nil)
        {
            let itemId = self.model.animationItem?.Id
            let purchaseId = self.selectedItemPurchase?.Id
            self.delegate!.touchedBuy(itemId!, purchaseId: purchaseId!);
        }
    }
    
    func touchedPriceSelection()
    {
        self._popoverItemPurchase = Popover(options: self.popoverOptions, showHandler: nil, dismissHandler: nil)
        self._popoverItemPurchase?.show(self.tblItemPurchaseSelection!, fromView: self.lblPrice!)
    }
    
    func touched_viewitem()
    {
        if(delegate != nil)
        {
            self.delegate!.touched_viewItem((self.model.animationItem?.gifLinkMobile)!)
        }
    }
}

class ItemPurchaseSelection : UITableViewCell {
    var lblDescription : UILabel?
    var model : ItemPurchaseDTO?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let width  = CGFloat(75)
        self.contentView.frame = CGRect(x: 0, y: 0, width: width, height: 20)
        
        if(lblDescription == nil){
            lblDescription = UILabel(frame: CGRect(x :5, y: 5,
                width: width, height: 18))
            lblDescription?.font = UIFont.boldSystemFontOfSize(12)
            lblDescription?.numberOfLines = 1
            lblDescription?.textAlignment = .Left
            lblDescription?.textColor = UIColor(red: 101/255, green: 117/255, blue: 117/255, alpha: 1)
            self.contentView.addSubview(lblDescription!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(data: ItemPurchaseDTO){
        self.model = data
        let price = self.model?.Price > 0 ? (self.model?.Price)! : self.model?.EventCoin
        self.lblDescription?.text = "\(price!) \((self.model?.Currency)!)/\((self.model?.NumberOfDayExprire)!) ngày"
        self.lblDescription?.sizeToFit()
    }
}



