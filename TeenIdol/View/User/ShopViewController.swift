//
//  ShopViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/1/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class VipRuleTableViewCell : UITableViewCell{
    var lblIndex : UILabel?
    var lblRuleDes : UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        lblIndex = UILabel(frame: CGRect(x: 5,y: 5,width: 26,height: 26))
        lblIndex?.backgroundColor = ColorAppDefault
        lblIndex?.layer.cornerRadius = 13
        lblIndex?.layer.masksToBounds = true
        lblIndex?.font = UIFont.boldSystemFontOfSize(14)
        lblIndex?.textColor = UIColor.whiteColor()
        lblIndex?.textAlignment = .Center
        self.addSubview(lblIndex!)
        lblRuleDes = UILabel(frame: CGRect(x: lblIndex!.frame.size.width + 10 ,y: 7,width: self.contentView.frame.size.width - 45,height: 22))
        //lblIndex?.backgroundColor = ColorAppDefault
        //lblIndex?.layer.cornerRadius = 13
        lblRuleDes?.font = UIFont.systemFontOfSize(14)
        lblRuleDes?.numberOfLines = 2
        lblRuleDes?.adjustsFontSizeToFitWidth = true
        self.addSubview(lblRuleDes!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(index: Int, data: VipFeatureDTO){
        self.lblIndex?.text = "\(index)"
        self.lblRuleDes?.text = data.Description
    }
}

class VipPriceCollectionViewCell : UICollectionViewCell {
    var lblPrice : UILabel?
    var lblTime : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.backgroundColor = UIColor.whiteColor()
        self.layer.cornerRadius = 10
        self.layer.borderColor = UIColor.groupTableViewBackgroundColor().CGColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = true
        lblPrice = UILabel(frame: CGRect(x: 0,y: 20,width: self.contentView.frame.size.width,height: 26))
        lblPrice?.font = UIFont.boldSystemFontOfSize(20)
        lblPrice?.textColor =  ColorAppDefault
        lblPrice?.textAlignment = .Center
        
        self.addSubview(lblPrice!)
        lblTime = UILabel(frame: CGRect(x: 0,y: 50,width: self.contentView.frame.size.width,height: 15))
        lblTime?.font = UIFont.systemFontOfSize(14)
        lblTime?.textColor =  UIColor.lightGrayColor()
        lblTime?.textAlignment = .Center
        
        self.addSubview(lblTime!)
    }
    
    func setData(data: ItemPurchaseDTO){
        self.lblPrice?.text = "\(data.Price) Ruby"
        self.lblTime?.text = "\(data.NumberOfDayExprire) ngày"
    }
}

class VipCategorySelectorCollectionViewCell : UICollectionViewCell {
    var lblName : UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.backgroundColor = UIColor.whiteColor()
        lblName = UILabel(frame: CGRect(x: 0,y: 10,width: self.contentView.frame.size.width,height: 30))
        lblName?.font = UIFont.boldSystemFontOfSize(20)
        lblName?.textColor =  UIColor.lightGrayColor()
        lblName?.textAlignment = .Center
        
        self.addSubview(lblName!)
    }
    
    func setData(name: String){
        self.lblName?.text = name
    }
    
    override var selected: Bool{
        get {
            return super.selected
        }
        set{
            if(newValue){
                super.selected = true
                self.lblName?.textColor = ColorAppDefault
            }else{
                super.selected = false
                self.lblName?.textColor = UIColor.lightGrayColor()
            }
        }
    }
}


class ShopViewController: UIViewController,  UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var btnVipNormal: UIButton!;
    @IBOutlet weak var btnVipSuper: UIButton!;
    @IBOutlet weak var tblRule: UITableView!
    @IBOutlet weak var colPrice: UICollectionView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var conViewLineToLeft : NSLayoutConstraint!
    @IBOutlet weak var colCatogory: UICollectionView!
    
    private var _selectedCategoryIndex = 0
    private var currentIndexSelected = 0

    var categoryId: Int = -1;
    var dataVipItem = FListResultDTO<VipItemModel>()
    private var vipItemSelected = VipItemModel(){
        didSet{
            self.colPrice.reloadData()
            self.tblRule.reloadData()
        }
    }
    private var vipPurchaseSelected : ItemPurchaseDTO?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        setupLeftMenuButton()
        setupNavigationBar()
        loadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpUI()
    {
        //self.title = "VIP Shop"
        
        self.tblRule.registerClass(VipRuleTableViewCell.self, forCellReuseIdentifier: "VipRuleIndentifier")
        self.tblRule.layer.cornerRadius = 10
        self.colPrice.registerClass(VipPriceCollectionViewCell.self, forCellWithReuseIdentifier: "VipPriceIndentifier")
        self.colCatogory.registerClass(VipCategorySelectorCollectionViewCell.self, forCellWithReuseIdentifier: "VipCategorySelectorIndentifier")
    }
    
    func setupLeftMenuButton(){
        let button = FunctionHelper.getNavigationBarButton("ic_arrow_left")
        button.addTarget(self, action: #selector(leftButtonPress), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem( UIBarButtonItem(customView:
            button), animated: true)
    }
    
    func leftButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupNavigationBar(){
        let title = UILabel(frame: CGRectMake(0,0,150,32))
        title.text = "VIP Shop"
        title.font = UIFont.boldSystemFontOfSize(17)
        title.textColor = UIColor.whiteColor()
        title.textAlignment = .Center
        self.navigationItem.titleView = title
        
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
    }
    
    func loadData()
    {
        if(dataVipItem.isLoadingData) {
            return
        }
        dataVipItem.isLoadingData = true
        JTProgressHUD.show()
        ItemDAO.List_ItemsVipInStore(0, pageSize: 10, categoryId: categoryId) { (result) in
            JTProgressHUD.hide()
            self.dataVipItem = result
            self.dataVipItem.isLoadingData = false
            self.colCatogory.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vipItemSelected.ListFeatures.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("VipRuleIndentifier", forIndexPath: indexPath) as! VipRuleTableViewCell
        cell.setData(indexPath.row + 1, data: vipItemSelected.ListFeatures[indexPath.row])
        cell.selectionStyle = .None
        return cell
    }
    
    //MARK: CollectionView DataSource + Delegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.colCatogory){
            return self.dataVipItem.ListItem.count
        }else{
            return vipItemSelected.ListPurchases.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        if(collectionView == self.colCatogory){
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if(collectionView == self.colCatogory){
            let index = indexPath.row
            if(self.currentIndexSelected == index) {return}
            self.currentIndexSelected = index
            self.vipItemSelected = self.dataVipItem.ListItem[self.currentIndexSelected]
        }else{
            self.vipPurchaseSelected = self.vipItemSelected.ListPurchases[indexPath.row]
            let str = "Mua \(self.vipItemSelected.VipItem.name) với giá \(self.vipPurchaseSelected!.Price) ruby."
            let alertView = UIAlertView(title: "Xác nhận", message: str, delegate: self, cancelButtonTitle: "Đồng ý", otherButtonTitles: "Hủy")
            alertView.show()
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if(collectionView == self.colCatogory){
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("VipCategorySelectorIndentifier", forIndexPath: indexPath) as! VipCategorySelectorCollectionViewCell
            let model = self.dataVipItem.ListItem[indexPath.row]
            cell.setData(model.VipItem.name)
            if(self.currentIndexSelected == indexPath.row){
                self.vipItemSelected = model
                cell.selected = true
                collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .None)
            }
            return cell
        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("VipPriceIndentifier", forIndexPath: indexPath) as! VipPriceCollectionViewCell
        cell.setData(vipItemSelected.ListPurchases[indexPath.row])
        return cell
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){ //Accept
            JTProgressHUD.show()
            UserDAO.User_BuyItems(self.vipItemSelected.VipItem.Id, purchaseId: self.vipPurchaseSelected!.Id, isVipItem: true) { (result) in
                JTProgressHUD.hide()
                FunctionHelper.showBannerAlertView(content: result.Message)
            }
        }
    }
}
