//
//  DomainViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import StoreKit
import Alamofire

class DomainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lbCoinUsed : UILabel!
    @IBOutlet weak var lbCoinAvaible: UILabel!
    @IBOutlet weak var lbCoinFromGift: UILabel!
    @IBOutlet weak var btnCardPayment: UIButton!
    @IBOutlet weak var tblInAppPurchase : UITableView!
    
    var _data  = Array<InAppPurchaseDTO>()
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Tài sản"
        setupNavigationBar()
        setupLeftMenuButton()
        self.setupInAppPurchaseData()
        self.tblInAppPurchase.registerNib(UINib(nibName: "InAppPurchaseTableViewCell", bundle: nil), forCellReuseIdentifier: "InAppPurchaseCellIndentifier")
        
        // Do any additional setup after loading the view.
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UpdateUI), name: UserRubyChangeNotificationName, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
//        if let config = SettingsHelper.getConfig(){
//            self.btnCardPayment.hidden = config.isPublishVersion
//        }
        UpdateUI()
        
        
        //self.title = "Purchase View Controller"
        
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: "Purchase View Controller")
        
        let builder = GAIDictionaryBuilder.createScreenView().build()
        tracker.send(builder as [NSObject : AnyObject])
    }
    
    deinit{
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func setupInAppPurchaseData(){
        /*
        let item1 = InAppPurchaseDTO(ruby: 150, price: "0.99$", storeId: "pur200Ruby")
        //let item2 = InAppPurchaseDTO(ruby: 480, price: "2.99$", storeId: "pur600Ruby")
        let item3 = InAppPurchaseDTO(ruby: 1500, price: "9.99$", storeId: "pur2000Ruby")
        //let item4 = InAppPurchaseDTO(ruby: 3200, price: "19.99$", storeId: "pur4000Ruby")
        //let item5 = InAppPurchaseDTO(ruby: 16000, price: "99.99$", storeId: "pur16000Ruby")
        
        self._data.append(item1)
        //self._data.append(item2)
        self._data.append(item3)
        //self._data.append(item4)
        //self._data.append(item5)
        
        return
        */
       JTProgressHUD.show()
        
        SwiftyStoreKit.retrieveProductsInfo(["com.tfl.liveidol.purchase1","com.tfl.liveidol.purchase2"]) { (result) in
           
            for p in result.retrievedProducts {
                let item = InAppPurchaseDTO()
                switch p.productIdentifier {
                case "com.tfl.liveidol.purchase1" :
                    item.rubyTotal = 150
                case "com.tfl.liveidol.purchase2":
                    item.rubyTotal = 1500
                default:
                   break
                }
                
                let strPrice = String(format: "%.2f", p.price.doubleValue)
                let symbol = "\(p.priceLocale.objectForKey(NSLocaleCurrencySymbol) as! String) "
                
                item.price = " \(strPrice) \(symbol)"
                item.storeId = p.productIdentifier
                self._data.append(item)
                
            }
            self._data.sortInPlace({ (t1, t2 ) -> Bool in
                return t1.rubyTotal > t2.rubyTotal
            })
            self.tblInAppPurchase.reloadData()
            JTProgressHUD.hide()
        }
    }
    
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableView DataSource + Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _data.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("InAppPurchaseCellIndentifier", forIndexPath: indexPath) as! InAppPurchaseTableViewCell
        cell.setData(self._data[indexPath.row])
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let data = self._data[indexPath.row]
        let productStr = data.storeId
        JTProgressHUD.show()
        SwiftyStoreKit.purchaseProduct(productStr) { result in
            self.showAlert(self.alertForPurchaseResult(result))
            switch result{
            case .Success:
                let urlReceipt = NSBundle.mainBundle().appStoreReceiptURL
                if(urlReceipt != nil){
                    let dataReceipt = NSData(contentsOfURL: urlReceipt!)
                    let base64Encoded = dataReceipt?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    UserDAO.System_AppPurchaseIOS(base64Encoded!, ruby: data.rubyTotal ,callback: { (result) in
                        if(result.ErrorCode == 0){
                            SettingsHelper.addCurrentUserRuby(data.rubyTotal)
                        }
                        FunctionHelper.showBannerAlertView(content: result.Message)
                    })
                }
                break
                
            default:
                break
            }
        }
    }
    func alertForProductRetrievalInfo(result: SwiftyStoreKit.RetrieveResults) -> UIAlertController {
        
        if let product = result.retrievedProducts.first {
            let priceString = NSNumberFormatter.localizedStringFromNumber(product.price, numberStyle: .CurrencyStyle)
            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
        }
        else if let invalidProductId = result.invalidProductIDs.first {
            return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
        }
        else {
            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
            return alertWithTitle("Could not retrieve product info", message: errorString)
        }
    }
    func showAlert(alert: UIAlertController) {
        guard let _ = self.presentedViewController else {
            JTProgressHUD.hide()
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertWithTitle(title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
        return alert
    }
    
    func alertForPurchaseResult(result: SwiftyStoreKit.PurchaseResult) -> UIAlertController {
        switch result {
        case .Success:
            return alertWithTitle("Cám ơn", message: "Thanh toán thành công.")
        case .Error(let error):
            switch error {
            case .Failed(let error):
                if error.domain == SKErrorDomain {
                    return alertWithTitle("Thanh toán thất bại", message: "Quá trình thanh toán thất bại, xin thử lại sau.")
                }
                return alertWithTitle("Thanh toán thất bại", message: "Xin thử lại.")
            case .InvalidProductId:
                return alertWithTitle("Thanh toán thất bại", message: "Xin thử lại.")
            case .NoProductIdentifier:
                return alertWithTitle("Thanh toán thất bại", message: "Xin thử lại.")
            case .PaymentNotAllowed:
                return alertWithTitle("Thanh toán thất bại", message: "Bạn không chấp nhận thanh toán")
            }
        }
    }
    
    func UpdateUI()
    {
       if let userInfo = SettingsHelper.getUserInfo() {
            lbCoinUsed.text = FunctionHelper.formatNumberWithSeperator((userInfo.userInfo?.TotalCoinUsed)!)
            lbCoinAvaible.text = FunctionHelper.formatNumberWithSeperator((userInfo.userInfo?.Coin)!)
        
        if let starInfo = userInfo.starDataInfo {
            lbCoinFromGift.text = FunctionHelper.formatNumberWithSeperator(starInfo.coinGetFromGift)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
