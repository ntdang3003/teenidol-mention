//
//  ChangePasswordViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/14/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var txtPass2: UITextField!
    var secureText = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupUI()
        setupLeftMenuButton()
    }

    func setupUI()
    {
        title = "Đổi mật khẩu"
        txtOldPass.secureTextEntry = true
        txtPass.secureTextEntry = true
        txtPass2.secureTextEntry = true
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
        
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    @IBAction func btUpdatePass()
    {
        if(txtPass.text?.isEmpty == true){
            FunctionHelper.showBannerAlertView("LiveIdol", content: "Mật khẩu rỗng.", callback: nil)
            return
        }
        
        if(txtPass2.text?.isEmpty == true){
            FunctionHelper.showBannerAlertView("LiveIdol", content: "Mật khẩu xác nhận rỗng.", callback: nil)
            return
        }
        
        if(txtPass.text != txtPass2.text)
        {
            FunctionHelper.showBannerAlertView("LiveIdol", content: "Xác nhận mật khẩu không giống nhau.", callback: nil)
            return
        }
        
        UserDAO.User_UpdateNewPassWord(OldPass: txtOldPass.text!, NewPass: txtPass.text!) { (result) in
            
            if(result.ErrorCode == 0)
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            FunctionHelper.showBannerAlertView("LiveIdol", content: result.Message, callback: nil)
        }
    }
    
    @IBAction func btShowPass(sender: AnyObject)
    {
        secureText = !secureText
        txtOldPass.secureTextEntry = secureText
        (sender as! UIButton).setTitle(secureText ? "Hiện" : "Ẩn", forState: .Normal)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
