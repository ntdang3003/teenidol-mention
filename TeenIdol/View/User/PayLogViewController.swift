//
//  PayLogViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/19/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

enum CoinLogType: Int {
    case Spent = 1
    case Payment = 2
}

class PayLogViewController: ButtonBarPagerTabStripViewController
//UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var constraintLayoutHeight_SpentLog: NSLayoutConstraint?;
    
    var data = FListResultDTO<LogModel>()
    
    var type = CoinLogType.Payment
    private var btPayment: ButtonBarViewCell?;
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {

        setupUI()
        setupNavigationBar()
        
        setupLeftMenuButton()
        
        //setupPutToRefresh()
    }
    
    override func viewWillAppear(animated: Bool) {
        //loadData()
    }
    
    func setupUI()
    {
        /*
        self.tbLog.dataSource = self
        self.tbLog.delegate = self
        
        tbLog.registerNib(UINib(nibName: "PayLogTableViewCell", bundle: nil), forCellReuseIdentifier: "PayCell")
         */
        
        settings.style.buttonBarBackgroundColor = .clearColor()
        settings.style.buttonBarItemBackgroundColor = .clearColor()
        settings.style.selectedBarBackgroundColor = .whiteColor()
        settings.style.buttonBarItemFont = UIFont.boldSystemFontOfSize(16)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .blackColor()
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 10
        settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(red: 225/255, green: 225/255, blue: 225/255, alpha: 1)
            newCell?.label.textColor = .whiteColor()
            if(self.btPayment == nil)
            {
                self.btPayment = oldCell;
            }
            var _height: CGFloat = 0;
            if(oldCell == self.btPayment)
            {
               _height = 0;
            }
            else
            {
                _height = 40;
            }
            self.constraintLayoutHeight_SpentLog?.constant = _height;
                
            UIView.animateWithDuration(0.5, animations: {
                
                self.view.layoutIfNeeded()
            })
        }
        super.viewDidLoad()
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(leftDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
        
        
        buttonBarView.removeFromSuperview()
        buttonBarView.frame = CGRectMake(0, 0, 200, 30)
        
        let selBar = buttonBarView.selectedBar.frame;
        buttonBarView.selectedBar.frame = CGRectMake(selBar.origin.x, 28, selBar.width, selBar.height);
        
        self.navigationItem.titleView = buttonBarView
    }
    
    func leftDrawerButtonPress()
    {
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        let child_1 = TableChildViewController(style: .Plain, itemInfo: "Nạp tiền", type: CoinLogType.Payment)
//        let child_2 = TableChildViewController(style: .Plain, itemInfo: "Chi tiêu", type: CoinLogType.Spent)
        
        let child_1 = TableChildViewController(nibName:nil, bundle:nil, itemInfo: "Nạp tiền", type: CoinLogType.Payment)
        let child_2 = TableChildViewController(nibName:nil, bundle:nil, itemInfo: "Chi tiêu", type: CoinLogType.Spent)
//        let child_1 = FollowTableView(nibName:nil, bundle:nil, itemInfo: "Theo dõi", type: User_GetListType.UserFollow)
//        let child_2 = FollowTableView(nibName:nil, bundle:nil, itemInfo: "Fan", type: User_GetListType.UserFan)

        return [child_1, child_2]
    }
    
    /*
    deinit{
        self.tbLog.dg_removePullToRefresh()
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tbLog.backgroundColor!
        
        self.tbLog.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<LogModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tbLog.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tbLog.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    func loadData()
    {
        if(data.isLoadingData) { return }
        data.isLoadingData = true
        if(self.data.iPageIndex == 0){
            JTProgressHUD.show()
        }
        
        UserDAO.ListUserSpent(self.data.iPageIndex, pageSize: 10, type: type.rawValue, startTime: 0, endTime: 0) { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            
            if(result.iPageIndex == 0)
            {
                self.tbLog.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0...result.ListItem.count - 1{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tbLog.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tbLog.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            
            self.data.iPageIndex = self.data.iPageIndex + 1
            self.data.isLoadingData = false
            self.tbLog.dg_stopLoading()
        }
    }
    
   
    //TODO: Select TYPE
    @IBAction func touchedType(sender: UIButton){
        let _type = CoinLogType(rawValue: sender.tag)
        if(_type == type) {return}
        
        self.type = _type!
        data = FListResultDTO<LogModel>()
        loadData()
        
        self.resetButtonColor()
        self.conViewLineToLeft.constant = sender.frame.origin.x
        UIView.animateWithDuration(0.35) {
            self.view.layoutIfNeeded()
        }
        sender.setTitleColor(ColorAppDefault, forState: .Normal)
    }
    
    func resetButtonColor(){
        self.btSpent.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
        self.btPayment.setTitleColor(UIColor.lightGrayColor(), forState: .Normal)
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == self.data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("PayCell", forIndexPath: indexPath) as! PayLogTableViewCell
        if(type.rawValue == 1)
        {
            cell.setData(data.ListItem[indexPath.row] as! CoinLogModel)
            return cell;
        }

        cell.setDataPayment(data.ListItem[indexPath.row] as! PaymentLogModel)
        return cell
    }
    */
}

class PayLogTableViewCell : UITableViewCell
{
    @IBOutlet weak var lbCardCost: UILabel!;
    @IBOutlet weak var lbCreatedDate: UILabel!;
    @IBOutlet weak var lbDescription: UILabel!;
    
    override func awakeFromNib() {
        lbDescription.font = UIFont.systemFontOfSize(13)
    }
    
    func setData(model: CoinLogModel)
    {
        lbCardCost.text = "\((model.coinLog?.coinAmount)!)"
        //lbCreatedDate.text = "\((model.coinLog?.createDate)!)"
        lbDescription.text = "\((model.coinLog?.Description)!)"
        let date = NSDate(timeIntervalSince1970: (Double((model.coinLog?.createDate)!) / 1000))
        lbCreatedDate.text = FunctionHelper.dateToString(date, mode: 5)
    }
    
    /*
    func setDataPayment(model: PaymentLogModel)
    {
        //let paymentValue = model.paymentLog?.amount == 0 ? "\((model.paymentLog?.coin)!) Ruby" : "\((model.paymentLog?.amount)!) VND"
        
        let paymentValue = "\(FunctionHelper.formatNumberWithSeperator((model.paymentLog?.amount)!)) VND\n\((model.paymentLog?.coin)!) Ruby"
        
        lbCardCost.text = paymentValue
        var status = ""
        switch (model.paymentLog?.status)! {
        case 0:
            status = "Đang xử lý"
            break
        case 1:
            status = "Thành công"
            break
        case 2:
            status = "Thất bại"
            break
        default:
            break
        }
        lbDescription.text = "\((model.paymentType?.name)!) \n\(status)"
        let date = NSDate(timeIntervalSince1970: (Double((model.paymentLog?.createDate)!) / 1000))
        lbCreatedDate.text = FunctionHelper.dateToString(date, mode: 5)
    }
 */
    
}
