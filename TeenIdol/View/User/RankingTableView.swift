//
//  RankingTableView.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/12/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class RankingTableView: UITableViewController, IndicatorInfoProvider {
    
    var itemInfo = IndicatorInfo(title: "View")
    
    var showInfo: ShowModel?
    var data =  FListResultDTO<GiveAwayModel>()
    var totalRuby: (InShow: Int, InSchedule: Int)?;
    
    private var _scheduleId:Int64 = -1;
    
    /// scheduleId: -1: all | != -1: in show
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, itemInfo: IndicatorInfo, scheduleId: Int64) {
        self.itemInfo = itemInfo
        self._scheduleId = scheduleId
        super.init(nibName:nibNameOrNil, bundle:nibBundleOrNil)
    }
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo, scheduleId: Int64) {
        self.itemInfo = itemInfo
        self._scheduleId = scheduleId
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .None
        
        tableView.registerNib(UINib(nibName: "rankingTableViewCell", bundle: nil), forCellReuseIdentifier: "rankingTopCell")
        tableView.registerNib(UINib(nibName: "IdolInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "rankingCell")
       
        
        tableView.delegate = self
        tableView.dataSource = self
        
        setupPutToRefresh()
        loadData()
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tableView.backgroundColor!
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<GiveAwayModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tableView.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    deinit{
        if(self.tableView != nil){
            self.tableView.dg_removePullToRefresh()
        }
    }
    
    func loadData()
    {
        if(data.isLoadingData) {
            return
        }
        data.isLoadingData = true
        let showId = showInfo?.ShowInfo?.Id
        
        if(showId == nil)
        {
            fatalError("showId is nil")
        }
        
        if(self.data.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }
        
        UserDAO.User_GetListTopUserUseCoinInShow(_scheduleId, showId: showId!, starId: -1, startDate: -1, endDate: -1, pageIndex: self.data.iPageIndex, pageSize: 10) { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            if(self.data.iPageIndex == 0)
            {
                self.tableView.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = (self.data.ListItem.count - 3) - result.ListItem.count
                for index in 0..<result.ListItem.count{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 1))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            self.data.isLoadingData = false
            self.data.iPageIndex  += 1
            self.tableView.dg_stopLoading()
        }
    }
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(data.ListItem.count>0 && data.ListItem.count <= 3)
        {
            return 1
        }
        else if(data.ListItem.count > 3)
        {
            return 2
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            if(data.ListItem.count > 3)
            {
                return 3;
            }
            else
            {
                return data.ListItem.count
            }
        }
        
        return data.ListItem.count - 3;
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(section == 0 && self.data.ListItem.count > 3){
            let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 10))
            viewHeader.backgroundColor = UIColor.groupTableViewBackgroundColor()
            return viewHeader
        }
        return UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 0))
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 1)
        {
            return 60;
        }
        return 185;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section == 1)
        {
            let newIndex = indexPath.row + 3
            if(newIndex > data.ListItem.count){
                return UITableViewCell()
            }
            
            //Load More If Need
            if(self.data.CountAllResult != self.data.ListItem.count && newIndex == self.data.ListItem.count - 1){
                self.loadData()
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("rankingCell", forIndexPath: indexPath) as! IdolInfoTableViewCell
            cell.setDataRanking(self.data.ListItem[newIndex],index: newIndex + 1)
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("rankingTopCell", forIndexPath: indexPath) as! rankingTableViewCell
            if(indexPath.row > data.ListItem.count){
                return UITableViewCell()
            }
            cell.setData(data.ListItem[indexPath.row], index: indexPath.row);
            return cell;
        }
    }
    
}
