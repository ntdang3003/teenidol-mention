//
//  StreamerProfileView.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class StreamerProfileEditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imgAvatar: UIImageView!;
    @IBOutlet weak var imgShow: UIImageView!;
    @IBOutlet weak var txtShowName: UITextField?;
    @IBOutlet weak var txtName: UITextField!;
    @IBOutlet weak var txtPass: UITextField!;
    
    @IBOutlet weak var imgPass: UIImageView!;
    
    var isAvatar = true
    
    var streamerInfo = UserModel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        setupUI()
        setupLeftMenuButton()
        
        updateUI()
    }
    
    func setupUI()
    {
        title = "Chỉnh sửa thông tin"
        
        imgAvatar.userInteractionEnabled = true
        imgShow.userInteractionEnabled = true
        imgPass.userInteractionEnabled = true
        
        FunctionHelper.circleBorderImageView(imgAvatar, color: UIColor(red: 245/255, green: 191/255, blue: 195/255, alpha: 1).CGColor, borderWidth: 3)
        FunctionHelper.makeBorderView(imgShow, borderWidth: 3, color: UIColor(red: 245/255, green: 191/255, blue: 195/255, alpha: 1), cornerRadius: 0)
        
        txtPass.text = "1234"
        txtPass.userInteractionEnabled = false
        
        let tapChangePass = UITapGestureRecognizer(target: self, action: #selector(onChangePass))
        let tapEditImgAvatar = UITapGestureRecognizer(target: self, action: #selector(onEditImgAvatar))
        let tapEditImgShow = UITapGestureRecognizer(target:  self, action: #selector(onEditImgShow))
        
        imgAvatar.addGestureRecognizer(tapEditImgAvatar)
        imgShow.addGestureRecognizer(tapEditImgShow)
        imgPass.addGestureRecognizer(tapChangePass)
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
        
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func onChangePass()
    {
        let changePassVC = ChangePasswordViewController.init(nibName: "ChangePasswordViewController", bundle: nil)
        self.navigationController?.pushViewController(changePassVC, animated: true)
    }
    
    func onEditImgAvatar()
    {
        isAvatar = true
        openAlbum()
    }
    
    func onEditImgShow()
    {
        isAvatar = false
        openAlbum()
    }
    
    func openAlbum()
    {
        let album = UIImagePickerController()
        album.title = isAvatar ? "Đổi ảnh Avatar" : "Đổi hình Show"
        album.delegate = self
        album.sourceType = .PhotoLibrary
        album.allowsEditing = true
        self.presentViewController(album, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        if isAvatar == true
        {
            imgAvatar.image = image
        }
        else
        {
            imgShow.image = image
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updateUI()
    {
        imgAvatar.sd_setImageWithURL(NSURL(string:(streamerInfo.userInfo?.AvatarPhoto)!), placeholderImage: UIImage(named: "ic_default_avatar"))
        imgShow.sd_setImageWithURL(NSURL(string:(streamerInfo.showInfo?.Photo)!), placeholderImage: UIImage(named: "ic_default_avatar"))
        
        txtName.text = streamerInfo.userInfo?.Name
        txtShowName?.text = streamerInfo.showInfo?.Name
    }
    
    @IBAction func btChangePass()
    {
        streamerInfo.userInfo?.Name = txtName.text!
        //streamerInfo.showInfo?.Name = (txtShowName?.text)!
        
        UserDAO.User_UpdateTeenIdolAccount(UserInfo: self.streamerInfo.userInfo!) { (result) in
            if result.ErrorCode == 0
            {
                self.streamerInfo.userInfo = result.DataObject as? UserDTO
                self.updateUI()
            }
            FunctionHelper.showBannerAlertView("Thông báo", content: result.Message, callback: nil)
        }
        
        // cap nhat avatar
        UserDAO.User_UploadPhoto(BasePhoto64: FunctionHelper.imgToStr_Base64(imgAvatar.image)) { (result) in
            
            FunctionHelper.showBannerAlertView("Thông báo", content: result.Message, callback: nil)
            
        }
    }
}
