//
//  InventoryShopViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/3/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class InventoryShopViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, PopupBuyAnimationItemDelegate, PreviewItemProtocol {
    @IBOutlet weak var colMain : UICollectionView!
    
    var data = FListResultDTO<ItemCategoryModel>()
    var popUpBuyItem: PopupBuyAnimationItemViewController!
    
    private var _popupController : CNPPopupController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if(colMain == nil)
//        {
//            colMain = (self.view.viewWithTag(10) as! UICollectionView);
//        }
        
        colMain.registerClass(InventoryShopCollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "InventoryHeaderIndentifier")
        colMain.registerNib(UINib(nibName: "InventoryShopCollectionViewCell", bundle:  nil), forCellWithReuseIdentifier: "InventoryCellIndentifier")
        
        self.setupUI()
        
        self.loadData()
        
        self.colMain.dataSource = self
        self.colMain.delegate = self
    }

    func setupUI()
    {
        self.setupLeftMenuButton()
        self.title = "Cửa hàng trang bị"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        if(data.isLoadingData) {
            return
        }
        data.isLoadingData = true
        JTProgressHUD.show()
        ItemDAO.List_ItemCategories(self.data.iPageIndex, pageSize: 100) { (result) in
            JTProgressHUD.hide()
            self.data = result
            self.data.isLoadingData = false
            self.data.iPageIndex += 1
            self.colMain.reloadData()
        }
    }
    func setupLeftMenuButton(){
        let button = FunctionHelper.getNavigationBarButton("ic_arrow_left")
        button.addTarget(self, action: #selector(leftButtonPress), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem( UIBarButtonItem(customView:
            button), animated: true)
    }
    
    func leftButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    //MARK: Collection DataSource & Delegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return data.ListItem.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let data = self.data.ListItem[section].listAnimationItem
        return data.ListItem.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = colMain.dequeueReusableCellWithReuseIdentifier("InventoryCellIndentifier", forIndexPath: indexPath) as! InventoryShopCollectionViewCell
        let data = self.data.ListItem[indexPath.section]
        cell.setData(data.listAnimationItem.ListItem[indexPath.row])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if(kind == UICollectionElementKindSectionHeader){
            let header = colMain.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "InventoryHeaderIndentifier", forIndexPath: indexPath) as! InventoryShopCollectionViewHeader
            let data = self.data.ListItem[indexPath.section]
            if(data.listAnimationItem.iPageIndex == 0){
                //Load data
                //header.isLoading = true
                ItemDAO.List_ItemsAnimationInStore(data.listAnimationItem.iPageIndex, pageSize: 100, categoryId: data.category.Id, isNewVersion: true, callback: { (result) in
                    data.listAnimationItem = result
                    if(data.listAnimationItem.ListItem.count > 0)
                    {
                        data.listAnimationItem.iPageIndex += 1
                        self.colMain.performBatchUpdates({ 
                            self.colMain.reloadSections(NSIndexSet(index: indexPath.section))
                            }, completion: nil)
                    }
                    //header.isLoading = false
                })
            }
            header.setData(data)
            return header
        }
        return UICollectionReusableView()
    }
    
    func showPopup(indexPath: NSIndexPath)
    {
        let category = self.data.ListItem[indexPath.section]
        popUpBuyItem = PopupBuyAnimationItemViewController(nibName: "PopupBuyAnimationItemViewController", bundle: nil) as PopupBuyAnimationItemViewController
        
        let frame = popUpBuyItem.view.frame
        //let frame = CGRect(x: 0, y: 0, width: 280, height: 240)
        popUpBuyItem.view.frame = frame
        popUpBuyItem.view.bounds = frame
        //self.presentViewController(con, animated: true, completion: nil)
        
        popUpBuyItem.delegate = self
        
        self._popupController = CNPPopupController(contents: [popUpBuyItem.view])
        self._popupController!.mainController = popUpBuyItem
        self._popupController!.theme = CNPPopupTheme.defaultTheme()
        //self._popupController!.theme.maxPopupWidth = con.view.bounds.size.width
        self._popupController!.theme.maxPopupWidth = 280.0
        self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupController!.theme.popupStyle = .Centered
        self._popupController!.presentPopupControllerAnimated(true)
        
        popUpBuyItem.model = category.listAnimationItem.ListItem[indexPath.row]
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        showPopup(indexPath)
    }
    
    func touchedBuy(itemId: Int, purchaseId: Int) {
        JTProgressHUD.show()
        UserDAO.User_BuyItems(itemId, purchaseId: purchaseId, isVipItem: false) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
                self.touchedClose()
            }
            
            FunctionHelper.showBannerAlertView(content: result.Message);
        }
    }
    
    func touchedClose() {
        self._popupController?.dismissPopupControllerAnimated(true)
    }
    
    func touched_viewItem(giftLink: String) {
        
        if ((NSURL(string: giftLink)) != nil)
        {
            self._popupController?.dismissPopupControllerAnimated(false)
        
            let vc = PreviewItemViewController(nibName: "PreviewItemViewController", bundle: nil)
            vc.delegate = self
            vc.urlGiftLink = giftLink
            vc.view.backgroundColor = UIColor.clearColor()
        
            vc.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
            self.presentViewController(vc, animated: true) {
            self.view.sendSubviewToBack((self._popupController?.mainController.view)!)
            }
        }
        else
        {
            FunctionHelper.showBannerAlertView(content: "Không có dữ liệu")
        }
  
    }
    
    func PreviewItem_Close() {
        self._popupController?.presentPopupControllerAnimated(true)
    }
}
