//
//  SettingTableViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 9/5/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class SettingTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupNavigationBar()
        setupLeftMenuButton()
        
        tableView.registerNib(UINib(nibName: "ProfileFunctionTableViewCell", bundle:nil), forCellReuseIdentifier: "ProfileFunctionIndentifier")
        tableView.backgroundColor = UIColor.groupTableViewBackgroundColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupUI()
    {
        title = "Cài đặt"
        tableView.separatorStyle = .None
    }
    
    func setupNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(leftMenuButton), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let btn: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func leftMenuButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }

    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let numberOfRows = 1
//        if let config = SettingsHelper.getConfig(){
//            if config.isPublishVersion == false { numberOfRows = 2 }
//        }
        return numberOfRows
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProfileFunctionIndentifier", forIndexPath: indexPath) as! ProfileFunctionTableViewCell
//        cell.imgRightArrow.hidden = false
//        
//        if (indexPath.section == 0){
//            switch indexPath.row {
//            case 0:
//                cell.imgIcon?.image = UIImage(named: "ic_profile_edit")
//                cell.lblName?.text = "Thông tin cá nhân"
//            case 1:
//                cell.imgIcon?.image = UIImage(named: "ic_profile_about_us")
//                cell.lblName?.text = "Thông tin"
//                
//            default:
//                break
//            }
//        }
        return cell
    }
    
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0){
            switch indexPath.row {
            case 0:
                let vc =  ProfileEditViewController(nibName: "ProfileEditViewController", bundle: nil)
                //vc.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(vc, animated: true)
                
            case 1:
                let vc = AboutUsViewController(nibName:  "AboutUsViewController", bundle: nil)
                //vc.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(vc, animated: true)
            default:
                break
            }
        }
    }

    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 10
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
}
