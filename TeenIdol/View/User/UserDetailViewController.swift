//
//  UserDetailViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 8/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class UserDetailViewController: ButtonBarPagerTabStripViewController, UserProfileHeaderProtocol, UIAlertViewDelegate {
    @IBOutlet weak var viewHeaderContainer: UIView!
    @IBOutlet weak var viewUserHeader : UserProfileHeaderView?
    private var model : UserModel? = nil
    private var strFacebookURLTemp : NSURL?
    var userId : Int64 = 0
    
    override func viewDidLoad() {
        self.setupUI()
        viewUserHeader = NSBundle.mainBundle().loadNibNamed("UserProfileHeaderView", owner: UserProfileHeaderView.self, options: nil)![0] as? UserProfileHeaderView
       // viewUserHeader?.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        //viewUserHeader?.translatesAutoresizingMaskIntoConstraints = true
        viewUserHeader?.delegate = self
        self.viewHeaderContainer.addSubview(viewUserHeader!)
        viewUserHeader?.frame = self.viewHeaderContainer.bounds
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        viewUserHeader?.frame = self.viewHeaderContainer.bounds
//    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if(self.navigationController?.navigationBarHidden == false){
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        //self.navigationController?.navigationBarHidden = true
        self.loadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBarHidden = false
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func setupUI(){
        settings.style.buttonBarBackgroundColor = UIColor.groupTableViewBackgroundColor()
        settings.style.buttonBarItemBackgroundColor =  UIColor.groupTableViewBackgroundColor()
        settings.style.selectedBarBackgroundColor = ColorAppDefault
        settings.style.buttonBarItemFont = UIFont.boldSystemFontOfSize(17)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 20
        settings.style.buttonBarItemTitleColor = .blackColor()
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.grayColor()
            newCell?.label.textColor = ColorAppDefault
        }
        super.viewDidLoad()
    }
    
    func loadData()
    {
        JTProgressHUD.show()
        UserDAO.User_GetDetailsV2(UserId: userId, callback: { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0){
                self.model = result.DataObject as? UserModel
                self.viewUserHeader?.setUserInfo(self.userId, userInfoModel: self.model!)
                //self.UpdateUI()
            }
        })
    }

    //MARK: Pager Tab Strip Datasource
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
//        let child_1 = UserDetailTableChildViewController(style: .Plain, itemInfo: "Theo dõi", type:  User_GetListType.UserFollow, userId: self.userId )
//        let child_2 = UserDetailTableChildViewController(style: .Plain, itemInfo: "Fan", type:  User_GetListType.UserFan, userId: self.userId)
        let child_1 = UserDetailTableChildViewController(nibName:nil, bundle:nil, itemInfo: "Theo dõi", type:  User_GetListType.UserFollow, userId: self.userId )
        let child_2 = UserDetailTableChildViewController(nibName:nil, bundle:nil, itemInfo: "Fan", type:  User_GetListType.UserFan, userId: self.userId)
        return [child_1, child_2]
        
    }
    
    //MARK: Pager Tab Strip Delegate
    override func  pagerTabStripViewController(pagerTabStripViewController: PagerTabStripViewController, updateIndicatorFromIndex fromIndex: Int, toIndex: Int){
//        if let controller = pagerTabStripViewController.viewControllers[toIndex] as? UserDetailTableChildViewController{
//            controller.loadData()
//        }
    }
    
    //MARK: User Profile Header Delegate
    func userHeader_onTouchedBack() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func userHeader_onTouchedFollow() {
        self.checkLogin { (loginState) in
            if(loginState == .Logged){
                UserDAO.User_FollowUser(FollowUserId: self.userId, IsFollow: !(self.model?.isFollow)!) { (result) in
                    
                    if(result.ErrorCode == 0)
                    {
                        self.model?.isFollow = !(self.model?.isFollow)!
                        self.viewUserHeader?.setUserInfo(self.userId, userInfoModel: self.model!)
                    }
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            }
        }
    }
    
    func userHeader_onTouchedFacebook(url: String) {
        if let fbURL =  NSURL(string: url){
            let alertView = UIAlertView(title: "Xác nhận", message: "Bạn muốn xem thông tin Facebook của người này?", delegate: self, cancelButtonTitle: "Đồng ý",otherButtonTitles: "Hủy")
            self.strFacebookURLTemp = fbURL
            alertView.show()

        }
    }
    
    func userHeader_onTouchedGuild(guildId: Int64) {
        let vc = HomeGuildViewController(nibName: "HomeGuildViewController", bundle: nil)
        vc.guildId = guildId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 0){
            UIApplication.sharedApplication().openURL( self.strFacebookURLTemp!)
        }
        self.strFacebookURLTemp = nil
    }
}


class UserDetailTableChildViewController: UITableViewController, IndicatorInfoProvider, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    private var itemInfo = IndicatorInfo(title: "View")
    private var data = FListResultDTO<UserModel>()
    private var followType = User_GetListType.UserFollow;
    private var userId: Int64 = 0
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, itemInfo: IndicatorInfo, type: User_GetListType, userId: Int64) {
        self.itemInfo = itemInfo
        self.followType = type
        self.userId = userId
        super.init(nibName:nibNameOrNil, bundle:nibBundleOrNil)
    }
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo, type: User_GetListType, userId: Int64) {
        self.itemInfo = itemInfo
        self.followType = type
        self.userId = userId
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .None
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.registerNib(UINib.init(nibName: "IdolInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "IdolTableViewCell")
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        setupPutToRefresh()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.data = FListResultDTO<UserModel>()
        self.loadData()
    }
    
    deinit{
        if(self.tableView != nil){
            self.tableView.dg_removePullToRefresh()
        }
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tableView.backgroundColor!
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<UserModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tableView.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    func loadData()
    {
        if (self.data.isLoadingData) {return}
        self.data.isLoadingData = true
        
        if(self.data.iPageIndex == 0){
            JTProgressHUD.show()
        }
        
        UserDAO.User_GetListUserFollowV2(followType: followType , pageIndex: self.data.iPageIndex, pageSize: 10, str_search: "", num_userId: self.userId) { (result) in
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            
            if(self.data.iPageIndex == 0){
                self.tableView.dg_stopLoading()
                self.tableView.reloadData()
                JTProgressHUD.hide()
            }else{
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0..<result.ListItem.count{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Fade)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            
            self.data.iPageIndex = self.data.iPageIndex + 1
            self.data.isLoadingData = false
            
        }
    }
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60.0
    }
    
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == data.ListItem.count - 1)
        {
            self.loadData()
        }
        let cell = tableView.dequeueReusableCellWithIdentifier("IdolTableViewCell", forIndexPath: indexPath) as! IdolInfoTableViewCell;
        cell.setData(data.ListItem[indexPath.row])
        return cell;
    }
    
    
    func titleForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        let attStr = NSAttributedString(string: "Không có thông tin", attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(18)])
        
        return attStr
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        var text = ""
        if(followType == User_GetListType.UserFollow)
        {
            text = "Danh sách bạn đang theo dõi"
        }
        else
        {
            text = "Danh sách người đang theo dõi bạn"
        }
        
        let attStr = NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.systemFontOfSize(15)])
        
        return attStr
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView) {
        if(followType == User_GetListType.UserFollow)
        {
            let root = navigationController?.popViewControllerAnimated(true) as! HomeTabBarViewController
            root.tabBarController?.selectedIndex = 1;
        }
        else
        {
            let link = "https://docs.google.com/forms/d/e/1FAIpQLSeV8X9GuTTzwKHaIfaZCw4VpO-wiKyDgR0ZVRny5_f1qBCfyQ/viewform"
            if let url = NSURL(string: (link))
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView) -> UIColor? {
        return UIColor.whiteColor()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_live_idol");
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView) -> Bool {
        if(data.ListItem.count == 0)
        {
            return true;
        }
        return false
    }

}
