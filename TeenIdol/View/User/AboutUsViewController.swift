//
//  AboutUsViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 7/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
    @IBOutlet var lblVersion : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Thông tin"
        self.setupLeftMenuButton()
        if let version = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as? String{
            lblVersion.text = "Phiên bản hiện tại: \(version)"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLeftMenuButton(){
        let button = FunctionHelper.getNavigationBarButton("ic_arrow_left")
        button.addTarget(self, action: #selector(leftButtonPress), forControlEvents: .TouchUpInside)
        self.navigationItem.setLeftBarButtonItem( UIBarButtonItem(customView:
            button), animated: true)
    }
    
    func leftButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
