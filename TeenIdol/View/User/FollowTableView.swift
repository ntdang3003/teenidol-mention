//
//  FollowTableView.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/5/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol FollowTableViewProtocol {
    func Follow_onSelectedLive(viewcontroller: UIViewController, userinfo: UserSMModel);
}

class FollowTableView: UITableViewController, IndicatorInfoProvider, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    var itemInfo = IndicatorInfo(title: "View")
    
    var data = FListResultDTO<UserModel>()
    
    var type = User_GetListType.UserFollow
    
    var delegate: FollowTableViewProtocol? = nil;
    var parentVC: UIViewController? = nil
    
    var _followUserId: Int64 = -1;
    var _cellFollowIndexPath: NSIndexPath? = nil
    var _canUnfollow: Bool = true
    var _currentUserId: Int64 = -1;
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, itemInfo: IndicatorInfo, type: User_GetListType) {
        self.itemInfo = itemInfo
        self.type = type
        super.init(nibName:nibNameOrNil, bundle:nibBundleOrNil)
    }
    
    init(style: UITableViewStyle, itemInfo: IndicatorInfo, type: User_GetListType) {
        self.itemInfo = itemInfo
        self.type = type
        super.init(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if FunctionHelper.osVersion() >= 9.0 {
            tableView.emptyDataSetDelegate = self
            tableView.emptyDataSetSource = self
        }
        
        
        tableView.separatorStyle = .None
        tableView.registerNib(UINib.init(nibName: "IdolInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "IdolTableViewCell")
        
        //setupPutToRefresh()
        //loadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupPutToRefresh()
        loadData()
    }
    
    deinit{
        if(self.tableView != nil){
            self.tableView.emptyDataSetDelegate = nil
            self.tableView.emptyDataSetSource = nil
            self.tableView.dg_removePullToRefresh()
        }
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.whiteColor()
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data.iPageIndex = 0
            self!.loadData()
            }, loadingView: loadingView)
        self.tableView.dg_setPullToRefreshFillColor(ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    func loadData()
    {
        if (self.data.isLoadingData) {return}
        self.data.isLoadingData = true
        
        if(self.data.iPageIndex == 0){
            JTProgressHUD.show()
        }
        
        if(_currentUserId == -1 || _currentUserId == SettingsHelper.getUserID())
        {
            _currentUserId = SettingsHelper.getUserID()
            _canUnfollow = true
        }
        else
        {
            _canUnfollow = false
        }
        
        UserDAO.User_GetListUserFollowV2(followType: type , pageIndex: self.data.iPageIndex, pageSize: 10, str_search: "", num_userId: _currentUserId) { (result) in
            self.data.CountAllResult = result.CountAllResult
            if(self.data.iPageIndex == 0){
                self.data.ListItem = result.ListItem
                
                self.tableView.reloadData()
                self.tableView.dg_stopLoading()
                JTProgressHUD.hide()
            }else{
                self.data.ListItem.appendContentsOf(result.ListItem)
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0..<result.ListItem.count{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Fade)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            
            self.data.iPageIndex = self.data.iPageIndex + 1
            self.data.isLoadingData = false
            self.tableView.dg_stopLoading()
        }
    }
    
    // MARK: - Table view data source
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
 
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count
    }
    
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("IdolTableViewCell", forIndexPath: indexPath) as! IdolInfoTableViewCell;
        cell.setData(data.ListItem[indexPath.row])
        
        return cell;
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // join show or profile user
        if(self.type == .UserFan){return}
        
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! IdolInfoTableViewCell
        let userInfo = cell.getInfo();
        
        //if(delegate != nil && cell.isLive)
        if(delegate != nil)
        {
            delegate?.Follow_onSelectedLive(self.parentVC!, userinfo: userInfo)
        }
        //else if(delegate == nil && cell.isLive)
        if(delegate == nil)
        {
            SettingsHelper.setCurrentShowIndex(indexPath.row)
            let _weakSelf = parentVC as! FollowViewController
            
            //            if(cell.isLive)
            //            {
            dispatch_async(dispatch_get_main_queue(), {
                if let _weakInstace = MediaStreamerViewController.shareInstance() {
                    _weakInstace.isStreamer = false
                    _weakInstace._imgBackground = cell.imageAvatar?.image
                    _weakInstace.setShowInfo(Int64((userInfo.ShowInfo?.Id)!), urlStreamer: (userInfo.ShowInfo?.ServerIP)!, streamKey: (userInfo.ShowInfo?.StreamKey)!)
                    _weakInstace.delegate = _weakSelf
                    
                    let navigate = UINavigationController(rootViewController: _weakInstace)
                    navigate.navigationBar.tintColor = UIColor.whiteColor()
                    navigate.navigationBar.translucent = false
                    navigate.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
                    navigate.toolbarHidden = true
                    _weakSelf.presentViewController(navigate, animated: true, completion: nil)
                }
            })
            //}
        }
    }
    
    // MARK: Delegate EmptyDataSet
    
    func titleForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        
        let attStr = NSAttributedString(string: "Không có thông tin", attributes: [
            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(18)])
        
        return attStr
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView) -> NSAttributedString? {
        return nil
//        var text = ""
//        if(type == User_GetListType.UserFollow)
//        {
//            text = "Danh sách bạn đang theo dõi"
//        }
//        else
//        {
//            text = "Danh sách người đang theo dõi bạn"
//        }
//        
//        let attStr = NSAttributedString(string: text, attributes: [
//            NSForegroundColorAttributeName : UIColor.lightGrayColor(),
//            NSFontAttributeName : UIFont.systemFontOfSize(15)])
//        
//        return attStr
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView, forState state: UIControlState) -> NSAttributedString? {
        
        var text = ""
        if(type == User_GetListType.UserFollow)
        {
            text = "Xem idol diễn"
        }
        else
        {
            text = "Đăng kí Idol"
        }
        
        if(SettingsHelper.getUserInfo()?.userInfo?.GroupUserId == .Star)
        {
            return nil
        }
        
        let attStr = NSAttributedString(string: text, attributes: [
            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSFontAttributeName : UIFont.boldSystemFontOfSize(15)])
        return attStr
    }
    
    func buttonBackgroundImageForEmptyDataSet(scrollView: UIScrollView, forState state: UIControlState) -> UIImage? {
        return UIImage(named: "bg_signin_button")
    }
    
    func emptyDataSetShouldAllowTouch(scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView) {
        if(type == User_GetListType.UserFollow)
        {
            let profile = navigationController?.popViewControllerAnimated(true)
            profile?.navigationController?.popViewControllerAnimated(true)
            //root!.tabBarController?.selectedIndex = 1;
        }
        else
        {
            let link = "https://docs.google.com/forms/d/e/1FAIpQLSeV8X9GuTTzwKHaIfaZCw4VpO-wiKyDgR0ZVRny5_f1qBCfyQ/viewform"
            if let url = NSURL(string: link)
            {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    
    func backgroundColorForEmptyDataSet(scrollView: UIScrollView) -> UIColor? {
        return UIColor.whiteColor()
    }
    
    func imageForEmptyDataSet(scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_live_idol");
    }
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView) -> Bool {
        if(data.ListItem.count == 0)
        {
            return true;
        }
        return false
 
    }
    
    
    // unfollow user
    /*
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let unfollowAction = UITableViewRowAction(style: .Normal, title: "Bỏ theo dõi") { (rowAction:UITableViewRowAction, indexPath:NSIndexPath) -> Void in
            
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! IdolInfoTableViewCell
            self._followUserId = cell.getInfo().UserInfo!.Id
            self._cellFollowIndexPath = indexPath
            let alert = UIAlertView(title: "Xác nhận", message: String(stringInterpolation: "Bạn muốn huỷ theo dõi với ", cell.lbName.text!," ?"), delegate: self.parentVC as! FollowViewController, cancelButtonTitle: "Hủy", otherButtonTitles: "Đồng ý")
            alert.show()
        }
        unfollowAction.backgroundColor = ColorAppDefault
        
        return [unfollowAction]
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == 1)
        {
            UserDAO.User_FollowUser(FollowUserId: self._followUserId, IsFollow: false) { (result) in
                if(result.ErrorCode == 0)
                {
                    // update ui
                    self.tableView.beginUpdates()
                    self.data.ListItem.removeAtIndex((self._cellFollowIndexPath?.row)!)
                    self.tableView.deleteRowsAtIndexPaths([self._cellFollowIndexPath!], withRowAnimation: UITableViewRowAnimation.Fade)
                    self.tableView.endUpdates()
                }
                FunctionHelper.showBannerAlertView(content: result.Message)
            }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return _canUnfollow
    }
    */
}
