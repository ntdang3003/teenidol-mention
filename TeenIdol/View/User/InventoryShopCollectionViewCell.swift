//
//  InventoryShopCollectionViewCell.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/3/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class InventoryShopCollectionViewHeader : UICollectionReusableView {
    var lblCategoryName : UILabel?
    var loadingActivity: UIActivityIndicatorView?;
    
    private var _isloading: Bool = false
    var isLoading: Bool {
        set {
        if(newValue == true)
        {
            _isloading = true
            loadingActivity?.startAnimating()
        }
        else
        {
            _isloading = false
            loadingActivity?.stopAnimating()
        }
        }
        get
        {
            return _isloading
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        lblCategoryName = UILabel(frame: CGRect(x: 5, y: 5, width: self.frame.size.width - 50, height: self.frame.size.height  - 10))
        lblCategoryName?.font = UIFont.systemFontOfSize(20)
        lblCategoryName?.textColor = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
        
        //let frameLbl = lblCategoryName?.frame;
//        loadingActivity = UIActivityIndicatorView(frame: CGRect(x: frameLbl!.width + 10, y: (lblCategoryName?.center.y)! - 10, width: 20, height: 20))
//        
//        loadingActivity?.hidesWhenStopped = true
//        loadingActivity?.hidden = false
//        loadingActivity?.activityIndicatorViewStyle = .Gray
        
        //self.addSubview(loadingActivity!)
        self.addSubview(lblCategoryName!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setData(data: ItemCategoryModel)
    {
        self.lblCategoryName?.text = data.category.Name.uppercaseString
    }
}

class InventoryShopCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgMain : UIImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblMovingName : MarqueeLabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor(red: 226/255, green: 226/255, blue: 226/255, alpha: 1).CGColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 10
        lblMovingName.type = .Continuous
        lblMovingName.animationCurve = .EaseInOut
        // Initialization code
    }
    
    func setData(data: AnimationItemModel){
        let url = NSURL(string: (data.animationItem?.photoLink)!)
        if(url != nil){
            self.imgMain.sd_setImageWithURL(url!)
        }
        lblMovingName.text = data.animationItem?.name
        //lblName.text = data.animationItem?.name
    }
}
