//
//  ProfileEditViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/14/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class ProfileEditViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var btGender1: UICheckbox!
    @IBOutlet weak var btGender2: UICheckbox!
    @IBOutlet weak var lbChangePass: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var imageAvatarView: UIView!
    
    var _userInfo = UserDTO()
    var isMen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLeftMenuButton()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        FunctionHelper.makeBorderView(imageAvatarView, borderWidth: 1, color: UIColor.lightGrayColor(), cornerRadius: 45)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
         /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setupUI()
    {
        
        self.title = "Chỉnh sửa thông tin"
        imageAvatarView.onTap { (tapGestureRecognizer) in
            self.openAlbum()
        }
        
        lbChangePass.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(changePass)))
        _userInfo = (SettingsHelper.getUserInfo()?.userInfo!)!
        updateUI()
    }
    
    func openAlbum()
    {
        let pickVC = UIImagePickerController()
        pickVC.delegate = self
        pickVC.sourceType = .PhotoLibrary
        pickVC.allowsEditing = true
        presentViewController(pickVC, animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        
        let photo64 = UIImagePNGRepresentation(image)?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        
        // loading
        
        UserDAO.User_UploadPhoto(BasePhoto64: photo64!) { (result) in
            
            if(result.ErrorCode == 0)
            {
                SettingsHelper.getUserInfo()?.userInfo = result.DataObject as? UserDTO
                self.updateUI()
            }
            
            FunctionHelper.showBannerAlertView("Thông báo", content: result.Message, callback: nil)
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func updateUI()
    {
        imageAvatar.sd_setImageWithURL(NSURL(string: _userInfo.AvatarPhoto), placeholderImage: UIImage(named: "ic_default_avatar"))
        
        isMen = _userInfo.Gender == 1 ? true : false
        
        btGender1.isCheck = isMen; btGender1.setupUI()
        btGender2.isCheck = !isMen; btGender2.setupUI()
        
        txtName.text = _userInfo.Name;
        lblEmail.text = _userInfo.Email
        //txtJoinedDate.text = FunctionHelper.dateToString(_userInfo.CreateDate!, mode: 1)
        
    }
    
    @IBAction func btGender1(sender: AnyObject)
    {
        btGender2.changeState()
    }
    
    @IBAction func btGender2(sender: AnyObject)
    {
        btGender1.changeState()
    }
    
    func changePass()
    {
        let vc = ChangePasswordViewController.init(nibName: "ChangePasswordViewController", bundle: nil)
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func btUpdateInfo(sender: AnyObject)
    {
        _userInfo.Name = self.txtName.text!
        _userInfo.Gender = self.btGender1.isCheck == true ? 1 : 2
        
        UserDAO.User_UpdateTeenIdolAccount(UserInfo: _userInfo) { (result) in
            
            FunctionHelper.showBannerAlertView("LiveIdol", content: result.Message, callback: nil)
        }
    }
    
}
