//
//  InventoryManagerCollectionViewCell.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/28/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

enum InventoryState : Int {
    case Valid = 0
    case Invalid = 1
}

class InventoryManagerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var btnRecharge: UIButton!
    @IBOutlet weak var imgMain: UIImageView!
    @IBOutlet weak var lblName: MarqueeLabel!
    @IBOutlet weak var lblDate: UILabel!
    var state = InventoryState.Valid
    private var _model : AnimationInventoryModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layer.cornerRadius = 5
        self.viewBorder.layer.cornerRadius = 5
        self.viewBorder.layer.borderWidth = 2
        //self.viewBorder.layer.masksToBounds = true
        self.btnRecharge.layer.cornerRadius = 3
        lblName.type = .Continuous
        lblName.animationCurve = .EaseInOut
    }
    
    func setData(model: AnimationInventoryModel){
        self._model = model
        
        //Check Exprite Date
        let expireDate = Double((self._model?.animationInventory?.expireDate)!)
        let dateNow = NSDate().timeIntervalSince1970 * 1000
        lblDate.text = FunctionHelper.dateToString((self._model?.animationInventory?.dateExpireParsed)!, mode: 6 )
        lblName.text = self._model?.animationItem?.name
        if let url = NSURL(string: (self._model?.animationItem?.photoLink)!){
            imgMain.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_car_placeholder"))
        }
        if(expireDate > dateNow){ //Valid
            self.state = .Valid
            self.contentView.alpha = 1
            self.btnRecharge.hidden = true
        }else{
            self.state = .Invalid
            self.contentView.alpha = 0.7
            self.btnRecharge.hidden = false
        }
        
    }
    
    func setCellSelected(value: Bool){
        if(value){
            self.viewBorder.layer.borderColor = ColorAppDefault.CGColor
            self.imgCheck.hidden = false
        }else{
            self.viewBorder.layer.borderColor = UIColor.clearColor().CGColor
            self.imgCheck.hidden = true
        }
    }
}
