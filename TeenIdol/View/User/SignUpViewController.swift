//
//  SignUpViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/17/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var txtEmail: UITextField!;
    //@IBOutlet weak var cbGender1: UICheckbox!;
    //@IBOutlet weak var cbGender2: UICheckbox!;
    @IBOutlet weak var txtName: UITextField!;
    @IBOutlet weak var txtPass: UITextField!;
    @IBOutlet weak var txtPass2: UITextField!;
    @IBOutlet weak var imgAvatar: UIImageView!;
    
    var isMen = false;
    var photo64 = "";
    private var isLoading = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLeftMenuButton()
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = false
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(leftMenuButton), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        
        let btn: UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func leftMenuButton()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func setupUI()
    {
        FunctionHelper.circleBorderImageView(imgAvatar,cornerRadius: 60)
        imgAvatar.userInteractionEnabled = true
        imgAvatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openAlbum)))
        //cbGender1.isCheck = isMen; cbGender1.changeState()
        //cbGender2.isCheck = !isMen; cbGender2.changeState()
    }
    
    func openAlbum()
    {
        let pickerVC = UIImagePickerController()
        pickerVC.sourceType = .PhotoLibrary
        pickerVC.delegate = self
        self.presentViewController(pickerVC, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        let data = UIImagePNGRepresentation(image)
        photo64 = (data?.base64EncodedStringWithOptions(.Encoding64CharacterLineLength))!
        
        self.imgAvatar.image = image;
    }
    
//    @IBAction func cbBoy(sender: AnyObject)
//    {
//        isMen = true
//        cbGender2.changeState()
//    }
//    
//    @IBAction func cbGirl(sender: AnyObject)
//    {
//        isMen = false
//        cbGender1.changeState()
//    }
//    
    @IBAction func btSignIn(sender: AnyObject)
    {
        if(isLoading){return}
        isLoading = true
        
        let user = UserDTO()
        
        user.Email = txtEmail.text!
        user.Password = txtPass.text!
        user.Name = txtName.text!
        user.Gender = isMen ? 1 : 2
        user.AvatarPhoto = ""
        
        if txtPass.text != txtPass2.text
        {
            FunctionHelper.showBannerAlertView("Thông báo", content: "Xác nhận mật khẩu không giống", callback: nil)
            txtPass.text = ""
            txtPass2.text = ""
            return
        }
        
        if user.Email.isEmpty || user.Name.isEmpty
        {
            FunctionHelper.showBannerAlertView("Thông báo", content: "Thiếu thông tin để đăng kí", callback: nil)
            return
        }
        JTProgressHUD.show()
        UserDAO.User_RegisterByTeenIdol(user) { (result) in
            JTProgressHUD.hide()
            if result.ErrorCode == 0
            {
                let session = result.DataObject as! UserSessionDTO
                SettingsHelper.setUserID(session.Id)
                SettingsHelper.setUserTokenKey(session.tokenKey)
                UserDAO.User_GetDetailsV2(UserId: SettingsHelper.getUserID(), callback: { (result1) in
                    if result1.ErrorCode == 0
                    {
                        SettingsHelper.setUserInfo(result1.DataObject as? UserModel)
                        if(!self.photo64.isEmpty){
                            UserDAO.User_UploadPhoto(BasePhoto64: self.photo64) { (result2) in
                                FunctionHelper.showBannerAlertView("Thông báo", content: result2.Message, callback: nil)
                            }
                        }
                    }
                }) 
                
                self.dismissViewControllerAnimated(true, completion: nil)
                //self.navigationController?.popViewControllerAnimated(true)
                
            }
            FunctionHelper.showBannerAlertView("Thông báo", content: result.Message, callback: nil)
            self.isLoading = false
        }
    }
    
    @IBAction func btClose(sender: AnyObject)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
