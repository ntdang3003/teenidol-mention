//
//  AttendanceViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/23/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol PopupAttendanceProtocol {
    func touchClose()
    func touchJoinVIP()
    func touchMoreInfo()
}

class DaySignInRewardView: UIView
{
    @IBOutlet var lbName: UILabel!;
    @IBOutlet var imgChecked: UIImageView!;
    @IBOutlet var imgReward: UIImageView!;
    @IBOutlet var lbDescription: UILabel?;
   
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        
    }
    
    func setData(model: DaySignInRewardModel)
    {
        let url = NSURL(string: model.daySignInReward!.rewardPhoto)
        imgReward.sd_setImageWithURL(url, placeholderImage: nil)
        //lbDescription?.text = model.daySignInReward!.Description
        lbName.text = model.daySignInReward!.name
       
    }
}

class AttendanceViewController: UIViewController {

    @IBOutlet weak var lbCountDay: UILabel!;
    @IBOutlet weak var viewMain: UIView!;
    @IBOutlet var btGetReward: UIButton!;
    
    var daycount: Int = 0;
    
    var data: FListResultDTO<DaySignInRewardModel>?
    var UIDaySignInList: [DaySignInRewardView]?
    var delegate: PopupAttendanceProtocol?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        FunctionHelper.makeBorderView(btGetReward, borderWidth: 0, color: UIColor.clearColor(), cornerRadius: 5)
        
        getUIDaySignInList()
        

        loadData()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getUIDaySignInList()
    {
        UIDaySignInList = [DaySignInRewardView]()
        for i in self.viewMain.subviews {
            if(i.isKindOfClass(DaySignInRewardView))
            {
                UIDaySignInList?.append(i as! DaySignInRewardView);
            }
        }
    }
    
    func loadData()
    {
        data = FListResultDTO<DaySignInRewardModel>()
        if((data?.isLoadingData)!) {
            return
        }
        data?.isLoadingData = true
    
        UserDAO.User_GetListDaySignIn(0, pageSize: 10) { (result) in
            
            if(result.CountAllResult > 0 && result.iPageIndex == 0)
            {
                self.data?.ListItem = result.ListItem
                self.UpdateUI()
            }
            
            self.data?.isLoadingData = false
        }
        
        UserDAO.User_GetUserDaySignInReward { (result) in
            if(result.DataObject != nil)
            {
                let user_daysignin = (result.DataObject as! User_DaySignInRewardDTO);
                self.daycount = user_daysignin.signInContinuesCount;
                self.lbCountDay.text = "(\(self.daycount) ngày)"
            }
        }
    }
    
    func UpdateUI()
    {
        for index in 0..<(UIDaySignInList?.count)!
        {
            let tmpData = data?.ListItem[index]
            let ui = UIDaySignInList![index];
            ui.setData(tmpData!)
            ui.tag = (tmpData?.daySignInReward?.Id)!
        }
    }
    
    @IBAction func btGetReward(sender: AnyObject)
    {
        JTProgressHUD.show()
        UserDAO.User_DoDaySignIn { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
               for i in self.UIDaySignInList!
               {
                    let user_daysignin = result.DataObject as! User_DaySignInRewardDTO
                    if(i.tag == user_daysignin.daySignIn!.Id)
                    {
                        self.daycount += 1;
                        self.lbCountDay.text = "(\(self.daycount) ngày)"
                        i.imgChecked.hidden = false;
                        SettingsHelper.addCurrentUserHeart(user_daysignin.daySignIn!.value)
                        break;
                    }
                }
            }
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    @IBAction func btClose(sender: AnyObject)
    {
        if(self.delegate != nil)
        {
            self.delegate?.touchClose()
        }
    }
    
    func joinVIP()
    {
        if(self.delegate != nil)
        {
            self.delegate?.touchJoinVIP()
        }
    }
    
    func moreInfo()
    {
        if(self.delegate != nil)
        {
            self.delegate?.touchMoreInfo()
        }
    }
    
}
