//
//  InAppPurchaseTableViewCell.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/27/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class InAppPurchaseDTO : NSObject{
    var rubyTotal = 0
    var price = "$"
    var storeId = ""
    override init() {
        super.init();
    }
    
    convenience init(ruby: Int, price : String, storeId : String){
        self.init()
        self.rubyTotal = ruby
        self.price = price
        self.storeId = storeId
    }
}

class InAppPurchaseTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRuby : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: InAppPurchaseDTO){
        lblRuby.text = String(data.rubyTotal)
        lblPrice.text = data.price
    }
    
}
