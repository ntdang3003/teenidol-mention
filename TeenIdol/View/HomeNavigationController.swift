//
//  HomeNavigationController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/4/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class HomeNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setViewControllers([HomeViewController()], animated: false)
        self.navigationBar.tintColor = UIColor.whiteColor()
        
    }
//    override func prefersStatusBarHidden() -> Bool {
//        return navigationBarHidden ?? false
//    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
