//
//  IdolQuestGiftCollectionViewCell.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/14/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
enum DisplayType {
    case Idol
    case User
    case Gift
}

class IdolQuestGiftCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgPhoto: UIImageView?;
    @IBOutlet weak var lbProgress: UILabel?;
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setData(displayType displayType: DisplayType, data: IdolQuestMissionGift)
    {
        if let url = NSURL(string: data.giftPhoto){
            self.imgPhoto?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_event_gift"))
        }
        
        if(displayType == .User)
        {
            lbProgress?.text = "\(data.Description)"
        }
        else if(displayType == .Idol)
        {
            lbProgress?.text = "\(data.max)"
        }
        else
        {
            lbProgress?.text = "\(data.count)/\(data.max)";
        }
    }
    
    
}
