//
//  PopupWebContentViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 7/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

protocol  PopupWebDelegate {
    func popupWebContent_onClose()
}

class PopupWebContentViewController: UIViewController {
    @IBOutlet weak var txtContent : UITextView!
    @IBOutlet weak var activitor : UIActivityIndicatorView!
    var delegate : PopupWebDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        activitor.startAnimating()
        do
        {
            if let url = NSURL(string: "https://teenidol.vn/File/LiveStreamPrivacy.txt"){
                var data = NSData(contentsOfURL: url)
                if(data == nil) { data = NSData()}
                self.txtContent.text = String(data: data!, encoding: NSUTF8StringEncoding)!
            }
        }
        activitor.stopAnimating()
    }
    
    @IBAction func touchedClose(sender: AnyObject?){
        if(self.delegate != nil){
            self.delegate?.popupWebContent_onClose()
        }
    }
}
