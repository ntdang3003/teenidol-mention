//
//  PopupLixiRewardViewController.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/9/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit
protocol PopupLixiRewardDelegate {
    func popupLixiRewardOnClose(withImages: [String])
}

class PopupLixiRewardViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView : UITableView!
    var model : GranaryRewardModel!
    var popupParent : CNPPopupController!
    private let reuseId = "LixiRewardCellReuseId"
    
    @IBOutlet weak var labelTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "PopupLixiRewardTableViewCell", bundle: nil)
        self.tableView.registerNib(nib, forCellReuseIdentifier: reuseId)
        labelTitle.text = self.model.name
    }

    
    @IBAction func touchClose(sender: AnyObject) {
        self.popupParent.dismissPopupControllerAnimated(true)
    }
    
    //MARK: TableView DataSource + Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.listReward.count
       // return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseId, forIndexPath: indexPath) as! PopupLixiRewardTableViewCell
        let temp = model.listReward[indexPath.row]
        cell.setData("\(temp.nameReward) \(temp.unit)")
        return cell
    }
}
