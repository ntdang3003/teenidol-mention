//
//  SignUpStreamerViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/3/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class SignUpStreamerViewController: UIViewController, UITextViewDelegate, UIAlertViewDelegate {

    @IBOutlet weak var txtFullName: UITextField!;
    @IBOutlet weak var txtEmail: UITextField!;
    @IBOutlet weak var txtPhone: UITextField!;
    @IBOutlet weak var txtYoutubeLink: UITextField!;
    @IBOutlet weak var txtFacebookLink: UITextField!;
    @IBOutlet weak var txtDescriptionTalen: UITextView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupNavigationBar()
        setupLeftMenuButton()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupUI()
    {
        txtDescriptionTalen.placeholder = "Tài năng, sở thích"
        txtDescriptionTalen.layer.cornerRadius = 5
        txtDescriptionTalen.layer.masksToBounds = true
    }
    
    func setupNavigationBar(){
        
        let title = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
        title.text = "Đăng ký thông tin"
        title.textColor = .whiteColor()
        
        self.navigationItem.titleView = title;
        
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
    }
    
    func setupLeftMenuButton(){
        let button: UIButton = UIButton(type: .Custom)
        button.frame = CGRectMake(0, 0, 32, 32)
        button.contentMode = .ScaleAspectFit
        button.setImage(UIImage(named: "ic_arrow_left"), forState: .Normal)
        button.imageView!.contentMode = .ScaleAspectFit
        button.addTarget(self, action: #selector(rightDrawerButtonPress), forControlEvents: .TouchUpInside)
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        let btn: UIBarButtonItem = UIBarButtonItem(customView:
            button)
        self.navigationItem.setLeftBarButtonItem(btn, animated: true)
    }
    
    func rightDrawerButtonPress()
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func SignUp(sender: AnyObject)
    {
        if((txtFullName.text?.isEmpty)!)
        {
            FunctionHelper.showBannerAlertView(content: "Họ tên không được rỗng. ")
            return
        }
        
        if((txtPhone.text?.isEmpty)!)
        {
            FunctionHelper.showBannerAlertView(content: "Số điện thoại không được rỗng. ")
            return
        }
        
        if((txtEmail.text?.isEmpty)!)
        {
            FunctionHelper.showBannerAlertView(content: "Email không được rỗng.")
            return
        }
        
        JTProgressHUD.show()
        UserDAO.Star_RegisterStar(txtFullName.text!, phone: txtPhone.text!, facebookLink: txtFacebookLink.text!, email: txtEmail.text!, youtubeLink: txtYoutubeLink.text!, talenDescription: txtDescriptionTalen.text!) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
            }
            let alert = UIAlertView(title: "LiveIdol", message: result.Message, delegate: nil, cancelButtonTitle: "Ok")
            alert.delegate = self
            alert.show()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
//        if(textView == txtDescriptionTalen)
//        {
//            if(text == "")
//            {
//                txtDescriptionTalen.placeholderLabel.hidden = false
//            }
//            else
//            {
//                txtDescriptionTalen.placeholderLabel.hidden = true
//            }
//        }
        return true
    }
}
