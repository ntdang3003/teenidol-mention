//
//  StreamBeginViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import FBSDKLoginKit
protocol StreamBeginDelegate {
    func streamBegin_didBegin(name: String, facebookToken : String)
    func streamBegin_didClose()
}

class StreamBeginViewController: UIViewController, PopupWebDelegate, CNPPopupControllerDelegate {
    @IBOutlet weak var btnShareFacebook : UIButton!
    @IBOutlet weak var btnShareTweet : UIButton!
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var btnAgree : UICheckbox!
    @IBOutlet weak var btnStart : UIButton!
    private var _popupController : CNPPopupController?
    
    var delegate : StreamBeginDelegate?
    var facebookShareToken = ""
    private var _isShareFacebookEnable = true
    private var _keyboardSize = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
          self.txtName.becomeFirstResponder()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let point = touch.locationInView(self.view)
        if(_keyboardSize.height > 0 && !CGRectContainsPoint(_keyboardSize, point)){
            self.txtName.resignFirstResponder()
        }
    }
    
    func keyboardWillShow(aNotification: NSNotification){
        _keyboardSize = (aNotification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    }
    
    func keyboardWillHide(aNotification: NSNotification){
        _keyboardSize = CGRect(x: 0, y: 0, width: 0, height: 0)
    }
    
    @IBAction func touchShareFacebook(sender: AnyObject){
        _isShareFacebookEnable = !_isShareFacebookEnable
        self.btnShareFacebook.setImage(UIImage(named: _isShareFacebookEnable ? "ic_checkbox_on" : "ic_checkbox_off" ), forState: .Normal)
    }

    @IBAction func touchShareTweet(sender: AnyObject){
        
    }
    
    @IBAction func touchBegin(sender: AnyObject){
        self.txtName.resignFirstResponder()
        
        if(_isShareFacebookEnable){
            let currentToken = FBSDKAccessToken.currentAccessToken()
            if(currentToken != nil && currentToken.hasGranted("publish_actions")){
                self.facebookShareToken = currentToken.tokenString
            }
            if(self.facebookShareToken.isEmpty){
                let login = FBSDKLoginManager()
                login.logInWithPublishPermissions(["publish_actions"], fromViewController: self) { (result, error) in
                    //login.logInWithReadPermissions(["public_profile","email"], fromViewController: self) { (result, error) in
                    if(error != nil){
                        FunctionHelper.showBannerAlertView("LiveIdol", content: "Có lỗi trong quá trình xin quyền từ facebook!", callback: nil)
                    }
                    else if (result.isCancelled){
                        FunctionHelper.showBannerAlertView("LiveIdol", content: "Bạn không chấp nhận quyền từ facebook!", callback: nil)
                    }else{
                        self.facebookShareToken = result.token.tokenString
                    }
                    if(self.delegate != nil){
                        self.delegate?.streamBegin_didBegin(self.txtName.text!, facebookToken: self.facebookShareToken)
                    }
                }
            }else{
                if(self.delegate != nil){
                    self.delegate?.streamBegin_didBegin(self.txtName.text!, facebookToken: self.facebookShareToken)
                }
            }
        }else{
            if(self.delegate != nil){
                self.delegate?.streamBegin_didBegin(self.txtName.text!, facebookToken: self.facebookShareToken)
            }
        }
    }
    
    @IBAction func touchClose(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.streamBegin_didClose()
        }
    }
    
    @IBAction func touchedAgree(sender: AnyObject)
    {
        if(btnAgree.isCheck)
        {
            btnStart.enabled = true
        }
        else
        {
            btnStart.enabled = false
        }
    }
    
    @IBAction func touchedMore(sender: AnyObject?){
        self.txtName.resignFirstResponder()
        
        let con = PopupWebContentViewController(nibName: "PopupWebContentViewController", bundle: nil)
        con.delegate = self
        con.view.translatesAutoresizingMaskIntoConstraints = true
        let width = self.view.frame.size.width * (4/5)
        let height = self.view.frame.size.height * (7/9)
        con.view.frame = CGRect(x: (self.view.frame.size.width - width)/2, y: (self.view.frame.size.height - height)/2, width: width, height: height)
        
        self._popupController = CNPPopupController(contents: [con.view])
        self._popupController?.delegate = self
        self._popupController!.mainController = con
        self._popupController!.theme = CNPPopupTheme.defaultTheme()
        self._popupController!.theme.popupStyle = .Centered
        self._popupController!.theme.maxPopupWidth = width
        self._popupController!.theme.popupContentInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self._popupController!.presentPopupControllerAnimated(true)
    }
    
    func popupWebContent_onClose() {
        self._popupController?.dismissPopupControllerAnimated(true)
        self.txtName.becomeFirstResponder()
    }
    
    func popupControllerDidDismiss(controller: CNPPopupController!) {
        self.txtName.becomeFirstResponder()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
