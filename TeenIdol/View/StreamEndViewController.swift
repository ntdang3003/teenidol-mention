//
//  StreamEndViewController.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import FBSDKLoginKit

protocol StreamEndProtocol {
    func streamEnd_didTouchHome(facebookToken: String)
}

class StreamEndViewController: UIViewController {

    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblTotalViewer : UILabel!
    @IBOutlet weak var lblTotalRuby : UILabel!
    
    @IBOutlet weak var imgAvatar : UIImageView!
    @IBOutlet weak var viewInfo : UIView!
    
    @IBOutlet weak var btnShareFacebook : UIButton!
    @IBOutlet weak var btnShareTweet : UIButton!
    
    var delegate : StreamEndProtocol?
    private var _model : EndShowModel?
    private var _imgURL = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateUI()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI(){
        FunctionHelper.circleBorderImageView(self.imgAvatar,cornerRadius: 45, color: ColorAppDefault.CGColor)
        
    }
    
    func updateUI(){
        if(self._model != nil){
            if let url = NSURL(string: self._imgURL){
                self.imgAvatar.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
            }
            self.lblTime.text = FunctionHelper.stringFromTimeInterval(NSTimeInterval(self._model!.duration))
            self.lblTotalViewer.text = String((self._model!.viewer))
            self.lblTotalRuby.text = String(self._model!.totalRuby)
        }
    }
    
    func setInfo(image: String, data: EndShowModel){
        //let date = NSDate().dateByAddingTimeInterval(Double(data.duration))
        self._imgURL = image
        self._model = data
    }
    
    @IBAction func touchShareFacebook(sender: AnyObject){
        let currentToken = FBSDKAccessToken.currentAccessToken()
        if(currentToken != nil && currentToken.hasGranted("publish_actions")){
        /*
            UserDAO.User_ShowShareFB(currentToken.tokenString, base64Photo: captureScreen(), callback: { (result) in
                if result.ErrorCode != 0
                {
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            })
        */
        }
        
        
    }
    
    
    func captureScreen() -> String
    {
        
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, 0);
        
        self.view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        
        let imageData:NSData = UIImagePNGRepresentation(image)!
        //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        return imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength);
    }
    
    
    @IBAction func touchShareTweet(sender: AnyObject){
        
    }
    
    @IBAction func touchHome(sender: AnyObject){
        if(self.delegate != nil){
            self.delegate?.streamEnd_didTouchHome("")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
