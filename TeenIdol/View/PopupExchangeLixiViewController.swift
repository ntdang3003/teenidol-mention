//
//  PopupExchangeLixiViewController.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/6/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import UIKit
let reuseId = "PopupLixiExchangeTableViewCellReuseId"

protocol PopupExchangeDelegate {
    func popupExchangeOnClose()
    func popupExchangeOnGetReward(model: GranaryRewardModel)
}

class PopupExchangeLixiViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,ExchangeCellDelegate, UICollectionViewDelegateFlowLayout {
    
    //@IBOutlet weak var collectionNguyenLieu: UICollectionView!
    @IBOutlet weak var collectionCongThuc: UICollectionView!
    
    internal var dataNguyenLieu = FListResultDTO<NguyenLieuModel>()
    internal var dataCongThuc = FListResultDTO<LixiRequireModel>()
    
    @IBOutlet weak var buttonHatDe: BorderButton!
    @IBOutlet weak var buttonHopSua: BorderButton!
    @IBOutlet weak var buttonHoaCo: BorderButton!
    
    
    var starId : Int64 = 0
    var scheduleId : Int64 = 0
    var delegate: PopupExchangeDelegate!
    var collectionSize = CGSize.zero
    var minLineSpacing : CGFloat = 10.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //collectionNguyenLieu.registerNib(UINib(nibName: "PopupLamBanhCollectionViewCell", bundle: nil) , forCellWithReuseIdentifier: "PopupLamBanhCollectionViewCell")
        collectionCongThuc.registerNib(UINib(nibName: "PopupLamBanhCongThucCollectionViewCell", bundle: nil) , forCellWithReuseIdentifier: "PopupLamBanhCongThucCollectionViewCell")
        self.loadDataNguyenLieu()
        self.loadCongThuc()
    }
    
    
    @IBAction func touchClose(sender: AnyObject) {
        if self.delegate != nil {
            self.delegate.popupExchangeOnClose()
        }
    }

    //MARK: CollectionView Delegate + Datasource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return collectionView == collectionCongThuc ? dataCongThuc.ListItem.count : dataNguyenLieu.ListItem.count
        return dataCongThuc.ListItem.count
     }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        if collectionView == collectionNguyenLieu {
//            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PopupLamBanhCollectionViewCell", forIndexPath: indexPath) as! PopupLamBanhCollectionViewCell
//            cell.setData(dataNguyenLieu.ListItem[indexPath.row])
//            return cell
//        }
//        else {
//            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PopupLamBanhCongThucCollectionViewCell", forIndexPath: indexPath) as! PopupLamBanhCongThucCollectionViewCell
//            cell.setData(dataCongThuc.ListItem[indexPath.row])
//            cell.delegate = self
//            return cell
//        }
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PopupLamBanhCongThucCollectionViewCell", forIndexPath: indexPath) as! PopupLamBanhCongThucCollectionViewCell
        cell.setData(dataCongThuc.ListItem[indexPath.row])
        cell.delegate = self
        return cell
    }
    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        if collectionView == collectionNguyenLieu {
//            if self.collectionSize == CGSize.zero {
//                let temp = self.cellSize(collectionView, minLineSpacing: 8, minCellWidth: 100, heightRatio: 0.35)
//                self.collectionSize = temp.size
//                self.minLineSpacing = temp.minLineSpacing
//            }
//            return self.collectionSize
//        }else{
//            return CGSize(width: 260, height: 80)
//        }
//        
//    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let _ = collectionView.cellForItemAtIndexPath(indexPath) {
            let model = dataCongThuc.ListItem[indexPath.row]
            JTProgressHUD.show()
            EventDAO.Event_GetReward(model.requiredId, starId: self.starId, scheduleId: self.scheduleId, callback: { (result) in
                JTProgressHUD.hide()
                if result.ErrorCode == 0 {
                    if self.delegate != nil {
                        self.delegate.popupExchangeOnGetReward(result.DataObject as! GranaryRewardModel)
                    }
                }else{
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            })
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return self.minLineSpacing
    }
    
    func cellSize(collectionView: UICollectionView, minLineSpacing : CGFloat = 1, minCellWidth : CGFloat = 100,  heightRatio : CGFloat = 1.0) -> (size: CGSize, minLineSpacing: CGFloat){
        var size = CGSize(width: minCellWidth, height: minCellWidth)
        var newMinLineSpacing = minLineSpacing
        let maxWidth = collectionView.bounds.size.width
        let numberOfColumn = CGFloat(Int(maxWidth / minCellWidth))
        let numberOfSpaceLine = CGFloat(numberOfColumn - 1)
        var totalSpacing = numberOfSpaceLine * newMinLineSpacing
        let modValue : CGFloat = ((maxWidth - totalSpacing)/numberOfColumn)
        var temp = fmod(modValue, 1.0)
        
        while temp != 0 {
            newMinLineSpacing += 0.5
            totalSpacing = numberOfSpaceLine * newMinLineSpacing
            temp = fmod(((maxWidth - totalSpacing) / numberOfColumn), 1.0)
            //temp = ((maxWidth - totalSpacing) / numberOfColumn).truncatingRemainder(dividingBy: 1.0)
        }
        
        size.width = (maxWidth - (newMinLineSpacing * numberOfSpaceLine)) / numberOfColumn
        size.height = size.width * heightRatio
        return (size, newMinLineSpacing)
    }
    
    func loadDataNguyenLieu(){
        if dataNguyenLieu.isLoadingData == true {return}
        dataNguyenLieu.isLoadingData = true
        if(self.dataNguyenLieu.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }
        
        EventDAO.Event_GetNguyenLieu { (result) in
           self.dataNguyenLieu.ListItem.appendContentsOf(result.ListItem)
            JTProgressHUD.hide()
            self.updateButtonNguyenLieu()
            self.dataNguyenLieu.isLoadingData = false
//            if self.dataNguyenLieu.iPageIndex == 0 {
//                JTProgressHUD.hide()
//                self.collectionNguyenLieu.reloadData()
//            }
//            self.dataNguyenLieu.isLoadingData = false
        }
    }
    
    func loadCongThuc() {
        if dataCongThuc.isLoadingData == true {return}
        dataCongThuc.isLoadingData = true
        EventDAO.Event_GetListLixi(0, pageSize: 12) { (result) in
            self.dataCongThuc.CountAllResult = result.CountAllResult
            self.dataCongThuc.ListItem.appendContentsOf(result.ListItem)
            self.collectionCongThuc.reloadData()
            self.dataCongThuc.isLoadingData = false
        }
    }
    
    private func updateButtonNguyenLieu(){
        for nl in dataNguyenLieu.ListItem {
            switch nl.userGranary.unitTypeId {
            case 12:
                self.buttonHatDe.setTitle("\(nl.userGranary.lixiValue)", forState: .Normal)
            case 13:
                self.buttonHopSua.setTitle("\(nl.userGranary.lixiValue)", forState: .Normal)
            case 14:
                self.buttonHoaCo.setTitle("\(nl.userGranary.lixiValue)", forState: .Normal)
            default:
                break
            }
        }
    }
    
    
    //MARK: Exchange Cell Delegate
    func exchangeCellDidGet(cell: PopupLamBanhCongThucCollectionViewCell) {
        if let indexPath = collectionCongThuc.indexPathForCell(cell){
            let model = dataCongThuc.ListItem[indexPath.row]
            JTProgressHUD.show()
            EventDAO.Event_GetReward(model.requiredId, starId: self.starId, scheduleId: self.scheduleId, callback: { (result) in
                JTProgressHUD.hide()
                if result.ErrorCode == 0 {
                    if self.delegate != nil {
                        self.delegate.popupExchangeOnGetReward(result.DataObject as! GranaryRewardModel)
                    }
                }else{
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            })
        }
    }
}
