//
//  UserJoinGuildViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/25/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class UserJoinGuildViewController: ListBaseTableViewController, UserJoinGuildProtocol {
    private var actionInstruction = false
    override func viewDidLoad() {
        super.viewDidLoad()
        actionInstruction =  SettingsHelper.isInstructionShowed("joinGuildActionInstruction")
        
        // Do any additional setup after loading the view.
    }
    
    override func registerNibCell() {
        
        tableView.registerNib(UINib(nibName: "UserJoinGuildTableViewCell", bundle: nil), forCellReuseIdentifier: "UserJoinCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadData() {
        if(data.isLoadingData) {
            return
        }
        
        data.isLoadingData = true
        
        if(self.data.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }
        
        UserDAO.User_GetListUserJoinGuild(guildId: guildId, pageIndex: self.data.iPageIndex, pageSize: 10, nameUser: "") { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            if(self.data.iPageIndex == 0)
            {
                self.tableView.reloadData()
                
                
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0..<result.ListItem.count{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            self.data.isLoadingData = false
            self.data.iPageIndex += 1
            self.tableView.dg_stopLoading()
            
            //Tutorial Animation
            if(self.actionInstruction == false){
                dispatch_after(DISPATCH_TIME_NOW , dispatch_get_main_queue(), {
                    if(self.tableView.visibleCells.count > 0){
                        let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! IdolInfoTableViewCell
                        cell.aniShowToturial()
                    }
                })
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == self.data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("UserJoinCell", forIndexPath: indexPath) as! IdolInfoTableViewCell
        cell.setDataForUserJoinGuild(data.ListItem[indexPath.row])
        if(currentCell != nil){
            currentCell!.isAnimation = false
            currentCell!.isAction = false
            currentCell = nil
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    var currentCell: IdolInfoTableViewCell?
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let newCell = tableView.cellForRowAtIndexPath(indexPath) as? IdolInfoTableViewCell
        if(currentCell != nil)
        {
            currentCell?.isAction = false
            if(currentCell == newCell)
            {
                currentCell = nil
                return
            }
        }
        
        newCell?.isAction = true
        newCell?.userId = data.ListItem[indexPath.row].userInfo!.Id
        newCell?.indexPath = indexPath;
        newCell?.delegateJoinGuild = self
        currentCell = newCell
    }
    
    func clearSource()
    {
        data.ListItem.removeAll()
        tableView.reloadData()
    }
    
    func DeclineJoinGuild() {
        if (currentCell == nil)
        {
            return
        }
        
        UserDAO.User_DeclineUserJoinGuild(guildId, userId: currentCell!.userId) { (result) in
            
            if(result.ErrorCode == 0)
            {
                let index = self.currentCell!.indexPath!
                self.data.ListItem.removeAtIndex(index.row)
                
                self.tableView.deleteRowsAtIndexPaths([index], withRowAnimation: .Fade)
            }
            
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    func AcceptJoinGuild() {
        
        if (currentCell == nil)
        {
            return
        }
        
        UserDAO.User_AcceptUserJoinGuild(guildId, userId: currentCell!.userId) { (result) in
            
            if(result.ErrorCode == 0)
            {
                let index = self.currentCell!.indexPath!
                self.data.ListItem.removeAtIndex(index.row)
                
                self.tableView.deleteRowsAtIndexPaths([index], withRowAnimation: .Fade)
            }
            
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
