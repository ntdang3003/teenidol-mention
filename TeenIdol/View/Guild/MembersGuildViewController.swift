//
//  MembersGuildViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class MembersGuildViewController: ListBaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Thành viên"
        self.setupBackButton()
        
        self.tableView.tableHeaderView =  UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: 1))
    }
    
    override func registerNibCell() {
        tableView.registerNib(UINib(nibName: "GuildMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "memberCell")
        tableView.registerNib(UINib(nibName: "GuildMemberTopTableViewCell", bundle: nil), forCellReuseIdentifier: "memberTopCell")
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(data.ListItem.count>0 && data.ListItem.count <= 1)
        {
            return 1
        }
        else if(data.ListItem.count > 1)
        {
            return 2
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        return 10
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            if(data.ListItem.count > 1)
            {
                return 1;
            }
            else
            {
                return data.ListItem.count
            }
        }
        
        return data.ListItem.count - 1;
    }
    
    override func loadData()
    {
        if(data.isLoadingData) {
            return
        }
        
        data.isLoadingData = true
        
        if(self.data.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }
        
        UserDAO.User_GetListUserInGuild(PageIndex: self.data.iPageIndex, pageSize: 10, guildId: guildId, isIdol: false , nameUser: "") { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            if(self.data.iPageIndex == 0)
            {
                self.tableView.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = (self.data.ListItem.count - 1)  - result.ListItem.count
                for index in 0..<result.ListItem.count {
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 1))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tableView.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            self.data.isLoadingData = false
            self.data.iPageIndex += 1
            self.tableView.dg_stopLoading()
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section == 1)
        {
            let newIndex = indexPath.row + 1
            if(newIndex > data.ListItem.count){
                return UITableViewCell()
            }
            
            //Load More If Need
            if(self.data.CountAllResult != self.data.ListItem.count && newIndex == self.data.ListItem.count - 1){
                self.loadData()
            }
            let cell = tableView.dequeueReusableCellWithIdentifier("memberCell", forIndexPath: indexPath) as! IdolInfoTableViewCell
            cell.setDataMemberGuild(data.ListItem[newIndex], index: newIndex + 1)
            return cell
            
        }else{
            if(indexPath.row > data.ListItem.count){
                return UITableViewCell()
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier("memberTopCell", forIndexPath: indexPath) as! rankingTableViewCell
            cell.setDataUserTopGuild(data.ListItem[indexPath.row])
            return cell
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 0)
        {
            return 176
        }
        return 60
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
}
