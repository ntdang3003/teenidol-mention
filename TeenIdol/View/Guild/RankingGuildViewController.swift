//
//  RankingGuildViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class OwnGuildContainer: UIView
{
    
    @IBOutlet weak var imgAvatar: UIImageView?
    @IBOutlet weak var lbName: UILabel?
    @IBOutlet weak var imgLevel: UIImageView?
    @IBOutlet weak var lbLeader: UILabel?
    
    var hasInfo = false;
    
    override func awakeFromNib() {
        
        FunctionHelper.circleBorderImageView(imgAvatar!)
        let guildInfo = SettingsHelper.getUserInfo()?.ownerGuild
        let guildLevel = SettingsHelper.getUserInfo()?.guildLevel
        let guildLeader = SettingsHelper.getUserInfo()?.guildLeader
        
        if guildInfo?.Id > 0
        {
            lbName?.text = guildInfo!.Name
            lbLeader?.text = guildLeader?.Name
            
            if let url = NSURL(string: guildInfo!.Avatar)
            {
                imgAvatar!.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
            }
            
            if let urlLevel = NSURL(string: (guildLevel?.Photo)!){
                imgLevel!.sd_setImageWithURL(urlLevel, placeholderImage: UIImage(named: "ic_level_placeholder"))
            }
            
            hasInfo = true;
        }
    }
}

let rankingTopCell = "GuildTopRanking"
let rankingMemberCell = "rankingCell"

class RankingGuildViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    let TitleForEmptyDataSet = "Không có thông tin"
    
    //@IBOutlet weak var view_ownGuildContainer: OwnGuildContainer?
    @IBOutlet weak var tbRanking: UITableView?
    
    //@IBOutlet weak var height_layoutGuildContainer: NSLayoutConstraint?
    
    var data = FListResultDTO<GuildModel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    func setupUI(){
        self.tbRanking!.separatorStyle = .None
        self.tbRanking!.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tbRanking!.frame.size.width, height: 1))
        self.tbRanking!.registerNib(UINib(nibName: "GuildTopRankTableViewCell", bundle: nil), forCellReuseIdentifier: rankingTopCell)
        self.tbRanking!.registerNib(UINib(nibName: "GuildRankTableViewCell", bundle: nil), forCellReuseIdentifier: rankingMemberCell)
        
        self.tbRanking!.delegate = self
        self.tbRanking!.dataSource = self
        setupNavigationBar()
        setupPutToRefresh()
        loadData()
    }
    
    func setupNavigationBar(){
        self.title = "Gia Tộc"
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.toolbarHidden = true
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        if(self.navigationController?.navigationBarHidden == false){
//            self.navigationController?.setNavigationBarHidden(true, animated: true)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tbRanking!.backgroundColor!
        
        self.tbRanking!.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<GuildModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tbRanking!.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tbRanking!.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    deinit{
        if(self.tbRanking != nil){
            self.tbRanking!.dg_removePullToRefresh()
        }
    }
    
    func loadData()
    {
        if(data.isLoadingData) {
            return
        }
        data.isLoadingData = true
        
        if(self.data.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }

        UserDAO.User_GetListGuild(PageIndex: self.data.iPageIndex, pageSize: 10, callback: { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            if(self.data.iPageIndex == 0)
            {
                self.tbRanking!.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = (self.data.ListItem.count - 3) - result.ListItem.count
                for index in 0..<result.ListItem.count{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 1))
                }
                self.tbRanking!.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tbRanking!.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            self.data.isLoadingData = false
            self.data.iPageIndex  += 1
            self.tbRanking!.dg_stopLoading()
        })
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(data.ListItem.count>0 && data.ListItem.count <= 3)
        {
            return 1
        }
        else if(data.ListItem.count > 3)
        {
            return 2
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        return 10
    }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0)
        {
            if(data.ListItem.count > 3)
            {
                return 3;
            }
            else
            {
                return data.ListItem.count
            }
        }
        
        return data.ListItem.count - 3;
    }
    
//    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if(section == 0 && self.data.ListItem.count > 3){
//            let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 10))
//            viewHeader.backgroundColor = UIColor.groupTableViewBackgroundColor()
//            return viewHeader
//        }
//        return UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 0))
//    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 1)
        {
            return 60;
        }
        return 220;
    }
 
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(indexPath.section == 1)
        {
            let newIndex = indexPath.row + 3
            if(newIndex > data.ListItem.count){
                return UITableViewCell()
            }
            
            //Load More If Need
            if(self.data.CountAllResult != self.data.ListItem.count && newIndex == self.data.ListItem.count - 1){
                self.loadData()
            }
            let cell = tableView.dequeueReusableCellWithIdentifier(rankingMemberCell, forIndexPath: indexPath) as! IdolInfoTableViewCell
            cell.setDataRankingGuild(self.data.ListItem[newIndex],index: newIndex + 1)
            return cell
            
        }else{
            if(indexPath.row > data.ListItem.count){
                return UITableViewCell()
            }
            
            let cell = tableView.dequeueReusableCellWithIdentifier(rankingTopCell, forIndexPath: indexPath) as! rankingTableViewCell
            cell.setDataRankingGuild(data.ListItem[indexPath.row], index: indexPath.row);
            return cell;
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var guildId: Int64 = 0
        if(indexPath.section == 1)
        {
            let cell = tableView.cellForRowAtIndexPath(indexPath) as! IdolInfoTableViewCell
            guildId = cell.guildId
        }
        else
        {
           let cell = tableView.cellForRowAtIndexPath(indexPath) as! rankingTableViewCell
            guildId = cell.guildId
        }
        
        let vc = HomeGuildViewController(nibName: "HomeGuildViewController", bundle: nil)
        vc.guildId = guildId
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
