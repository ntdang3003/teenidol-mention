//
//  ListBaseTableViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

/// Member & idol protocol
protocol ListBaseTableViewProtocol {
    func setupUI()
    func registerNibCell(tableView: UITableView)
    func configCellForRow(data: FListResultDTO<GuildMemberModel>, tableView: UITableView, indexPath: NSIndexPath) -> UITableViewCell
    func heightForCell() -> CGFloat
}

class ListBaseTableViewController: UITableViewController {

    var data = FListResultDTO<GuildMemberModel>()
    var guildId: Int64 = 0
    
    var delegate: ListBaseTableViewProtocol?;
    
    private var isIdol = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupPutToRefresh()
        tableView.separatorStyle = .None
        
        if(delegate != nil)
        {
            delegate?.registerNibCell(tableView)
        }else{
            setupUI()
            registerNibCell()
        }
    }
    
    func setupUI()
    {
    }
    
    func registerNibCell()
    {
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupPutToRefresh()
    {
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = self.tableView.backgroundColor!
        
        self.tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.data = FListResultDTO<GuildMemberModel>()
            self!.loadData()
            }, loadingView: loadingView)
        self.tableView.dg_setPullToRefreshFillColor( ColorAppDefault)
        self.tableView.dg_setPullToRefreshBackgroundColor(UIColor.whiteColor())
    }
    
    deinit{
        if(self.tableView != nil){
            self.tableView.dg_removePullToRefresh()
        }
    }
    
    func setConfig(guildId: Int64, isIdol: Bool)
    {
        self.guildId = guildId
        self.isIdol = isIdol
        
        loadData()
    }
    
    func loadData()
    {
        if(data.isLoadingData) {
            return
        }
        
        data.isLoadingData = true
        
        if(self.data.iPageIndex == 0)
        {
            JTProgressHUD.show()
        }
        
        UserDAO.User_GetListUserInGuild(PageIndex: self.data.iPageIndex, pageSize: 10, guildId: guildId, isIdol: isIdol , nameUser: "") { (result) in
            if(self.data.iPageIndex == 0){
                JTProgressHUD.hide()
            }
            self.data.CountAllResult = result.CountAllResult
            self.data.ListItem.appendContentsOf(result.ListItem)
            if(self.data.iPageIndex == 0)
            {
                self.tableView.reloadData()
            }
            else
            {
                var arrIndex = Array<NSIndexPath>()
                let currentRow = self.data.ListItem.count - result.ListItem.count
                for index in 0...result.ListItem.count - 1{
                    arrIndex.append(NSIndexPath(forRow: currentRow + index, inSection: 0))
                }
                self.tableView.insertRowsAtIndexPaths(arrIndex, withRowAnimation: .Automatic)
            }
            
            self.tableView!.tableFooterView = FunctionHelper.getTableFooterLoadingView(self.data.ListItem.count == self.data.CountAllResult)
            self.data.isLoadingData = false
            self.data.iPageIndex += 1
            self.tableView!.dg_stopLoading()
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.ListItem.count
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(delegate != nil)
        {
            return (delegate?.heightForCell())!
        }
        return 0;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(self.data.CountAllResult != self.data.ListItem.count && indexPath.row == self.data.ListItem.count - 1)
        {
            self.loadData()
        }
        
        if(delegate != nil)
        {
            let cell = delegate?.configCellForRow(data,tableView: tableView, indexPath: indexPath)
            return cell!
        }
        return UITableViewCell()
    }
    
}
