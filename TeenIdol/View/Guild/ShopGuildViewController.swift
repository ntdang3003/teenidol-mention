//
//  ShopGuildViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit


class InventoryGuildViewController: InventoryShopViewController, IndicatorInfoProvider
{
    var guildId: Int64 = 0;
    var itemInfo = IndicatorInfo(title: "View")
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupUI()
    {
    
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?,itemInfo: IndicatorInfo)
    {
        self.itemInfo = itemInfo
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: "InventoryShopViewController", bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    override func loadData() {
        
        JTProgressHUD.show()
        let item = ItemCategoryModel()
        item.category = ItemCategoryDTO()
        item.category.Id = 10
        item.category.Name = "Gia tộc"
        
        let item2 = ItemCategoryModel()
        item2.category = ItemCategoryDTO()
        item2.category.Id = 9
        
        data.ListItem.append(item)
        data.ListItem.append(item2)
        
        self.colMain.reloadData()
        JTProgressHUD.hide()
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if(kind == UICollectionElementKindSectionHeader){
            let header = colMain.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "InventoryHeaderIndentifier", forIndexPath: indexPath) as! InventoryShopCollectionViewHeader
            let data = self.data.ListItem[indexPath.section]
            if(data.listAnimationItem.iPageIndex == 0){
                
                ItemDAO.List_ItemsAnimationInStoreForGuild(data.listAnimationItem.iPageIndex, pageSize: 100, categoryId: data.category.Id, isNewVersion: true, guildId: guildId, callback: { (result) in
                    data.listAnimationItem = result
                    if(data.listAnimationItem.ListItem.count > 0)
                    {
                        data.listAnimationItem.iPageIndex += 1
                        self.colMain.performBatchUpdates({
                            self.colMain.reloadSections(NSIndexSet(index: indexPath.section))
                            }, completion: nil)
                    }
                    
                })
            }
            header.setData(data)
            return header
        }
        return UICollectionReusableView()
        
    }
    
    override func touchedBuy(itemId: Int, purchaseId: Int) {
        
        if(SettingsHelper.getUserInfo()?.ownerGuild?.Id == 0)
        {
            FunctionHelper.showBannerAlertView(content: "Bạn cần phải gia nhập gia tộc.")
            return
        }
        
        JTProgressHUD.show()
        UserDAO.User_BuyItemsForGuild(itemId, purchaseId: purchaseId, isVipItem: false, guildId: guildId) { (result) in
            JTProgressHUD.hide()
            if(result.ErrorCode == 0)
            {
                self.touchedClose()
            }
            
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

class ItemVipGuildViewController: ShopViewController, IndicatorInfoProvider
{
    var guildId: Int64 = 0
    var itemInfo = IndicatorInfo(title: "View")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryId = 13
    }
    
    override func setupLeftMenuButton() {
        
    }
    
    override func setupNavigationBar() {
        
    }
    
    init(itemInfo: IndicatorInfo) {
        self.itemInfo = itemInfo
        super.init(nibName: "ShopViewController", bundle: nil)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - IndicatorInfoProvider
    func indicatorInfoForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

class ShopGuildViewController: FollowViewController {
    var guildId: Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewControllersForPagerTabStrip(pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = InventoryGuildViewController(itemInfo: "Trang bị")
        let child_2 = ItemVipGuildViewController(itemInfo: "Vip")
        return [child_1, child_2]
    }
}
