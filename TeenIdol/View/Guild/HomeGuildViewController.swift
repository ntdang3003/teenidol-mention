//
//  HomeGuildViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class HomeGuildViewController: UIViewController, ListBaseTableViewProtocol, UIScrollViewDelegate {
    
    @IBOutlet weak var imgAvatar: UIImageView?
    @IBOutlet weak var lbName: UILabel?
    @IBOutlet weak var imgLevel: UIImageView?
    @IBOutlet weak var lbSlogan: UILabel?
    @IBOutlet weak var imgIconGuild: UIImageView?
    
    @IBOutlet weak var imgLeaderAvatar: UIImageView?
    @IBOutlet weak var lbLeaderName: UILabel?
    @IBOutlet weak var lbPosition: UILabel?
    
    @IBOutlet weak var lbMembers: UILabel?
    @IBOutlet weak var lbIdols: UILabel?
    
    @IBOutlet weak var btStatus: UIButton?
    @IBOutlet weak var viewGuildLevel: YLProgressBar?
    @IBOutlet weak var lbRubyGuild: UILabel?
    @IBOutlet weak var btEditInfo: UIButton?
    
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet weak var conHeaderHeight : NSLayoutConstraint!
    @IBOutlet weak var conHeaderToTop : NSLayoutConstraint!
    
    var guildId: Int64 = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tòa thị chính"
        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if(self.navigationController?.navigationBarHidden == true){
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        FunctionHelper.circleBorderImageView(imgAvatar!)
        self.loadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.hidesBarsOnSwipe = false
    }
    
    func setupNavigationBar(){
        self.setupBackButton()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        self.navigationController?.toolbarHidden = true
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.barTintColor = ColorAppDefault
        self.navigationController?.navigationBar.setBackgroundImage(nil, forBarMetrics: .Default)
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(named:"bg_navigation"), forBarPosition: .Any, barMetrics: .Default)
        navigationBar?.shadowImage = UIImage()
    }
    
    func setupTabBar(){
        UITabBar.appearance().tintColor = ColorAppDefault
    }
    
    func setupUI()
    {
        self.setupNavigationBar()
        self.setupTabBar()
        FunctionHelper.circleBorderImageView(self.imgLeaderAvatar!)
        if let userInfo = SettingsHelper.getUserInfo(){
            let guildPosition = userInfo.guildPosition
            let guildInfo = userInfo.ownerGuild
            if (guildInfo!.Id != 0 && guildId == guildInfo?.Id)
            {
                btStatus?.setBackgroundImage(UIImage(named: "ic_guild_leave"), forState: .Normal)
                btStatus?.tag = 100
                
                // truong toc
                if(guildPosition?.position == 1)
                {
                    btStatus?.setBackgroundImage(UIImage(named: "ic_guild_review"), forState: .Normal)
                    btStatus?.tag = 101
                    
                    btEditInfo?.hidden = false
                }
            }
            else if (guildId != guildInfo?.Id)
            {
                btStatus?.setBackgroundImage(UIImage(named: "ic_guild_join"), forState: .Normal)
                btStatus?.tag = 102
            }
        }
        //viewGuildLevel?.transform = CGAffineTransformMakeRotation(CGFloat(-(M_PI/2)));
        
        //self.scrollView.contentInset = UIEdgeInsets(top: 103, left: 0, bottom: 0, right: 0)
        //self.scrollView.contentOffset = CGPoint(x: 0, y: -103)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        self.updateHeaderView()
    }
    
    func updateHeaderView(){
        //var headerRect = CGRect(x: 0, y: 0, width: self.tbFunctions.bounds.width, height: 103)
//        if self.tbFunctions.contentOffset.y < 0 {
//            headerRect.origin.y = self.tbFunctions.contentOffset.y
//            headerRect.size.height = -self.tbFunctions.contentOffset.y
//        }
//        viewHeader!.frame = headerRect
        
        if self.scrollView.contentOffset.y < 0{
            conHeaderToTop.constant = self.scrollView.contentOffset.y
            conHeaderHeight.constant = (95 - self.scrollView.contentOffset.y)
        }else{
            conHeaderToTop.constant = 0
            conHeaderHeight.constant = 95
        }
        self.view.layoutIfNeeded()
    }
    
    func loadData()
    {
        UserDAO.User_GetGuildInfo(self.guildId) { (result) in
            if( result.ErrorCode == 0)
            {
                let model = result.DataObject as! GuildModel
                FunctionHelper.runSyncWithMainThread({
                    self.updateUI(model)
                })
            }
        }
    }
    
    func updateUI(model: GuildModel)
    {
        lbName?.text = model.guildInfo?.Name
        lbSlogan?.text = "\"\((model.guildInfo?.Description)!)\""
        lbIdols?.text = displayMember(current: model.guildInfo!.TotalIdol, max: model.currentLevel!.TotalIdol)
        lbMembers?.text = displayMember(current: model.guildInfo!.TotalUser, max: model.currentLevel!.TotalUser)
        lbLeaderName?.text = model.owner?.Name
        
        if let  url = NSURL(string: (model.owner?.AvatarPhoto)!)
        {
            imgLeaderAvatar?.sd_setImageWithURL(url, placeholderImage:  UIImage(named:"ic_default_avatar"))
        }
        
        if let url = NSURL(string: (model.guildInfo?.Avatar)!)
        {
            imgIconGuild?.sd_setImageWithURL(url, placeholderImage: UIImage(named:"ic_default_avatar"))
        }
        
        if let url = NSURL(string: (model.guildInfo?.PhotoLink)!)
        {
            imgAvatar?.sd_setImageWithURL(url, placeholderImage: UIImage(named: "ic_default_avatar"))
        }
        
        if let url = NSURL(string: (model.currentLevel?.Photo)!)
        {
            imgLevel?.sd_setImageWithURL(url, placeholderImage: nil)
        }
        
        let level_percent = CGFloat((model.guildInfo?.Ruby)!) / CGFloat((model.nextLevel?.Point)!)
        viewGuildLevel?.progress = CGFloat(level_percent)
        lbRubyGuild?.text = FunctionHelper.formatNumberWithSeperator((model.guildInfo?.Ruby)!)
        
        //self.viewGuildLevel?.progress = 0.5
        
    }
    
    private func displayMember(current current:Int, max: Int) -> String
    {
        return "\(current)/\(max)"
    }
    
    @IBAction func bt_userAction(sender: UIButton)
    {
        let tag = sender.tag
        switch tag {
        case 100:
            leaveGuild()
            break
            
        case 101:
            reviewGuild()
            break
            
        case 102:
            joinGuild()
            break
            
        default:
            break
        }
    }
    
    private func leaveGuild()
    {
        UserDAO.User_KickOutUser(guildId, userId: SettingsHelper.getUserID()) { (result) in
            FunctionHelper.showBannerAlertView(content: result.Message)
        }
    }
    
    private func joinGuild()
    {
        self.checkLogin { (state) in
            if(state == .Logged){
                UserDAO.User_SendRequestJoinGuild(self.guildId, userId: SettingsHelper.getUserID()) { (result) in
                    FunctionHelper.showBannerAlertView(content: result.Message)
                }
            }
        }
        
    }
    
    private func reviewGuild()
    {
        let vc = UserJoinGuildViewController(style: .Plain)
        vc.setConfig(guildId, isIdol: false)
        navigationController?.pushViewController(vc, animated: true)
        
    }
    var isMemberView = false;
    @IBAction func bt_membersView()
    {
        //let vc = ListBaseTableViewController(style: .Plain)
        let vc = MembersGuildViewController(style: .Grouped)
        isMemberView = true
        //vc.delegate = self
        vc.setConfig(guildId, isIdol: false)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_idolsView()
    {
        let vc = IdolsGuildViewController(style: .Plain)
        isMemberView = false
        vc.delegate = self
        vc.setConfig(guildId, isIdol: true)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bt_shopView()
    {
        let vc = ShopGuildViewController(nibName: "ShopGuildViewController", bundle: nil)
        vc.guildId = guildId
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // MARK: ListBase Protocol
    
    func registerNibCell(tableView: UITableView) {
        if(isMemberView)
        {
            tableView.registerNib(UINib(nibName: "GuildMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "memberCell")
        }
        else
        {
            tableView.registerNib(UINib(nibName: "GuildIdolTableViewCell", bundle: nil), forCellReuseIdentifier: "idolGuildCell")
        }
    }
    
    func heightForCell() -> CGFloat {
        if(isMemberView)
        {
            return 60
        }
        return 208
    }
    
    func configCellForRow(data: FListResultDTO<GuildMemberModel>, tableView: UITableView, indexPath: NSIndexPath) -> UITableViewCell {
        if(isMemberView)
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("memberCell", forIndexPath: indexPath) as! IdolInfoTableViewCell
            cell.setDataMemberGuild(data.ListItem[indexPath.row], index: indexPath.row)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCellWithIdentifier("idolGuildCell", forIndexPath: indexPath) as! rankingTableViewCell
            cell.setDataIdolGuild(data.ListItem[indexPath.row])
            return cell
        }
    }
    
    @IBAction func btEditInfo(sender: UIButton!)
    {
        FunctionHelper.showBannerAlertView(content: "Tính năng đang phát triển")
    }
    
}
