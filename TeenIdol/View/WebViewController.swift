//
//  WebViewController.swift
//  LiveIdol
//
//  Created by TFL Mac on 9/14/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {

    var url = ""
    @IBOutlet weak var webView: UIWebView?
    @IBOutlet weak var activityLoad: UIActivityIndicatorView?
    @IBOutlet weak var lbTitle: UILabel?
    @IBOutlet weak var backgroundView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        loadweb()
        // Do any additional setup after loading the view.
    }

    func setupUI()
    {
        backgroundView?.backgroundColor = ColorAppDefault
        activityLoad?.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    init(title: String, url: String)
    {
        super.init(nibName: "WebViewController", bundle: nil)
        self.lbTitle?.text = title
        self.url = url
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func loadweb()
    {
        if let url = NSURL(string: url)
        {
            webView?.loadRequest(NSURLRequest(URL: url))
        }
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        
        activityLoad?.stopAnimating()
        
    }
    
    @IBAction func bt_close()
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
