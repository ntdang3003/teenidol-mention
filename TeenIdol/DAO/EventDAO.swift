//
//  EventDAO.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import Alamofire

class EventDAO: NSObject {

    class func User_CheckIdolQuest(starID: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_CheckIdolQuest(SettingsHelper.getUserID(), starId: starID, key: SettingsHelper.getUserTokenKey())
       
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                
                let dic =  JSON(response.result.value!)
                itemresult = DoResultDTO(data: dic, className: IdolQuestStateDTO.self)
                callback(result: itemresult)
        }
    }

    class func User_CurrentIdolQuestInfo(starId: Int64, callback:((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_CurrentIdolQuestInfo(SettingsHelper.getUserID(), starId: starId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                
                let dic =  JSON(response.result.value!)
                itemresult = DoResultDTO(data: dic, className: IdolQuestModel.self)
                callback(result: itemresult)
        }
    }
    
    class func User_CancelIdolQuest(scheduleId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_CancelIdolQuest()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId": "\(scheduleId)",
                     "key":"\(SettingsHelper.getUserTokenKey())"]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_DoneIdolQuest(scheduleId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_DoneIdolQuest()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId": "\(scheduleId)",
                     "key":"\(SettingsHelper.getUserTokenKey())"]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_IdolGetQuest(scheduleId: Int64, level: Int, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_IdolGetQuest()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId": "\(scheduleId)",
                     "level": "\(level)",
                     "key":"\(SettingsHelper.getUserTokenKey())"]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_GetRewardInfo(starId: Int64, callback:((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.User_GetRewardInfo(SettingsHelper.getUserID(), starId: starId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                
                let dic =  JSON(response.result.value!)
                itemresult = DoResultDTO(data: dic, className: IdolQuestModel.self)
                callback(result: itemresult)
        }
    }

    
    class func Star_InfoIdolGift(starId starId: Int64, callback:((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.Star_InfoIdolGift(actionUserId: SettingsHelper.getUserID(), starId: starId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                
                let dic =  JSON(response.result.value!)
                itemresult = DoResultDTO(data: dic, className: IdolGiftShowModel.self)
                callback(result: itemresult)
        }
    }
    
    //MARK: Event Li Xi
    
    class func Event_GetListLixi(pageIndex: Int,pageSize: Int, callback: ((result: FListResultDTO<LixiRequireModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<LixiRequireModel>()
        let strURL = URLConstant.Event_GetGranaryInfo(SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: LixiRequireModel = LixiRequireModel(jsondata: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func Event_GetUserLixiInfo(type: Int, callback:((result: DoResultDTO) -> Void))
    {
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.Event_GetUserLixi(SettingsHelper.getUserID(), typeId: type, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult = DoResultDTO(data: dic, className: UserGranaryDTO.self)
                callback(result: itemresult)
        }
    }
    
    class func Event_GetReward(requireId: Int64, starId: Int64, scheduleId: Int64,  callback:((result: DoResultDTO) -> Void)){
        var itemresult: DoResultDTO = DoResultDTO()
        let strURL = URLConstant.Event_GetGranaryReward()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId": "\(scheduleId)",
                     "requireId": "\(requireId)",
                     "starId": "\(starId)",
                     "key":SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic,className :GranaryRewardModel.self)
            callback(result: itemresult)
        }
    }
    
    class func Event_GetIdolTotalGranary(starId: Int64, callback:((result: FListResultDTO<IdolGranaryInfoDTO>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<IdolGranaryInfoDTO>()
        let strURL = URLConstant.Event_GetIdolTotalChicken(starId)
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: IdolGranaryInfoDTO = IdolGranaryInfoDTO(jsondata: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func Event_GetNguyenLieu(callback:((result: FListResultDTO<NguyenLieuModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<NguyenLieuModel>()
        let strURL = URLConstant.Event_GetNguyenLieu(SettingsHelper.getUserID(), key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                let items = (dic["Result"].array)
                for subdic in items! {
                    let item: NguyenLieuModel = NguyenLieuModel(jsondata: subdic)
                    itemresult.ListItem.append(item)
                }
                callback(result: itemresult)
        }
    }
}
