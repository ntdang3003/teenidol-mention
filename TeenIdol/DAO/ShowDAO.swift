//
//  ShowDAO.swift
//  TeenIdol
//
//  Created by Chip on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ShowDAO: NSObject {
    class func StreamBegin (actionUserId :Int64 ,name : String, callback : ((result : DoResultDTO)-> Void))
    {
        var itemresult =  DoResultDTO()
        let strURL : String = URLConstant.getSharedInstance().SERVER_IP + URLConstant.getSharedInstance().StreamBegin;
        let param = ["actionUserId" : "\(actionUserId)",
                     "name" : name,
                     "key" : SettingsHelper.sharedInstance.KeySecret];
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: BeginShowDTO.self)
            callback(result: itemresult)
        }

    }
    
    class func StreamEnd (actionUserId :Int64 ,scheduleId : Int64, callback : ((result : DoResultDTO)-> Void))
    {
        var itemresult =  DoResultDTO()
        let strURL : String = URLConstant.getSharedInstance().SERVER_IP + URLConstant.getSharedInstance().StreamEnd;
        let param = ["actionUserId" : "\(actionUserId)",
                     "scheduleId" : "\(scheduleId)",
                     "key" : SettingsHelper.sharedInstance.KeySecret];
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: EndShowDTO.self)
            callback(result: itemresult)
        }
        
    }
}
