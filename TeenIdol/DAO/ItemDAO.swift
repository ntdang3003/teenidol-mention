//
//  ItemDAO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/1/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
import Alamofire

class ItemDAO: NSObject {

    class func List_ItemCategories(pageIndex: Int, pageSize: Int, callback: ((result: FListResultDTO<ItemCategoryModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<ItemCategoryModel>()
        let strURL = URLConstant.List_ItemCategories(pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
            //URLConstant.User_GetListUserInShow(SettingsHelper.getUserID(), showId: showId, pageIndex: pageIndex, pageSize: pageSize,key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: ItemCategoryModel = ItemCategoryModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    
    class func List_ItemsVipInStore(pageIndex: Int, pageSize: Int, categoryId: Int, callback : ((result : FListResultDTO<VipItemModel>)->Void)) {
        
        let itemresult: FListResultDTO = FListResultDTO<VipItemModel>()
        let strURL = URLConstant.ListItemsVipInStore(pageIndex, pageSize: pageSize, categoryId: categoryId, key: SettingsHelper.getUserTokenKey())
    
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: VipItemModel = VipItemModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func List_ItemsAnimationInStore(pageIndex: Int, pageSize: Int, categoryId: Int64, isNewVersion: Bool, callback: ((result: FListResultDTO<AnimationItemModel>)-> Void))
    {
        //let _isNewVersion = isNewVersion ? "true" : "false"
        
        let itemresult: FListResultDTO = FListResultDTO<AnimationItemModel>()
        let strURL = URLConstant.ListItemsAnimationInStore(pageIndex, pageSize: pageSize, categoryId: categoryId, isNewVersion: isNewVersion, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: AnimationItemModel = AnimationItemModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func List_ItemsAnimationInStoreForGuild(pageIndex: Int, pageSize: Int, categoryId:Int64, isNewVersion: Bool, guildId: Int64, callback: ((result: FListResultDTO<AnimationItemModel>) -> Void))
    {
        //let _isNewVersion = isNewVersion ? "true" : "false"
        
        let itemresult: FListResultDTO = FListResultDTO<AnimationItemModel>()
        let strURL = URLConstant.List_ItemsAnimationInStoreForGuild(pageIndex, pageSize: pageSize, categoryId: categoryId, isNewVersion: isNewVersion, guildId: guildId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: AnimationItemModel = AnimationItemModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
}
