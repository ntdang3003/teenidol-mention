//
//  GiftDAO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import Alamofire

class GiftDAO: NSObject {

    class func List_ListGiftInShow(giftCategoryId GiftCategoryId:Int, pageIndex PageIndex:Int, pageSize PageSize:Int, callback : ((result : FListResultDTO<GiftModel>)->Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<GiftModel>()
        let strURL = URLConstant.List_ListGiftInShow(SettingsHelper.getUserID(), giftCategoryId: GiftCategoryId, pageIndex: PageIndex, pageSize: PageSize)
        Alamofire.request(.GET, strURL).responseJSON() { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic =  JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].array)
                for subdic in items! {
                    let item: GiftModel = GiftModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
        }
    }
    
    class func List_ListGiftInShow_sync(giftCategoryId GiftCategoryId:Int, pageIndex PageIndex:Int, pageSize PageSize:Int, callback : ((result : FListResultDTO<GiftModel>)->Void))
    {
        let semaphore = dispatch_semaphore_create(0)
        let itemresult: FListResultDTO = FListResultDTO<GiftModel>()
        let strURL = URLConstant.List_ListGiftInShow(SettingsHelper.getUserID(), giftCategoryId: GiftCategoryId, pageIndex: PageIndex, pageSize: PageSize)
        Alamofire.request(.GET, strURL).responseJSON(queue:  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic =  JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].array)
                for subdic in items! {
                    let item: GiftModel = GiftModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
            dispatch_semaphore_signal(semaphore)
        }
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER)
    }
    
    class func List_ListGiftCategory(callback: (result: FListResultDTO<GiftCategoryModel>)-> Void)
    {
        let itemresult: FListResultDTO = FListResultDTO<GiftCategoryModel>()
        let strURL = URLConstant.List_ListGiftCategory(SettingsHelper.getUserID())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: GiftCategoryModel = GiftCategoryModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func List_GetAllGift(callback: (result: FListResultDTO<GiftModel>)-> Void)
    {
        
        List_ListGiftCategory(){ (result) in
            if result.CountAllResult > 0
            {
                let fResult = FListResultDTO<GiftModel>()
                let weakResult = result
                let queue = dispatch_queue_create("com.giftall.manager", DISPATCH_QUEUE_SERIAL)
                
                for i in 0 ... weakResult.ListItem.count - 1{
                    dispatch_async(queue, {
                        let model = result.ListItem[i]
                        List_ListGiftInShow_sync(giftCategoryId: model.giftCategoryDTO!.Id, pageIndex: 0, pageSize: 100, callback: { (giftResult) in
                            fResult.ListItem.appendContentsOf(giftResult.ListItem)
                            fResult.CountAllResult = fResult.CountAllResult + giftResult.CountAllResult
                            if(i == result.ListItem.count - 1){
                                callback(result: fResult)
                            }
                        })
                    })
                }
            }
        }
    }
    
    class func GiveGiftV2(giftId: Int, quantity: Int, scheduleId: Int64, toUserId userId:Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.GiveGiftV2()
        
        let param = [ "actionUserId": "\(SettingsHelper.getUserID())",
                      "giftId": "\(giftId)",
                      "quantity": "\(quantity)",
                      "scheduleId": "\(scheduleId)",
                      "toUserId": "\(userId)",
                      "platform" : "2",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: GiftModel.self)
            callback(result: itemresult)
        }
    }
    
    class func User_GetListGiftV2(pageIndex: Int, pageSize: Int,  callback: (result: FListResultDTO<GiftModel>)-> Void)
    {
        let itemresult: FListResultDTO = FListResultDTO<GiftModel>()
        let strURL = URLConstant.List_GetListGiftV2(SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON() { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic =  JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].array)
                for subdic in items! {
                    let item: GiftModel = GiftModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
        }
    }
}



