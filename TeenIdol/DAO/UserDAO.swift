//
//  UserDAO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit
import Alamofire
 

enum User_GetListType : Int{
    case UserFollow = 0
    case UserFan = 1
}

class UserDAO: NSObject {
    
    class func User_GetListUserInShow(showId: Int64, pageIndex: Int, pageSize: Int, callback : (( result : FListResultDTO<UserModel>)->Void)) {
        let itemresult: FListResultDTO = FListResultDTO<UserModel>()
        let strURL = URLConstant.User_GetListUserInShow(SettingsHelper.getUserID(), showId: showId, pageIndex: pageIndex, pageSize: pageSize,key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: UserModel = UserModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_GetUserInfoInShow(userId: Int64, callback:((result: DoResultDTO)->Void)){
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_GetUserInfoInShow(SettingsHelper.getUserID(),userId: userId, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserModel.self)
            callback(result: itemresult)
        }
    }
    
    class func User_GetAnimationItemJoinShow(userId: Int64, callback:((result: DoResultDTO)->Void)){
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_GetAnimationItemJoinShow(userId, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: AnimationItemModel.self)
            callback(result: itemresult)
        }
    }
    
    class func User_ParticipateInShow(showId: Int64, connectionId:String, callback : ((result : DoResultDTO)->Void)) {
        var itemresult = DoResultDTO()
        //let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.sharedInstance.User_ParticipateInShow))"
        let strURL = URLConstant.User_ParticipateInShow()
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "showId": "\(showId)",
                      "clientIndentifier": "",
                      "connectionId" : connectionId,
                      "key" : SettingsHelper.getUserTokenKey()]
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: ShowModel.self)
            callback(result: itemresult)
        }
    }
    
    // MARK: Star DAO
    class func List_SearchListStar(pageIndex PageIndex: Int, pageSize PageSize: Int, search Search:String, callback: ((result: FListResultDTO<UserModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<UserModel>()
        //let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.List_SearchListStar, PageIndex, PageSize, Search, SettingsHelper.sharedInstance.KeySecret))"
        
        let strURL = URLConstant.List_SearchListStar(-1, pageIndex: PageIndex, pageSize: PageSize, search: Search, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: UserModel = UserModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    // MARK: Sign up & in
    class func User_RegisterByTeenIdol(userInfo: UserDTO, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        // let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.sharedInstance.User_RegisterByTeenIdol))"
        
        let strURL = URLConstant.User_RegisterByTeenIdol()
        
        let paramUser = [ "Email" : userInfo.Email,
                          "Name": userInfo.Name,
                          "Password": userInfo.Password,
                          "Gender": userInfo.Gender,
                          "LastIpConnected": userInfo.LastIpConnected]
        
        let param = ["user": paramUser,
                     "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserSessionDTO.self )
            callback(result: itemresult)
        }
    }
    
    class func User_RegisterByTeenIdolV2(userInfo: UserDTO, contentPhoto: String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        // let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.sharedInstance.User_RegisterByTeenIdol))"
        
        let strURL = URLConstant.User_RegisterByTeenIdolV2()
        
        let paramUser = [ "Email" : userInfo.Email,
                          "Name": userInfo.Name,
                          "Password": userInfo.Password,
                          "Gender": userInfo.Gender,
                          "LastIpConnected": userInfo.LastIpConnected]
        
        let param = ["user": paramUser,
                     "contentPhoto": contentPhoto,
                     "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserSessionDTO.self )
            callback(result: itemresult)
        }
    }
    
    
    class func User_LoginByFacebook(accessToken:String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_LoginByFacebook()
        
        let param = [ "accessToken" : "\(accessToken)",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserSessionDTO.self )
            callback(result: itemresult)
        }
        
    }
    
    class func User_LoginTeenidol(email Email:String, pass Password:String, ipAddress IpAddress:String, userToken UserToken:String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_LoginTeenIdolV2()
        
        let param = [ "email" :  "\(Email)",
                      "password": "\(Password)",
                      "ipAddress": "\(IpAddress)",
                      "userToken": "\(UserToken)",
                      "key": ""]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserSessionDTO.self)
            //SettingsHelper.setUserID(dic["Id"].int64Value)
            //SettingsHelper.setUserTokenKey(dic["Key"].stringValue)
            callback(result: itemresult)
        }
    }
    
    // MARK: Profile
    class func User_GetDetailsV2(UserId userId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_GetDetailsV2(SettingsHelper.getUserID(), userId: userId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserModel.self)
            callback(result: itemresult)
        }
    }
    
    class func User_UpdateNewPassWord(OldPass oldpass:String, NewPass newpass: String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_UpdateNewPassword()
        
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "oldPass": oldpass,
                      "newPass": newpass,
                      "key" : SettingsHelper.getUserTokenKey()]
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_UpdateTeenIdolAccount(UserInfo userInfo: UserDTO, callback: ((result : DoResultDTO) ->Void))
    {
        var itemresult = DoResultDTO()
        // let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.sharedInstance.User_UpdateTeenIdolAccount))"
        let strURL = URLConstant.User_UpdateTeenIdolAccout()
        
        let paramUser = [ "Id" : "\(userInfo.Id)",
                          "Name": userInfo.Name,
                          "AvatarPhoto": userInfo.AvatarPhoto,
                          "Email" : userInfo.Email,
                          "Gender": userInfo.Gender]
        
        let param = ["user": paramUser,
                     "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_UploadPhoto(BasePhoto64 basePhoto64: String, callback: ((result : DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        //let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.sharedInstance.User_UploadPhoto))"
        let strURL = URLConstant.User_UploadPhoto()
        
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "base64Photo" : basePhoto64,
                      "key" : SettingsHelper.getUserTokenKey()]
        
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserDTO.self)
            itemresult.DataObject = SettingsHelper.getUserInfo()?.userInfo;
            (itemresult.DataObject as! UserDTO).AvatarPhoto = dic["Result"].stringValue
            callback(result: itemresult)
        }
    }
    
    class func User_FollowUser(FollowUserId followUserId:Int64, IsFollow isFollow:Bool, callback: ((result: DoResultDTO) -> Void))
    {
        
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_FollowUser()
        
        let str_Follow:String = isFollow ? "true" : "false"
        
        let param = [ "currentUserId": "\(SettingsHelper.getUserID())",
                      "followUserId": "\(followUserId)",
                      "isFollow": str_Follow,
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: UserDTO.self)
            callback(result: itemresult)
        }
        
    }
    
    class func User_ShareFB(callback: ((result: DoResultDTO)-> Void))
    {
        let itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_ShareFacebook()
        
        let param = [ "currentUserId": "\(SettingsHelper.getUserID())",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            callback(result: itemresult)
        }
    }
    
    
    /// Lấy danh sách follow ver 2.0.
    ///
    /// FollowType:
    ///
    ///    + UserFollow: Lấy đang sách các user mà mình đã theo dõi
    ///    + UserFan: Lấy đang sách các user mà đã theo dõi mình
    
    class func User_GetListUserFollowV2(followType followType:User_GetListType, pageIndex PageIndex:Int, pageSize PageSize:Int, str_search str_Search: String, num_userId num_UserID: Int64,callback: ((result: FListResultDTO<UserModel>) ->Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<UserModel>()
        
        var strURL = ""
        
        switch followType {
        case .UserFollow:
            strURL = URLConstant.User_GetListUserFollowV2(SettingsHelper.getUserID(), pageIndex: PageIndex, pageSize: PageSize, contextKey: str_Search, num_UserId: num_UserID, key: SettingsHelper.getUserTokenKey())
            
        case .UserFan:
            strURL = URLConstant.User_GetListUserFanV2(SettingsHelper.getUserID(), pageIndex: PageIndex, pageSize: PageSize, contextKey: str_Search, num_UserId: num_UserID, key: SettingsHelper.getUserTokenKey())
            
        }
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: UserModel = UserModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_GetListUserFanV2(pageIndex PageIndex:Int, pageSize PageSize:Int, str_search str_Search: String, num_userId num_UserId: Int64, callback: ((result: FListResultDTO<UserModel>) ->Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<UserModel>()
        let strURL = URLConstant.User_GetListUserFanV2(SettingsHelper.getUserID(), pageIndex: PageIndex, pageSize: PageSize, contextKey: str_Search, num_UserId: num_UserId, key: SettingsHelper.getUserTokenKey())
        
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: UserModel = UserModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
        
    }
    
    class func User_DoPayment(type: Int, cardSerial:String, cardPin:String, isUseVoucher: Bool, code: String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_DoPayment()
        
        let param = [ "actionUserId": "\(SettingsHelper.getUserID())",
                      "type" : "\(type)",
                      "cardSerial": "\(cardSerial)",
                      "cardPin": "\(cardPin)",
                      "isUseVoucher": "\(isUseVoucher)",
                      "code": code,
                      "flatform": "\(2)",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param, encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: PaymentLogDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_GetListUserOnlineV2(pageIndex: Int, pageSize: Int, searchKey: String = "", callback : ((result : FListResultDTO<UserSMModel>)->Void)) {
        let itemresult: FListResultDTO = FListResultDTO<UserSMModel>()
        let strURL = URLConstant.User_GetListUserOnlineV2(Int64(SettingsHelper.getUserID()), pageIndex: pageIndex, pageSize: pageSize,key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: UserSMModel = UserSMModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func StreamBegin(name:String, facebookToken: String, width: Int, height: Int, connectionType: String,
                           callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.StreamBegin()
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafeMutablePointer(&systemInfo.machine) {
            ptr in String.fromCString(UnsafePointer<CChar>(ptr))
        }
        var model = "Unknown"
        if let _model = String.fromCString(modelCode!) {
            model = _model
        }
        let currentDevice = UIDevice.currentDevice()
        var versionStr = ""
        if let version = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as? String{
            versionStr = version
        }
        //OS | APP | ConnectionType  | Model
        let deviceInfo = "\(currentDevice.systemName) \(currentDevice.systemVersion) | \(versionStr) | \(connectionType) | \(model)"
        
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "name" : name,
                      "facebookToken": facebookToken,
                      "width" : "\(width)",
                      "height" : "\(height)",
                      "platform" : "2",
                      "device": "\(deviceInfo)",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: BeginShowModel.self )
            callback(result: itemresult)
        }
        
    }
    
    class func StreamEnd(scheduleId:Int64,
                         callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.StreamEnd()
        
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "scheduleId" : "\(scheduleId)",
                      "key": SettingsHelper.getUserTokenKey()]
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON{ (response) in
            if(response.result.value == nil)
            {
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: EndShowModel.self )
            callback(result: itemresult)
        }
        
    }
    
    class func User_GetListUserV2(PageIndex pageIndex: Int, PageSize pageSize:Int, Str_Search str_Search: String, callback: ((result: FListResultDTO<UserModel>)-> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<UserModel>()
        var strURL = URLConstant.User_GetListUserV2(SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, contextkey: str_Search, key: SettingsHelper.getUserTokenKey())
        strURL = strURL.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: UserModel = UserModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
        
    }
    class func ListUserSpent(pageIndex: Int, pageSize: Int, type: Int, startTime: Int64, endTime: Int64, callback:((result: FListResultDTO<LogModel>) -> Void))
    {
        
        let itemresult = FListResultDTO<LogModel>()
        let strURL = URLConstant.User_GetListUserSpent(SettingsHelper.getUserID(),pageIndex: pageIndex, pageSize: pageSize, type: type, startTime: startTime, endTime: endTime, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0)
                {
                    let items = (dic["Items"].array)
                    
                    if(type == 1)
                    {
                        for subdic in items! {
                            let item: CoinLogModel = CoinLogModel(dic: subdic)
                            itemresult.ListItem.append(item)
                        }
                    }
                    
                    if( type == 2)
                    {
                        for subdic in items! {
                            let item: PaymentLogModel = PaymentLogModel(dic: subdic)
                            itemresult.ListItem.append(item)
                        }
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_SendCoinChat(targetUserId: Int64, scheduleId : Int64, message : String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_SendCoinChat()
        
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "scheduleId": "\(scheduleId)",
                      "targetUserId": "\(targetUserId)",
                      "message": message,
                      "key" : SettingsHelper.getUserTokenKey()]
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func System_AppPurchaseIOS(receiptData: String, ruby: Int  ,callback: ((result : DoResultDTO) -> Void)){
        var itemResult = DoResultDTO()
        
        let strURL = URLConstant.System_AppPurchaseIOS()
        let param = [ "actionUserId" : "\(SettingsHelper.getUserID())",
                      "receiptData": receiptData,
                      "coin": String(ruby),
                      "key" : SettingsHelper.getUserTokenKey()]
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemResult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemResult = DoResultDTO(data: dic)
            callback(result: itemResult)
        }
    }
    
    class func User_BuyItems(itemId: Int, purchaseId: Int, isVipItem: Bool, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.UserBuyItems()
        let _isVip = isVipItem ? "true":"false"
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "itemId": "\(itemId)",
                     "purchaseId": "\(purchaseId)",
                     "isVipItem" : _isVip,
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func Star_RegisterStar(name: String, phone: String, facebookLink: String, email: String, youtubeLink: String, talenDescription: String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.Star_RegisterStar();
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "name": "\(name)",
                     "phone": "\(phone)",
                     "facebookLink": "\(facebookLink)",
                     "email":"\(email)",
                     "youtubeLink":"\(youtubeLink)",
                     "talenDescription":"\(talenDescription)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
        
    }
    
    class func List_GetListUserBuySeatInShow(scheduleId : Int64, callback: ((result: FListResultDTO<ShowItemModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<ShowItemModel>()
        //let strURL: String = "\(URLConstant.sharedInstance.SERVER_IP)\(String(format: URLConstant.List_SearchListStar, PageIndex, PageSize, Search, SettingsHelper.sharedInstance.KeySecret))"
        
        let strURL = URLConstant.List_GetListUserBuySeatInShow(SettingsHelper.getUserID(), scheduleId: scheduleId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: ShowItemModel = ShowItemModel(jsondata: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func List_GetListShowItemPrice(pageIndex: Int, pageSize: Int , minPrice: Int, callback: ((result: FListResultDTO<ShowItemPriceDTO>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<ShowItemPriceDTO>()
        
        let strURL = URLConstant.List_GetListShowItemPrice(SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, minPrice: minPrice, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: ShowItemPriceDTO = ShowItemPriceDTO(jsondata: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_BuySeat(seatIndex: Int, scheduleId: Int64, price: Int, isEvent: Bool, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_BuySeat();
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "index": "\(seatIndex)",
                     "scheduleId": "\(scheduleId)",
                     "coin": "\(price)",
                     "isInEvent": isEvent ? "true" : "false",
                     "platform" : "2",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
        
    }
    
    class func User_GetStreamInfo(scheduleId : Int64 , callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_StreamInfo(scheduleId);
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: EndShowModel.self)
            callback(result: itemresult)
        }
    }
    
    class func User_InsertDeviceToken(deviceToken: String)
    {
        //var itemresult = DoResultDTO()
        let strURL = URLConstant.User_InsertDeviceToken();
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafeMutablePointer(&systemInfo.machine) {
            ptr in String.fromCString(UnsafePointer<CChar>(ptr))
        }
        
        let model = String.fromCString(modelCode!)
        let currentDevice = UIDevice.currentDevice()
        let deviceInfo = String(stringInterpolation: "model: ", model!,", os version: ",currentDevice.systemName," ",currentDevice.systemVersion)
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "token": deviceToken,
                     "platformId": "\(2)",
                     "deviceInfo": "\(deviceInfo)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                return;
            }
        }
    }
    
    class func User_LogoutTeenIdol()
    {
        //var itemresult = DoResultDTO()
        let strURL = URLConstant.User_LogoutTeenIdol();
        
        let param = ["deviceToken": SettingsHelper.getDeviceToken(),
                     "userId": "\(SettingsHelper.getUserID())",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            return;
        }
    }
    
    class func User_KickUser(targetUserId : Int64, scheduleId : Int64 , callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_KickUser(SettingsHelper.getUserID(), targetUserId: targetUserId, scheduleId: scheduleId, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_DisableChat(targetUserId : Int64, scheduleId : Int64 , isCanChat: Bool, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_DisablePublicChat(SettingsHelper.getUserID(), targetUserId: targetUserId, scheduleId: scheduleId, isCanChat: isCanChat ? "true" : "false", key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_GetUserDaySignInReward(callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_GetUserDaySignInReward(SettingsHelper.getUserID(), key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: User_DaySignInRewardDTO.self)
            callback(result: itemresult)
        }
    
    }
    
    class func User_DoDaySignIn(callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_DoDaySignIn()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: User_DaySignInRewardDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_GetListDaySignIn(pageIndex: Int, pageSize: Int, callback: ((result: FListResultDTO<DaySignInRewardModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<DaySignInRewardModel>()
        
        let strURL = URLConstant.User_GetListDaySignIn(SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].array)
                    for subdic in items! {
                        let item: DaySignInRewardModel = DaySignInRewardModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_GetListAnimationItem(userId: Int64, pageIndex: Int, pageSize: Int, callback: ((result: FListResultDTO<AnimationInventoryModel>)-> Void))
    {
        //let _isNewVersion = isNewVersion ? "true" : "false"
        
        let itemresult: FListResultDTO = FListResultDTO<AnimationInventoryModel>()
        let strURL = URLConstant.User_GetListAnimationItem(SettingsHelper.getUserID(), userId: userId, pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic =  JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: AnimationInventoryModel = AnimationInventoryModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    class func User_ChoseAnimationDefaultItem(animationItemId: Int, rootCategoryId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_ChoseAnimationDefaultItem(SettingsHelper.getUserID(), animationItemId: animationItemId, rootCategoryId: rootCategoryId, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: AnimationInventoryDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_GiveFreeCoin(scheduleId: Int64, starId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_GiveFreeCoin()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId" : "\(scheduleId)",
                     "starId" : "\(starId)",
                     "freeCoin" : "10",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: DaySignInRewardDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func User_BanFacebookLiveChat(targetUserId : Int64, scheduleId : Int64 , userName: String, isCanChat: Bool, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_BanFacebookLiveChat()
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "scheduleId" : "\(scheduleId)",
                     "userId" : "\(targetUserId)",
                     "userName" : userName,
                     "value" : isCanChat ? "true" : "false",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_IsUserCanFacebookChat(targetUserId : Int64, scheduleId : Int64 ,callback: ((result: Bool) -> Void))
    {
        var itemresult = false
        let strURL = URLConstant.User_IsUserCanFacebookChat(targetUserId, scheduleId: scheduleId)
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = dic["Result"].boolValue
            callback(result: itemresult)
        }
    }
    
    class func User_GetListTopUserUseCoinInShow(scheduleId: Int64, showId: Int64, starId: Int64, startDate: Int64, endDate: Int64, pageIndex: Int, pageSize: Int, callback:((result: FListResultDTO<GiveAwayModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<GiveAwayModel>()
        let strURL = URLConstant.User_GetListTopUserUseCoinInShow(actionUserId: SettingsHelper.getUserID(), scheduleId: scheduleId, showId: showId, starId: starId, pageIndex: pageIndex, pageSize: pageSize, startDate: startDate, endDate: endDate, key: SettingsHelper.getUserTokenKey())
        
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: GiveAwayModel = GiveAwayModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
    }
    
    // totalCoinInShow | totalCoinInSchedule
    class func User_GetTotalCoinUsed(scheduleId: Int64, showId: Int64, callback: ((result: (InShow: Int, InSchedule: Int)) -> Void))
    {
        var itemresult = (0,0);
        let strURL = URLConstant.User_GetTotalCoinUsed(actionUserId: SettingsHelper.getUserID(), scheduleId: scheduleId, showId: showId, key: SettingsHelper.getUserTokenKey())
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = (dic["Result"]["TotalCoinInShow"].intValue,dic["Result"]["TotalCoinInSchedule"].intValue)
            callback(result: itemresult)
        }
    }
    
    class func User_GetListGuild(PageIndex pageIndex: Int, pageSize: Int, callback: ((result: FListResultDTO<GuildModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<GuildModel>()
        //let strURL = URLConstant.User_GetListGuild(creatorId: SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        
        let strURL = URLConstant.User_GetListGuild_Post()
        
        let param = ["creatorId": "\(SettingsHelper.getUserID())",
                     "pageIndex" : "\(pageIndex)",
                     "pageSize" : "\(pageSize)",
                     "key": "\(SettingsHelper.getUserTokenKey())"];

        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].arrayValue)
                for subdic in items {
                    let item: GuildModel = GuildModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
        }

        /*
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: GuildModel = GuildModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
        */
        
        
    }
    
    class func User_GetGuildInfo(guildId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        
        let strURL = URLConstant.User_GetGuildInfo_Post()
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "guildId" : "\(guildId)",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: GuildModel.self)
            callback(result: itemresult)
        }

        
        //let strURL = URLConstant.User_GetGuildInfo(actionUserId: SettingsHelper.getUserID(), guildId: guildId, key: SettingsHelper.getUserTokenKey())
        
        /*
        Alamofire.request(.GET, strURL)
            .responseJSON { response in
                if(response.result.value == nil){
                    callback(result: itemresult)
                    return;
                }
                let dic = JSON(response.result.value!)
                itemresult.CountAllResult = dic["Total"].intValue
                if(itemresult.CountAllResult > 0){
                    let items = (dic["Items"].arrayValue)
                    for subdic in items {
                        let item: GuildModel = GuildModel(dic: subdic)
                        itemresult.ListItem.append(item)
                    }
                }
                callback(result: itemresult)
        }
        */
    }
    
    /// isIdol = true: get members | = false: get idols
    class func User_GetListUserInGuild(PageIndex pageIndex: Int, pageSize: Int, guildId: Int64, isIdol: Bool, nameUser: String, callback: ((result: FListResultDTO<GuildMemberModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<GuildMemberModel>()
        //let strURL = URLConstant.User_GetListGuild(creatorId: SettingsHelper.getUserID(), pageIndex: pageIndex, pageSize: pageSize, key: SettingsHelper.getUserTokenKey())
        
        let strURL = URLConstant.User_GetListUserInGuild_Post()
        
        let param = ["creatorId": "\(SettingsHelper.getUserID())",
                     "pageIndex" : "\(pageIndex)",
                     "pageSize" : "\(pageSize)",
                     "guildId": "\(guildId)",
                     "isIdol": "\(isIdol)",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].arrayValue)
                for subdic in items {
                    let item: GuildMemberModel = GuildMemberModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
        }
    }
    
    class func User_BuyItemsForGuild(itemId: Int, purchaseId: Int, isVipItem: Bool, guildId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_BuyItemsForGuild()
        let _isVip = isVipItem ? "true":"false"
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "itemId": "\(itemId)",
                     "purchaseId": "\(purchaseId)",
                     "isVipItem" : _isVip,
                     "guildId": "\(guildId)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_GetListUserJoinGuild(guildId guildId: Int64, pageIndex: Int, pageSize: Int, nameUser: String, callback: ((result: FListResultDTO<GuildMemberModel>) -> Void))
    {
        let itemresult: FListResultDTO = FListResultDTO<GuildMemberModel>()
        
        let strURL = URLConstant.User_GetListUserJoinGuild()
        
        let param = ["creatorId": "\(SettingsHelper.getUserID())",
                     "guildId": "\(guildId)",
                     "pageIndex" : "\(pageIndex)",
                     "pageSize" : "\(pageSize)",
                     "nameUser": "\(nameUser)",
                     "key": "\(SettingsHelper.getUserTokenKey())"];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult.CountAllResult = dic["Total"].intValue
            if(itemresult.CountAllResult > 0){
                let items = (dic["Items"].arrayValue)
                for subdic in items {
                    let item: GuildMemberModel = GuildMemberModel(dic: subdic)
                    itemresult.ListItem.append(item)
                }
            }
            callback(result: itemresult)
        }
    }
    
    class func User_SendRequestJoinGuild(guildId: Int64, userId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_SendRequestJoinGuild()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "guildId": "\(guildId)",
                     "userId": "\(userId)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }

    }
    
    class func User_DeclineUserJoinGuild(guildId: Int64, userId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_DeclineUserJoinGuild()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "guildId": "\(guildId)",
                     "userId": "\(userId)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }

    }
    
    class func User_AcceptUserJoinGuild(guildId: Int64, userId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_AcceptUserJoinGuild()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "guildId": "\(guildId)",
                     "userId": "\(userId)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }

    }
    
    class func User_KickOutUser(guildId: Int64, userId: Int64, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_KickOutUser()
        
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "guildId": "\(guildId)",
                     "userId": "\(userId)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_ReportIdol(Base64photo base64Photo: String, starId: Int64, note: String, platform: Int, connectionType: String, callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_ReportIdol()
        
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafeMutablePointer(&systemInfo.machine) {
            ptr in String.fromCString(UnsafePointer<CChar>(ptr))
        }
        var model = "Unknown"
        if let _model = String.fromCString(modelCode!) {
            model = _model
        }
        let currentDevice = UIDevice.currentDevice()
        var versionStr = ""
        if let version = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] as? String{
            versionStr = version
        }
        //OS | APP | ConnectionType  | Model
        let deviceInfo = "\(currentDevice.systemName) \(currentDevice.systemVersion) | \(versionStr) | \(connectionType) | \(model)"
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "base64Photo": "\(base64Photo)",
                     "starId": "\(starId)",
                     "note": "\(note)",
                     "platform":"\(platform)",
                     "device" : "\(deviceInfo)",
                     "key": SettingsHelper.getUserTokenKey()];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func User_ShowShareFB(fbToken: String, base64Photo: String, callback: ((result: DoResultDTO) ->Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.User_ReportIdol()
    
        let param = ["actionUserId": "\(SettingsHelper.getUserID())",
                     "fbToken": "\(fbToken)",
                     "base64Photo": "\(base64Photo)",
                     "key": SettingsHelper.getUserTokenKey()];
    
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic)
            callback(result: itemresult)
        }
    }
    
    class func Stream_GetStreamInfo(streamKey: String,  callback: ((result: DoResultDTO) -> Void))
    {
        var itemresult = DoResultDTO()
        let strURL = URLConstant.Stream_GetStreamInfo(streamKey)
        
        Alamofire.request(.GET, strURL).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult = DoResultDTO(data: dic, className: StreamInfoDTO.self)
            callback(result: itemresult)
        }
    }
    
    class func Star_LoginTeenIdol(email: String, password: String, ipAddress: String, token: String, callback: ((result: DoResultDTO) -> Void)){
        var itemresult = DoResultDTO()
        let strURL = URLConstant.Star_LoginTeenIdol()
        
        let param = ["email": email,
                     "password": password,
                     "ipAddress": ipAddress,
                     "userToken": token];
        
        Alamofire.request(.POST, strURL,parameters: param,encoding: .JSON).responseJSON { (response) in
            if(response.result.value == nil){
                callback(result: itemresult)
                return;
            }
            let dic = JSON(response.result.value!)
            itemresult =  DoResultDTO(data: dic, className: UserSessionDTO.self )
            callback(result: itemresult)
        }
    }
}
