//
//  DaySignInRewardModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/23/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class DaySignInRewardModel: NSObject {

    var user_daySignInReward: User_DaySignInRewardDTO?
    var daySignInReward: DaySignInRewardDTO?
    var nameRewardObjectTypeName: String = ""
    var nameRewardObjectName: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON) {
        self.init()
        self.user_daySignInReward = User_DaySignInRewardDTO(jsondata: dic["User_DaySignInReward"])
        self.daySignInReward = DaySignInRewardDTO(jsondata: dic["DaySignInReward"])
        self.nameRewardObjectName = dic["nameRewardObjectName"].stringValue;
        self.nameRewardObjectTypeName = dic["nameRewardObjectTypeName"].stringValue
    }
    
}
