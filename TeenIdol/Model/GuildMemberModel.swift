//
//  GuildMemberModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GuildMemberModel: NSObject {

    var groupLevel: GroupLevelDTO?
    var guildPosition: GuildPostionDTO?
    var userInfo: UserDTO?
    var showStar: ShowDTO?
    var userGuild: UserGuildDTO?
    var userLevel: GroupLevelDTO?
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        
        self.groupLevel = GroupLevelDTO(jsondata: dic["GroupLevel"])
        self.guildPosition = GuildPostionDTO(jsondata: dic["GuildPosition"])
        self.userInfo = UserDTO(jsondata: dic["User"])
        self.showStar = ShowDTO(jsondata: dic["ShowStar"])
        self.userGuild = UserGuildDTO(jsondata: dic["User_Guild"])
        self.userLevel = GroupLevelDTO(jsondata: dic["GroupLevel"])
    }
    
}
