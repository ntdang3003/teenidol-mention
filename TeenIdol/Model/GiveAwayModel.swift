//
//  GiveAwayModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiveAwayModel: NSBlockOperation {

    var giveAway: GiveAwayDTO?
    var senderUser: UserDTO?
    var senderUserLevel: GroupLevelDTO?
    var receiverUser: UserDTO?
    var gift: GiftDTO?
    var schedule: ScheduleDTO?
    var giftAnimation: GiftAnimationDTO?
    
    var totalPrice: Int = 0
    var totalQuantity: Int = 0
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON) {
        self.init()
        
        self.giveAway = GiveAwayDTO(jsondata: dic["GiveAway"])
        self.senderUser = UserDTO(jsondata: dic["SenderUser"])
        self.senderUserLevel = GroupLevelDTO(jsondata: dic["SenderUserLevel"])
        self.receiverUser = UserDTO(jsondata: dic["ReceiverUser"])
        self.gift = GiftDTO(jsondata: dic["Gift"])
        self.schedule = ScheduleDTO(jsondata: dic["Schedule"])
        self.giftAnimation = GiftAnimationDTO(jsondata: dic["GiftAnimation"])
        self.totalPrice = dic["TotalPrice"].intValue
        self.totalQuantity = dic["TotalQuantity"].intValue
    }
}
