//
//  EventModel.swift
//  LiveIdol
//
//  Created by Đăng Nguyễn on 1/6/17.
//  Copyright © 2017 TFL. All rights reserved.
//

import Foundation

class IdolGranaryInfoDTO : NSObject {
    var total = 0
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        total = jsondata["total"].intValue
    }
}

class UserGranaryDTO : NSObject  {
    var id : Int64 = 0
    var userId : Int64 = 0
    var unitTypeId : Int64 = 0
    var lixiValue  = 0
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        id = jsondata["Id"].int64Value
        userId = jsondata["UserId"].int64Value
        unitTypeId = jsondata["UnitTypeId"].int64Value
        lixiValue = jsondata["Value"].intValue
    }
}

class LixiRequireModel : NSObject {
    var name = ""
    var requiredId  : Int64 = 1
    var listRequire = [LixiRequireDataDTO]()
    var photo = ""
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        name = jsondata["Name"].stringValue
        requiredId = jsondata["RequireId"].int64Value
        photo = jsondata["Photo"].stringValue
        //let arrRequire = jsondata["listRequire"].arrayValue
        listRequire.appendContentsOf(jsondata["listRequire"].arrayValue.map({ LixiRequireDataDTO(jsondata:$0)}))
    }
}

class LixiRequireDataDTO : NSObject {
    var id : Int64 = 0
    var requireId : Int64 = 0
    var lixiValue  = 0
    var unitTypeId : Int64  = 0
    var name = ""
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        
        let rewardInfo = jsondata["RequireData"]
        let unitType = jsondata["UnitType"]
        
        id = rewardInfo["Id"].int64Value
        requireId = rewardInfo["RequireId"].int64Value
        lixiValue = rewardInfo["Value"].intValue
        unitTypeId = rewardInfo["UnitTypeId"].int64Value
        name = unitType["Name"].stringValue
    }
}

class GranaryRewardModel : NSObject {
    var message = ""
    var listReward : [(nameReward: String, unit: String)] = []
    var userInfo : UserDTO!
    var name = ""
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        message = jsondata["sms"].stringValue
        name = jsondata["dataGift"]["name"].stringValue
        userInfo  = UserDTO(jsondata: jsondata["User"])
        let arrJson = jsondata["reward"].arrayValue
        for j in arrJson {
            let r = (nameReward: j["name"].stringValue, unit: j["value"].stringValue)
            listReward.append(r)
        }
    }
}

class NguyenLieuModel: NSObject {
    var userGranary : UserGranaryDTO!
    var name = ""
    var photo = ""
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil){return}
        userGranary = UserGranaryDTO(jsondata: jsondata["UserGranary"])
        let unitType = jsondata["UnitType"]
        name = unitType["Name"].stringValue
        photo =  unitType["UserGetPhoto"].stringValue
    }
}
