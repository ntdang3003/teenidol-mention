//
//  ShowModel.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit
 

class ShowModel: NSObject {
    var ShowInfo: ShowDTO? = nil
    var ShowScheduleStatus = ""
    var ListShowItem: [ShowItem] = []
    var UserInfo : UserDTO?
    var Schedule : ScheduleDTO?
    var Status : ScheduleStatus?
    var TotalStar: Int = 0;
    var samplePercent : Float = 0
    var sampleStar  = false
    override init() {
        super.init();
    }
    convenience init(dic: JSON){
        self.init()
        self.ShowInfo = ShowDTO(jsondata: dic["Show"])
        self.ShowScheduleStatus = dic["ShowScheduleStatus"].stringValue
        self.UserInfo = UserDTO(jsondata: dic["User"])
        self.Schedule = ScheduleDTO(jsondata: dic["Schedule"])
        self.Status = self.Schedule?.Status
        self.TotalStar = dic["TotalStar"].intValue
        self.samplePercent = dic["SamplePercent"].floatValue
        self.sampleStar = dic["SampleStar"].boolValue
        let lstdic = dic["ListShow"].arrayValue
        if lstdic.count > 0 {
            for subdic in lstdic {
                let item = ShowItem(json: subdic)
                self.ListShowItem.append(item)
            }
        }
    }
    
    func increaseUserOnline(count: Int){
         self.ShowInfo?.OnlineUser +=  1
    }
    
    func decreaseUserOnline(count: Int){
        if(self.ShowInfo?.OnlineUser > 0){
             self.ShowInfo?.OnlineUser -=  1
        }
    }
}

class ShowItem: NSObject{
    var StarUser: UserDTO!
    var UserStarData : StarDataDTO? = nil
    var FollowInfo: FollowDTO!
    
    required init(json: JSON) {
        super.init()
        self.StarUser = UserDTO(jsondata: json["StarUser"])
        self.UserStarData = StarDataDTO(jsondata: json["StarData"])
        self.FollowInfo = FollowDTO(jsondata: json["FollowInfo"])
    }
}
