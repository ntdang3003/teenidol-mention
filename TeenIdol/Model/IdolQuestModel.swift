//
//  IdolQuestModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

enum QuestState: Int
{
    case Cancel = 0;
    case Incomplete = 1;
    case CanGetReward = 2;
    case Rewarded = 3
}

class IdolQuestModel: NSObject {

    var questInfo: IdolQuestMissionDTO? = nil
    var giftInfo: [IdolQuestMissionGift]? = [];
    var userLastHitInfo: UserLastHitInfo? = nil
    
    var listIdolReward: [IdolQuestMissionGift] = []
    var listUserReward: [IdolQuestMissionGift] = []
    
    var idolQuestState:Int? = 0 ;
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        
        questInfo = IdolQuestMissionDTO(jsondata: dic["questInfo"])
        userLastHitInfo = UserLastHitInfo(jsondata: dic["userInfo"])
        let model = dic["GiftInfo"].arrayValue;
        for i in model
        {
            let gift = IdolQuestMissionGift(jsondata: i)
            giftInfo?.append(gift)
        }
        
        for i in dic["IdolRewardInfo"].arrayValue
        {
            let reward = IdolQuestMissionGift(jsondata: i);
            listIdolReward.append(reward)
        }
        
        for i in dic["UserRewardInfo"].arrayValue
        {
            let reward = IdolQuestMissionGift(jsondata: i);
            listUserReward.append(reward)
        }
        
        idolQuestState = dic["idolQuestState"].intValue;
    }
    
}
