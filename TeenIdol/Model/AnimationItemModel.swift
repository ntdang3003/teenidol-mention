//
//  AnimationItemModel.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/29/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
class AnimationItemModel: NSObject {
    var animationItem : AnimationItemDTO? = nil
    var animationInvetory: AnimationInventoryDTO? = nil
    var itemCategory: ItemCategoryDTO? = nil
    var listItemPurchases: [ItemPurchaseDTO] = []
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON) {
        self.init()
        
        animationItem = AnimationItemDTO(jsondata: dic["AnimationItem"])
        animationInvetory = AnimationInventoryDTO(jsondata: dic["AnimationInventory"]);
        itemCategory = ItemCategoryDTO(jsondata: dic["ItemCategory"]);
        
        for i in dic["ListItemPurchase"].arrayValue
        {
            listItemPurchases.append(ItemPurchaseDTO(jsondata: i))
        }
    }
    
    convenience init(inventoryModel: AnimationInventoryModel) {
        self.init()
        self.animationItem = inventoryModel.animationItem
        self.animationInvetory = inventoryModel.animationInventory
        self.itemCategory = inventoryModel.itemCategory
    }
}

class AnimationInventoryModel : NSObject{
    var animationItem : AnimationItemDTO? = nil
    var animationInventory : AnimationInventoryDTO? = nil
    var userInfo : UserDTO? = nil
    var itemPurchase : ItemPurchaseDTO? = nil
    var itemCategory : ItemCategoryDTO? = nil
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON) {
        self.init()
        
        animationItem = AnimationItemDTO(jsondata: dic["AnimationItem"])
        animationInventory = AnimationInventoryDTO(jsondata: dic["AnimationInventory"]);
        userInfo = UserDTO(jsondata: dic["User"]);
        itemPurchase = ItemPurchaseDTO(jsondata: dic["ItemPurchase"])
        itemCategory = ItemCategoryDTO(jsondata: dic["RootCategory"])
    }
}
