//
//  IdolGiftShowModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class LimitDTO: NSObject
{
    var Id:Int = 0;
    var requireGiftAmound: Int = 0;
    var rewardDolar: Int = 0;
    
    override init() {
        super.init();
    }

    convenience init(jsonData: JSON)
    {
        self.init()
        self.Id = jsonData["Id"].intValue
        self.requireGiftAmound = jsonData["RequireGiftAmount"].intValue
        self.rewardDolar = jsonData["RewardDolar"].intValue
    }
}

class IdolGiftShowModel: NSObject {
    
    var rank: Int = 0
    var quantityGift: Int = 0
    var UserInfo: UserDTO?
    var StarData: StarDataDTO?
    var Gift: GiftDTO?
    
    var currentLimit: LimitDTO?
    var nextLimit: LimitDTO?
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        self.rank = dic["Rank"].intValue
        self.quantityGift = dic["QuanlytiGift"].intValue
        self.UserInfo = UserDTO(jsondata: dic["User"]);
        self.StarData = StarDataDTO(jsondata: dic["UserStarData"])
        self.Gift = GiftDTO(jsondata: dic["Gift"])
        self.currentLimit = LimitDTO(jsonData: dic["CurrentLimit"])
        self.nextLimit = LimitDTO(jsonData: dic["NextLimit"])
    }
}
