//
//  FListResultDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

class FListResultDTO<T>: NSObject {
    var iPageIndex = 0
    var isLoadingData = false
    var ListItem:  Array<T> = [];
    var CountAllResult = 0
    override init() {
        super.init();
        
    }
}
