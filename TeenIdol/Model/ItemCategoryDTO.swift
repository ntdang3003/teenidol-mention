//
//  ItemCategoryDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class ItemCategoryDTO: NSObject {

    var Id: Int64 = 0;
    var Name = "";
    var Description = "";
    var Status = 0;
   var Photo = "";
    var parentId: Int64 = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        
        Id = jsondata["Id"].int64Value;
        Name = jsondata["Name"].stringValue
        Description = jsondata["Description"].stringValue
        Status = jsondata["Status"].intValue;
        Photo = jsondata["Photo"].stringValue;
        parentId = jsondata["parentId"].int64Value;
    }
}
