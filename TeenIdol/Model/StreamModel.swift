//
//  StreamModel.swift
//  LiveIdol
//
//  Created by Chip on 5/19/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class BeginShowModel: NSObject {
    var showId: Int64 = -1
    var scheduleId: Int64 = -1
    var showName = ""
    var streamServer = ""
    var streamKey = ""
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.showId = jsondata["ShowId"].int64Value
        self.scheduleId = jsondata["ScheduleId"].int64Value
        self.showName = jsondata["ShowName"].stringValue
        self.streamServer = jsondata["StreamServer"].stringValue
        self.streamKey = jsondata["StreamKey"].stringValue
    }
}

class EndShowModel: NSObject {
    var name = ""
    var duration = 0
    var viewer : Int64 = -1
    var totalRuby = 0
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.name = jsondata["Name"].stringValue
        self.duration = jsondata["Duration"].intValue
        self.viewer = jsondata["Viewer"].int64Value
        self.totalRuby = jsondata["RubyGet"].intValue
    }
}
