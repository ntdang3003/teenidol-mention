//
//  GiftCategoryModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftCategoryModel: NSObject {

    var giftCategoryDTO: GiftCategoryDTO? = nil;
    var listGift = FListResultDTO<GiftDTO>()
    
    override init() {
        super.init();

    }
    
    convenience init(dic: JSON) {
        self.init()
        giftCategoryDTO = GiftCategoryDTO(jsondata: dic["GiftCategory"])
        
    }
}
