//
//  ShowItemModel.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 6/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class ShowItemModel: NSObject {
    var userShowItem : UserShowItemDTO?
    var userInfo : UserDTO?
    var showItem : ShowItemDTO?
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        userShowItem = UserShowItemDTO(jsondata: jsondata["UserShowItem"])
        userInfo = UserDTO(jsondata: jsondata["UserInfo"])
        showItem = ShowItemDTO(jsondata: jsondata["ShowItem"]);
    }
    
    convenience init(signalRData: SignalRUserBuySeatDTO) {
        self.init()
        self.userInfo = UserDTO()
        self.userInfo?.AvatarPhoto = (signalRData.userInfo?.avatar)!
        self.userInfo?.Name = (signalRData.userInfo?.name)!
        
        self.userShowItem = UserShowItemDTO()
        self.userShowItem?.currentPhoto = (signalRData.seatInfo?.photoGif)!
        self.userShowItem?.currentPrice = (signalRData.seatInfo?.price)!
        self.userShowItem?.index = (signalRData.seatInfo?.index)!
    }
}

class UserShowItemDTO : NSObject{
    var Id:Int = 0;
    var scheduleId:Int64 = 0;
    var userId:Int64 = 0 ;
    var showItemId = 0;
    var currentPrice = 0;
    var info = "";
    var createDate = 0;
    var eventCoin = "";
    var status = 0;
    var currentPhoto = "";
    var index = 0
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        Id = jsondata["Id"].intValue;
        scheduleId = jsondata["ScheduleId"].int64Value;
        userId = jsondata["UserId"].int64Value;
        showItemId = jsondata["ShowItemId"].intValue
        currentPrice = jsondata["CurrentPrice"].intValue
        info = jsondata["Info"].stringValue;
        createDate = jsondata["CreateDate"].intValue;
        eventCoin = jsondata["EventCoin"].stringValue
        status = jsondata["Status"].intValue;
        currentPhoto = jsondata["CurrentPhoto"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
    }

}

class ShowItemDTO : NSObject{
    var Id:Int = 0;
    var name = "";
    var basicPrice:Int = 0 ;
    var photoLink = "";
    var isEnableOverTaken = true;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        Id = jsondata["Id"].intValue;
        name = jsondata["Name"].stringValue;
        basicPrice = jsondata["BasicPrice"].intValue;
        photoLink = jsondata["PhotoLink"].stringValue
        isEnableOverTaken = jsondata["IsEnableOverTaken"].boolValue
    }
}

class ShowItemPriceDTO : NSObject{
    var Id:Int = 0;
    var price = 0;
    var eventCoin:Int = 0 ;
    var photoLink = "";
    var photoMobileLink = "";
    var animationLink = "";
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        let showItemPriceData = jsondata["ShowItemPrice"]
        if(showItemPriceData != nil){
            Id = showItemPriceData["Id"].intValue;
            price = showItemPriceData["Price"].intValue;
            eventCoin = showItemPriceData["EventCoin"].intValue;
            photoLink = showItemPriceData["Photo"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
            //photoMobileLink = jsondata["PhotoMobile"].boolValue
            //animationLink = jsondata["Animation"].boolValue
        }
    }
}
