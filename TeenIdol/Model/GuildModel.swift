//
//  GuildModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GuildModel: NSObject {

    var guildInfo : GuildDTO?
    var currentLevel: GuildLevelDTO?
    var nextLevel: GuildLevelDTO?
    var owner: UserDTO?
    var Rank = 0
    var TotalCoin = 0
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        
        self.Rank = dic["Rank"].intValue
        self.TotalCoin = dic["TotalCoin"].intValue
        self.guildInfo = GuildDTO(jsondata: dic["Guild"])
        self.currentLevel = GuildLevelDTO(jsondata: dic["GuildLevel"])
        self.nextLevel = GuildLevelDTO(jsondata: dic["NextLevel"])
        self.owner = UserDTO(jsondata: dic["Owner"])
    }
    
}
