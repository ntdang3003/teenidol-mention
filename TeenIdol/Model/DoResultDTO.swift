//
//  DoResultDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit

class DoResultDTO: NSObject {
    var ErrorCode: Int = -1
    var Message: String = ""
    var DataObject : AnyObject? = nil
    var Config : Int = 0
    override init() {
        super.init();
    }
    
    convenience init(data: JSON) {
        self.init();
        ErrorCode = data["ErrorCode"].intValue;
        Message = data["Message"].stringValue;
        Config  = data["config"].intValue;
        
    }
    convenience init(data : JSON,className : AnyClass) {
        self.init(data: data);
        
        if(className.self == ShowModel.self)
        {
            DataObject = ShowModel(dic: data["Result"])
        }
        else if(className.self == UserModel.self){
            DataObject = UserModel(dic: data["Result"])
        }
        else if (className.self == AnimationItemModel.self){
            DataObject = AnimationItemModel(dic: data["Result"])
        }
        else if(className.self == UserSessionDTO.self)
        {
            DataObject = UserSessionDTO(jsondata: data["Result"])
        }
        else if(className.self == BeginShowModel.self)
        {
            DataObject = BeginShowModel(jsondata: data["Result"])
        }
        else if(className.self == EndShowModel.self)
        {
            DataObject = EndShowModel(jsondata: data["Result"])
        }
        else if(className.self == User_DaySignInRewardDTO.self)
        {
            DataObject = User_DaySignInRewardDTO(jsondata: data["Result"])
        }
        else if(className.self == DaySignInRewardDTO.self)
        {
            DataObject = User_DaySignInRewardDTO(jsondata: data["Result"])
        }else if (className.self == AnimationItemDTO.self)
        {
            DataObject = AnimationItemDTO(jsondata: data["Result"])
        }
        else if(className.self == IdolQuestStateDTO.self)
        {
            DataObject = IdolQuestStateDTO(jsondata: data["Result"])
        }
        else if(className.self == IdolQuestModel.self)
        {
            DataObject = IdolQuestModel(dic: data["Result"])
        }
        else if(className.self == IdolGiftShowModel.self)
        {
            DataObject = IdolGiftShowModel(dic: data["Result"])
        }
        else if(className.self == GuildModel.self)
        {
            DataObject = GuildModel(dic: data["Result"])
        }
        else if(className.self == StreamInfoDTO.self)
        {
            DataObject = StreamInfoDTO(jsondata: data["Result"])
        }
        else if(className.self == UserGranaryDTO.self)
        {
            DataObject = UserGranaryDTO(jsondata: data["Result"])
        }
        else if(className.self == GranaryRewardModel.self)
        {
            DataObject = GranaryRewardModel(jsondata: data["Result"]);
        }
        else if (className.self == IdolGranaryInfoDTO.self){
            DataObject = IdolGranaryInfoDTO(jsondata: data["Result"]);
        }
    }
}
