//
//  ItemCategoryModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class ItemCategoryModel: NSObject {

    var category = ItemCategoryDTO()
    var Parent = ItemCategoryDTO()
    var listAnimationItem = FListResultDTO<AnimationItemModel>()
    override init() {
        super.init();
    }
    convenience init(dic: JSON){
        self.init()
        self.category = ItemCategoryDTO(jsondata: dic["ItemCategory"])
        self.Parent = ItemCategoryDTO(jsondata: dic["parent"])
    }
    
}
