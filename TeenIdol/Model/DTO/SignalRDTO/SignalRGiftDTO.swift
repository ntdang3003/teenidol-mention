//
//  SignalRGiftDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class SignalRGiftDTO: NSObject {
    var id: Int64 = -1
    var date: Int64 = 0
    var name: String = ""
    var quantity: Int = 0
    var photo: String = ""
    var animation: String = ""
    var photoGif: String = ""
    var type = 0
    var animationTime = 0
    var animationWidth = 0
    var animationHeight = 0
    var animationGifTime : Double = 0
    
    override init() {
        super.init()
    }
    
    convenience init (jsondata: JSON){
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.id = jsondata["id"].int64Value
        self.name = jsondata["name"].stringValue
        self.date = jsondata["date"].int64Value
        self.quantity = jsondata["quantity"].intValue
        self.photo = jsondata["photo"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        self.animation = jsondata["animation"].stringValue
        self.photoGif = jsondata["photoGif"].stringValue
        self.type = jsondata["type"].intValue
        self.animationTime = jsondata["animationTime"].intValue
        self.animationWidth = jsondata["animationWidth"].intValue
        self.animationHeight = jsondata["animationHeight"].intValue
        self.animationGifTime = jsondata["animationGifTime"].doubleValue
        self.photoGif = self.photoGif.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
    }
}

class SignalRSeatDTO : NSObject{
    var index = 0
    var price = 0
    var photoGif = ""
    var animationPhoto = ""
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        if(jsondata == nil){
            return
        }
        self.index = jsondata["index"].intValue
        self.price = jsondata["price"].intValue
        self.photoGif = jsondata["photoGif"].stringValue
        self.animationPhoto = jsondata["animationPhoto"].stringValue
    }
}

class SignalRUserGetLixiDTO : SignalRUserDoActionDTO {
    var sms = ""
    var giftInfo : SignalRGiftDTO!
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.sms = jsondata["sms"].stringValue
        self.giftInfo = SignalRGiftDTO(jsondata: jsondata["dataGift"])
        self.actionTemplate = self.sms
        self.generateActionText()
    }
    
    override func generateActionText() {
        self.actionAttributeText = NSMutableAttributedString(string: self.actionTemplate, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(14), NSForegroundColorAttributeName : UIColor.blueColor()])
    }
}

