//
//  SignalRUserDoActionDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 4/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
let imageTemplate : String = "{ImageTemplate}"
let userNameTemplate : String = "{UserNameTemplate}"
class SignalRUserDoActionDTO: NSObject {
    var userInfo: SignalRUserDTO? = nil
    var actionTemplate : String = ""
    var userNameColor: UIColor? = nil
    var actionAttributeText : NSMutableAttributedString?  = nil
    var actionTextHeight : CGFloat = 0.0
    var arrMatches : NSArray? = nil
    var code = SignalRAlertCode.KickUser
    
    func generateActionText() {
        userNameColor =  UIColor(red: 51, green: 122, blue: 183)
        actionAttributeText = NSMutableAttributedString(string: actionTemplate, attributes: [NSFontAttributeName: FontDefautLarge])
        arrMatches = SignalRHelper.getMatchesTemplate(actionAttributeText!)
        
        if(self.userInfo?.vipColor.isEmpty == false){
            userNameColor = SignalRHelper.getHexColor(userInfo!.vipColor,alpha: 1)
        }
        
        var strChange : NSAttributedString?  = nil
        
        for match in arrMatches! {
            let newRange = NSMakeRange(match.range.location, match.range.length)
            let str : NSString = (actionAttributeText?.string)!
            let subString = str.substringWithRange(newRange)
            if(imageTemplate == subString){
                strChange = SignalRHelper.getVipImageAttributeString(userInfo!.guildPhoto.isEmpty == false ? userInfo!.guildPhoto : userInfo!.vipPhotoMobile)
            }else if (userNameTemplate == subString){
                let att = [NSForegroundColorAttributeName : self.userNameColor!,
                           NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
                strChange = NSAttributedString(string: userInfo!.name, attributes: att)
                
            }
            if(strChange == nil) {continue}
            actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
        }
    }
    
    func getUserDoActionTemplate() -> String{
        if(self.userInfo?.vipPhotoMobile.isEmpty == true
            && self.userInfo?.guildPhoto.isEmpty == true){
            return userNameTemplate
        }
        return "\(imageTemplate) \(userNameTemplate)"
    }
    
    func calculateActionTextHeight(width:CGFloat) -> CGFloat{
        actionTextHeight = FunctionHelper.calculateTextHeightWithWidth(width, text: self.actionAttributeText!, font: FontDefautLarge).height
        return actionTextHeight
    }
    
    func releaseNoUse(){
        self.actionTemplate = ""
        self.userNameColor = nil
        self.arrMatches = nil
    }
}

class SignalRUserDoActionToTargetDTO : SignalRUserDoActionDTO{
    var targetUser : SignalRUserDTO? = nil
    var targetUserColor = UIColor(red: 51, green: 122, blue: 183)
    
    override init() {
        super.init()
    }
    
    init(jsondata: JSON, code: SignalRAlertCode) {
        super.init()
        self.code = code
        self.userInfo = SignalRUserDTO(jsondata: jsondata["user"])
        self.targetUser = SignalRUserDTO(jsondata: jsondata["target"])

        switch self.code {
        case  .DisablePublicChat:
            self.actionTemplate = "\(super.getUserDoActionTemplate()) đã chặn chat {TargetUserName}"
        case  .EnablePublicChat:
           self.actionTemplate = "\(super.getUserDoActionTemplate()) đã cho phép {TargetUserName} trò chuyện"
        case .KickUser:
           self.actionTemplate = "\(super.getUserDoActionTemplate()) đã kick {TargetUserName} ra khỏi phòng."
        case .PublicMessage:
            self.actionTemplate = "\(super.getUserDoActionTemplate())"
            if(self.targetUser != nil && self.targetUser?.id > 0){
                self.actionTemplate.appendContentsOf(" ► \(self.getTargetUserTemplate())")
//                if(self.targetUser?.vipId > 0){
//                    targetUserColor = SignalRHelper.getHexColor(self.targetUser!.vipColor, alpha: 1)!
//                }
            }
        default:
            break
        }
    }
    
    override func generateActionText() {
        super.generateActionText()
        if let attText = self.actionAttributeText{
            self.arrMatches = SignalRHelper.getMatchesTemplate(attText)
        }else{
            self.arrMatches = []
        }
        
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.targetUser?.vipColor != nil && self.targetUser?.vipColor.isEmpty == false){
            targetUserColor = SignalRHelper.getHexColor(targetUser!.vipColor,alpha: 1)!
        }
        
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{TargetVipImage}"){
                    strChange = SignalRHelper.getVipImageAttributeString(targetUser!.vipPhotoMobile)
                }else if (subString == "{TargetUserName}"){
                    let att = [NSForegroundColorAttributeName : self.targetUserColor,
                               NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
                    strChange = NSAttributedString(string: targetUser!.name, attributes: att)
                }
                if(strChange == nil) {continue}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
    
    func getTargetUserTemplate() -> String{
        if(targetUser?.vipId > 0){
            return "{TargetVipImage} {TargetUserName}"
        }
        return "{TargetUserName}"
    }
}
