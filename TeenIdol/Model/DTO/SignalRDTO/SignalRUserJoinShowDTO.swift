//
//  SignalRUserJoinShowDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 4/27/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class SignalRUserJoinShowDTO: SignalRUserDoActionDTO {
    var animationItemId: Int  = -1
    var animationItemImage : String  = ""
    
    override init() {
        super.init()
    }
    
    convenience init(model: UserModel, animationModel: AnimationItemModel?) {
        self.init()
        self.userInfo = SignalRUserDTO()
        self.userInfo?.id = (model.userInfo?.Id)!
        self.userInfo?.name = (model.userInfo?.Name)!
        self.userInfo?.vipId = (model.vipInfo?.Id)!
        self.userInfo?.vipColor = (model.vipInfo?.hexColor)!
        self.userInfo?.vipPhotoMobile = (model.vipInfo?.photoLinkMobile)!
        
        if let temp = animationModel{
            self.animationItemId = (temp.animationItem?.Id)!
            self.animationItemImage = (temp.animationItem?.photoLink)!
            if(self.animationItemId > 0){
                self.actionTemplate = "\(super.getUserDoActionTemplate()) đã vào phòng bằng {AnimationItem}"
            }

        }else{
             self.actionTemplate = "\(super.getUserDoActionTemplate()) đã vào phòng."
        }
        self.generateActionText()
    }
    
    
    override func generateActionText() {
        super.generateActionText()
        
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{AnimationItem}"){
                    var _placeHoler : String? = "ic_car_placeholder"
                    strChange = SignalRHelper.getImageAttributeString("animation-\(self.animationItemId)", urlStr: animationItemImage, imageSize: CGSize(width: 20,height: 20),font: UIFont.boldSystemFontOfSize(20), placeHolder: &_placeHoler)
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }

    }
}
