//
//  SignalRUserSendMessagePopupDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 5/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class SignalRUserSendMessagePopupDTO: SignalRUserDoActionDTO {
    var message = ""
    var date : Int64 = 0
    var messageAttributeText : NSAttributedString? = nil
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.userInfo = SignalRUserDTO(jsondata: jsondata["user"])
        self.message = jsondata["message"].stringValue
        self.actionTemplate = super.getUserDoActionTemplate()
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        
        let textColor = self.userInfo?.vipId > 0  ? self.userNameColor! : UIColor.blackColor();
        self.messageAttributeText = FunctionHelper.parseStringToEmojiAttributedString(self.message, color: textColor, font: FontDefautLarge, size: CGSize(width: 12, height: 12))
    }

}
