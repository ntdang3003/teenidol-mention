//
//  SignalRUserGiveGiftDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class SignalRUserGiveHeartDTO : SignalRUserDoActionDTO{
    var heartQuantity = 0
    override init() {
        super.init()
    }
    
    convenience init(jsonData : JSON) {
        self.init()
        self.userInfo = SignalRUserDTO(jsondata: jsonData["user"])
        self.heartQuantity = jsonData["quantityFreeCoin"].intValue
        
        self.actionTemplate = "\(super.getUserDoActionTemplate()) đã tặng {HeartQuantity} {HeartImage}"
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{HeartQuantity}"){
                    let att = [NSForegroundColorAttributeName : UIColor(red: 253/255, green: 171/255, blue: 54/255) ,NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
                    strChange = NSAttributedString(string: "\(self.heartQuantity)", attributes: att)
                }
                else if (subString == "{HeartImage}"){
                    var _placeHoler : String? = "ic_gift_heart"
                    strChange = SignalRHelper.getImageAttributeString("heart_key", urlStr: "https://teenidol.vn/Content/Image/heart.png", imageSize: CGSize(width: 15,height: 15),font: UIFont.boldSystemFontOfSize(15), placeHolder: &_placeHoler)
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
}

class SignalRUserBuySeatDTO : SignalRUserDoActionDTO{
    var seatInfo : SignalRSeatDTO?
    
    override init() {
        super.init()
    }
    
    convenience init(jsonData : JSON) {
        self.init()
        self.userInfo = SignalRUserDTO(jsondata: jsonData["user"])
        self.seatInfo = SignalRSeatDTO(jsondata: jsonData["seat"])
        
        self.actionTemplate = "\(super.getUserDoActionTemplate()) đã mua {SeatInfo}"
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{SeatInfo}"){
                    let str = "Ghế số \((self.seatInfo?.index)!) - \((self.seatInfo?.price)!) ruby"
                    let att = [NSForegroundColorAttributeName : UIColor.blueColor(),
                               NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
                    strChange = NSAttributedString(string: str, attributes: att)
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
}

class SignalRUserGiveGiftDTO: SignalRUserDoActionDTO {
    var targetUser : SignalRStarDTO? = nil
    var giftInfo : SignalRGiftDTO? = nil
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.userInfo = SignalRUserDTO(jsondata: jsondata["user"])
        self.targetUser = SignalRStarDTO(jsondata: jsondata["star"])
        self.giftInfo = SignalRGiftDTO(jsondata: jsondata["gift"])
        self.actionTemplate = "\(super.getUserDoActionTemplate()) đã tặng quà {GiftImage}"
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{GiftImage}"){
                    var _placeHoler : String? = "ic_gift_placeholder"
                    strChange = SignalRHelper.getImageAttributeString(self.generateCacheKey(), urlStr: (self.giftInfo?.photo)!, imageSize: CGSize(width: 17,height: 17),font: UIFont.boldSystemFontOfSize(17), placeHolder: &_placeHoler)
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
    
    func generateCacheKey() -> String{
        var rs = String((self.giftInfo?.id)!)
        rs.appendContentsOf("-")
        rs.appendContentsOf(String((self.giftInfo?.quantity)!))
        return rs
    }
}

//{IdolName} vừa hoàn thành nhiệm vụ, nhận được {GiftQuantity}{GiftName}
class SignalRStarGetRewardDTO: SignalRUserDoActionDTO {
    var starInfo : SignalRStarDTO? = nil
    var giftInfo : SignalRGiftDTO? = nil
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        //self.userInfo = SignalRUserDTO(jsondata: jsondata["user"])
        self.starInfo = SignalRStarDTO(jsondata: jsondata["star"])
        self.giftInfo = SignalRGiftDTO(jsondata: jsondata["gift"])
        self.actionTemplate = "{IdolName} vừa hoàn thành nhiệm vụ, nhận được {GiftImage}"
        self.generateActionText()
    }
    
    override func generateActionText() {
       // super.generateActionText()
        
        //self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        actionAttributeText = NSMutableAttributedString(string: actionTemplate, attributes: [NSFontAttributeName: FontDefautLarge])
        arrMatches = SignalRHelper.getMatchesTemplate(actionAttributeText!)
        userNameColor = ColorAppDefault
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{GiftImage}"){
                    var _placeHoler : String? = "ic_gift_placeholder"
                    strChange = SignalRHelper.getImageAttributeString(self.generateCacheKey(), urlStr: (self.giftInfo?.photo)!, imageSize: CGSize(width: 17,height: 17),font: UIFont.boldSystemFontOfSize(17), placeHolder: &_placeHoler)
                }
//                else if (subString == "{GiftQuantity}"){
//                    let str = "\((self.giftInfo?.quantity)!)"
//                    let att = [NSForegroundColorAttributeName : UIColor.blueColor(),
//                               NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
//                    strChange = NSAttributedString(string: str, attributes: att)
//                }
                else if (subString == "{IdolName}"){
                    let att = [NSForegroundColorAttributeName : self.userNameColor!,
                               NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
                    strChange = NSAttributedString(string: starInfo!.name, attributes: att)
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
    
    func generateCacheKey() -> String{
        var rs = String((self.giftInfo?.id)!)
        rs.appendContentsOf("-")
        rs.appendContentsOf(String((self.giftInfo?.quantity)!))
        return rs
    }
}
//{UserTemplate} vừa giúp Idol hoàn thành nhiệm vụ, được tặng {ListRewardTemplate}

class SignalRUserGetIdolQuestReward: SignalRUserDoActionDTO {
    class SignalRRewardDTO : NSObject{
        var rewardDes = ""
        var photo = ""
        
        override init() {
            super.init()
        }
        
        convenience init(jsonData: JSON) {
            self.init()
            self.rewardDes = jsonData["description"].stringValue
            self.photo = jsonData["photo"].stringValue
            self.photo = self.photo.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        }
    }
    var listReward = Array<SignalRRewardDTO>()
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.userInfo = SignalRUserDTO(jsondata: jsondata["user"])
        let arrReward = jsondata["listGift"].arrayValue
        if(arrReward.count > 0){
            for i in 0 ..< arrReward.count{
                self.listReward.insert(SignalRRewardDTO(jsonData: arrReward[i]), atIndex: i)
            }
        }
        self.actionTemplate = "\(super.getUserDoActionTemplate()) vừa giúp Idol hoàn thành nhiệm vụ, được tặng {ListRewardTemplate}"
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(subString == "{ListRewardTemplate}"){
                    strChange = self.generateListRewardAttributeString()
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
    
    func generateListRewardAttributeString() -> NSAttributedString{
        let rs = NSMutableAttributedString(string: "")
        if(self.listReward.count > 0){
            var _placeHoler : String? = "ic_gift_placeholder"
            let _desAtt = [NSForegroundColorAttributeName : UIColor.blueColor(),
                                      NSFontAttributeName : UIFont.boldSystemFontOfSize(13)]
            for i in 0 ..< self.listReward.count-1 {
                let temp = self.listReward[i]
                let rewardAttStr = NSMutableAttributedString(attributedString: NSAttributedString(string: temp.rewardDes , attributes: _desAtt))
                rewardAttStr.appendAttributedString(SignalRHelper.getImageAttributeString(temp.photo, urlStr: temp.photo, imageSize: CGSize(width: 17,height: 17),font: UIFont.boldSystemFontOfSize(17), placeHolder: &_placeHoler))
                rs.appendAttributedString(rewardAttStr)
                rs.appendAttributedString(NSAttributedString(string: ", "))
            }
            let last = self.listReward[self.listReward.count-1]
            let rewardAttStr = NSMutableAttributedString(attributedString: NSAttributedString(string: last.rewardDes , attributes: _desAtt))
            rewardAttStr.appendAttributedString(SignalRHelper.getImageAttributeString(last.photo, urlStr: last.photo, imageSize: CGSize(width: 17,height: 17),font: UIFont.boldSystemFontOfSize(17), placeHolder: &_placeHoler))
            rs.appendAttributedString(rewardAttStr)
        }
        return rs
    }
}
