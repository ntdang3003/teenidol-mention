//
//  SignalRUserSendMessageDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 4/27/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
let messageTemplate = "{Message}"
class SignalRUserSendMessageDTO: SignalRUserDoActionToTargetDTO {
    //var targetUser : SignalRUserDTO? = nil
    //var targetUserColor: UIColor? = ColorPublicChatUserNameDefault
    var message = ""
    var date : Int64 = 0
    
    override init(jsondata: JSON, code: SignalRAlertCode) {
        //super.init(jsondata: jsondata, code: .PublicMessage)
        super.init(jsondata: jsondata, code: .PublicMessage)
        if(jsondata == nil)
        {
            return;
        }
        self.message = jsondata["message"].stringValue
        self.actionTemplate.appendContentsOf("{:} \(messageTemplate)")
        self.generateActionText()
    }
    
    override func generateActionText() {
        super.generateActionText()
        let textColor = self.userInfo?.vipColor.isEmpty == true ? UIColor.blackColor() : self.userNameColor;
        self.arrMatches = SignalRHelper.getMatchesTemplate(self.actionAttributeText!)
        var newRange = NSMakeRange(0, 0)
        var strTemp : NSString = ""
        if(self.arrMatches != nil && self.arrMatches?.count > 0){
            var strChange : NSAttributedString?  = nil
            for match in self.arrMatches! {
                newRange = NSMakeRange(match.range.location, match.range.length)
                strTemp  = (actionAttributeText?.string)!
                let subString = strTemp.substringWithRange(newRange)
                
                if(messageTemplate == subString){
                    strChange = FunctionHelper.parseStringToEmojiAttributedString(self.message, color: textColor!, font: FontDefautLarge, size: CGSizeMake(14.0, 14.0))
                }
                else if (subString == "{:}"){
                    strChange = NSAttributedString(string: ":", attributes:  [NSForegroundColorAttributeName: userNameColor!])
                }
                if(strChange == nil) {return}
                actionAttributeText?.replaceCharactersInRange(newRange,withAttributedString: strChange!)
            }
        }
    }
    
    override func releaseNoUse() {
        super.releaseNoUse()
        self.message = ""
        self.targetUser = nil
    }
}
