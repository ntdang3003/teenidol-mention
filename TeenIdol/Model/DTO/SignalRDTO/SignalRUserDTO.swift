//
//  SignalRUserDTO.swift
//  TeenIdol
//
//  Created by Dang Nguyen on 4/26/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class SignalRUserDTO: NSObject {
    var id: Int64 = -1
    var name: String = ""
    var vipId: Int = -1
    var vipPhoto: String = ""
    var vipColor: String = ""
    var avatar: String = ""
    var badgeName: String = ""
    var badgePhoto: String = ""
    var animationItemId: Int = -1
    var animationItemPhoto: String = ""
    var vipPhotoMobile: String = ""
    var coin: Int = 0
    var isCanChat: Bool = true
    
    //Guide
    var guildId : Int = 0
    var guildName = ""
    var guildPhoto = ""
    
    override init() {
        super.init()
    }
    
    convenience init (jsondata: JSON){
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.id = jsondata["id"].int64Value
        self.name = jsondata["name"].stringValue
        self.vipId = jsondata["vipId"].intValue
        self.vipPhoto = jsondata["vipPhoto"].stringValue
        self.vipColor = jsondata["vipColor"].stringValue
        self.avatar = jsondata["avatar"].stringValue
        self.badgeName = jsondata["badgeName"].stringValue
        self.badgePhoto = jsondata["badgePhoto"].stringValue
        self.animationItemId = jsondata["animationItemId"].intValue
        self.animationItemPhoto = jsondata["animationItemPhoto"].stringValue
        self.vipPhotoMobile = jsondata["vipPhotoMobile"].stringValue
        self.coin = jsondata["coin"].intValue
        self.guildId = jsondata["guildId"].intValue
        self.guildName = jsondata["guildName"].stringValue
        self.guildPhoto = jsondata["guildPhoto"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
    }
    
    convenience init(model: UserModel) {
        self.init()
        self.id = (model.userInfo?.Id)!
        self.avatar  = (model.userInfo?.AvatarPhoto)!
        self.name = (model.userInfo?.Name)!
        self.vipId = (model.vipInfo?.Id)!
        self.vipColor = (model.vipInfo?.hexColor)!
        self.vipPhoto = (model.vipInfo?.photoLink)!
        //self.vipPhotoMobile = (model.vipInfo?.photoLinkMobile)!
        var chatImageLink = ""
        if(model.userInfo?.GroupUserId == .Star){
           chatImageLink = "https://file.teenidol.vn/File/File/icon_idol_mobile.png"
            self.vipColor = self.colorToHex(ColorAppDefault) as String
        } else if (model.userInfo?.GroupUserId == .CC){
            chatImageLink = "https://file.teenidol.vn/File/File/icon_GSV.png"
        } else if (model.ownerGuild?.Id > 0){
            chatImageLink = (model.ownerGuild?.Avatar)!
        }
        self.vipPhotoMobile = chatImageLink
    }
    
    func colorToHex(color: UIColor) -> NSString
    {
        let components = CGColorGetComponents(color.CGColor)
        let r : Float = Float(components[0])
        let g : Float = Float(components[1])
        let b : Float = Float(components[2])
        
        return NSString(format: "#%02lX%02lX%02lX", lroundf(r * 255),
                        lroundf(g * 255),
                        lroundf(b * 255))
    }
    
    func toJSON() -> [String:AnyObject]{
        var data = [String:AnyObject]()
        data["id"] = Int(self.id)
        data["name"] = self.name
        data["avatar"] = self.avatar
        data["vipId"] = self.vipId
        data["vipPhoto"] = self.vipPhoto
        data["vipPhotoMobile"] = self.vipPhotoMobile
        data["vipColor"] = self.vipColor
        data["guildPhoto"] = self.guildPhoto
        data["guildId"] = self.guildId
        data["guildName"] = self.guildName
        return data
    }
}


class SignalRStarDTO: NSObject {
    var id: Int64 = -1
    var name: String = ""
    var avatar: String = ""
    var coinGet: Int = -1
    var freeCoinGet: Int = -1
    var endTime: Int64 = -1
    var currentLevelId: Int = -1
    var currentLevelName: String = ""
    var currentLevelPhoto: String = ""
    var nextLevelId: Int = -1
    var nextLevelName: String = ""
    var nextLevelPhoto: String = ""
    var nextLevelNeed: Int = -1
    var nextLevelProcess: Int = -1
    var totalStar: Int = -1;
    
    override init() {
        super.init()
    }
    
    convenience init (jsondata: JSON){
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.id = jsondata["id"].int64Value
        self.name = jsondata["name"].stringValue
        self.avatar = jsondata["avatar"].stringValue
        self.coinGet = jsondata["coinGet"].intValue
        self.freeCoinGet = jsondata["freeCoinGet"].intValue
        self.endTime = jsondata["endTime"].int64Value
        self.currentLevelId = jsondata["currentLevelId"].intValue
        self.currentLevelName = jsondata["currentLevelName"].stringValue
        self.currentLevelPhoto = jsondata["currentLevelPhoto"].stringValue
        self.totalStar = jsondata["totalStar"].intValue
    }
}
