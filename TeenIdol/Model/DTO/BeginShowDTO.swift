//
//  BeginShowDTO.swift
//  TeenIdol
//
//  Created by Chip on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SwiftyJSON
class BeginShowDTO: NSObject {
    var ShowId : Int64 = 0;
    var ScheduleId : Int64 = 0;
    var ShowName : String = "";
    var StreamServer: String = "";
    var StreamKey: String = "";
    
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        ShowId = jsondata["ShowId"].int64Value
        ScheduleId = jsondata["ScheduleId"].int64Value
        ShowName = jsondata["ShowName"].stringValue
        StreamServer = jsondata["StreamServer"].stringValue
        StreamKey = jsondata["StreamKey"].stringValue
    }
}
