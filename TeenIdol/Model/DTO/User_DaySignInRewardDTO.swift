//
//  User_DaySignInRewardDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/23/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class User_DaySignInRewardDTO: NSObject {
    var userId: Int64 = 0;
    var daySignInId: Int = 0;
    var lastTimeSignIn: Int64 = 0;
    var signInContinuesCount: Int = 0;
    var daySignIn : DaySignInRewardDTO? = nil
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.userId = jsondata["Id"].int64Value
        self.daySignInId = jsondata["DaySignInId"].intValue
        self.lastTimeSignIn = jsondata["LastTimeSignIn"].int64Value
        self.signInContinuesCount = jsondata["SignInContinuesCount"].intValue
        
        self.daySignIn = DaySignInRewardDTO(jsondata: jsondata["DaySignInReward"])
    }
    
}
