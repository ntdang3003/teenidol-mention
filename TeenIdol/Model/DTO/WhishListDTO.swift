//
//  WhishListDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class WhishListDTO: NSObject {

    var Id:Int = 0;
    var fromUserId:Int64 = 0;
    var toUserId:Int64 = 0;
    var scheduleId = 0;
    var repertoireId = 0;
    
    var timeRequest:Int64?;
    var timeReply:Int64?;
    var createDate:Int64?;
    
    var eventId = 0;
    var eventCoin = 0;
    
    var content = "";
    
    var status = 0;
    var currentPrice = 0;
    
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["ID"].intValue;
        fromUserId = jsondata["FromUserId"].int64Value
        toUserId = jsondata["ToUserId"].int64Value
        scheduleId = jsondata["ScheduleId"].intValue;
        repertoireId = jsondata["RepertoiredId"].intValue
        
        timeRequest = jsondata["TimeRequest"].int64Value
        timeReply = jsondata["TimeReply"].int64Value
        createDate = jsondata["CreateDate"].int64Value
        
        eventId = jsondata["EventId"].intValue
        eventCoin = jsondata["EventCoin"].intValue
        
        content = jsondata["Content"].stringValue;
        status = jsondata["Status"].intValue
        currentPrice = jsondata["CurrentPrice"].intValue
        
    }

    
}
