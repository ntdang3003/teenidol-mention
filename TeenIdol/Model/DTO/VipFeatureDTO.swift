//
//  VipFeatureDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class VipFeatureDTO: NSObject {

    var Id:Int64 = 0;
    var name = ""
    var Description = ""
    var status: Int = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["Id"].int64Value
        name = jsondata["Name"].stringValue
        Description = jsondata["Description"].stringValue
        status = jsondata["Status"].intValue
    }
    
}
