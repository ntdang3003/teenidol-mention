//
//  EndShowDTO.swift
//  TeenIdol
//
//  Created by Chip on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
import SwiftyJSON
class EndShowDTO: NSObject {
    var Duration : Int = 0;
    var Viewer : Int64 = 0;
    var Name : String = "";
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Duration = jsondata["Duration"].intValue
        Viewer = jsondata["Viewer"].int64Value
        Name = jsondata["Name"].stringValue
    }

}
