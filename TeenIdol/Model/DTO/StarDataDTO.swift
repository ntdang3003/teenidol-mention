//
//  StarDataDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class StarDataDTO: NSObject {

    var userId:Int = 0;
    var coinGetFromGift = 0;
    var streamKey = "";
    var totalViewer = 0;
    var totalFollow = 0;
    var levelId = 0 ;
    var createDate:Int64=0;
    var phoneNumber = "";
    var address = "";
    var facebook="";
    var twitter="";
    var instagram="";
    var photo="";
    var linkVideo="";
    var talentDescription="";
    var timeShowDescription="";
    var status = 0;
    var totalFreeCoinGet = 0;
    var starSay = "";
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        userId = jsondata["UserId"].intValue;
        coinGetFromGift = jsondata["CoinGetFromGift"].intValue;
        streamKey = jsondata["StreamKey"].stringValue;
        totalViewer = jsondata["TotalViewer"].intValue;
        totalFollow = jsondata["TotalFollow"].intValue;
        levelId = jsondata["LevelId"].intValue;
        createDate = jsondata["CreateDate"].int64Value;
        phoneNumber = jsondata["PhoneNumber"].stringValue;
        address = jsondata["Address"].stringValue;
        facebook = jsondata["Facebook"].stringValue;
        twitter = jsondata["Twitter"].stringValue;
        instagram = jsondata["Instagram"].stringValue;
        photo = jsondata["Photo"].stringValue;
        linkVideo = jsondata["LinkVideo"].stringValue;
        talentDescription = jsondata["TalentDescription"].stringValue;
        timeShowDescription = jsondata["TimeShowDescription"].stringValue;
        status = jsondata["Status"].intValue;
        totalFreeCoinGet = jsondata["TotalFreeCoinGet"].intValue;
        starSay = jsondata["StarSay"].stringValue;
    }
    
}
