//
//  PaymentTypeDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class PaymentTypeDTO: NSObject {

    var Id: Int = 0
    var name = "";
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["Id"].intValue
        name = jsondata["Name"].stringValue
    }
    
}
