//
//  IdolQuestStateDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class IdolQuestStateDTO: NSObject {

    var openQuestState: Int = 0;
    var countQuestInDay: Int = 0;
    var maxQuestInDay: Int = 0;
    var level: Int = 0;
    var isCanGetNewQuest: Bool?;
    var isComplete: Bool?
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        
        openQuestState = jsondata["openQuestState"].intValue
        countQuestInDay = jsondata["countQuestInDay"].intValue
        maxQuestInDay = jsondata["maxQuestInDay"].intValue
        level = jsondata["level"].intValue
        isCanGetNewQuest = jsondata["isCanGetNewQuest"].boolValue
        isComplete = jsondata["isComplete"].boolValue
    }
}
