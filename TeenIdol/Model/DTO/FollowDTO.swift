//
//  FollowDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class FollowDTO: NSObject {

    var Id:Int = 0;
    var FollowToUserID:Int64 = 0;
    var CreateDate:Int64?;
    var Status = 0;
    var LastVisitDate:Int64?
    var UserId = 0;
    var TotalCoinTrade = 0;
    var TotalEventCoinTrade = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        
        Id = jsondata["ID"].intValue;
        FollowToUserID = jsondata["FollowToUserId"].int64Value;
        CreateDate = jsondata["CreateDate"].int64Value;
        Status = jsondata["Status"].intValue;
        LastVisitDate = jsondata["LastVisitDate"].int64Value;
        UserId = jsondata["UserId"].intValue;
        
    }
}
