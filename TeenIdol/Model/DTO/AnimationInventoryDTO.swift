//
//  AnimationInventoryDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class AnimationInventoryDTO: NSObject {

    var Id:Int = 0;
    var animationItemId = 0;
    var userId:Int64 = 0;
    var createDate: Int64 = 0;
    var expireDate: Int64 = 0;
    var itemPurchaseId = 0;
    var countUsed = 0;
    var numberOfDayPurchase = 0;
    var pricePurchase = 0;
    var isDefault = false;
    var eventCoin = 0;
    var dateExpireParsed : NSDate?
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["Id"].intValue
        animationItemId = jsondata["AnimationItemId"].intValue
        userId = jsondata["UserId"].int64Value
        createDate = jsondata["CreateDate"].int64Value
        expireDate = jsondata["ExpireDate"].int64Value
        itemPurchaseId = jsondata["ItemPurchaseId"].intValue
        countUsed = jsondata["CountUsed"].intValue
        numberOfDayPurchase = jsondata["numberOfDayPurchase"].intValue
        pricePurchase = jsondata["PricePurchase"].intValue
        isDefault = jsondata["IsDefault"].boolValue
        eventCoin = jsondata["EventCoin"].intValue
        dateExpireParsed = NSDate(timeIntervalSince1970: NSTimeInterval(self.expireDate / 1000))
    }
    
}
