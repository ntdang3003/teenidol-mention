//
//  VipInventoryDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class VipInventoryDTO: NSObject {

    var Id:Int = 0;
    var vipItemId = 0;
    var userId = 0;
    var createDate:Int64 = 0;
    var expireDate:Int64 = 0;
    var countUse = 0;
    var lastDateUse: Int64 = 0;
    var itemPurchaseId = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        
        Id = jsondata["ID"].intValue;
        vipItemId = jsondata["VipItemId"].intValue;
        userId = jsondata["UserId"].intValue;
        createDate = jsondata["CreateDate"].int64Value;
        expireDate = jsondata["ExpireDate"].int64Value;
        countUse = jsondata["CountUse"].intValue;
        lastDateUse = jsondata["LastDateUse"].int64Value
        itemPurchaseId = jsondata["ItenPurchaseId"].intValue;
    }
    
}
