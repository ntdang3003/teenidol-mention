//
//  GiftAnimationDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftAnimationDTO: NSObject {

    var Id:Int = 0;
    var GiftId:Int = 0;
    var Quantity = 0;
    var PhotoLink = "";
    var AnimationPhotoLink = "";
    var Type = 0;
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata:JSON) {
        self.init()
        Id = jsondata["Id"].intValue;
        GiftId = jsondata["GiftDTO"].intValue;
        Quantity = jsondata["Quantity"].intValue;
        PhotoLink = jsondata["PhotoLink"].stringValue;
        AnimationPhotoLink = jsondata["AnimationPhotoLink"].stringValue;
        Type = jsondata["Type"].intValue;
        
        PhotoLink = PhotoLink.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        
        if(AnimationPhotoLink != "")
        {
            AnimationPhotoLink = AnimationPhotoLink.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        }
    }
    
}
