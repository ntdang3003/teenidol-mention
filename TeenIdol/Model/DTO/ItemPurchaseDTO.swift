//
//  ItemPurchaseDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class ItemPurchaseDTO: NSObject {
    
    var Id: Int = 0;
    var ObjectId: Int = 0;
    var ObjectTypeId : Int = 0;
    var Price = 0;
    var NumberOfDayExprire = 0;
    var CreateDate: Int64 = 0;
    var Status = 0;
    var EventCoin = 0;
    var Currency = "";
    
    var PriceToStr : String
    {

        get{
            let price = Price > 0 ? "\((Price))" : "\((EventCoin))"
            
            let str = " \(price) \((Currency))"
            return str
        }
    }

    override init() {
        super.init()
    }
    
    convenience init(jsondata : JSON) {
        self.init()
        
        Id = jsondata["Id"].intValue;
        ObjectId = jsondata["OjectId"].intValue
        ObjectTypeId = jsondata["ObjectTypeId"].intValue
        
        Price = jsondata["Price"].intValue
        NumberOfDayExprire = jsondata["NumberOfDayExprire"].intValue
        CreateDate = jsondata["CreateDate"].int64Value
        Status = jsondata["Status"].intValue
        EventCoin = jsondata["EventCoin"].intValue
        Currency = jsondata["Currency"].stringValue
        
    }
    
}
