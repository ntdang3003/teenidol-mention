//
//  DaySignInRewardDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/23/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class DaySignInRewardDTO: NSObject {

    var Id: Int = 0;
    var name: String = "";
    var Description: String = "";
    var minCount: Int = 0;
    var priority: Int = 0;
    var value: Int = 0;
    var rewardType = 0;
    var rewardPhoto: String = "";
    var status: Int = 0;
    var rewardCurrency: String = "";
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.Id = jsondata["Id"].intValue
        self.name = jsondata["Name"].stringValue;
        self.Description = jsondata["MinCount"].stringValue;
        self.priority = jsondata["Priority"].intValue;
        self.value = jsondata["Value"].intValue;
        self.rewardType = jsondata["RewardType"].intValue;
        self.rewardPhoto = jsondata["RewardPhoto"].stringValue;
        self.status = jsondata["Status"].intValue
        self.rewardCurrency = jsondata["RewardCurrecy"].stringValue;
        
    }
    
}
