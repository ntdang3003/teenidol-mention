//
//  GroupLevelDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GroupLevelDTO: NSObject {

    var Id:Int=0;
    var groupUserId = 0;
    var point = 0;
    var status = 0;
    var name = "";
    var photo = "";
    
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata:JSON)
    {
        self.init()
        
        Id = jsondata["ID"].intValue;
        groupUserId = jsondata["GroupUserId"].intValue;
        point = jsondata["Point"].intValue;
        status = jsondata["Status"].intValue;
        name = jsondata["Name"].stringValue;
        photo = jsondata["Photo"].stringValue;
    }
    
}
