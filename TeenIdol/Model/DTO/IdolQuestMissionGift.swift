//
//  IdolQuestMissionGift.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class IdolQuestMissionGift: NSObject {

    var giftPhoto: String = ""
    var count: Int = 0
    var max: Int = 0
    var Description: String = ""
    var DescriptionMobile: String = ""
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        
        giftPhoto = jsondata["Photo"].stringValue
        giftPhoto = giftPhoto.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        count = jsondata["Received"].intValue
        max = jsondata["Quantity"].intValue
        //DescriptionMobile = jsondata["DescriptionMobile"].stringValue
        Description = jsondata["DescriptionMobile"].stringValue
    }
    
}
