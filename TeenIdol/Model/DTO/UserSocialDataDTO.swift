//
//  UserSocialDataDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class UserSocialDataDTO: NSObject {

    var userId:Int64=0;
    var facebookId: Int64 = 0;
    var facebookToken = "";
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata:JSON) {
        self.init()
        userId = jsondata["UserId"].int64Value;
        facebookId = jsondata["FacebookId"].int64Value;
        facebookToken = jsondata["FacebookToken"].stringValue;
        
    }
    
}
