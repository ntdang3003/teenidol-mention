//
//  AnimationItemDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/29/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class AnimationItemDTO: NSObject {
    var Id : Int = 0
    var name = ""
    var des = ""
    var createDate : Int64 = 0
    var itemCategoryId  = 0
    var priority = 0
    var photoLink = ""
    var status = 0
    var gifLinkMobile = ""
    var timeShowMobile : Double = 0
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        Id = jsondata["Id"].intValue
        name = jsondata["Name"].stringValue
        createDate = jsondata["CreateDate"].int64Value
        itemCategoryId = jsondata["ItemCategoryId"].intValue
        priority = jsondata["Priority"].intValue
        photoLink = jsondata["PhotoLink"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        gifLinkMobile = jsondata["GifLinkMobile"].stringValue.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        
        //timeShowMobile = 3.0
        timeShowMobile    = jsondata["TimeShowMobile"].doubleValue
    }
}
