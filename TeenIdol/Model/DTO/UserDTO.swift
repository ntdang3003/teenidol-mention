//
//  UserDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit
 
enum  GroupUserEnum : Int{
   case User = 1
    case Star = 2
   case Mod = 3
   case Admin = 4
   case Guest = 5
   case CC = 6
   case SuperIT = 7
    case ABC = 8
    case drf = 9
}
class UserDTO: NSObject {
    var Id: Int64 = 0
    var GroupUserId: GroupUserEnum = GroupUserEnum.User
    var Email = ""
    var Name = ""
    var Password = ""
    var Salt = ""
    var Status: Int = 0
    var CreateDate: NSDate? = nil
    var Gender: Int = 0
    var LevelId: Int = 0
    var LastIpConnected = ""
    var AvatarPhoto = ""
    var ResetPasswordKey = ""
    var ResetPasswordKeyCreateDate: NSDate? = nil
    var Coin: Int = 0
    var TotalCoinUsed: Int = 0
    var TotalFreeCoin: Int = 0
    var TotalFreeCoinUsed: Int = 0
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.Id = jsondata["Id"].int64Value
        let groupUserId = jsondata["GroupUserId"].intValue;
        if(groupUserId > 0){
            self.GroupUserId = GroupUserEnum(rawValue: groupUserId)!
        }
        self.Email = jsondata["Email"].stringValue
        self.Name = jsondata["Name"].stringValue
        self.Password = jsondata["Password"].stringValue
        self.Salt = jsondata["Salt"].stringValue
        self.Status = jsondata["Status"].intValue
        self.CreateDate = NSDate(timeIntervalSince1970: jsondata["CreateDate"].doubleValue)
        
        self.Gender = jsondata["Gender"].intValue
        self.LevelId = jsondata["LevelId"].intValue
        self.LastIpConnected = jsondata["LastIpConnected"].stringValue
        self.AvatarPhoto = jsondata["AvatarPhoto"].stringValue
        
        self.ResetPasswordKey = jsondata["ResetPasswordKey"].stringValue
        self.ResetPasswordKeyCreateDate = NSDate(timeIntervalSince1970: jsondata["ResetPasswordKeyCreateDate"].doubleValue)
        
        self.Coin = jsondata["Coin"].intValue
        self.TotalCoinUsed = jsondata["TotalCoinUsed"].intValue
        self.TotalFreeCoin = jsondata["TotalFreeCoin"].intValue
        self.TotalFreeCoinUsed = jsondata["TotalFreeCoinUsed"].intValue
        
    }
    
    func toDictionary() -> Dictionary<String, AnyObject>
    {
        let dic = Dictionary<String, AnyObject>();
//        dic["ObjectID"] = ObjectID;
//        dic["Content"] = Content;
//        dic["ObjectType_ID"] = ObjectType_ID;
//        dic["User_ID"] = User_ID;
//        if(ID > 0)
//        {
//            dic["ID"] = ID;
//        }
//        
        return dic;
    }
}
