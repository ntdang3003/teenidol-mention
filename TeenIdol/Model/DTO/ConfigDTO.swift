//
//  ConfigDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/28/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class StreamInfoDTO : NSObject {
    var videoUpload : Double = 0.0
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        self.videoUpload = jsondata["Video"]["Upload"].doubleValue
    }
}
