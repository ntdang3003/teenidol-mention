//
//  UserModel.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/29/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class UserModel: NSObject {
    var userInfo: UserDTO? = nil
    var starDataInfo: StarDataDTO? = nil
    var socialDataInfo: UserSocialDataDTO? = nil
    var groupLevel: GroupLevelDTO? = nil
    var groupUser: GroupUserDTO? = nil
    var starLevel: GroupLevelDTO? = nil
    var showCategory: ShowCategoryDTO? = nil
    var showInfo: ShowDTO? = nil
    var followInfo: FollowDTO? = nil
    var vipInventory: VipInventoryDTO? = nil
    var scheduleInfo: ScheduleDTO? = nil
    var vipInfo : VipItemDTO? = nil
    var totalSumValue = 0;
    var actions = [];
    var priority : Int? = nil
    var isFollow = false
    
    var ownerGuild: GuildDTO?
    var guildLevel: GuildLevelDTO?
    var guildLeader: UserDTO?
    var guildPosition: GuildPostionDTO?
    
    override init() {
        super.init();
    }
    
    convenience init(dic: JSON){
        self.init()
        self.userInfo = UserDTO(jsondata: dic["User"])
        self.vipInfo = VipItemDTO(jsondata: dic["VipItem"])
        self.starDataInfo = StarDataDTO(jsondata: dic["UserStarData"])
        self.socialDataInfo = UserSocialDataDTO(jsondata: dic["UserSocialData"])
        self.groupUser = GroupUserDTO(jsondata:dic["GroupUser"])
        self.groupLevel = GroupLevelDTO(jsondata: dic["GroupLevel"])
        self.starLevel = GroupLevelDTO(jsondata: dic["StarLevel"])
        self.showCategory = ShowCategoryDTO(jsondata: dic["ShowCategory"])
        self.showInfo = ShowDTO(jsondata: dic["Show"])
        self.followInfo = FollowDTO(jsondata: dic["FollowByInfo"])
        self.scheduleInfo = ScheduleDTO(jsondata: dic["Schedule"])
        self.vipInventory = VipInventoryDTO(jsondata: dic["VipInventory"])
        self.totalSumValue = dic["TotalSumValue"].intValue
        if(self.followInfo != nil){
            self.isFollow = self.followInfo?.Status != 1 ? false : true
        }
        
        self.ownerGuild = GuildDTO(jsondata: dic["Guild"])
        self.guildLevel = GuildLevelDTO(jsondata: dic["GuildLevel"])
        self.guildLeader = UserDTO(jsondata: dic["GuildLeader"])
        self.guildPosition = GuildPostionDTO(jsondata: dic["GuildPosition"])
    }
    
    convenience init(data: SignalRUserDTO) {
        self.init()
        self.userInfo = UserDTO()
        self.userInfo?.Id = data.id
        self.userInfo?.Name = data.name
        self.userInfo?.AvatarPhoto = data.avatar
        self.vipInfo = VipItemDTO()
        self.vipInfo?.Id = data.vipId
        self.vipInfo?.photoLink = data.vipPhoto
        self.vipInfo?.hexColor = data.vipColor
    }
    
    func setPriority(){
        self.priority = 0
    }
    
    var isStreamer: Bool
    {
        get
        {
            if(starDataInfo?.userId > 0 && userInfo?.GroupUserId == .Star )
            {
                return true
            }
            return false
        }
    }
    
    func isFacebookUser() -> Bool{
        if let vip = self.vipInfo{
            return vip.Id == 100
        }
        return false
    }
}
