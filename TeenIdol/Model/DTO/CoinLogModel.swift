//
//  CoinLogModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class CoinLogModel: LogModel {
    
    var coinLog: CoinLogDTO? = nil;
    var userInfo: UserDTO? = nil;
    var userReceive: UserDTO? = nil;
    var userStarData: StarDataDTO? = nil;
    var giveAway: GiveAwayDTO? = nil;
    var userShowItem: UserShowItem? = nil ;
    var vipInventory: VipInventoryDTO? = nil;
    var vipInventoryAnimationInfo: AnimationInventoryDTO? = nil;
    var objectName = "";
    var wishList: WhishListDTO? = nil;
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        
        coinLog =  CoinLogDTO(jsondata: dic["CoinLog"])
        userInfo = UserDTO(jsondata: dic["User"])
        userStarData = StarDataDTO(jsondata: dic["UserStarDataInfo"])
    }
    
}
