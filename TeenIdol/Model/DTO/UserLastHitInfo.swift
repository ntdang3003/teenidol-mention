//
//  UserLastHitInfo.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class UserLastHitInfo: NSObject {

    var Id : Int64 = 0
    var name: String = ""
    var avatarPhoto: String = ""
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        
        Id = jsondata["Id"].int64Value
        name = jsondata["Name"].stringValue
        avatarPhoto = jsondata["AvatarPhoto"].stringValue
    }
    
}
