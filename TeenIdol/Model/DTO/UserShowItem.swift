//
//  UserShowItem.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class UserShowItem: NSObject {

    var Id:Int = 0;
    var scheduleId = 0;
    var userId = 0;
    var showItemId = 0;
    var currentPrice = 0;
    var info = "";
    
    var status = 0;
    var createDate: Int64 = 0;
    var eventCoin = 0;
   var eventId = 0;
    
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["Id"].intValue;
        scheduleId = jsondata["ScheduleId"].intValue
        userId = jsondata["UserId"].intValue
        showItemId = jsondata["ShowItemId"].intValue
        currentPrice = jsondata["CurrentPrice"].intValue
        info = jsondata["Info"].stringValue
        status = jsondata["Status"].intValue
        createDate = jsondata["CreateDate"].int64Value
        eventCoin = jsondata["EventCoin"].intValue
        eventId = jsondata["EventId"].intValue
    }
}
