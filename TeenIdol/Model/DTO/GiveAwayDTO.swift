//
//  GiveAwayDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiveAwayDTO: NSObject {

    var Id:Int = 0;
    var giftId:Int = 0;
    var quantity = 0;
    var createDate:Int64 = 0;
    var userSendId = 0;
    var userRecievedId = 0;
    var scheduleId = 0;
    var currentPrice = 0;
    var eventCoin = 0;
    var eventId = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["ID"].intValue
        giftId = jsondata["GiftId"].intValue
        quantity = jsondata["Quantity"].intValue
        createDate = jsondata["CreateDate"].int64Value
        userSendId = jsondata["UserSendId"].intValue
        
        scheduleId = jsondata["ScheduleId"].intValue
        currentPrice = jsondata["CurrentPrice"].intValue
        
        eventId = jsondata["EventId"].intValue;
        eventCoin = jsondata["EventCoin"].intValue
    }
}
