//
//  GuildLevelDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GuildLevelDTO: NSObject {

    var Id: Int64 = 0
    var LevelIndex = 0
    var Name = ""
    var Photo = ""
    var Point = 0
    var Status = 0
    var TotalIdol = 0
    var TotalUser = 0
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        
        self.Id = jsondata["Id"].int64Value
        self.LevelIndex = jsondata["LevelIndex"].intValue
        self.Name = jsondata["Name"].stringValue
        self.Photo = jsondata["Photo"].stringValue
        self.Status = jsondata["Status"].intValue
        self.TotalIdol = jsondata["TotalIdol"].intValue
        self.TotalUser = jsondata["TotalUser"].intValue
        self.Point = jsondata["Point"].intValue
    }
    
}
