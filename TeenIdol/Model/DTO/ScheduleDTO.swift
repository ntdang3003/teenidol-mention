//
//  ScheduleDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/25/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit
 

class ScheduleDTO: NSObject {
    var Id: Int64 = 0
    var Status: ScheduleStatus?
    var ShowId: Int64 = 0
    var StartTime: NSDate? = nil
    var EndTime: NSDate? = nil
    var ShowRoomId: Int64 = 0
    var TypeShow: Int = 0
    var NumberStar: Int = 0
    var OnlineUser: Int = 0
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }
        //let secondsOfSevenHours = (7 * 60 * 60) * 1000;
        let secondsOfSevenHours = 0
        let startTimeTick = jsondata["StartTime"].int64Value - secondsOfSevenHours;
        let endTimeTick = jsondata["EndTime"].int64Value - secondsOfSevenHours;
        
        self.Id = jsondata["Id"].int64Value
        self.Status = ScheduleStatus(rawValue: jsondata["Status"].intValue)!
        self.ShowId = jsondata["ShowId"].int64Value
        self.StartTime = NSDate(timeIntervalSince1970: Double(startTimeTick / 1000))
        self.EndTime = NSDate(timeIntervalSince1970: Double(endTimeTick / 1000))
        self.ShowRoomId = jsondata["ShowRoomId"].int64Value
        self.TypeShow = jsondata["TypeShow"].intValue
        self.NumberStar = jsondata["NumberStar"].intValue
        self.OnlineUser = jsondata["OnlineUser"].intValue
        
    }
}
