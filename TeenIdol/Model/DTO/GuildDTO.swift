//
//  GuildDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/16/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GuildDTO: NSObject {

    var Avatar = ""
    var BankAccountId :Int64 = 0
    var CreateDate: Int64 = 0
    var Description = ""
    var Id: Int64 = 0
    var Name = ""
    var Owner = 0
    var PhotoLink = ""
    var Ruby = 0
    var Status = 0
    var TotalIdol = 0
    var TotalMember = 0
    var TotalUser = 0
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        self.Id = jsondata["Id"].int64Value
        self.BankAccountId = jsondata["BankAccountId"].int64Value
        self.CreateDate = jsondata["CreateDate"].int64Value
        self.Description = jsondata["Description"].stringValue
        self.Name = jsondata["Name"].stringValue
        self.Avatar = jsondata["Avatar"].stringValue
        self.Owner = jsondata["Owner"].intValue
        self.PhotoLink = jsondata["PhotoLink"].stringValue
        self.Ruby = jsondata["Ruby"].intValue
        self.Status = jsondata["Status"].intValue
        self.TotalIdol = jsondata["TotalIdol"].intValue
        self.TotalMember = jsondata["TotalMember"].intValue
        self.TotalUser = jsondata["TotalUser"].intValue
    }
    
}
