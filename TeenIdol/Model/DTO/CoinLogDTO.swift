//
//  CoinLogDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class CoinLogDTO: NSObject {

    var Id:Int = 0;
    var userId:Int64 = 0;
    var type = 0;
    var coinAmount = 0;
    var Description = "";
    var actionId = 0;
    var toUserId = 0;
    var objectId = 0;
    var objectTypeId = 0;
    var status = 0;
    var freeCoin = 0;
    var eventId = 0;
    var createDate:Int64 = 0;

    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        Id = jsondata["Id"].intValue;
        userId = jsondata["UserId"].int64Value
        type = jsondata["Type"].intValue
        coinAmount = jsondata["CoinAmount"].intValue
        
        Description = jsondata["Description"].stringValue.stringByReplacingOccurrencesOfString("$", withString: "")

        
        actionId = jsondata["ActionId"].intValue
        toUserId = jsondata["ToUserId"].intValue
        objectId = jsondata["ObjectId"].intValue
        objectTypeId = jsondata["ObjectTypeId"].intValue
        
        status = jsondata["Status"].intValue
        freeCoin = jsondata["FreeCoin"].intValue
        
        eventId = jsondata["EventId"].intValue
        createDate = jsondata["CreateDate"].int64Value;
    }
}
