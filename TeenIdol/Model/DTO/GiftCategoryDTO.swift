//
//  GiftCategoryDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftCategoryDTO: NSObject {

    var Id:Int = 0;
    var Name="";
    var Description="";
    var Status=0;
    var Photo="";
    var ViewPriority=0;
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata:JSON)
    {
        self.init();
        Id = jsondata["Id"].intValue;
        Name = jsondata["Name"].stringValue;
        Description = jsondata["Description"].stringValue;
        Status = jsondata["Status"].intValue;
        Photo = jsondata["Photo"].stringValue;
        ViewPriority = jsondata["ViewPriority"].intValue;
    }
}
