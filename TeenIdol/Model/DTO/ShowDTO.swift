//
//  ShowDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/23/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import UIKit
 

class NotificationDTO : NSObject{
    var alert = ""
    var objectId : Int64 = 0
    var objectTypeId : Int = 0
    var streamServer : String? = ""
    var streamKey : String? = ""
    var imageLink = ""
    
}

class ShowDTO: NSObject {
    var Id: Int64 = 0
    var ServerId: Int = 0
    var Creator: Int = 0
    var ShowCategoryId: Int = 0
    var Status: Int = 0
    var MaxViewer: Int = 0
    var OnlineUser: Int = 0
    var StatusShow: Int = 0
    var TypeShow: Int = 0
    var OnlineUserLoggedIn: Int = 0
    var CreateDate: NSDate? = nil
    var UpdateDate: NSDate? = nil
    var EnablePublicChat: Bool = false
    var Name  = ""
    var Photo  = ""
    var StreamKey  = ""
    var Description  = ""
    var LinkVideo  = ""
    var ServerIP = ""
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        if(jsondata == nil)
        {
            return;
        }

        self.Id = jsondata["Id"].int64Value
        self.ServerId = jsondata["ServerId"].intValue
        self.Creator = jsondata["Creator"].intValue
        self.ShowCategoryId = jsondata["ShowCategoryId"].intValue
        self.Status = jsondata["Status"].intValue
        self.MaxViewer = jsondata["MaxViewer"].intValue
        self.OnlineUser = jsondata["OnlineUser"].intValue
        self.StatusShow = jsondata["StatusShow"].intValue
        
        self.OnlineUserLoggedIn = jsondata["OnlineUserLoggedIn"].intValue
        self.CreateDate = NSDate(timeIntervalSince1970: jsondata["CreateDate"].doubleValue)
        self.UpdateDate = NSDate(timeIntervalSince1970: jsondata["UpdateDate"].doubleValue)
        self.EnablePublicChat = jsondata["EnablePublicChat"].boolValue
        
        self.Name = jsondata["Name"].stringValue
        self.Photo = jsondata["Photo"].stringValue
        
        self.StreamKey = jsondata["StreamKey"].stringValue
        self.Description = jsondata["Description"].stringValue
        self.LinkVideo = jsondata["LinkVideo"].stringValue
        self.ServerIP = "rtmp://210.245.18.43/live/"
    }}
