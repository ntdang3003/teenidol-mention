//
//  GiftDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftDTO: NSObject {

    var Id:Int = 0;
    var GiftCategoryId:Int = 0;
    var Name="";
    var Description="";
    var Price = 0;
    var Status = 0;
    var CreateDate: Int64=0;
    var PhotoLink = "";
    var PhotoGiftLink = "";
    var ViewPriority = 0;
    var IconLink = "";
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata:JSON) {
        self.init()
        Id = jsondata["Id"].intValue;
        GiftCategoryId = jsondata["GiftCategoryId"].intValue;
        Name = jsondata["Name"].stringValue;
        Description = jsondata["Description"].stringValue;
        Price = jsondata["Price"].intValue;
        Status = jsondata["Status"].intValue;
        CreateDate = jsondata["CreateDate"].int64Value;
        PhotoLink = jsondata["PhotoLink"].stringValue;
        PhotoGiftLink = jsondata["PhotoGifLink"].stringValue;
        ViewPriority = jsondata["ViewPriority"].intValue;
        IconLink = jsondata["IconLink"].stringValue;
        
        PhotoLink = PhotoLink.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        if(PhotoGiftLink != ""){
            PhotoGiftLink = PhotoGiftLink.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        }
    }
}
