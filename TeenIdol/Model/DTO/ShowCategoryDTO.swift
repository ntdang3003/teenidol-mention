//
//  ShowCategoryDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class ShowCategoryDTO: NSObject {

    var ID:Int = 0;
    var name = "";
    var Description = "";
    var status = 0;
    
    override init() {
        super.init()
    }
    
    
    convenience init(jsondata: JSON)
    {
        self.init();
        
        ID = jsondata["ID"].intValue;
        name = jsondata["Name"].stringValue;
        Description = jsondata["Description"].stringValue;
        status = jsondata["Status"].intValue;
    }
}
