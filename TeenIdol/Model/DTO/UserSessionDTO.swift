//
//  UserSession.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/10/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class UserSessionDTO: NSObject {

    var Id:Int64 = 0;
    var tokenKey:String = "";
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        
        Id = jsondata["Id"].int64Value
        tokenKey = jsondata["Key"].stringValue
    }
}
