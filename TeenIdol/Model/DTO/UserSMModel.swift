//
//  UserSMModel.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class UserSMModel: NSObject {
    var ShowInfo: ShowDTO? = nil
    var UserInfo : UserDTO?
    var UserStarData : StarDataDTO?
    var imgBackgroud : UIImage?
    var schedule : ScheduleDTO?
    var samplePercent : Double = 0
    var sampleStar = false
    override init() {
        super.init();
    }
    convenience init(dic: JSON){
        self.init()
        self.ShowInfo = ShowDTO(jsondata: dic["Show"])
        self.UserInfo = UserDTO(jsondata: dic["User"])
        self.UserStarData = StarDataDTO(jsondata: dic["UserStarData"])
        self.schedule = ScheduleDTO(jsondata: dic["Schedule"])
        self.samplePercent = dic["SamplePercent"].doubleValue
        self.sampleStar = dic["SampleStar"].boolValue
    }
}
