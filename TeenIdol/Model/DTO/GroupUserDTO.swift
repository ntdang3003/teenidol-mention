//
//  GroupUserDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GroupUserDTO: NSObject {

    var ID :Int = 0;
    var Name = "";
    var Description = "";
    var Status = 0;
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata:JSON) {
        self.init()
        
        ID = jsondata["ID"].intValue;
        Name = jsondata["Name"].stringValue;
        Description = jsondata["Description"].stringValue;
        Status = jsondata["Status"].intValue;
    }
    
}
