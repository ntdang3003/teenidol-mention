//
//  VipItemDTO.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 4/29/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
class VipItemDTO: NSObject {
    var Id : Int = 0
    var name  = ""
    var priority  = 0
    var photoLink = ""
    var hexColor = ""
    var storePhoto = ""
    var photoLinkMobile = ""
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON) {
        self.init()
        Id = jsondata["Id"].intValue
        name = jsondata["Name"].stringValue
        priority = jsondata["Priority"].intValue
        photoLink = jsondata["PhotoLink"].stringValue
        hexColor = jsondata["HexColor"].stringValue
        storePhoto = jsondata["StorePhoto"].stringValue
        photoLinkMobile = jsondata["PhotoLinkMobile"].stringValue
    }
}
