//
//  IdolQuestMissionDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 7/13/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class IdolQuestMissionDTO: NSObject {

    var Id: Int = 0
    var name: String = ""
    var level: Int = 0
    var status: Int = 0;
    var numberUserReward: Int = 0;
    
    override init() {
        super.init();
    }
    
    convenience init(jsondata : JSON) {
        self.init();
        
        Id = jsondata["Id"].intValue
        name = jsondata["Name"].stringValue
        level = jsondata["Level"].intValue
        status = jsondata["Status"].intValue
        numberUserReward = jsondata["NumberUserReward"].intValue
    }
    
}
