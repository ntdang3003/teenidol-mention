//
//  GuildPostionDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GuildPostionDTO: NSObject {

    var Id = 0
    var name = ""
    var photo = ""
    var position = 0
    var status = 0
    var animation = ""
    
    override init()
    {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        self.Id = jsondata["Id"].intValue
        self.name = jsondata["Name"].stringValue
        self.photo = jsondata["Photo"].stringValue
        self.position = jsondata["Position"].intValue
        
        self.status = jsondata["Status"].intValue
        self.animation = jsondata["Animation"].stringValue
        
    }
    
}
