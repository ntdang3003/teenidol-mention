//
//  PaymentLogDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/9/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 
enum paymentStatusEnum: Int { case Pending = 0, Completed = 1, Failed = 2 }

class PaymentLogDTO: NSObject {

    var Id:Int = 0;
    var userId:Int = 0;
    var createDate:Int64 = 0 ;
    var amount = 0;
    var coin = 0;
    var paymentLogId = 0;
    var paymenTypeId = 0;
    var transId = "";
    var status = 0;
    
    override init() {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init();
        
        Id = jsondata["ID"].intValue;
        userId = jsondata["UserId"].intValue;
        createDate = jsondata["CreateDate"].int64Value;
        amount = jsondata["Amount"].intValue
        coin = jsondata["Coin"].intValue
        paymentLogId = jsondata["PaymentLogId"].intValue;
        paymenTypeId = jsondata["PaymentTypeId"].intValue;
        transId = jsondata["TransId"].stringValue
        status = jsondata["Status"].intValue;
    }
}
