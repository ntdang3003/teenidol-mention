//
//  UserGuildDTO.swift
//  LiveIdol
//
//  Created by TFL Mac on 8/18/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class UserGuildDTO: NSObject {

    var guildPositionId = 0
    var guildId:Int64 = 0
    var joinDate: Int64 = 0
    var ruby = 0
    var status = 0
    
    override init()
    {
        super.init()
    }
    
    convenience init(jsondata: JSON)
    {
        self.init()
        
        self.guildPositionId = jsondata["GuildPositionId"].intValue
        self.guildId = jsondata["GuildId"].int64Value
        self.ruby = jsondata["Ruby"].intValue
        self.status = jsondata["Status"].intValue
        self.joinDate = jsondata["JoinDate"].int64Value
    }
}
