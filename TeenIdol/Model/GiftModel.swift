//
//  GiftModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 5/6/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class GiftModel: NSObject {

    var giftDTO: GiftDTO? = nil;
    var giftCategoryDTO: GiftCategoryDTO? = nil;
    var giftAnimationDTO: GiftAnimationDTO? = nil;
    var listGiftAnimations = FListResultDTO<GiftAnimationDTO>()
    var starData: StarDataDTO? = nil;
    override init() {
        super.init();
    }
    
    convenience init(dic:JSON) {
        self.init()
        giftDTO = GiftDTO(jsondata: dic["Gift"]);
        giftCategoryDTO = GiftCategoryDTO(jsondata: dic["GiftCategory"])
        giftAnimationDTO = GiftAnimationDTO(jsondata: dic["GiftAnimation"])
        starData = StarDataDTO(jsondata: dic["StarDataDTO"]);
        
        let jsonListAnimation = dic["ListGiftAnimations"].arrayValue
        if(jsonListAnimation.count > 0){
            for i in 0 ... jsonListAnimation.count - 1 {
                let temp = GiftAnimationDTO(jsondata: jsonListAnimation[i])
                self.listGiftAnimations.ListItem.append(temp)
                self.listGiftAnimations.CountAllResult = self.listGiftAnimations.CountAllResult + 1
            }
        }
    }
}
