//
//  VipItemModel.swift
//  LiveIdol
//
//  Created by TFL Mac on 6/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit
 

class VipItemModel: NSObject {

    var VipItem = VipItemDTO();
    var ListFeatures: [VipFeatureDTO] = [];
    var ListPurchases: [ItemPurchaseDTO] = [];
    
    override init() {
        super.init()
    }
    
    convenience init(dic: JSON)
    {
        self.init()
        
        VipItem = VipItemDTO(jsondata: dic["VipItem"])
        
        for i in dic["ListFeatures"].arrayValue
        {
             let data = VipFeatureDTO(jsondata: i)
             ListFeatures.append(data)
        }
       
        for i in dic["ListPurchases"].arrayValue
        {
            let data = ItemPurchaseDTO(jsondata: i)
            ListPurchases.append(data)
        }
        
    }
}
