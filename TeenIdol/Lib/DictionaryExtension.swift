//
//  DictionaryExtension.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 3/24/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

import Foundation

extension Dictionary{
    func intForKey(key: Key) -> Int{
        if (self[key] as? Int) != nil {
            return self[key] as! Int
        }
        return 0
    }
    
    func longForKey(key: Key) -> Int64{
        if (self[key] as? Int64) != nil {
            return self[key] as! Int64
        }
        return 0
    }
    
    func anyObjectForKey(key: Key) -> AnyObject?{
        if (self[key] as? AnyObject) != nil {
            return self[key] as! AnyObject?
        }
        return NSNull()
    }
    
    func boolForKey(key: Key) -> Bool{
        if (self[key] as? Bool) != nil {
            return self[key] as! Bool
        }
        return false
    }
    
    func stringForKey(key:Key) -> String {
        if (self[key] as? String) != nil {
            return self[key] as! String
        }
        return ""
    }
    
//    func dateForKey(key: Key) -> Int64{
//        if (self[key] as? Int) != nil {
//            return self[key] as! Int
//        }
//        return 0
//    }
}
