//
//  UICheckbox.swift
//  LiveIdol
//
//  Created by Dang Nguyen on 5/2/16.
//  Copyright © 2016 TFL. All rights reserved.
//

import UIKit

class UICheckbox: UIButton {

    var isCheck  = false
    private let _imgCheckPath : String = "ic_checkbox_on"
    private let _imgUnCheckPath : String = "ic_checkbox_off"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.addTarget(self, action: #selector(changeState), forControlEvents: .TouchUpInside)
    }
    
    func setupUI(){
        self.setImage(UIImage(named: isCheck ? _imgCheckPath : _imgUnCheckPath), forState: UIControlState.Normal)
    }
    
    func changeState(){
        isCheck = !isCheck
        setupUI()
    }
    
}
