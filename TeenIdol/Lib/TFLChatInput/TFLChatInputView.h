//
//  TFLChatInputView.h
//  TFLChatInput
//
//  Created by Dang Nguyen on 1/12/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UITextView+Placeholder.h"
@protocol TFLChatInputDelegate <NSObject>

@required
-(void)chatInput_didTouchRightButton;
-(void)chatInput_didChangedToSize: (CGSize) newSize;
@optional
-(void)chatInput_didTouchLeftButton;
-(void)chatInput_didRemoveTargetUser;
@end

IB_DESIGNABLE
@interface TFLChatInputView : UIView <UITextViewDelegate>
@property (nonatomic, strong) NSString * placeHolder;
@property (nonatomic, strong) NSString* plainText;
@property (nonatomic) CGSize maxSize;
@property (nonatomic) int maxTextLength;
@property (nonatomic) CGSize keyboardSize;
@property (nonatomic, weak) id<TFLChatInputDelegate> delegate;

@property (nonatomic) bool isShowingEmoji;
@property (nonatomic) bool isEnableBubbleChat;

-(void) resignFirstReponser;
-(void) becomeFirstReponser;
-(CGSize) totalSize;
-(void) setTargetUserName: (NSString*) name;
-(void) cleanUp;
@end


@interface ChatEmojiCollectionViewCell: UICollectionViewCell
{
    UIImageView *imgEmoji;
}
@property (nonatomic, retain) UIImageView *imgEmoji;
@property (nonatomic, retain) NSString *strEmojiCode;
@end