//
//  TFLChatInputView.m
//  TFLChatInput
//
//  Created by Dang Nguyen on 1/12/16.
//  Copyright © 2016 Dang Nguyen. All rights reserved.
//

#import "TFLChatInputView.h"
#import "EmojiTextAttachment.h"
#import "NSAttributedString+EmojiExtension.h"
#define TEXT_MAX_LENGTH 150

@interface TFLChatInputView () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnLeftButton;
@property (weak, nonatomic) IBOutlet UITextView *txtChatInput;
@property (weak, nonatomic) IBOutlet UIButton *btnRightButton;
@property (weak, nonatomic) IBOutlet UIButton *btnBubbleChat;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveTarget;
@end


@implementation TFLChatInputView{
    UITextView * txtSizingTextView;
    CGSize textViewLastBound;
    bool isTextGenerated;
    __weak IBOutlet UICollectionView *colEmoji;
    __weak IBOutlet UIView *viewHeader;
    __weak IBOutlet NSLayoutConstraint *conViewHeaderHeight;
    __weak IBOutlet UILabel * lblTargetUserName;
    __weak IBOutlet UIView * viewBorderChatInput;
    __weak IBOutlet UIView * viewBorderTargetUser;
    __weak IBOutlet NSLayoutConstraint *conButtonTargetUserWidth;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        [self loadControl];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self = [super initWithFrame:frame]){
       [self loadControl];
    }
    return self;
}

-(void)setIsEnableBubbleChat:(bool)value{
    _isEnableBubbleChat = value;
    NSString * img = _isEnableBubbleChat ? @"ic_chatinput_rocket_actived" : @"ic_chatinput_rocket_deactived";
    [self.btnBubbleChat setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];
}

-(void)setTextGenerated: (bool) value{
    isTextGenerated  = value;
    if(isTextGenerated){
        self.btnRightButton.tintColor = [UIColor redColor];
    }else{
        self.btnRightButton.tintColor = [UIColor lightGrayColor];
    }
    //[self updateRightButtonIcon];
}

-(void)setKeyboardSize:(CGSize)keyboardSize{
//    if(_keyboardSize.height != keyboardSize.height){
//            _keyboardSize = keyboardSize;
//        self.frame = CGRectMake(0,0 , self.frame.size.width, conViewHeaderHeight.constant + _keyboardSize.height);
//        //conViewFooterHeight.constant = _keyboardSize.height;
////        CGSize newSize = CGSizeMake(self.txtChatInput.bounds.size.width, self.txtChatInput.bounds.size.height+ conViewFooterHeight.constant);
////        [self.delegate chatInput_didChangedToSize:newSize];
//        [self layoutIfNeeded];
//    }
    _keyboardSize = keyboardSize;
    CGRect temp = self.frame;
    self.frame = CGRectMake(temp.origin.x,temp.origin.y , temp.size.width, conViewHeaderHeight.constant + _keyboardSize.height);
    [self layoutIfNeeded];
}
-(void)setIsShowingEmoji:(bool)isShowingEmoji{
    _isShowingEmoji = isShowingEmoji;
    NSString * img = _isShowingEmoji ? @"ic_chatinput_emoji_actived" : @"ic_chatinput_emoji_deactived" ;
    [self.btnLeftButton setImage:[UIImage imageNamed:img] forState:UIControlStateNormal];

}

//_(void)setIsEnableBubbleChat: (bool) isEnableBubbleChat{
//    
//}

-(void)loadControl{
    UIView * view  = [[[NSBundle bundleForClass:[self class]]loadNibNamed:@"TFLChatInputView" owner:self options:nil] firstObject];
    [self addSubview:view];
    view.frame = self.bounds;
    
    //Kích thước của keyboard
    self.keyboardSize = CGSizeMake(320.0, 216.0);
    
    //Setup cho các TextView
    self.txtChatInput.delegate = self;
    self.txtChatInput.textContainerInset = UIEdgeInsetsMake(5.0,2.0, 5.0, 2.0);
    
    txtSizingTextView = [[UITextView alloc]init];
    txtSizingTextView.textContainerInset = self.txtChatInput.textContainerInset;
    txtSizingTextView.font = self.txtChatInput.font;
    txtSizingTextView.frame = self.txtChatInput.frame;
    //Tham số mặc định
    if(self.maxSize.height == 0) self.maxSize = CGSizeMake(self.txtChatInput.bounds.size.width, 30);
    if(!self.maxTextLength) self.maxTextLength = TEXT_MAX_LENGTH;
    
    self.plainText = @"";
    if(!self.placeHolder || [self.placeHolder isEqualToString:@""])
        self.placeHolder = @"Nội dung chat";
    self.txtChatInput.placeholder = self.placeHolder;
    [self setTextGenerated:false];
    
    colEmoji.dataSource = self;
    colEmoji.delegate = self;
    colEmoji.showsVerticalScrollIndicator = false;
    [colEmoji registerClass:[ChatEmojiCollectionViewCell class] forCellWithReuseIdentifier:@"ChatEmojiCollectionViewCell"];

    //Border button
    self.btnBubbleChat.layer.borderWidth = 1;
    self.btnBubbleChat.layer.cornerRadius = 3;
    self.btnBubbleChat.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:0.8].CGColor;
    //self.btnLeftButton.layer.borderWidth = 1;
    //self.btnLeftButton.layer.cornerRadius = 3;
    //self.btnLeftButton.layer.borderColor = self.btnBubbleChat.layer.borderColor ;
    //self.btnRightButton.layer.borderWidth = 1;
   
    //self.txtChatInput.layer.cornerRadius = 5;
   // self.txtChatInput.layer.borderWidth = 1;
    //self.txtChatInput.layer.borderColor = self.btnBubbleChat.layer.borderColor;
    
    self.btnRightButton.layer.cornerRadius = 3;
    
    viewBorderChatInput.layer.cornerRadius = 3;
    viewBorderChatInput.layer.borderWidth = 1;
    viewBorderChatInput.layer.borderColor = self.btnBubbleChat.layer.borderColor;
    
    viewBorderTargetUser.layer.cornerRadius = 3;
    viewBorderTargetUser.layer.masksToBounds = true;
}

-(CGSize) totalSize{
   //return CGSizeMake(viewHeader.frame.size.width, conViewHeaderHeight.constant + colEmoji.frame.size.height);
    return CGSizeMake(viewHeader.frame.size.width, self.frame.size.height);
}

#pragma mark - [Touches]
- (IBAction)touchLeftButton:(id)sender {
    //Thay đổi trạng thái focus của nút bên trái
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatInput_didTouchLeftButton)]){
        [self.delegate chatInput_didTouchLeftButton];
    }
}

- (IBAction)touchRightButton:(id)sender {
    _plainText = [self.txtChatInput.textStorage getPlainString];
    if(self.delegate && [self.delegate respondsToSelector:@selector(chatInput_didTouchRightButton)]){
        [self.delegate chatInput_didTouchRightButton];
    }
    if(![_plainText isEqualToString:@""]){
        self.txtChatInput.text = @"";
        _plainText = @"";
        [self setTextGenerated:false];
        [self resizeTextView];
    }
}

- (IBAction)touchBubbleChat:(id)sender {
    self.isEnableBubbleChat = !self.isEnableBubbleChat;
    self.txtChatInput.placeholder = self.isEnableBubbleChat ? @"3 ruby / tin nhắn" : @"Nội dung" ;
}

- (IBAction)touchRemoveTarget:(id)sender {
    //self.btnRemoveTarget.hidden = true;
    conButtonTargetUserWidth.constant = 0;
    [self layoutIfNeeded];
    lblTargetUserName.text = @"Mọi người";
    if(self.delegate != nil && [self.delegate respondsToSelector:@selector(chatInput_didRemoveTargetUser)]){
        [self.delegate chatInput_didRemoveTargetUser];
    }
}

#pragma mark [TextView Delegate]

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text length] == 0)
    {
        if(range.length == [[textView text] length])
        {
            [self setTextGenerated:false];
        }
    }
    else
    {
        if(range.location == 0)
        {
            [self setTextGenerated:true];
        } else {
            NSString * strNewText = [textView.text stringByReplacingCharactersInRange:range withString:text];
            if(strNewText.length > self.maxTextLength) return NO;
        }
    }
    return YES;
    
}
- (void)textViewDidChange:(UITextView *)textView{
    [self resizeTextView];
    
}

-(void) resizeTextView{
//    txtSizingTextView.attributedText = self.txtChatInput.attributedText;
//    CGSize targetSize = [txtSizingTextView sizeThatFits:self.maxSize];
//    CGFloat resultHeight = targetSize.height > self.maxSize.height ? self.maxSize.height : targetSize.height;
//    //NSLog(@"%f",ceil(resultHeight));
//    if(self.txtChatInput.contentSize.height <= self.maxSize.height){
//        textViewLastBound = self.txtChatInput.bounds.size;
//        self.txtChatInput.bounds = CGRectMake(0, 0, self.txtChatInput.bounds.size.width, resultHeight);
//        if(textViewLastBound.height != self.txtChatInput.bounds.size.height)
//        if(self.delegate && [self.delegate respondsToSelector:@selector(chatInput_didChangedToSize:)]){
//            conViewHeaderHeight.constant = self.txtChatInput.bounds.size.height + 15;
//            [self layoutIfNeeded];
//            CGSize newSize = CGSizeMake(self.txtChatInput.bounds.size.width,self.frame.size.height);
//            [self.delegate chatInput_didChangedToSize:newSize];
//        }
//    }
}

-(void)updateRightButtonIcon{
    if(!isTextGenerated){
        [self.btnRightButton setImage:[UIImage imageNamed:@"ic_chatinput_sendheart_deactived"] forState:UIControlStateNormal];
        [self.btnRightButton setImage:[UIImage imageNamed:@"ic_chatinput_sendheart_actived"] forState:UIControlStateHighlighted];
    }else{
        [self.btnRightButton setImage:[UIImage imageNamed:@"ic_chatinput_send_actived"] forState:UIControlStateNormal];
        [self.btnRightButton setImage:nil forState:UIControlStateHighlighted];
    }
}

#pragma mark - [Method]
-(void) resignFirstReponser{
    [self.txtChatInput resignFirstResponder];
}
-(void) becomeFirstReponser{
    [self.txtChatInput becomeFirstResponder];
}

-(void) setTargetUserName: (NSString*) name{
    lblTargetUserName.text = name;
    conButtonTargetUserWidth.constant = 15;
    [self layoutIfNeeded];
}

-(void) cleanUp{
    lblTargetUserName.text = @"Mọi người";
    conButtonTargetUserWidth.constant = 0;
    [self layoutIfNeeded];
}

#pragma mark - [Collection Emoji DataSource - Delegate]

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 65;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChatEmojiCollectionViewCell *cell = (ChatEmojiCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"ChatEmojiCollectionViewCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    NSString* code = [NSString stringWithFormat:@"o-%ld",(long)indexPath.row];
    NSString * buttonPath= [NSString stringWithFormat:@"ic_emoji_%@",code];
    cell.imgEmoji.image = [UIImage imageNamed:buttonPath];
    cell.strEmojiCode = code;
    [cell setTag:indexPath.row];
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(40, 40);
}
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(5,5,0,5);  // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}
- (void)resetTextStyle {
    NSRange wholeRange = NSMakeRange(0, self.txtChatInput.textStorage.length);
    
    [self.txtChatInput.textStorage removeAttribute:NSFontAttributeName range:wholeRange];
    [self.txtChatInput.textStorage addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:wholeRange];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ChatEmojiCollectionViewCell *cell =  (ChatEmojiCollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    EmojiTextAttachment *emojiTextAttachment = [EmojiTextAttachment new];
    emojiTextAttachment.fontDescender = self.txtChatInput.font.descender;
    emojiTextAttachment.emojiTag = [NSString stringWithFormat:@"{%@}",cell.strEmojiCode];
    emojiTextAttachment.image = cell.imgEmoji.image;
    emojiTextAttachment.emojiSize = 17.0;
    //Insert emoji image
    [self.txtChatInput.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment] atIndex:self.txtChatInput.selectedRange.location];
    self.txtChatInput.selectedRange = NSMakeRange(self.txtChatInput.selectedRange.location + 1, self.txtChatInput.selectedRange.length);
    [self resizeTextView];
    //self.plainText = [self.plainText stringByAppendingString:[NSString stringWithFormat:@"{%@}",cell.strEmojiCode]];
    [self resetTextStyle];
    [self setTextGenerated:true];
    return;
}
@end


@implementation ChatEmojiCollectionViewCell
@synthesize imgEmoji;
- (id)initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    {
        imgEmoji = [[UIImageView alloc] init];
        imgEmoji.frame = CGRectMake(8 , 8, 24, 24);
        [self addSubview:imgEmoji];
    }
    return self;
}
@end