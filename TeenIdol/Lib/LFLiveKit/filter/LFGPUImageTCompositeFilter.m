//
//  LFGPUImageTCompositeFilter.m
//  LFLiveKit
//
//  Created by Kuroo on 9/28/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LFGPUImageTCompositeFilter.h"

NSString *const kGPUImageTCompositeVertexShaderString = SHADER_STRING
(
 attribute vec4 position;
 attribute vec4 inputTextureCoordinate;
 attribute vec4 inputTextureCoordinate2;
 
 uniform float imageWidthFactor;
 uniform float imageHeightFactor;
 const float sharpness = 1.15;
 
 varying vec2 textureCoordinate;
 varying vec2 textureCoordinate2;
 varying vec2 leftTextureCoordinate;
 varying vec2 rightTextureCoordinate;
 varying vec2 topTextureCoordinate;
 varying vec2 bottomTextureCoordinate;
 
 varying float centerMultiplier;
 varying float edgeMultiplier;
 
 void main()
 {
     gl_Position = position;
     
     vec2 widthStep = vec2(imageWidthFactor, 0.0);
     vec2 heightStep = vec2(0.0, imageHeightFactor);
     
     textureCoordinate = inputTextureCoordinate.xy;
     textureCoordinate2 = inputTextureCoordinate2.xy;
     leftTextureCoordinate = inputTextureCoordinate2.xy - widthStep;
     rightTextureCoordinate = inputTextureCoordinate2.xy + widthStep;
     topTextureCoordinate = inputTextureCoordinate2.xy + heightStep;
     bottomTextureCoordinate = inputTextureCoordinate2.xy - heightStep;
     
     centerMultiplier = 1.0 + 4.0 * sharpness;
     edgeMultiplier = sharpness;
 }
 );

//#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
//NSString *const kGPUImageAlphaBlendFragmentShaderString = SHADER_STRING
//(
// varying highp vec2 textureCoordinate;
// varying highp vec2 textureCoordinate2;
// 
// uniform sampler2D inputImageTexture;
// uniform sampler2D inputImageTexture2;
// 
// uniform lowp float mixturePercent;
// 
// void main()
// {
//     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
//     lowp vec4 textureColor2 = texture2D(inputImageTexture2, textureCoordinate2);
//     
//     gl_FragColor = vec4(mix(textureColor.rgb, textureColor2.rgb, textureColor2.a * mixturePercent), textureColor.a);
// }
// );
//#else
NSString *const kGPUImageTCompositeFragmentShaderString = SHADER_STRING
(
 precision mediump float;
 varying vec2      textureCoordinate;
 varying vec2      textureCoordinate2;
 varying vec2 leftTextureCoordinate;
 varying vec2 rightTextureCoordinate;
 varying vec2 topTextureCoordinate;
 varying vec2 bottomTextureCoordinate;
 uniform sampler2D inputImageTexture;
 uniform sampler2D inputImageTexture2;
 
 varying float centerMultiplier;
 varying float edgeMultiplier;
 
// const float sharpness = 1.15;
 const float contrast = 1.15;
 
 vec4 sharpen(vec4 color) {
//     mediump vec4 leftTextureColor = texture2D(inputImageTexture2, textureCoordinate2 + vec2(-1.0/360.0, 0.0));
//     mediump vec4 rightTextureColor = texture2D(inputImageTexture2, textureCoordinate2 + vec2(1.0/360.0, 0.0));
//     mediump vec4 topTextureColor = texture2D(inputImageTexture2, textureCoordinate2 + vec2(0.0, 1.0/640.0));
//     mediump vec4 bottomTextureColor = texture2D(inputImageTexture2, textureCoordinate2 + vec2(0.0, -1.0/640.0));
//     
//     return color * (1.0 + sharpness * 4.0) - sharpness * (leftTextureColor + rightTextureColor + topTextureColor + bottomTextureColor);
     
     mediump vec4 leftTextureColor = texture2D(inputImageTexture2, leftTextureCoordinate);
     mediump vec4 rightTextureColor = texture2D(inputImageTexture2, rightTextureCoordinate);
     mediump vec4 topTextureColor = texture2D(inputImageTexture2, topTextureCoordinate);
     mediump vec4 bottomTextureColor = texture2D(inputImageTexture2, bottomTextureCoordinate);
     
     return color * centerMultiplier - (leftTextureColor * edgeMultiplier + rightTextureColor * edgeMultiplier + topTextureColor * edgeMultiplier + bottomTextureColor * edgeMultiplier);
 }
 
 void main(void) {
     vec4 color = texture2D(inputImageTexture, textureCoordinate);
     vec4 color2 = texture2D(inputImageTexture2, textureCoordinate2);
     color2 = sharpen(color2);
     color = vec4(mix(color2.rgb, color.rgb, 0.7), 1.0);
     color = vec4(((color.rgb - vec3(0.5))*contrast + vec3(0.5)), color.w);
     color = vec4(pow(color.rgb, vec3(0.81, 0.86, 0.77)), color.w);
     //                   color = vec4(pow(color.rgb, vec3(0.85, 0.85, 0.85)), color.w);
     //                   color.r *= 1.02;
     gl_FragColor = color;
 }
 );
//#endif

@implementation LFGPUImageTCompositeFilter

- (id)init;
{
    if (!(self = [super initWithVertexShaderFromString:kGPUImageTCompositeVertexShaderString fragmentShaderFromString:kGPUImageTCompositeFragmentShaderString]))
    {
        return nil;
    }
    
    imageWidthFactorUniform = [filterProgram uniformIndex:@"imageWidthFactor"];
    imageHeightFactorUniform = [filterProgram uniformIndex:@"imageHeightFactor"];
    
    return self;
}

- (void)setupFilterForSize:(CGSize)filterFrameSize;
{
    runSynchronouslyOnVideoProcessingQueue(^{
        [GPUImageContext setActiveShaderProgram:filterProgram];
        
        if (GPUImageRotationSwapsWidthAndHeight(inputRotation))
        {
            glUniform1f(imageWidthFactorUniform, 1.0 / filterFrameSize.height);
            glUniform1f(imageHeightFactorUniform, 1.0 / filterFrameSize.width);
        }
        else
        {
            glUniform1f(imageWidthFactorUniform, 1.0 / filterFrameSize.width);
            glUniform1f(imageHeightFactorUniform, 1.0 / filterFrameSize.height);
        }
    });
}

@end
