//
//  LFGPUImageTCompositeFilter.h
//  LFLiveKit
//
//  Created by Kuroo on 9/28/16.
//  Copyright © 2016 admin. All rights reserved.
//

#if __has_include(<GPUImage/GPUImage.h>)
#import <GPUImage/GPUImage.h>
#elif __has_include("GPUImage/GPUImage.h")
#import "GPUImage/GPUImage.h"
#else
#import "GPUImage.h"
#endif

#import <Foundation/Foundation.h>

@interface LFGPUImageTCompositeFilter : GPUImageTwoInputFilter {
    GLint imageWidthFactorUniform, imageHeightFactorUniform;
}

@end
